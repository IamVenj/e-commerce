<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanySettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_name');
            $table->longtext('slug')->nullable();
            $table->string('location');
            $table->string('phone_number');
            $table->string('email');
            $table->text('facebook')->nullable();
            $table->text('twitter')->nullable();
            $table->text('google_plus')->nullable();
            $table->text('instagram')->nullable();
            $table->integer('products_amount_for_discount');
            $table->double('discount')->nullable();
            $table->integer('delivery_per_km')->nullable();
            $table->text('address')->nullable();
            $table->double('lat')->nullable();
            $table->double('lng')->nullable();
            $table->double('cut_business')->nullable();
            $table->double('cut_individual')->nullable();
            $table->longtext('shipping_guide')->nullable();
            $table->longtext('shipping_return')->nullable();
            $table->longtext('about')->nullable();
            $table->longtext('terms_and_condition')->nullable();
            $table->text('credit_hint')->nullable();
            $table->double('online_payment_limit')->nullable();
            $table->double('symmart_standard_price')->default(50);
            $table->double('symmart_premium_price')->default(150);
            $table->double('symmart_national_price')->default(200);
            $table->double('symmart_express_price')->default(100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_settings');
    }
}
