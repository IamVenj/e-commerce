<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->bigInteger('vendor_id')->unsigned()->nullable();
            $table->foreign('vendor_id')->references('id')->on('users')->onDelete('cascade');
            // special package needs PACKAGE TO be created before the orders table so integity constraint will be caused. fix this issue on the second update!

            $table->bigInteger('package_id')->unsigned()->nullable();
            // $table->foreign('package_id')->references('id')->on('special_packages')->onDelete('cascade');
            $table->string('order_code');
            $table->integer('quantity');
            $table->double('paid_amount')->nullable();
            $table->bigInteger('color_id')->unsigned()->nullable();
            $table->foreign('color_id')->references('id')->on('colors')->onDelete('cascade');
            // main orders needs main_order_id TO be created before the main_orders table so integity constraint will be caused. fix this issue on the second update!

            $table->bigInteger('main_order_id')->unsigned();
            // $table->foreign('main_order_id')->references('id')->on('main_orders')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
