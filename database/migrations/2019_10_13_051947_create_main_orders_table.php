<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('unique_bank_code')->nullable();
            $table->string('order_code');
            $table->double('total_price');
            $table->double('discount_price')->default(0);
            $table->double('delivery_price');
            $table->string('delivery_method');
            $table->string('payment_method');
            $table->boolean('vendor_delivery_confirmation')->default(0);
            $table->boolean('customer_delivery_confirmation')->default(0);
            $table->boolean('payment_status')->default(0);
            $table->string('order_pin')->default(0);
            $table->bigInteger('deliveryp_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_orders');
    }
}
