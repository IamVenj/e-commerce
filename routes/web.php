<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/clear-cache', function() {
   Artisan::call("config:cache"); 
   return "successfully cached";
});

Route::get('/linkstorage', function() {
    Artisan::call("storage:link"); 
    return "successfully linked!";
 });

Route::get('/Terms-and-conditions/vendor', 'TermsController@indexVendor');
Route::get('/delivery/personnel', 'DeliveryPersonnel@index');
Route::post('/delivery/personnel/create', 'DeliveryPersonnel@store');
Route::patch('/delivery/personnel/{id}/edit', 'DeliveryPersonnel@update');
Route::delete('/delivery/personnel/{id}/destroy', 'DeliveryPersonnel@destroy');
Route::patch('/delivery/personnel/{id}/assign', 'DeliveryPersonnel@assignPersonnel');
Route::get('/orders/{id}/print', 'UserOrderController@printView');
Route::get('/orders/{username}/vendor', 'UserOrderController@getOrderForVendor');

/*
|-----------------------------------------------------------------
| Updated Data
|-----------------------------------------------------------------
*/


Route::get('/contact', 'ContactController@index');
Route::post('/contact', 'ContactController@store');

Route::get('/add-vendors', 'UserListController@create');
Route::get('/admin/vendors', 'UserListController@mainIndex');
Route::delete('/destroy/{vendor_id}/vendor', 'UserListController@destroyVendor');
Route::patch('/deactivate/{vendor_id}/vendor', 'UserListController@deactivateVendor');
Route::patch('/activate/{vendor_id}/vendor', 'UserListController@activateVendor');
Route::post('/add-vendors', 'UserListController@store');

Route::get('/under-construction', 'UnderConstructionController@index');

Route::patch('/account/deactivate', 'AccountManagementController@deactivateAccount');
Route::patch('/account/activate', 'AccountManagementController@activateAccount');

Route::patch('/debt/item/{debt_id}/status/{debt_status}', 'DebtController@updateEach');

Route::get('/package', 'PackageController@index');
Route::get('/{vendor_name}/package/create/{vendor_id}', 'PackageController@create');
Route::get('/create/package', 'PackageController@indexVendor');
Route::post('/create/package', 'PackageController@store');
Route::get('/package/{package_name}/images/{package_id}', 'PackageController@showPackageImages');
Route::delete('/package/{image_id}/image/destroy', 'PackageController@destroyPackageImage');
Route::patch('/package/{image_id}/image/update/{package_id}', 'PackageController@updatePackageImage');
Route::post('/package/{package_name}/image/add/{package_id}', 'PackageController@addPackageImage');
Route::get('/package/{package_name}/products/{package_id}', 'PackageController@indexProduct');
Route::delete('/package/{packageId}/product/{packageProductId}/remove', 'PackageController@removeProduct');
Route::post('/package/product/{packageId}/add', 'PackageController@addProduct');
Route::patch('/package/edit/{package_id}', 'PackageController@update');
Route::delete('/package/destroy/{package_id}', 'PackageController@destroy');

Route::get('/package/{package_name}/detail/{package_id}', 'PackageController@showCustomer');

route::get('/create/faq', 'FaqController@index');
route::post('/create/faq', 'FaqController@store');
route::patch('/faq/{id}/edit', 'FaqController@update');
route::delete('/faq/{id}/destroy', 'FaqController@destroy');
route::get('/faq', 'FaqController@indexCustomer');

route::get('/update/shipping/guide', 'ShippingGuideController@index');
route::post('/update/shipping/guide', 'ShippingGuideController@update');
route::get('/shipping/guide', 'ShippingGuideController@indexCustomer');

route::get('/update/shipping/return', 'ShippingReturnController@index');
route::post('/update/shipping/return', 'ShippingReturnController@update');
route::get('/shipping/return', 'ShippingReturnController@indexCustomer');

route::get('/update/Terms-and-conditions', 'TermsController@index');
route::post('/update/Terms-and-conditions', 'TermsController@update');
route::get('/Terms-and-conditions', 'TermsController@indexCustomer');

route::get('/update/about', 'AboutController@index');
route::post('/update/about', 'AboutController@update');
route::get('/about', 'AboutController@indexCustomer');

route::get('/vendor/items', 'VendorItemListController@index');
route::get('/{vendor_name}/items/{userId}', 'VendorItemListController@showItems');
route::delete('/items/{productId}/destroy', 'VendorItemListController@deleteItem');

route::patch('/update/status', 'AccountManagementController@changeStatus');

/*
|--------------------------------------------------------------------
| The Above is a middleware view ||
|--------------------------------------------------------------------
*/

Route::get('/', 'HomeController@index');

/*
|-----------------------------------------------------------------
| localization
|-----------------------------------------------------------------
*/

Route::post('/lang', 'LanguageController@update');

/*
|------------------------------------------------------------------
*/

/*
|--------------------------------------------------------------------
| CART
|--------------------------------------------------------------------
*/

Route::get('/cart', 'CartController@index');
Route::post('/cart', 'CartController@store');
Route::post('/cart/package', 'CartController@storePackage');
Route::get('/empty-cart', 'CartController@empty');
Route::get('/get-cart', 'CartController@get_cart_list');
Route::get('/get-cart-main', 'CartController@cart_item');
Route::post('/update-cart', 'CartController@update');
Route::post('/remove-cartitem', 'CartController@removeFromCart');

/*
|------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------
| WISHLIST
|--------------------------------------------------------------------
*/

Route::get('/wishes', 'WishController@index');
Route::delete('/remove-wish/{id}', 'WishController@destroy');
Route::post('/add-wish', 'WishController@store');

/*
|------------------------------------------------------------------
*/


Route::get('/checkout', 'CheckoutController@index');


/*
/---------------------------------------
/ Testing YenePaySDK Integration
/---------------------------------------
*/

Route::post('/payment/{id}/pay', 'PaymentController@pay');
Route::get('/yenepay-payment-status', 'PaymentController@YenePay_status');
Route::post('/ipn', 'PaymentController@ipn');

/*
|------------------------------------------------------------------
*/


/*
|-------------------------------------
| Products
|-------------------------------------
|
| This is the route to products
|
*/

Route::get('/{category_name}/products/{category_id}', "ProductController@showBasedOnCategory");
Route::post('/category-products', "ProductController@productsBasedOnCategory");
Route::get('/{product_name}/detail/{product_id}', 'ProductController@show');

/*
|------------------------------------------------------------------
*/


/*
|-------------------------------------
| Search
|-------------------------------------
|
| This is the route to autocomplete AJAX and search post request
|
*/


Route::post('/auto-complete', 'SearchController@autoComplete');
Route::get('/search', 'SearchController@search');
Route::post('/search-result', 'SearchController@searchProductsBasedOnCategory');

/*
|------------------------------------------------------------------
*/



/*
|--------------------------------------
| Authentication
|--------------------------------------
|
| This is the route for user login & register
| Login & Register Authentication
|
*/

Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::post('/login', 'Auth\LoginController@store');
Route::post('/logout', 'Auth\LoginController@destroy');

/*
|------------------------------------------------
| -------------------------------------------------------------------------
|------------------------------------------------
*/

Route::get('/register', 'Auth\RegisterController@index');
Route::post('/register', 'Auth\RegisterController@store');

/*
|----------------------------------------------
| After Login / Post Login
|----------------------------------------------
|
| This is the route after a user logs in
| There are three types of users
| Admin | Seller | customer
|
*/

Route::get('/dashboard', 'DashboardController@index');
Route::get('/getYearlyIncome', 'DashboardController@getYearlyIncome');
Route::get('/getMonthlyIncome', 'DashboardController@getMonthlyIncome');
Route::get('/getDailyIncome', 'DashboardController@getDailyIncome');
Route::get('/getCashIncome', 'DashboardController@getCashIncome');
Route::get('/getAllIncome', 'DashboardController@getAdminAllIncome');

/*
|-------------------------------------------------------------------------------------
| Admin Role
|-------------------------------------------------------------------------------------
|
| This role can:
|  - customize carousel
|  - create Category & Sub-category
|  - view List of vendors & customers registered
|  - 
|
*/

/*
|-------------------------------------------------------------------------------------
|  Home Carousel
|-------------------------------------------------------------------------------------
*/

Route::get('/custom-carousel', 'CustomCarouselController@index');
Route::post('/custom-carousel', 'CustomCarouselController@store');
Route::patch('/update-image-carousel/{carousel_id}', 'CustomCarouselController@updateImage');
Route::patch('/edit-carousel/{carousel_id}', 'CustomCarouselController@update');
Route::delete('/delete-carousel/{carousel_id}', 'CustomCarouselController@destroy');


/*
|-------------------------------------------------------------------------------------
|  Category
|-------------------------------------------------------------------------------------
*/


Route::get('/category', 'CategoryController@index');
Route::post('/create-category', 'CategoryController@store');

/*
|-------------------------------------------------------------------------------------
|  List of Users | vendor and customer alike
|-------------------------------------------------------------------------------------
*/


Route::get('/users', 'UserListController@index');
Route::post('/users', 'UserListController@showUsers');


/*
|-------------------------------------------------------------------------------------
|--------------------------------- SELLER ROLE --------------------------------
|-------------------------------------------------------------------------------------
|
| This role can:
|  - View Orders
|  - Create & view Products
|
*/

Route::get('/orders', 'UserOrderController@indexBasedOnUser');
Route::get('/orders/{user_id}/users', 'UserOrderController@indexBasedOnTimestamp');
Route::get('/orders/{user_id}/users/{timestamp}/timestamp', 'UserOrderController@indexOrder');
Route::patch('/orders/{user_id}/users/{timestamp}/timestamp', 'UserOrderController@updateVendorDeliveryConfirmation');
Route::get('/vendor/orders/{id}', 'UserOrderController@show');
Route::get('/vendor/orders/{id}/detail', 'UserOrderController@showDetail');
Route::get('/admin/orders/{id}', 'UserOrderController@showAdmin');
Route::get('/admin/orders/{id}/detail', 'UserOrderController@showAdminDetail');
Route::get('/admin/orders', 'UserOrderController@indexBasedOnUserAdmin')->middleware('admin');


/*
|-------------------------------------------------------------------------------------
|  Orders for user
|-------------------------------------------------------------------------------------
*/

Route::get('/my-orders', 'UserOrderController@showOrderToCustomer');
Route::get('/my/{id}/orders', 'UserOrderController@detailOrderToCustomer');
Route::delete('/cancel-my-order/{order_id}', 'UserOrderController@cancelOrder');
Route::delete('/cancel/order/{order_id}/all', 'UserOrderController@cancelAllOrder');
Route::post('/vendor-delivery-confirmation', 'UserOrderController@vendor_delivery_confirmation');
Route::patch('/confirm-customer-delivery/{order_id}', 'UserOrderController@delivery_confirmation_from_customer');

/*
|------------------------------------------------------------------
*/

/*
|-------------------------------------------------------------------------------------
|  Create product & view products
|-------------------------------------------------------------------------------------
*/

Route::get('/create-product', 'SellerProductController@create');
Route::post('/create-product', 'SellerProductController@store');
Route::get('/view-products', 'SellerProductController@index');
Route::get('/{product_name}/images/{product_id}', 'SellerProductController@show_product_images');
Route::get('/{product_name}/colors/{product_id}', 'SellerProductController@show_product_colors');
Route::patch('/update-product-image/{productImage_id}', 'SellerProductController@update_selected_image');
Route::delete('/delete-product-image/{productImage_id}', 'SellerProductController@destroy_selected_image');
Route::post('/add-product-images', 'SellerProductController@add_image');
Route::delete('/delete-product-color/{productColor_id}', 'SellerProductController@destroy_selected_color');
Route::patch('/update-product-color/{productColor_id}', 'SellerProductController@update_selected_color');
Route::post('/add-selected-colors', 'SellerProductController@add_selected_color');
Route::delete('/destroy-product/{product_id}', 'SellerProductController@destroy');
Route::patch('/update-product/{product_id}', 'SellerProductController@update');

/*
|------------------------------------------------------------------
*/


/*
|-------------------------------------------------------------------------------------
|  First Time Login --> Wizard
|-------------------------------------------------------------------------------------
*/

Route::get('/wizard', 'WizardController@index')->name('wizard-view');
Route::post('/wizard', 'WizardController@store');


/*
|------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------
| AJAX - requests
|-----------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------
| Categories
|-----------------------------------------------------------------
*/

Route::post('/get-categories', 'CategoryController@postview');
Route::post('/show-categories-edit-modal', 'CategoryController@show_categories_edit_modal');
Route::post('/show-categories-edit-image-modal', 'CategoryController@show_categories_edit_image_modal');
Route::post('/edit-category', 'CategoryController@update');
Route::post('/edit-category-image', 'CategoryController@updateImage');
Route::delete('/delete-category', 'CategoryController@destroy');

/*
|------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------
| Reviews
|-----------------------------------------------------------------
*/

Route::post('/review', 'ReviewController@store');
Route::patch('/update-review/{id}', 'ReviewController@update');
Route::delete('/delete-review/{id}', 'ReviewController@destroy');
Route::get('/my-reviews/{product_id}', 'ReviewController@myReview');
Route::get('/vendor-reviews', 'ReviewController@showVendors');
Route::get('{shop_name}/reviews/{id}', 'ReviewController@showVendorReviews');


/*
|------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------
| Settings
|-----------------------------------------------------------------
*/


Route::get('/settings', 'SettingsController@index');
Route::patch('/settings', 'SettingsController@store');


/*
|-----------------------------------------------------------------
| Account
|-----------------------------------------------------------------
*/

Route::get('/my-account', 'AccountManagementController@index');
Route::patch('/my-account', 'AccountManagementController@updateAccount');
Route::patch('/my-account/bank', 'AccountManagementController@updateBank');
Route::get('/account', 'AccountManagementController@customerAccountView');
Route::patch('/account', 'AccountManagementController@updateAccountForCustomer');
Route::patch('/update-profile-pic', 'AccountManagementController@updateProfilePic');
Route::patch('/change-u-password', 'AccountManagementController@changePassword');
Route::patch('/update-delivery-option', 'AccountManagementController@updateDelivery');

/*
|------------------------------------------------------------------
*/



/*
|-----------------------------------------------------------------
| My Address
|-----------------------------------------------------------------
*/

Route::get('/my-address', 'AddressManagementController@index');
Route::patch('/update-address/{locations_id}', 'AddressManagementController@update');
Route::post('/add-address', 'AddressManagementController@store');
Route::delete('/remove-map/{locations_id}', 'AddressManagementController@destroy');

/*
|------------------------------------------------------------------
*/



/*
|-----------------------------------------------------------------
| Forgot-password & Reset Password
|-----------------------------------------------------------------
*/

Route::get('/forgot-password', 'Auth\ForgotPasswordController@index');
Route::post('/forgot-password', 'Auth\ForgotPasswordController@sendResetLinkEmail');


Route::get('/reset-password/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/reset-password', 'Auth\ResetPasswordController@reset');

/*
|------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------
| Debt
|-----------------------------------------------------------------
*/

Route::get('/debts', 'DebtController@index');
Route::get('/debt/{id}', 'DebtController@show');
Route::patch('/debt/{id}', 'DebtController@update');
Route::get('/my-debt', 'DebtController@seller_index');

/*
|------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------
| Newsletter
|-----------------------------------------------------------------
*/

Route::post('/join-newsletter', 'NewsletterController@store');
Route::get('/newsletter', 'NewsletterController@create');
Route::post('/newsletter/store', 'NewsletterController@storeLetter');
Route::patch('/newsletter/{id}/edit', 'NewsletterController@updateLetter');
Route::delete('/newsletter/{id}/destroy', 'NewsletterController@destroy');
Route::post('/newsletter/{id}/send', 'NewsLetterController@sendNewsLetter');

/*
|-----------------------------------------------------------------
| Notifications
|-----------------------------------------------------------------
*/

Route::post('/notification/index', 'NotificationController@index');
Route::post('/notification/read', 'NotificationController@read');

/*
|-----------------------------------------------------------------
| Special Offerz
|-----------------------------------------------------------------
*/

Route::get('/special-offers', 'SpecialOfferController@index');
Route::post('/get-vendor-special-offer', 'SpecialOfferController@getVendorProducts');
Route::post('/special-offers', 'SpecialOfferController@store');
Route::delete('/special-offer/{id}', 'SpecialOfferController@destroy');