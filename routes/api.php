    <?php

    use Illuminate\Http\Request;

    /*
    |--------------------------------------------------------------------------
    | API Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register API routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | is assigned the "api" middleware group. Enjoy building your API!
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Auth API
    |--------------------------------------------------------------------------|
    */

    Route::post('/register', 'APIController@register');
    Route::post('/login', 'APIController@login');

    /*
    |--------------------------------------------------------------------------
    | Products
    |--------------------------------------------------------------------------|
    */

    // Route::post('/products', 'APIController@productsBasedOnCategory');
    Route::post('/products', "APIController@showProducts");
    Route::get('/product/detail/{product_id}', 'APIController@showSelectedProduct');
    Route::get('/package/detail/{packageId}', 'APIController@showSelectedPackage');

    /*
    |--------------------------------------------------------------------------
    | Category
    |--------------------------------------------------------------------------|
    */

    Route::get('/categories', 'APIController@getCategory');
    Route::get('/parent-category', 'APIController@parentCategory');
    Route::get('/child-category/{parentId}', 'APIController@childCategory');

    /*
    |--------------------------------------------------------------------------
    | Home
    |--------------------------------------------------------------------------|
    */

    Route::get('/home', 'APIController@homeRequest');
    Route::post('/search', 'APIController@search');
    Route::post('/forgot-password', 'APIController@forgotPassword');

    /*
    |--------------------------------------------------------------------------
    | All Authorized API routes
    |--------------------------------------------------------------------------|
    */

    Route::middleware('auth:api')->group(function(){

        Route::get('/user/account', 'APIController@userAccount');
        Route::get('/user/order', 'APIController@mainOrder');
        Route::post('/logout', 'APIController@destroyUserLogin');

        Route::get('/auth/check', 'APIController@checkAuth');
        Route::post('/cart/product', 'APIController@addProductToCart');
        Route::post('/pay', 'APIController@pay');

        Route::post('/change-password', 'APIController@changeClientPassword');
        Route::post('/update-profile', 'APIController@updateProfile');
        Route::patch('/update-address', 'APIController@updateClientAddress');
        Route::patch('/deactivate-account', 'APIController@deactivateAccount');

        Route::patch('/profile/update', 'APIController@updateProfilePicture');
        Route::delete('/main-order/delete/{orderId}', 'APIController@destroyMainOrder');
        Route::delete('/order/delete/{orderId}', 'APIController@destroyOrder');

        /*
        |--------------------------------------------------------------------------
        | Orders
        |--------------------------------------------------------------------------|
        */

        Route::get('/my-orders', 'UserOrderController@showOrderToCustomer');
        Route::delete('/cancel-my-order/{order_id}', 'UserOrderController@cancelOrder');
        Route::patch('/confirm-customer-delivery/{order_id}', 'UserOrderController@delivery_confirmation_from_customer');

        /*
        |--------------------------------------------------------------------------
        | Wish
        |--------------------------------------------------------------------------|
        */

        Route::get('/user/wish', 'APIController@myWishes');
        Route::delete('/remove-wish/{id}', 'WishController@destroy');
        Route::delete('/wish/remove/{productId}', 'APIController@removeWish');
        Route::post('/add-wish', 'APIController@addWish');

        /*
        |--------------------------------------------------------------------------
        | Review
        |--------------------------------------------------------------------------|
        */

        Route::post('/review', 'ReviewController@store');
        Route::patch('/update-review/{id}', 'ReviewController@update');
        Route::delete('/delete-review/{id}', 'ReviewController@destroy');

    });
