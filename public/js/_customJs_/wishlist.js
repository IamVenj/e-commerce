function add_wish(i)
{
	// token not made yet!!!!!!!!!!!!!!!!!!!
	var token = $("meta[name=csrf-token]");

	$.ajaxSetup({

		headers: {

			'X-CSRF-TOKEN': token.attr('content')

		}

	});

	$.ajax({

		url: '/add-wish',

		type: 'POST',

		data: {product_id: i},

		beforeSend: function() {

		},

		success: function(response) {

			swal(response.productName, "is added to wishlist !", "success");

			$(".heart-comp").addClass('text-danger');

		},

		error: function(error) {

			if(error.responseJSON['message'] == 'Unauthenticated.')
			{
				location.replace('/login');
			}

		},

	});


}

function add_wish_for_package(i)
{
	// token not made yet!!!!!!!!!!!!!!!!!!!
	var token = $("meta[name=csrf-token]");

	$.ajaxSetup({

		headers: {

			'X-CSRF-TOKEN': token.attr('content')

		}

	});

	$.ajax({

		url: '/add-wish',

		type: 'POST',

		data: {package_id: i},

		beforeSend: function() {

		},

		success: function(response) {

			swal(response.packageName, "is added to wishlist !", "success");

			$(".heart-comp").addClass('text-danger');

		},

		error: function(error) {

			if(error.responseJSON['message'] == 'Unauthenticated.')
			{
				location.replace('/login');
			}

		},

	});


}