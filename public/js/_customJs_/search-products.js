$("#search-products").typeahead({

	source:function(query, result)
	{

		var category = $("#search-category").val();

		$.ajaxSetup({

			headers: {

				'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")

			}

		});

		$.ajax({

			url:'/auto-complete',

			method: 'POST',

			data: {query: query, category:category},

			dataType: "json",

			success: function(data) {

				result($.map(data, function(item){

					return item;

				}));

			}

		});

	}

});