// $(document).ready(function() {

get_cart();

function get_cart() {

	var token = $("input[name=cart-csrf-token]").val();

	$.ajaxSetup({

		headers:{

			'X-CSRF-TOKEN': token

		}

	});

	$.ajax({

		url: '/get-cart',

		type: 'get',

		data: {},

		success: function(response) {

			$(".cart-item").html(response.output);
			$("#total-cart").html(response.total_price);
			$("#sub-total-cart-main").html(response.total_price + ' Birr');
			$("#cart_count").html(response.cart_count);

			$("#discount").html(response.discount + ' Birr');
			$("input[name=discount]").val(response.discount);
			
			if(document.getElementById("delivery-1").checked) {
				var delivery_price = response.delivery_price_standard;
			} else if(document.getElementById("delivery-2").checked) {
				var delivery_price = response.delivery_price_express;
			} else if(document.getElementById("delivery-3").checked) {
				var delivery_price = response.delivery_price_premium;
			} else if(document.getElementById("delivery-4").checked) {
				var delivery_price = response.delivery_price_national;
			}

			$("input[name=delivery_price").val(delivery_price);

			if(response.online_payment_limit != null)
			{
				if(Number(response.total_price_main) + Number(delivery_price) <= Number(response.online_payment_limit))
				{

					$("#delivery_price").html(delivery_price + ' Birr');
					$("#total_price_main").html(Number(response.total_price_main) + Number(delivery_price) + ' Birr');
					$("input[name=total_price_main]").val(Number(response.total_price_main) + Number(delivery_price));

					document.getElementById("cash-payment").style.display = "block";
					document.getElementById("payments-2").checked = true;
					document.getElementById("bank-payment").style.display = "none";
					document.getElementById("payments-1").checked = false;
				}
				else
				{

					$("#delivery_price").html(delivery_price + ' Birr');
					$("#total_price_main").html(Number(response.total_price_main) + Number(delivery_price) + ' Birr');
					$("input[name=total_price_main]").val(Number(response.total_price_main) + Number(delivery_price));

					document.getElementById("cash-payment").style.display = "none";
					document.getElementById("payments-2").checked = false;
					document.getElementById("bank-payment").style.display = "block";
					document.getElementById("payments-1").checked = true;
				}

				document.getElementById("warning-payment-method").style.display = "none";
			} else {
				document.getElementById("cash-payment").style.display = "none";
				document.getElementById("bank-payment").style.display = "none";
				document.getElementById("payments-1").checked = false;
				document.getElementById("payments-2").checked = false;
				$("#place_order_btn").attr("disabled", "disabled");
				// Admin needs to create Payment limit in CompanySettings::update for this to work!
				$("#total_price_main").html('Sorry! Payment Limit is not generated yet!');
				document.getElementById("warning-payment-method").style.display = "block";
				$("#warning-payment-method").html('Sorry! Payment Limit is not generated yet!');
			}

			


		},

		error: function(error) {

			// console.log(error);

        }

	});
}


//  --------------------------------------------------------------------------------------------------
//
//  --------------------------------------------------------------------------------------------------

var cart_form_package = $(".cart_form_package");

cart_form_package.submit(function(e){

	var token = $("input[name=cart-csrf-token]").val();

	var package_id = $("input[name=cart-package-id]").val();

	var package_name = $("input[name=package_name]").val();

	var package_price = $("input[name=package_price]").val();

	var package_availability = $("input[name=package_availability]").val();

	var package_quantity = $("#qty").val();

	$.ajaxSetup({

		headers:{

			'X-CSRF-TOKEN': token

		}

	});

	e.preventDefault();

	$.ajax({

		url: cart_form_package.attr('action'),

		type: cart_form_package.attr('method'),

		data: {package_id: package_id, package_name: package_name, package_price: package_price, package_quantity: package_quantity, package_availability: package_availability},

		beforeSend: function() {

			$("#add_cart").attr("disabled", "disabled");
			document.getElementById('loader-roller'+package_id).style.display = "inline-block";

		},

		success: function(response) {

			$("#add_cart").removeAttr("disabled", "disabled");

			document.getElementById('loader-roller'+package_id).style.display = "none";

			get_cart();

			var message = $('.message').fadeIn('fast');
		
			if(response.status == 0)
			{
				var message = $('.message').fadeIn('fast');			       
	                
	          	var successMessage = '<div class="form-group mt-4">';

	              successMessage += '<div class="alert golden">';

	              successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';
	              
	              successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

	              + response.message + '</p>';

	              successMessage +=  '</div></div>';


	              $(message).html(successMessage).delay(4500).fadeOut('slow');
		        
			}
			else if(response.status == 1)
			{
				swal(response.package_name,"is added to cart !", "success");
			}
			
		},

		error: function(error) {

			document.getElementById('loader-roller'+package_id).style.display = "none";

			$("#add_cart").removeAttr("disabled", "disabled");

	        var message = $('.message').fadeIn('fast');
	        
	        if(error.responseJSON['errors'] != null)
	        {
	                
	          var errorMessage = '<div class="form-group mt-4">';

	              errorMessage += '<div class="alert golden">';

	              errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

	              $.each(error.responseJSON['errors'], function(key, errors)
	              {
	              
	              errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

	              + errors + '</p>';

	              });

	              errorMessage +=  '</div></div>';


	              $(message).html(errorMessage).delay(4500).fadeOut('slow');
	                
	                
	        }

	        else
	        {

	          $(message).html('').fadeIn('slow');

	        }
		}

	});

});


var cart_form = $(".cart-form");

cart_form.submit(function(e){

	var token = $("input[name=cart-csrf-token]").val();

	var product_id = $("input[name=cart-product-id]").val();

	var product_name = $("input[name=product_name]").val();

	var product_price = $("input[name=product_price]").val();

	var product_availability = $("input[name=product_availability]").val();

	var product_quantity = $("#qty").val();

	var product_color = $("input[name=color]:checked").val();

	var product_size = $("input[name=size]:checked").val();

	console.log(product_price);

	$.ajaxSetup({

		headers:{

			'X-CSRF-TOKEN': token

		}

	});

	e.preventDefault();

	$.ajax({

		url: cart_form.attr('action'),

		type: cart_form.attr('method'),

		data: {product_id: product_id, product_name: product_name, product_price: product_price, product_color: product_color, product_size: product_size, product_quantity: product_quantity, product_availability: product_availability},

		beforeSend: function() {

			$("#add_cart").attr("disabled", "disabled");
			document.getElementById('loader-roller'+product_id).style.display = "inline-block";

		},

		success: function(response) {

			$("#add_cart").removeAttr("disabled", "disabled");

			document.getElementById('loader-roller'+product_id).style.display = "none";

			get_cart();

			var message = $('.message').fadeIn('fast');
		
			if(response.status == 0)
			{
				var message = $('.message').fadeIn('fast');			       
	                
	          	var successMessage = '<div class="form-group mt-4">';

	              successMessage += '<div class="alert golden">';

	              successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

	              
	              
	              successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

	              + response.message + '</p>';

	              successMessage +=  '</div></div>';


	              $(message).html(successMessage).delay(4500).fadeOut('slow');
		        
			}
			else if(response.status == 1)
			{
				swal(response.product_name,"is added to cart !", "success");
			}
			
		},

		error: function(error) {

			document.getElementById('loader-roller'+product_id).style.display = "none";

			$("#add_cart").removeAttr("disabled", "disabled");

	        var message = $('.message').fadeIn('fast');
	        
	        if(error.responseJSON['errors'] != null)
	        {
	                
	          var errorMessage = '<div class="form-group mt-4">';

	              errorMessage += '<div class="alert golden">';

	              errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

	              $.each(error.responseJSON['errors'], function(key, errors)
	              {
	              
	              errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

	              + errors + '</p>';

	              });

	              errorMessage +=  '</div></div>';


	              $(message).html(errorMessage).delay(4500).fadeOut('slow');
	                
	                
	        }

	        else
	        {

	          $(message).html('').fadeIn('slow');

	        }
		}

	});

});

function buy(i) {
	var token = $("input[name=cart-csrf-token]").val();

	var product_id = $("input[name=cart-product-id"+i+"]").val();

	var product_name = $("input[name=product_name"+i+"]").val();

	var product_price = $("input[name=product_price"+i+"]").val();

	var product_availability = $("input[name=product_availability"+i+"]").val();

	var product_quantity = $("#qty"+i).val();

	var product_color = $("input[name=color"+i+"]:checked").val();

	var product_size = $("input[name=size"+i+"]:checked").val();

	console.log(product_price);

	$.ajaxSetup({

		headers:{

			'X-CSRF-TOKEN': token

		}

	});

	$.ajax({

		url: '/cart',

		type: 'pOST',

		data: {product_id: product_id, product_name: product_name, product_price: product_price, product_color: product_color, product_size: product_size, product_quantity: product_quantity, product_availability: product_availability},

		beforeSend: function() {

			$("#add_cart").attr("disabled", "disabled");
			document.getElementById('loader-roller2'+i).style.display = "inline-block";

		},

		success: function(response) {

			$("#quick-preview"+i).modal("hide");

			document.getElementById('loader-roller2'+i).style.display = "none";

			$("#add_cart").removeAttr("disabled", "disabled");

			$("#cart_count").html(response.cart_count);

			var message = $('.message').fadeIn('fast');
		
			if(response.status == 0)
			{
				var message = $('.message').fadeIn('fast');			       
	                
	          	var successMessage = '<div class="form-group mt-4">';
	            successMessage += '<div class="alert golden">';
	            successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';
	              
	            successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'
	            + response.message + '</p>';
	            successMessage +=  '</div></div>';

	            $(message).html(successMessage).delay(4500).fadeOut('slow');

				window.location.href = "/cart";
		        
			}
			else if(response.status == 1)
			{
				$(message).html("");

				get_cart();

				swal(response.product_name,"is added to cart !", "success");

				window.location.href = "/cart";
			}
			
		},

		error: function(error) {

			document.getElementById('loader-roller2'+i).style.display = "none";

			$("#add_cart").removeAttr("disabled", "disabled");

	        var message = $('.message').fadeIn('fast');
	        
	        if(error.responseJSON['errors'] != null)
	        {
	                
	          var errorMessage = '<div class="form-group mt-4">';

	              errorMessage += '<div class="alert golden">';

	              errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

	              $.each(error.responseJSON['errors'], function(key, errors)
	              {
	              
	              errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

	              + errors + '</p>';

	              });

	              errorMessage +=  '</div></div>';


	              $(message).html(errorMessage).delay(4500).fadeOut('slow');
	                
	                
	        }

	        else
	        {

	          $(message).html('').fadeIn('slow');

	        }
		}

	});	
}


//  --------------------------------------------------------------------------------------------------
//
//  --------------------------------------------------------------------------------------------------

function cart_submit(i){

	var token = $("input[name=cart-csrf-token]").val();

	var product_id = $("input[name=cart-product-id"+i+"]").val();

	var product_name = $("input[name=product_name"+i+"]").val();

	var product_price = $("input[name=product_price"+i+"]").val();

	var product_availability = $("input[name=product_availability"+i+"]").val();

	var product_quantity = $("#qty"+i).val();

	var product_color = $("input[name=color"+i+"]:checked").val();

	var product_size = $("input[name=size"+i+"]:checked").val();

	console.log(product_price);

	$.ajaxSetup({

		headers:{

			'X-CSRF-TOKEN': token

		}

	});

	$.ajax({

		url: '/cart',

		type: 'pOST',

		data: {product_id: product_id, product_name: product_name, product_price: product_price, product_color: product_color, product_size: product_size, product_quantity: product_quantity, product_availability: product_availability},

		beforeSend: function() {

			$("#add_cart").attr("disabled", "disabled");
			document.getElementById('loader-roller'+i).style.display = "inline-block";

		},

		success: function(response) {

			$("#quick-preview"+i).modal("hide");

			document.getElementById('loader-roller'+i).style.display = "none";

			$("#add_cart").removeAttr("disabled", "disabled");

			$("#cart_count").html(response.cart_count);

			var message = $('.message').fadeIn('fast');
		
			if(response.status == 0)
			{
				var message = $('.message').fadeIn('fast');			       
	                
	          	var successMessage = '<div class="form-group mt-4">';

	              successMessage += '<div class="alert golden">';

	              successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

	              
	              
	              successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

	              + response.message + '</p>';

	              successMessage +=  '</div></div>';


	              $(message).html(successMessage).delay(4500).fadeOut('slow');
		        
			}
			else if(response.status == 1)
			{
				$(message).html("");

				get_cart();

				swal(response.product_name,"is added to cart !", "success");
			}
			
		},

		error: function(error) {

			document.getElementById('loader-roller'+i).style.display = "none";

			$("#add_cart").removeAttr("disabled", "disabled");

	        var message = $('.message').fadeIn('fast');
	        
	        if(error.responseJSON['errors'] != null)
	        {
	                
	          var errorMessage = '<div class="form-group mt-4">';

	              errorMessage += '<div class="alert golden">';

	              errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

	              $.each(error.responseJSON['errors'], function(key, errors)
	              {
	              
	              errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

	              + errors + '</p>';

	              });

	              errorMessage +=  '</div></div>';


	              $(message).html(errorMessage).delay(4500).fadeOut('slow');
	                
	                
	        }

	        else
	        {

	          $(message).html('').fadeIn('slow');

	        }
		}

	});

};



//  --------------------------------------------------------------------------------------------------
//
//  --------------------------------------------------------------------------------------------------



function removeFromCart(i)
{
	var token = $("input[name=global-token]");

	var rowId = $("#rowId"+i).val();

	$.ajaxSetup({

		headers:{

			'X-CSRF-TOKEN': token.val()

		}

	});

	$.ajax({

		url: '/remove-cartitem',

		type: 'POST',

		data: {rowId: rowId},

		beforeSend: function() {

		},

		success: function(response) {

			get_cart();

			location.replace('/cart')

			swal(response.message, '','success');

		},

		error: function(error) {

			console.log(error);

		}

	});
}


//  --------------------------------------------------------------------------------------------------
//
//  --------------------------------------------------------------------------------------------------


function update_cart(i)
{
	var token = $("input[name=global-token]");

	var rowId = $("#rowId"+i).val();

	var qty = $("#qty"+i).val();

	var current_price = $("#current_price"+i).val();

	// console.log($("#rowId"+i).val() + ' -- ' + i);
	

	$.ajaxSetup({

		headers:{

			'X-CSRF-TOKEN': token.val()

		}

	});

	$.ajax({

		url: '/update-cart',

		type: 'POST',

		data: {rowId: rowId, qty: qty},

		beforeSend: function() {

		},

		success: function(response) {

			get_cart();

			$(".total-price-of-one-item"+i).html(qty * current_price);

			$(".total-price-of-one-item"+i).val(qty * current_price);

		},

		error: function(error) {

			console.log(error);

		}

	});
}