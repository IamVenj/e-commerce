if($("#address_map").val()!="") {
	edit();
} else {
	add();
}
function add() {
	var _map = new google.maps.Map(document.getElementById('_map'), {
	center: {
	        lat: 9.024666568,
	        lng: 38.737330384
	  },
	zoom: 12
	});

	var _marker = new google.maps.Marker({
	position: {
			lat: 9.024666568,
			lng: 38.737330384
	},
	map: _map,
	draggable: true
	});

	var _searchBox = new google.maps.places.SearchBox(document.getElementById('__address'));

	google.maps.event.addListener(_searchBox, 'places_changed', function() {

		var places = _searchBox.getPlaces();
		var _bounds = new google.maps.LatLngBounds();
		var i, place;

		for (i = 0; place=places[i]; i++) {
	      _bounds.extend(place.geometry.location);
	      _marker.setPosition(place.geometry.location);
		}

	_map.fitBounds(_bounds);
	_map.setZoom(18);

	});


	google.maps.event.addListener(_marker, 'position_changed', function() {

	  var lat = _marker.getPosition().lat();
	  var lng = _marker.getPosition().lng();

	  $('#lat').val(lat);
	  $('#lng').val(lng);

	});
}

function edit() {
	var lat = $('#_lat1').val();
    var lng = $('#_lng1').val();

    var map = new google.maps.Map(document.getElementById('map1'), {
          center: {
                lat: parseFloat(lat),
                lng: parseFloat(lng)
          },
          zoom: 18
        });

        var marker = new google.maps.Marker({
          position: {
            lat: parseFloat(lat),
            lng: parseFloat(lng)
          },
          map: map,
          draggable: true
    });

    var searchBox = new google.maps.places.SearchBox(document.getElementById('searchTextField1'));            

    (function (searchBox, map, marker) {
        google.maps.event.addListener(searchBox, 'places_changed', function() {

            var places = searchBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, place;

            for (i = 0; place=places[i]; i++) {
                bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location);
            }

                map.fitBounds(bounds);
                map.setZoom(18);

          });
    })(searchBox, map, marker);


        google.maps.event.addListener(marker, 'position_changed', function() {

          var lat = marker.getPosition().lat();
          var lng = marker.getPosition().lng();

          $('#_lat1').val(lat);
          $('#_lng1').val(lng);

        });
}