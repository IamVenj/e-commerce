// ----------- //
// File Upload //
// ----------- //


$('#uploadFile').change(function(){

  $('#image_preview').html("");

  $("#error_multiple_files").html("");

  error_images = "";

  var total_file=document.getElementById("uploadFile").files.length;    

  var file = document.getElementById("uploadFile").files;

  if(total_file > 6)
  {
    error_images += "You cannot select more than 6 images";

    $("#error_multiple_files").html("<span class='text-danger'>"+error_images+"</span><br/><br/>");
  }
  else
  {
    $("#error_multiple_files").html("");

    for(var i=0; i < total_file; i++)
    {
      
      var file_name = document.getElementById("uploadFile").files[i].name;

      var extension = file_name.split('.').pop().toLowerCase();

      // console.log(extension);
      error_array.push(extension);

      if(jQuery.inArray(extension, ['png', 'jpg', 'jpeg']) == -1)
      {
        error_images += "Only png, jpg or jpeg files are allowed!";


        $("#error_multiple_files").html("<span class='text-danger'>"+error_images+"</span><br/><br/>");

        document.getElementById("show-current-images").style.display = 'none';
      }
      else
      {
        $("#error_multiple_files").html("");

        error_images = "";

        error_array = [];
        
        document.getElementById("show-current-images").style.display = 'block';

        $('#image_preview').append("<div class='col-md-4'><img style='max-width: 100%; height: auto !important; object-fit: cover; margin-bottom:20px; box-shadow: 0px 10px 10px #00000025; border-radius: 10px;' src='"+URL.createObjectURL(event.target.files[i])+"' /></div>");
      }


      img.push(event.target.files[i]);

      // formData.append('images[]', document.getElementById("uploadFile").files[i]);

    }

  }

}); 