(function($) {
  'use strict';
  $(function() {

    getMonthlyIncome();
    getYearlyIncome();
    getDailyIncome();
    getCashIncome();
    getAllIncome();

    function getCashIncome() {
      var url = '/getCashIncome';
      var request = $.ajax({
        method: 'GET',
        url: url
      });
      request.done( function (response) {
        getCashChart(response);
      });
    }

    function getCashChart(response) {
      if ($('.cash-sales-chart-b').length) {
        for (var i = $('.cash-sales-chart-b').length - 1; i >= 0; i--) {
            var cashSalesBCanvas = $(".cash-sales-chart-b").get(i).getContext("2d");
            var data = {
              labels: response.months,
              datasets: [
                {
                  label: 'Sales',
                  data: response.total_product_price,
                  borderColor: [
                    '#2090ff', '#686868'
                  ],
                  borderWidth: 2,
                  fill: false
                }
              ]
            };
            var options = {
              scales: {
                yAxes: [{
                  display: false,
                  gridLines: {
                    drawBorder: false,
                    lineWidth: 0,
                    color: "#686868"
                  },
                  ticks: {
                    min : 0,
                    max : response.max,
                    stepSize: 10,
                    fontColor: "#686868"
                  }
                }],
                xAxes: [{
                  display: false,
                  gridLines: {
                    drawBorder: true,
                    lineWidth: 0,
                    color: "#686868"
                  }
                }]
              },
              legend: {
                display: false
              },
              elements: {
                point: {
                  radius: 0
                }
              },
              stepsize: 1
            };
            var cashSalesB = new Chart(cashSalesBCanvas, {
              type: 'bar',
              data: data,
              options: options
            });
          }
        }
    }


    

    function getMonthlyIncome() {
      var url = '/getMonthlyIncome';
      var request = $.ajax({
        method: 'GET',
        url: url
      });
      request.done( function (response) {
        monthlyIncomeChart(response);
      });
    }

    function monthlyIncomeChart(response) {
      if ($('.monthly-income-chart').length) {
        for (var i = $('.monthly-income-chart').length - 1; i >= 0; i--) {
          var monthlyIncomeCanvas = $(".monthly-income-chart").get([i]).getContext("2d");
          var data = {
            labels: response.months,
            datasets: [
              {
                label: 'Support',
                data: response.total_product_price,
                borderColor: [
                  '#ffc100'
                ],
                borderWidth: 2,
                fill: false
              }
            ]
          };
         var options = {
            scales: {
              yAxes: [{
                display: false,
                gridLines: {
                  drawBorder: false,
                  lineWidth: 0,
                  color: "rgba(0,0,0,0)"
                },
                ticks: {
                  min: 0,
                  max: response.max,
                  stepSize: 10,
                  fontColor: "#686868"
                }
              }],
              xAxes: [{
                display: false,
                gridLines: {
                  drawBorder: false,
                  lineWidth: 0,
                  color: "rgba(0,0,0,0)"
                }
              }]
            },
            legend: {
              display: false
            },
            elements: {
              point: {
                radius: 0
              }
            },
            stepsize: 1
          };
          var monthlyIncome = new Chart(monthlyIncomeCanvas, {
            type: 'line',
            data: data,
            options: options
          });
        }
        
      }
    }

    
    function getYearlyIncome() {
      var url = '/getYearlyIncome';
      var request = $.ajax({
        method: 'GET',
        url: url
      });
      request.done( function (response) {
        yearlySalesChart(response);
      });
    }

    function yearlySalesChart(response)
    {
        if ($('.yearly-sales-chart').length) {
          for (var i = $('.yearly-sales-chart').length - 1; i >= 0; i--) {
            var yearlySalesCanvas = $(".yearly-sales-chart").get(i).getContext("2d");
            var data = {
              labels: response.yrs,
              datasets: [
                {
                  label: 'Support',
                  data: response.total_product_price,
                  borderColor: [
                    '#ff4747'
                  ],
                  borderWidth: 2,
                  fill: false
                }
              ]
            };
            var options = {
              scales: {
                yAxes: [{
                  display: false,
                  gridLines: {
                    drawBorder: false,
                    lineWidth: 0,
                    color: "rgba(0,0,0,0)"
                  },
                  ticks: {
                    min: 0,
                    max: response.max,
                    stepSize: 10,
                    fontColor: "#686868"
                  }
                }],
                xAxes: [{
                  display: false,
                  gridLines: {
                    drawBorder: false,
                    lineWidth: 0,
                    color: "rgba(0,0,0,0)"
                  }
                }]
              },
              legend: {
                display: false
              },
              elements: {
                point: {
                  radius: 0
                }
              },
              stepsize: 1
            };
            var yearlySales = new Chart(yearlySalesCanvas, {
              type: 'line',
              data: data,
              options: options
            });
          }
        }
    }
    
    function getDailyIncome() {
      var url = '/getDailyIncome';
      var request = $.ajax({
        method: 'GET',
        url: url
      });
      request.done( function (response) {
        dailySalesChart(response);
      });
    }

    function dailySalesChart(response) {
      if ($('._daily-deposits-chart_').length) {
        for (var i = $('._daily-deposits-chart_').length - 1; i >= 0; i--) {
          var dailyDepositsCanvas = $("._daily-deposits-chart_").get(i).getContext("2d");
          var data = {
            labels: response.days,
            datasets: [
              {
                label: 'Support',
                data: response.total_product_price,
                borderColor: [
                  '#71c016'
                ],
                borderWidth: 2,
                fill: false
              }
            ]
          };
          var options = {
            scales: {
              yAxes: [{
                display: false,
                gridLines: {
                  drawBorder: false,
                  lineWidth: 0,
                  color: "rgba(0,0,0,0)"
                },
                ticks: {
                  min : 0,
                  max : response.max,
                  stepSize: 10,
                  fontColor: "#686868"
                }
              }],
              xAxes: [{
                display: false,
                gridLines: {
                  drawBorder: false,
                  lineWidth: 0,
                  color: "rgba(0,0,0,0)"
                }
              }]
            },
            legend: {
              display: false
            },
            elements: {
              point: {
                radius: 0
              },
              line: {
                tension: 0
              }
            },
            stepsize: 1
          };
          var dailyDeposits = new Chart(dailyDepositsCanvas, {
            type: 'pie',
            data: data,
            options: options
          });
        }
      }
    }


    if ($('#cash-deposits-chart').length) {
      var cashDepositsCanvas = $("#cash-deposits-chart").get(0).getContext("2d");
      var data = {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8"],
        datasets: [
          {
            label: 'Returns',
            data: [27, 35, 30, 40, 52, 48, 54, 46, 70],
            borderColor: [
              '#ff4747'
            ],
            borderWidth: 2,
            fill: false,
            pointBackgroundColor: "#fff"
          },
          {
            label: 'Sales',
            data: [29, 40, 37, 48, 64, 58, 70, 57, 80],
            borderColor: [
              '#4d83ff'
            ],
            borderWidth: 2,
            fill: false,
            pointBackgroundColor: "#fff"
          },
          {
            label: 'Loss',
            data: [90, 62, 80, 63, 72, 62, 40, 50, 38],
            borderColor: [
              '#ffc100'
            ],
            borderWidth: 2,
            fill: false,
            pointBackgroundColor: "#fff"
          }
        ]
      };
      var options = {
        scales: {
          yAxes: [{
            display: true,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#e9e9e9",
              zeroLineColor: "#e9e9e9",
            },
            ticks: {
              min: 0,
              max: 100,
              stepSize: 20,
              fontColor: "#6c7383",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }],
          xAxes: [{
            display: true,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#e9e9e9",
            },
            ticks : {
              fontColor: "#6c7383",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }]
        },
        legend: {
          display: false
        },
        legendCallback: function(chart) {
          var text = [];
          text.push('<ul class="dashboard-chart-legend">');
          for(var i=0; i < chart.data.datasets.length; i++) {
            text.push('<li><span style="background-color: ' + chart.data.datasets[i].borderColor[0] + ' "></span>');
            if (chart.data.datasets[i].label) {
              text.push(chart.data.datasets[i].label);
            }
          }
          text.push('</ul>');
          return text.join("");
        },
        elements: {
          point: {
            radius: 3
          },
          line :{
            tension: 0
          }
        },
        stepsize: 1,
        layout : {
          padding : {
            top: 0,
            bottom : -10,
            left : -10,
            right: 0
          }
        }
      };
      var cashDeposits = new Chart(cashDepositsCanvas, {
        type: 'line',
        data: data,
        options: options
      });
      document.getElementById('cash-deposits-chart-legend').innerHTML = cashDeposits.generateLegend();
    }

    if ($('#cash-deposits-chart-dark').length) {
      var cashDepositsCanvas = $("#cash-deposits-chart-dark").get(0).getContext("2d");
      var data = {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8"],
        datasets: [
          {
            label: 'Returns',
            data: [27, 35, 30, 40, 52, 48, 54, 46, 70],
            borderColor: [
              '#ff4747'
            ],
            borderWidth: 2,
            fill: false,
            pointBackgroundColor: "#26293b"
          },
          {
            label: 'Sales',
            data: [29, 40, 37, 48, 64, 58, 70, 57, 80],
            borderColor: [
              '#4d83ff'
            ],
            borderWidth: 2,
            fill: false,
            pointBackgroundColor: "#26293b"
          },
          {
            label: 'Loss',
            data: [90, 62, 80, 63, 72, 62, 40, 50, 38],
            borderColor: [
              '#ffc100'
            ],
            borderWidth: 2,
            fill: false,
            pointBackgroundColor: "#26293b"
          }
        ]
      };
      var options = {
        scales: {
          yAxes: [{
            display: true,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#313452",
              zeroLineColor: "#313452",
            },
            ticks: {
              min: 0,
              max: 100,
              stepSize: 20,
              fontColor: "#b1b1b5",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }],
          xAxes: [{
            display: true,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#313452",
            },
            ticks : {
              fontColor: "#b1b1b5",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }]
        },
        legend: {
          display: false
        },
        legendCallback: function(chart) {
          var text = [];
          text.push('<ul class="dashboard-chart-legend">');
          for(var i=0; i < chart.data.datasets.length; i++) {
            text.push('<li><span style="background-color: ' + chart.data.datasets[i].borderColor[0] + ' "></span>');
            if (chart.data.datasets[i].label) {
              text.push(chart.data.datasets[i].label);
            }
          }
          text.push('</ul>');
          return text.join("");
        },
        elements: {
          point: {
            radius: 3
          },
          line :{
            tension: 0
          }
        },
        stepsize: 1,
        layout : {
          padding : {
            top: 0,
            bottom : -10,
            left : -10,
            right: 0
          }
        }
      };
      var cashDeposits = new Chart(cashDepositsCanvas, {
        type: 'line',
        data: data,
        options: options
      });
      document.getElementById('cash-deposits-chart-dark-legend').innerHTML = cashDeposits.generateLegend();
    }

    function getAllIncome() {
      var url = '/getAllIncome';
      var request = $.ajax({
        method: 'GET',
        url: url
      });
      request.done( function (response) {
        totalSalesChart(response);
        // console.log(response[0].max);
      });
    }

    function totalSalesChart(response) {
      if ($('#total-sales-chart').length) {
        var totalSalesChartCanvas = $("#total-sales-chart").get(0).getContext("2d");
        var data = {
          labels: response[0].months,
          datasets: [
            {
              label: '2019',
              data: response[0].total_product_price,
              borderColor: [
                'transparent'
              ],
              borderWidth: 2,
              fill: true,
              backgroundColor: "rgba(47,91,191,0.77)"
            },
            {
              label: '2018',
              data: response[1].total_product_price,
              borderColor: [
                'transparent'
              ],
              borderWidth: 2,
              fill: true,
              backgroundColor: "rgba(77,131,255,0.77)"
            },
            {
              label: 'Past years',
              data: response[2].total_product_price,
              borderColor: [
                'transparent'
              ],
              borderWidth: 2,
              fill: true,
              backgroundColor: "rgba(77,131,255,0.43)"
            }
          ]
        };
        var options = {
          scales: {
            yAxes: [{
              display: false,
              gridLines: {
                drawBorder: false,
                lineWidth: 1,
                color: "#e9e9e9",
                zeroLineColor: "#e9e9e9",
              },
              ticks: {
                display : true,
                min: 0,
                max: response[0].max,
                stepSize: 10,
                fontColor: "#6c7383",
                fontSize: 16,
                fontStyle: 300,
                padding: 15
              }
            }],
            xAxes: [{
              display: false,
              gridLines: {
                drawBorder: false,
                lineWidth: 1,
                color: "#e9e9e9",
              },
              ticks : {
                display: true,
                fontColor: "#6c7383",
                fontSize: 16,
                fontStyle: 300,
                padding: 15
              }
            }]
          },
          legend: {
            display: false
          },
          legendCallback: function(chart) {
            var text = [];
            text.push('<ul class="dashboard-chart-legend mb-0 mt-4">');
            for(var i=0; i < chart.data.datasets.length; i++) {
              text.push('<li><span style="background-color: ' + chart.data.datasets[i].backgroundColor + ' "></span>');
              if (chart.data.datasets[i].label) {
                text.push(chart.data.datasets[i].label);
              }
            }
            text.push('</ul>');
            return text.join("");
          },
          elements: {
            point: {
              radius: 0
            },
            line :{
              tension: 0
            }
          },
          stepsize: 1,
          layout : {
            padding : {
              top: 0,
              bottom : 0,
              left : 0,
              right: 0
            }
          }
        };
        var totalSalesChart = new Chart(totalSalesChartCanvas, {
          type: 'line',
          data: data,
          options: options
        });
        document.getElementById('total-sales-chart-legend').innerHTML = totalSalesChart.generateLegend();
      }
    }
    

    $('#recent-purchases-listing').DataTable({
      "aLengthMenu": [
        [5, 10, 15, -1],
        [5, 10, 15, "All"]
      ],
      "iDisplayLength": 10,
      "language": {
        search: ""
      },
      searching: false, paging: false, info: false
    });

    if ($('#downloads-chart-a').length) {
      var downloadsChartAChartCanvas = $("#downloads-chart-a").get(0).getContext("2d");

      var data = {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9",'10', '11','12', '13', '14', '15','16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26','27','28','29', '30','31', '32', '33', '34', '35', '36', '37','38', '39', '40'],
        datasets: [
          {
            label: '2019',
            data: [42, 42, 30, 30, 18, 22, 16, 21, 22, 22, 22, 20, 24, 20, 18, 22, 30, 34 ,32, 33, 33, 24, 32, 34 , 30, 34, 19 ,34, 18, 10, 22, 24, 20, 22, 20, 21, 10, 10, 5, 9, 14 ],
            borderColor: [
              'transparent'
            ],
            borderWidth: 2,
            fill: true,
            backgroundColor: "rgba(255,255,255,0.77)"
          },
          {
            label: '2018',
            data: [35, 28, 32, 42, 44, 46, 42, 50, 48, 30, 35, 48, 42, 40, 54, 58, 56, 55, 59, 58, 57, 60, 58, 54, 38, 40, 42, 44, 42, 43, 42, 38, 43, 41, 43, 50, 58 ,58, 68, 72, 72 ],
            borderColor: [
              'transparent'
            ],
            borderWidth: 2,
            fill: true,
            backgroundColor: "rgba(255,255,255,0.43)"
          }
        ]
      };
      var options = {
        scales: {
          yAxes: [{
            display: false,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#e9e9e9",
              zeroLineColor: "#e9e9e9",
            },
            ticks: {
              display : true,
              min: 0,
              max: 80,
              stepSize: 10,
              fontColor: "#6c7383",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }],
          xAxes: [{
            display: false,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#e9e9e9",
            },
            ticks : {
              display: true,
              fontColor: "#6c7383",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }]
        },
        legend: {
          display: false
        },
        elements: {
          point: {
            radius: 0
          },
          line :{
            tension: 0
          }
        },
        stepsize: 1,
        layout : {
          padding : {
            top: 0,
            bottom : 0,
            left : 0,
            right: 0
          }
        }
      };
      var downloadsChartAChart = new Chart(downloadsChartAChartCanvas, {
        type: 'line',
        data: data,
        options: options
      });
    }

    if ($('#downloads-chart-b').length) {
      var downloadsChartBChartCanvas = $("#downloads-chart-b").get(0).getContext("2d");

      var data = {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9",'10', '11','12', '13', '14', '15','16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26','27','28','29', '30','31', '32', '33', '34', '35', '36', '37','38', '39', '40'],
        datasets: [
          {
            label: '2019',
            data: [52, 52, 40, 40, 28, 32, 26, 31, 32, 32, 32, 20, 24, 20, 18, 22, 30, 34 ,32, 33, 33, 24, 32, 34 , 30, 34, 19 ,34, 18, 10, 22, 24, 20, 22, 20, 21, 10, 10, 5, 9, 14 ],
            borderColor: [
              'transparent'
            ],
            borderWidth: 2,
            fill: true,
            backgroundColor: "rgba(255,255,255,0.77)"
          },
          {
            label: '2018',
            data: [35, 28, 32, 42, 44, 46, 42, 50, 48, 30, 35, 48, 42, 40, 54, 58, 56, 55, 59, 58, 57, 60, 66, 64, 60, 55, 49, 44, 42, 43, 42, 38, 43, 41, 43, 40, 38 ,38, 38, 32, 32 ],
            borderColor: [
              'transparent'
            ],
            borderWidth: 2,
            fill: true,
            backgroundColor: "rgba(255,255,255,0.43)"
          }
        ]
      };
      var options = {
        scales: {
          yAxes: [{
            display: false,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#e9e9e9",
              zeroLineColor: "#e9e9e9",
            },
            ticks: {
              display : true,
              min: 0,
              max: 80,
              stepSize: 10,
              fontColor: "#6c7383",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }],
          xAxes: [{
            display: false,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#e9e9e9",
            },
            ticks : {
              display: true,
              fontColor: "#6c7383",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }]
        },
        legend: {
          display: false
        },
        elements: {
          point: {
            radius: 0
          },
          line :{
            tension: 0
          }
        },
        stepsize: 1,
        layout : {
          padding : {
            top: 0,
            bottom : 0,
            left : 0,
            right: 0
          }
        }
      };
      var downloadsChartBChart = new Chart(downloadsChartBChartCanvas, {
        type: 'line',
        data: data,
        options: options
      });
    }

    if ($('#feedbacks-chart-a').length) {
      var feedbacksChartAChartCanvas = $("#feedbacks-chart-a").get(0).getContext("2d");

      var data = {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9",'10', '11','12', '13', '14', '15','16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26','27','28','29', '30','31', '32', '33', '34', '35', '36', '37','38', '39', '40'],
        datasets: [
          {
            label: '2019',
            data: [42, 42, 30, 30, 18, 22, 16, 21, 22, 22, 22, 20, 24, 20, 18, 22, 30, 34 ,32, 33, 33, 24, 32, 34 , 30, 34, 19 ,34, 18, 10, 22, 24, 20, 22, 20, 21, 10, 10, 5, 9, 14 ],
            borderColor: [
              'transparent'
            ],
            borderWidth: 2,
            fill: true,
            backgroundColor: "rgba(255,255,255,0.77)"
          },
          {
            label: '2018',
            data: [68, 58, 62, 66, 44, 46, 42, 50, 48, 30, 35, 48, 42, 40, 54, 58, 56, 55, 59, 58, 57, 60, 46, 54, 38, 40, 42, 44, 42, 43, 42, 38, 43, 41, 43, 50, 58 ,58, 68, 72, 72 ],
            borderColor: [
              'transparent'
            ],
            borderWidth: 2,
            fill: true,
            backgroundColor: "rgba(255,255,255,0.43)"
          }
        ]
      };
      var options = {
        scales: {
          yAxes: [{
            display: false,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#e9e9e9",
              zeroLineColor: "#e9e9e9",
            },
            ticks: {
              display : true,
              min: 0,
              max: 80,
              stepSize: 10,
              fontColor: "#6c7383",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }],
          xAxes: [{
            display: false,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#e9e9e9",
            },
            ticks : {
              display: true,
              fontColor: "#6c7383",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }]
        },
        legend: {
          display: false
        },
        elements: {
          point: {
            radius: 0
          },
          line :{
            tension: 0
          }
        },
        stepsize: 1,
        layout : {
          padding : {
            top: 0,
            bottom : 0,
            left : 0,
            right: 0
          }
        }
      };
      var feedbacksChartAChart = new Chart(feedbacksChartAChartCanvas, {
        type: 'line',
        data: data,
        options: options
      });
    }

    if ($('#feedbacks-chart-b').length) {
      var feedbacksChartBChartCanvas = $("#feedbacks-chart-b").get(0).getContext("2d");

      var data = {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9",'10', '11','12', '13', '14', '15','16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26','27','28','29', '30','31', '32', '33', '34', '35', '36', '37','38', '39', '40'],
        datasets: [
          {
            label: '2019',
            data: [22, 22, 10, 10, 8, 12, 16, 21, 22, 22, 22, 20, 24, 20, 18, 22, 30, 34 ,32, 33, 33, 24, 32, 34 , 30, 34, 19 ,34, 18, 10, 22, 24, 20, 22, 20, 21, 10, 20, 15, 19, 24 ],
            borderColor: [
              'transparent'
            ],
            borderWidth: 2,
            fill: true,
            backgroundColor: "rgba(255,255,255,0.77)"
          },
          {
            label: '2018',
            data: [68, 58, 62, 66, 44, 46, 42, 50, 48, 30, 35, 48, 42, 40, 54, 58, 56, 55, 59, 58, 57, 60, 46, 54, 38, 40, 42, 44, 42, 43, 42, 38, 43, 41, 43, 50, 58 ,58, 68, 72, 72 ],
            borderColor: [
              'transparent'
            ],
            borderWidth: 2,
            fill: true,
            backgroundColor: "rgba(255,255,255,0.43)"
          }
        ]
      };
      var options = {
        scales: {
          yAxes: [{
            display: false,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#e9e9e9",
              zeroLineColor: "#e9e9e9",
            },
            ticks: {
              display : true,
              min: 0,
              max: 80,
              stepSize: 10,
              fontColor: "#6c7383",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }],
          xAxes: [{
            display: false,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#e9e9e9",
            },
            ticks : {
              display: true,
              fontColor: "#6c7383",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }]
        },
        legend: {
          display: false
        },
        elements: {
          point: {
            radius: 0
          },
          line :{
            tension: 0
          }
        },
        stepsize: 1,
        layout : {
          padding : {
            top: 0,
            bottom : 0,
            left : 0,
            right: 0
          }
        }
      };
      var feedbacksChartBChart = new Chart(feedbacksChartBChartCanvas, {
        type: 'line',
        data: data,
        options: options
      });
    }

    if ($('#customers-chart-a').length) {
      var customersChartAChartCanvas = $("#customers-chart-a").get(0).getContext("2d");

      var data = {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9",'10', '11','12', '13', '14', '15','16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26','27','28','29', '30','31', '32', '33', '34', '35', '36', '37','38', '39', '40'],
        datasets: [
          {
            label: '2019',
            data: [42, 42, 30, 30, 18, 22, 16, 21, 22, 22, 22, 20, 24, 20, 18, 22, 30, 34 ,32, 33, 33, 24, 32, 34 , 30, 34, 19 ,34, 18, 10, 22, 24, 20, 22, 20, 21, 10, 10, 5, 9, 14 ],
            borderColor: [
              'transparent'
            ],
            borderWidth: 2,
            fill: true,
            backgroundColor: "#f3f3f3"
          },
          {
            label: '2018',
            data: [35, 28, 32, 42, 44, 46, 42, 50, 48, 30, 35, 48, 42, 40, 54, 58, 56, 55, 59, 58, 57, 60, 66, 54, 38, 40, 42, 44, 42, 43, 42, 38, 43, 41, 43, 50, 58 ,58, 68, 72, 72 ],
            borderColor: [
              'transparent'
            ],
            borderWidth: 2,
            fill: true,
            backgroundColor: "rgba(255,255,255,0.43)"
          }
        ]
      };
      var options = {
        scales: {
          yAxes: [{
            display: false,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#e9e9e9",
              zeroLineColor: "#e9e9e9",
            },
            ticks: {
              display : true,
              min: 0,
              max: 80,
              stepSize: 10,
              fontColor: "#6c7383",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }],
          xAxes: [{
            display: false,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#e9e9e9",
            },
            ticks : {
              display: true,
              fontColor: "#6c7383",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }]
        },
        legend: {
          display: false
        },
        elements: {
          point: {
            radius: 0
          },
          line :{
            tension: 0
          }
        },
        stepsize: 1,
        layout : {
          padding : {
            top: 0,
            bottom : 0,
            left : 0,
            right: 0
          }
        }
      };
      var customersChartAChart = new Chart(customersChartAChartCanvas, {
        type: 'line',
        data: data,
        options: options
      });
    }

    if ($('#customers-chart-b').length) {
      var customersChartBChartCanvas = $("#customers-chart-b").get(0).getContext("2d");

      var data = {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9",'10', '11','12', '13', '14', '15','16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26','27','28','29', '30','31', '32', '33', '34', '35', '36', '37','38', '39', '40'],
        datasets: [
          {
            label: '2019',
            data: [42, 42, 30, 30, 18, 22, 16, 21, 22, 22, 22, 20, 24, 20, 18, 22, 30, 34 ,32, 33, 33, 24, 32, 34 , 30, 34, 19 ,34, 18, 10, 22, 24, 20, 22, 20, 21, 10, 10, 5, 9, 14 ],
            borderColor: [
              'transparent'
            ],
            borderWidth: 2,
            fill: true,
            backgroundColor: "#f3f3f3"
          },
          {
            label: '2018',
            data: [55, 48, 52, 62, 64, 66, 62, 60, 48, 30, 35, 48, 42, 40, 54, 58, 56, 55, 59, 58, 57, 60, 66, 54, 38, 40, 42, 44, 42, 43, 42, 38, 43, 41, 33, 30, 28 ,28, 32, 36, 42 ],
            borderColor: [
              'transparent'
            ],
            borderWidth: 2,
            fill: true,
            backgroundColor: "rgba(255,255,255,0.43)"
          }
        ]
      };
      var options = {
        scales: {
          yAxes: [{
            display: false,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#e9e9e9",
              zeroLineColor: "#e9e9e9",
            },
            ticks: {
              display : true,
              min: 0,
              max: 80,
              stepSize: 10,
              fontColor: "#6c7383",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }],
          xAxes: [{
            display: false,
            gridLines: {
              drawBorder: false,
              lineWidth: 1,
              color: "#e9e9e9",
            },
            ticks : {
              display: true,
              fontColor: "#6c7383",
              fontSize: 16,
              fontStyle: 300,
              padding: 15
            }
          }]
        },
        legend: {
          display: false
        },
        elements: {
          point: {
            radius: 0
          },
          line :{
            tension: 0
          }
        },
        stepsize: 1,
        layout : {
          padding : {
            top: 0,
            bottom : 0,
            left : 0,
            right: 0
          }
        }
      };
      var customersChartBChart = new Chart(customersChartBChartCanvas, {
        type: 'line',
        data: data,
        options: options
      });
    }

  });
})(jQuery);