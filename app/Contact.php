<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CompanySettings;
use App\Mail\ContactMail;

class Contact extends Model
{
    protected $fillable = ['fullname', 'subject', 'email', 'message'];

    public function storeContactMessage($fullname, $email, $message, $subject)
    {
    	$contacts = $this::create([

    		'fullname' => $fullname, 
    		'subject' => $subject, 
    		'email' => $email,
    		'message' => $message

    	]);
        $settings = CompanySettings::first();
        \Mail::send(new ContactMail($contacts, $settings));

    }

    public function checkIfContactExists($fullname, $email, $message, $subject)
    {
    	$count = $this::where('fullname', $fullname)->where('email', $email)->where('message', $message)->where('subject', $subject)->count();
    	if($count > 0)
    		return true;
    	return false;
    }
}
