<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cloudder;

class ProductImage extends Model
{
    protected $fillable = ['product_id', 'image_public_id', 'image_version', 'package_id'];

    protected $hidden = ['product_id'];

    public function products()
    {
    	return $this->belongsTo("App\Product");
    }

    public function package()
    {
        return $this->belongsTo("App\SpeicalPackage");
    }

    public function addProductImageForASingleProduct($product_id, $image_public_id, $image_version, $package_id)
    {
        if(!is_null($product_id)) {
        	$this::create([
        		'product_id' => $product_id,
        		'image_public_id' => $image_public_id,
                'image_version' => $image_version
        	]);    
        } else {
            $this::create([
                'package_id' => $package_id,
                'image_public_id' => $image_public_id,
                'image_version' => $image_version
             ]);
        }
    }


    public function updateProductImageForASingleProduct($product_id, $image_public_id, $image_version, $package_id, $id)
    {
        $productImage = $this::find($id);

        if(!is_null($product_id)) {
            $productImage->product_id = $product_id;
            $productImage->image_public_id = $image_public_id;
            $productImage->image_version = $image_version;   
        } else {
            $productImage->package_id = $package_id;
            $productImage->image_public_id = $image_public_id;
            $productImage->image_version = $image_version;
        }

        $productImage->save();
    }

    public function destroyImage($image_id) {
        $productImage = $this::find($image_id);
        Cloudder::destroy($productImage->image_public_id);
    }

    public function destroySelectedImage($id) {
        $productImage = $this::find($id);
        // Cloudder::destroy($productImage->image_public_id);
        $productImage->delete();
    }
}
