<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class CompanySettings extends Model
{
    protected $fillable = ['company_name', 'slug', 'location', 'phone_number', 'email', 'facebook', 'twitter', 'google_plus', 'instagram', 'products_amount_for_discount', 'discount', 'delivery_per_km', 'address', 'lat', 'lng', 'cut_individual', 'cut_business', 'shipping_guide', 'shipping_return', 'about', 'terms_and_condition', 'credit_hint', 'online_payment_limit', 'symmart_standard_price', 'symmart_premium_price', 'symmart_national_price', 'symmart_express_price'];

     public function update_settings($company_name, $slug, $location, $phone_number, $email, $facebook, $twitter, $google_plus, $instagram, $logo, $favicon, $products_amount_for_discount, $discount, $delivery_per_km, $map_address, $lat, $lng, $cut_individual, $cut_business, $credit_hint, $online_payment_limit, $symmart_express_price, $symmart_standard_price, $symmart_premium_price, $symmart_national_price)
    {

    	$settings = $this::first();

    	/*
        /------------------------------------------------------------
        / If logo and favicon request is not null get its extension,
        / and Delete the existing file then update the new file.
        / while saving, move the new image in the public folder.
        /------------------------------------------------------------
        */

        /*
        /--------------------------------------------------------------------------
        / even If logo and favicon request is null just update the rest
        /--------------------------------------------------------------------------
        */

    	if (!is_null($logo) && !is_null($favicon)) 
    	{
    		$logo_update = 'logo.'.$logo->getClientOriginalExtension();
    		$favicon_update = 'favicon.'.$favicon->getClientOriginalExtension();
    		Storage::delete(['public/uploads/'.$logo_update, 'public/uploads/'.$favicon_update]);
			Storage::putFileAs('public/uploads', $logo, $logo_update);
			Storage::putFileAs('public/uploads', $favicon, $favicon_update);

    	}

    	elseif(!is_null($logo) && is_null($favicon))

    	{

    		$logo_update = 'logo.'.$logo->getClientOriginalExtension();
    		Storage::delete('public/uploads/'.$logo_update);
			Storage::putFileAs('public/uploads', $logo, $logo_update);

    	}

    	elseif(is_null($logo) && !is_null($favicon))

    	{

    		$favicon_update = 'favicon.'.$favicon->getClientOriginalExtension();
    		Storage::delete('public/uploads/'.$favicon_update);
			Storage::putFileAs('public/uploads', $favicon, $favicon_update);

    	}


    	$settings->location = $location;
    	$settings->slug = $slug;
    	$settings->email = $email;
    	$settings->company_name = $company_name;
    	$settings->phone_number = $phone_number;
    	$settings->facebook = $facebook;
    	$settings->twitter = $twitter;
    	$settings->google_plus = $google_plus;
    	$settings->instagram = $instagram;
        $settings->products_amount_for_discount = $products_amount_for_discount;
        $settings->discount = $discount;
        $settings->delivery_per_km = $delivery_per_km;
        $settings->address = $map_address;
        $settings->lat = $lat;
        $settings->lng = $lng;
        $settings->cut_individual = $cut_individual;
        $settings->cut_business = $cut_business;
        $settings->credit_hint = $credit_hint;
        $settings->online_payment_limit = $online_payment_limit;
        $settings->symmart_express_price = $symmart_express_price;
        $settings->symmart_standard_price = $symmart_standard_price;
        $settings->symmart_premium_price = $symmart_premium_price;
        $settings->symmart_national_price = $symmart_national_price;

    	$settings->save();

    }

    public function updateGuide($guide, $doc) {
        $setting = $this->first();
        $setting->shipping_guide = $guide;
        $setting->save();
        $this->updateDoc($doc, "shipping_guide");
    }

    public function updateReturn($ship_return, $doc) {
        $setting = $this::first();
        $setting->shipping_return = $ship_return;
        $setting->save();
        $this->updateDoc($doc, "shipping_and_return");
    }

    public function updateAbout($about) {
        $setting = $this::first();
        $setting->about = $about;
        $setting->save();
    }

    public function updateTerms($terms_and_condition, $doc) {
        $setting = $this::first();
        $setting->terms_and_condition = $terms_and_condition;
        $setting->save();
        $this->updateDoc($doc, "terms_and_condition");
    }

    public function updateDoc($doc, $doc_type) {
        if($doc_type == "shipping_guide") {
            $document = 'Symmart-Shipping-Guide.'.$doc->getClientOriginalExtension();
            (Storage::exists('public/uploads/document/'.$document)) ? Storage::delete('public/uploads/document/'.$document) : null;
            Storage::putFileAs('public/uploads/document', $doc, $document);
        } elseif($doc_type == "shipping_and_return") {
            $document = 'Symmart-Shipping-and-Return.'.$doc->getClientOriginalExtension();
            (Storage::exists('public/uploads/document/'.$document)) ? Storage::delete('public/uploads/document/'.$document) : null;
            Storage::putFileAs('public/uploads/document', $doc, $document);
        } elseif($doc_type == "terms_and_condition") {
            $document = 'Symmart-Terms-and-Conditions.'.$doc->getClientOriginalExtension();
            (Storage::exists('public/uploads/document/'.$document)) ? Storage::delete('public/uploads/document/'.$document) : null;
            Storage::putFileAs('public/uploads/document', $doc, $document);
        }
    }

    // public function downloadDoc($doc) {
    //     Storage::download('public/uploads/document/'.$doc.'.pdf');
    // }
}
