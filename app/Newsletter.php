<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $fillable = ['email'];

    protected $hidden = ['email'];

    public function addUserForNewsLetter($email)
    {
    	$email_count = $this::where('email', $email)->count();

    	if($email_count == 0)
    	{
    		
	    	$this::create([

	    		'email' => $email

	    	]);

    	}

    }
}
