<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageProduct extends Model
{
    protected $fillable = ['package_id', 'product_id'];
    protected $hidden = ['product_id'];

    public function product() {
    	return $this->belongsTo('App\Product');
    }

    public function package() {
    	return $this->hasMany('App\SpecialPackage', 'id', 'package_id');
    }

    public function getPackage($id) {
        
    }

    public function createPackageRelation($packageId, $productId) {
    	$this::create([
    		'package_id' => $packageId,
    		'product_id' => $productId
    	]);
    }

    public function removeProductFromPackage($id) {
        $this::find($id)->delete();
    }
}
