<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryP extends Model
{
    protected $fillable = ['name', 'phone_number'];
    public function createDeliverer($name, $phone_number) {
    	$this::create([
    		'name' => $name,
    		'phone_number' => $phone_number
    	]);
    }

    public function updateDeliverer($id, $name, $phone_number) {
    	$d = $this::find($id);
    	$d->name = $name;
    	$d->phone_number = $phone_number;
    	$d->save();
    } 

    public function destroyDeliverer($id) {
    	$this::find($id)->delete();
    }

    public function getAllPersonnel() {
        $this::all();
    }
}
