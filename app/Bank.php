<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = ['bank_name', 'bank_account', 'user_id'];
    public function add($account, $bank_name, $user_id) {
    	$this::create([
    		'bank_account' => $account,
    		'bank_name' => $bank_name,
    		'user_id' => $user_id
    	]);
    }

    public function updateAccount($id, $account, $bank_name) {
    	$bank = $this::find($id);
    	$bank->bank_account = $account;
    	$bank->bank_name = $bank_name;
    	$bank->save();
    }
}
