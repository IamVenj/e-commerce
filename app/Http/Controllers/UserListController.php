<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UserListController extends Controller
{
    private $_user;

	public function __construct()
	{
		$this->middleware(['auth', 'admin']);
        $this->_user = new User();
	}

    public function index()
    {	
		return view('post-login.pages.registered-users.index');	
    }

    public function showUsers()
    {
    	$users = $this->_user::with(['addresses', 'bank'])->where('role', request('user_role'))->get();
    	return response()->json(["user"=>$users], 200);
    }

    public function create()
    {
        return view('post-login.pages.addUser.create');
    }

    public function mainIndex() {
        $vendors = $this->_user::where('role', 2)->whereNotNull('shop_name')->latest()->get();
        return view('post-login.pages.addUser.index', compact('vendors'));
    }

    public function destroyVendor($id) {
        $this->_user->destroyUser($id);
        return back()->with('success', 'Vendor is successfully deleted!');
    }

    public function deactivateVendor($id) {
        $this->_user->deactivateAdmin($id);
        return back()->with('success', 'Vendor is successfully Deactivated!');
    }

    public function activateVendor($id) {
        $this->_user->activate($id);
        return back()->with('success', 'Vendor is successfully Activated!');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'shopname' => 'required',
            'about_shop' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
        ]);

        $email = $request->email;
        $password = $request->password;
        $shopname = $request->shopname;
        $about_shop = $request->about_shop;
        $phone_number = $request->phone_number;

        $address = $request->address;
        $lat = $request->lat;
        $lng = $request->lng;

        $this->_user->addVendor($email, $password, $shopname, $about_shop, $phone_number, $address, $lat, $lng);
        return back()->with('success', 'Vendor is successfully created!');
    }
}
