<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UnderConstructionController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
    public function index()
    {
    	return view('pre-login.partials.underxcon');
    }
}
