<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FAQ;

class FaqController extends Controller
{
	private $_faq;
    public function __construct()
    {
    	$this->middleware(['auth', 'admin'])->except(['indexCustomer']);
    	$this->_faq = new FAQ();
    }

    public function index() {
        $faqz = $this->_faq::latest()->get();
    	return view('post-login.pages.custom-pages.faq.index', compact('faqz'));
    }

    public function indexCustomer() {
        $faqz = $this->_faq::latest()->get();
        return view('pre-login.pages.faq', compact('faqz'));
    }

    public function store(Request $request) {
    	$this->validate(request(), [
    		'question' => 'required',
    		'answer' => 'required'
    	]);

    	$question = $request->question;
    	$answer = $request->answer;
    	$this->_faq->createFAQ($question, $answer);

    	return back()->with('success', 'FAQ is successfully created!');
    }

    public function update($id, Request $request) {
    	$this->validate(request(), [
    		'question' => 'required',
    		'answer' => 'required'
    	]);

    	$question = $request->question;
    	$answer = $request->answer;
    	$this->_faq->updateFAQ($question, $answer, $id);

    	return back()->with('success', 'FAQ is successfully updated!');
    }

    public function destroy($id) {
    	$this->_faq->destroyFAQ($id);
    	return back()->with('success', 'FAQ is successfully deleted!');
    }
}
