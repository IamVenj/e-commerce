<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

use App\WishList;

use App\User;

class WishController extends Controller
{
	private $_product, $_wishList, $_user;

    public function __construct()
    {
    	$this->middleware('auth');

    	$this->_product = new Product();

    	$this->_wishList = new WishList();

        $this->_user = new User();
    }


    public function index(Request $request)
    {
        $user_wishes = $this->_wishList::with(['user', 'product'])->where('user_id', auth()->user()->id)->get();

        if($request->wantsJson())
        {
            return response()->json(['wish'=>$user_wishes], 200);
        }

        return view('pre-login.pages.my.wishlist', compact('user_wishes'));
    }


    public function store(Request $request)
    {
    	$product_id = $request->product_id;

    	$product_data = $this->_product::find($product_id);

    	$this->_wishList->add_wish($product_id, auth()->user()->id);

    	return response()->json(['productName'=>$product_data->product_name], 200);
    }

    public function destroy($id, Request $request)
    {
    	$this->_wishList->destroyWish($id);

        if($request->wantsJson())
        {
            return response()->json(['status' => 'success', 'message' => 'Wish is removed!'], 200);
        }

    	return back()->with('success', 'Wish is removed!');
    }
}
