<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Cart;

use App\Color;

use Cloudder;

use App\CompanySettings;

class CartController extends Controller
{
	private $_setting;

	public function __construct()
	{
		$this->_setting = new CompanySettings();
		$this->middleware(['auth', 'wizard']);
	}

	/*
	|-----------------------------------------------------------
	|
	|-----------------------------------------------------------
	*/

    public function index()
    {
    	// dd(Cart::content());
    	return view('pre-login.pages.cart');
    }

    /*
	|-----------------------------------------------------------
	|
	|-----------------------------------------------------------
	*/

	public function storePackage(Request $request) {
		$package_id = $request->package_id;
		$package_name = $request->package_name;
		$package_price = $request->package_price;
		$package_quantity = $request->package_quantity;
		$package_availability = $request->package_availability - $package_quantity;

		$this->validate(request(), [
			'package_quantity' => 'required'
		]);
		
		$duplicates = Cart::search(function($cartItem, $rowId) use ($request) {
			($cartItem->id == $request->product_id && $cartItem->associatedModel == "App\SpecialPackage") ? true : false;
		});

		if($duplicates->isNotEmpty()) {
			return response()->json(['status'=>0, 'message'=>'Item is already in your cart!'], 200);
		} else {
			if($package_availability > 0) {
				if($package_quantity > 0) {
					Cart::add(['id'=>$package_id, 'name'=>$package_name, 'qty'=>$package_quantity, 'price'=>$package_price])->associate('App\SpecialPackage');	
					return response()->json(['status' => 1, 'package_name' => $package_name], 200);
				} else {
					return response()->json(['status'=>0, 'message'=>'Quantity should be greater than 0'], 200);
				}
			} else {
				return response()->json(['status'=>0, 'message'=>'Sorry! Package is not available!'], 200);
			}
		}
	}

    public function store(Request $request)
    {

		$product_id = $request->product_id;
		$product_name = $request->product_name;
		$product_price = $request->product_price;
		$product_quantity = $request->product_quantity;
		$product_color = $request->product_color;
		$product_size = $request->product_size;
		$product_availability = $request->product_availability - $product_quantity;


		if(!empty($product_size))
		{
			$this->validate(request(), [

				'product_size' => 'required',
				'product_color' => 'required',
				'product_quantity' => 'required'

			]);
		}
		else
		{

			$this->validate(request(), [

				'product_color' => 'required',
				'product_quantity' => 'required'

			]);

		}

		$duplicates = Cart::search(function($cartItem, $rowId) use ($request) {
			($cartItem->id == $request->product_id && $cartItem->associatedModel == "App\Product") ? true : false;
		});

		if($duplicates->isNotEmpty()) {
			return response()->json(['status'=>0, 'message'=>'Item is already in your cart!'], 200);
		} else {
			if($product_availability > 0) {
				if($product_quantity > 0) {
					Cart::add(['id'=>$product_id, 'name'=>$product_name, 'qty'=>$product_quantity, 'price'=>$product_price, 'options'=>['size'=>$product_size, 'color'=>$product_color]])->associate('App\Product');	
					return response()->json(['status' => 1, 'product_name' => $product_name], 200);
				} else {
					return response()->json(['status'=>0, 'message'=>'Quantity should be greater than 0'], 200);
				}
			} else {
				return response()->json(['status'=>0, 'message'=>'Sorry! Product is not available!'], 200);
			}
		}
    }


    /*
	|-----------------------------------------------------------
	|
	|-----------------------------------------------------------
	*/


    public function get_cart_list()
    {
    	$output = '';

    	$total_price = '';

		if (Cart::instance('default')->count() > 0) {
			
			foreach (Cart::content() as $item) {

				if($item->associatedModel == "App\Product") {

					$output .= '<div class="product product-widget">
	
							<div class="product-thumb">
	
								<img src="'.$item->model->productImage()->first()->image_public_id.'" alt="'.$item->model->productImage()->first()->image_public_id.'">
	
							</div>
	
							<div class="product-body">
	
								<h3 class="product-price">'.$item->model->current_price.' <span class="qty">x'.$item->qty.'</span></h3>
	
								<h2 class="product-name"><a href="/'.$item->model->product_name.'/detail/'.$item->model->id.'">'.$item->model->product_name.'</a></h2>
	
							</div>
	
							<input type="hidden" id="rowId'.$item->model->id.'" value="'.$item->rowId.'">
	
						</div>';

				} elseif($item->associatedModel == "App\SpecialPackage") {

					$output .= '<div class="product product-widget">
	
							<div class="product-thumb">
	
								<img src="'.$item->model->images->first()->image_public_id.'" alt="'.$item->model->images->first()->image_public_id.'">
	
							</div>
	
							<div class="product-body">
	
								<h3 class="product-price">'.$item->model->discount_price.' <span class="qty">x'.$item->qty.'</span></h3>
	
								<h2 class="product-name"><a href="/package/'.$item->model->package_name.'/detail/'.$item->model->id.'">'.$item->model->package_name.'</a></h2>
	
							</div>
	
							<input type="hidden" id="rowId'.$item->model->id.'" value="'.$item->rowId.'">
	
						</div>';

				}

			
			
			}

			$total_price = Cart::subtotal();



		}

		else
		{
			$total_price = "Empty";
		}

		$company_discount = 0;

		$total_price_main = $total_price;

		$delivery_price_per_km = $this->_setting->first()->delivery_per_km;

		if(Cart::instance('default')->count() >= $this->_setting->first()->products_amount_for_discount)
		{
			 $total_price_main = $total_price - ($this->_setting->first()->discount / 100) * intval($total_price);
			 $company_discount = ($this->_setting->first()->discount / 100) * intval($total_price);
		}

		return response()->json(['status' => 1, 'output' => $output, 'total_price' => explode('.00', $total_price), 'cart_count'=>Cart::instance('default')->count(), 'discount'=>$company_discount, 'total_price_main'=>$total_price_main, 'delivery_price_per_km' => $delivery_price_per_km, 'online_payment_limit' => $this->_setting->first()->online_payment_limit, 'delivery_price_standard'=>$this->_setting->first()->symmart_standard_price, 'delivery_price_express'=>$this->_setting->first()->symmart_express_price, 'delivery_price_premium'=>$this->_setting->first()->symmart_premium_price, 'delivery_price_national'=>$this->_setting->first()->symmart_national_price], 200);
    }


    /*
	|-----------------------------------------------------------
	|
	|-----------------------------------------------------------
	*/


    public function update(Request $request)
    {
    	$qty = $request->qty;
    	$rowId = $request->rowId;
    	Cart::update($rowId, $qty);
    	return response()->json(['status' => 1], 200);
    }


    /*
	|-----------------------------------------------------------
	|
	|-----------------------------------------------------------
	*/


    public function empty()
    {
    	Cart::destroy();

    	return back()->with('success', 'Cart is emptied!');
    }


    /*
	|-----------------------------------------------------------
	|
	|-----------------------------------------------------------
	*/


    public function  removeFromCart(Request $request)
    {
    	$rowId = $request->rowId;

    	Cart::remove($rowId);

    	return response()->json(['status' => 1, 'message' => 'item is successfully removed'], 200);
    }
}
