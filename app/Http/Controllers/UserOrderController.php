<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Product;
use App\User;
use App\Debt;
use App\CompanySettings;
use App\MainOrder;
use App\DeliveryP;

class UserOrderController extends Controller
{
    private $_order, $_product, $_user, $_debt, $_settings, $_mainOrder, $_deliveryP;

	public function __construct()
	{
		$this->middleware(['auth', 'wizard']);
        $this->middleware('seller')->except('indexBasedOnUserAdmin','showOrderToCustomer', 'cancelOrder', 'delivery_confirmation_from_customer', 'indexBasedOnTimestamp', 'indexOrder', 'vendor_delivery_confirmation','updateVendorDeliveryConfirmation', 'showAdmin', 'detailOrderToCustomer', 'showAdminDetail', 'cancelAllOrder', 'printView');

        $this->_order = new Order();
        $this->_product = new Product();
        $this->_user = new User();
        $this->_debt = new Debt();
        $this->_settings = new CompanySettings();
        $this->_mainOrder = new MainOrder();
        $this->_deliveryP = new DeliveryP();
	}

    // public function index()
    // {
    //     $current_user = auth()->user()->id;
    //     $orders = [];
    //     $users_products = $this->_product::where('user_id', $current_user)->get();

    //     foreach ($users_products as $products) {
    //         array_push($orders, $products->order()->orderBy('created_at', 'desc')->get());
    //     }        
    //     return view('post-login.pages.product.order.index', compact('orders'));
    // }

    public function indexOrder($user_id, $timestamp)
    {
        $orders = $this->_mainOrder->getOrder($user_id, $this->_order->convertDate($timestamp), auth()->user()->role);
        $settings = $this->_settings->get();
        $customer = $this->_user::with(['addresses'])->find($user_id);
        $delivery_personnel = $this->_deliveryP::all();
        // dd($orders);
        $status_array = [];
        foreach ($orders as $order) {
            foreach ($order->order as $key => $value) {
                array_push($status_array, $value->payment_status);
            }
        }
        return view('post-login.pages.product.order.index', compact('orders', 'settings', 'customer', 'status_array', 'delivery_personnel'));
    }

    public function getOrderForVendor() {
        $orders = $this->_order::with(['mainOrderId', 'product', 'package', 'color'])->where('vendor_id', \Auth::id())->get();
        return view('post-login.pages.product.order.vendorOrder', compact('orders'));
    }

    public function printView($id) {
        $orderInfo = $this->_mainOrder::with(['order', 'order.vendor', 'order.product', 'order.vendor.addresses', 'user'])->find($id);
        return view('post-login.partials.printView', compact('orderInfo'));
    }

    public function indexBasedOnUser()
    {
        $users = [];
        $timeline = [];
        foreach($this->_mainOrder->getUserInOrder(2) as $key => $value) {
           array_push($users, $this->_user->getUser($key));
           array_push($timeline, $this->_mainOrder->getOrderCountForUserTimeline($key, auth()->user()->role));
        }
        return view('post-login.pages.product.order.indexBasedOnUser', compact('users', 'timeline'));
    }

    public function indexBasedOnUserAdmin()
    {
        $users = [];
        $timeline = [];
        foreach($this->_mainOrder->getUserInOrder(1) as $key => $value) {
           array_push($users, $this->_user->getUser($key));
           array_push($timeline, $this->_mainOrder->getOrderCountForUserTimeline($key, auth()->user()->role));
        }

        return view('post-login.pages.product.order.indexBasedOnUser', compact('users', 'timeline'));
    }

    public function indexBasedOnTimestamp($user_id)
    {
        $timestamp = [];

        foreach($this->_mainOrder->getUserDate($user_id, auth()->user()->role) as $key => $value) {
            array_push($timestamp, $key);
        }
        return view('post-login.pages.product.order.indexBasedOnTimestamp', compact('timestamp', 'user_id'));
    }

    public function updateVendorDeliveryConfirmation($user_id, $timestamp)
    {
        $this->_mainOrder->groupVendorAdminDeliveryConfirmation($user_id, $timestamp);
        return back()->with('success', "Delivery status is updated!");
    }

    public function show($id) {
        $orders = $this->_order->getTheOrder($id);
        return view('post-login.pages.product.order.mainIndex', compact('orders'));
    }

    public function showAdmin($id)
    {
        $orders = $this->_order->getTheOrder($id);
        $settings = $this->_settings->get();
        for ($i=0; $i < $orders->count(); $i++) { 
            $customer = $orders[$i]->mainOrderId->user->addresses;
        }
        return view('post-login.pages.product.order.mainIndex', compact('orders', 'settings', 'customer'));
    }

    public function showDetail($id)
    {
        $order = $this->_order::with(['mainOrderId.user', 'mainOrderId', 'product.productImage', 'product', 'product.category', 'package', 'package.images'])->find($id);
        $current_user = auth()->user();

        if(!is_null($order))
        {
            $customer = $order->mainOrderId->user;
            if(!is_null($order->product_id)) {
                if($order->product->user_id == $current_user->id)
                {
            	   return view('post-login.pages.product.order.show', compact('order', 'customer'));
                }
            } else {
                if($order->package->user_id == $current_user->id)
                {
                   return view('post-login.pages.product.order.show', compact('order', 'customer'));
                }
            }
        }

        return back();
    }

    public function showAdminDetail($id)
    {
        $order = $this->_order::with(['mainOrderId.user', 'mainOrderId.user.addresses', 'mainOrderId', 'product.productImage', 'product', 'product.category', 'package', 'package.images'])->find($id);
        if(!is_null($order))
        {
            $customer = $order->mainOrderId->user;
            $settings = $this->_settings->get();
            return view('post-login.pages.product.order.show', compact('order', 'customer', 'settings'));
        }
        return back();
    }

    public function showOrderToCustomer(Request $request)
    {
        $orders = $this->_mainOrder::with(['order'])->where('user_id', auth()->user()->id)->latest()->get();
        $status_array = [];
        foreach ($orders as $order) {
            foreach ($order->order as $key => $value) {
                array_push($status_array, $value->payment_status);
            }
        }

        // dd($orders);

        if ($request->wantsJson()) {
            return response()->json(['orders', $orders], 200);
        }

        return view('pre-login.pages.my.order.orders', compact('orders', 'status_array'));
    }

    public function detailOrderToCustomer(Request $request, $id) {
        $orders = $this->_order::with(['vendor', 'product', 'product.user', 'package', 'package.user', 'mainOrderId'])->where('main_order_id', $id)->latest()->get();

        if ($request->wantsJson()) {
            return response()->json(['orders', $orders], 200);
        }

        return view('pre-login.pages.my.order.show-order', compact('orders'));
    }

    public function cancelOrder($order_id, Request $request)
    {
        $this->_order->destroyOrder($order_id);
        if($request->wantsJson()) {
            return response()->json(['data' => 'Order is successfully canceled!'], 200);
        }
        return back()->with('success', 'Order is successfully canceled!');
    }

    // public function vendor_delivery_confirmation(Request $request)
    // {

    //     if($this->_user->isConnected() == true)
    //     {
    //         $status = $request->delivery_state;
    //         $id = $request->id;

    //         $this->_order->vendorDeliveryConfirmation($id, $status);
    //         $this->_order->updatePaymentStatus(1, $id);

    //         return response()->json(['status' => 1, 'message' => 'Delivery status is confirmed']);
    //     }
    //     else
    //     {
    //         return response()->json(['status' => 0, 'message' => 'Internet connection is required']);
    //     }

    // }

    public function delivery_confirmation_from_customer($id, Request $request)
    {
        if($this->_user->isConnected() == false)
        {
            $this->validate(request(), [
                'delivery_status' => 'required'
            ]);

            $status = $request->delivery_status;
            $this->_mainOrder->customerDeliveryConfirmation($id, $status);

            if($request->wantsJson())  {
                return response()->json(['status'=>'success', 'message'=>'Delivery status is confirmed'], 200);
            }

            return back()->with('success', 'Delivery status is confirmed');
        }
        else
        {
            if($request->wantsJson()) {
                return response()->json(['status'=>'error', 'message'=>'Internet connection is required!'], 200);
            }

            return back()->withErrors('Internet connection is required!');
        }
    }

    public function cancelAllOrder($id) {
        $this->_mainOrder->destroyOrder($id);
        return back()->with('success', 'Order is successfully cancelled!');
    }
}
