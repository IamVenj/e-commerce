<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanySettings;

class ShippingGuideController extends Controller
{
	private $_settings;
    public function __construct() {
    	$this->middleware(['auth', 'admin'])->except(['indexCustomer']);
    	$this->_settings = new CompanySettings();
    }

    public function index() {
        $guide = $this->_settings::first()->shipping_guide;
    	return view('post-login.pages.custom-pages.shipping.guide.index', compact('guide'));
    }

    public function indexCustomer() {
        $guide = $this->_settings::first()->shipping_guide;
        return view('pre-login.pages.shipping-guide', compact('guide'));
    }

    public function update(Request $request) {
    	$this->validate(request(), [
    		'guide' => 'required',
            'shipping_guide_pdf' => 'mimes:pdf'
    	]);
    	$this->_settings->updateGuide($request->guide, $request->shipping_guide_pdf);
    	return back()->with('success', 'Guide is successfully updated!');
    }
}
