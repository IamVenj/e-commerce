<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanySettings;

class TermsController extends Controller
{
    private $_settings;
    public function __construct() {
    	$this->middleware(['auth', 'admin'])->except(['indexCustomer', 'indexVendor']);
    	$this->_settings = new CompanySettings();
    }

    public function index() {
        $terms_and_condition = $this->_settings::first()->terms_and_condition;
    	return view('post-login.pages.custom-pages.terms.index', compact('terms_and_condition'));
    }

    public function indexCustomer() {
        $terms_and_condition = $this->_settings::first()->terms_and_condition;
        return view('pre-login.pages.terms-and-condition', compact('terms_and_condition'));
    }

    public function indexVendor() {
        return view('pre-login.pages.vendor-terms-and-condition');
    }

    public function update(Request $request) {
    	$this->validate(request(), [
    		'terms_and_condition' => 'required',
            'terms_and_condition_pdf' => 'mimes:pdf'
    	]);

    	$this->_settings->updateTerms($request->terms_and_condition, $request->terms_and_condition_pdf);
    	return back()->with('success', 'Terms & condition is successfully updated!');
    }
}
