<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Product;

use App\SpecialOffers;

use Cloudder;

class SpecialOfferController extends Controller
{

	private $_user, $_product, $_offer;

	public function __construct()
	{
		$this->_user = new User();

		$this->_product = new Product();

		$this->_offer = new SpecialOffers();

		$this->middleware(['auth', 'admin']);
	}

   	public function index()
    	{
    		$vendors = $this->_user->getVendors();

    		$offers = $this->_offer->getLatestOffers();

    		return view('post-login.pages.special-offers.index', compact('vendors', 'offers'));
    	}

    	public function getVendorProducts(Request $request)
    	{
    		$vendorProducts = $this->_product->getProductsOfThisUser($request->id);

    		$output = '<div class="form-group">

  				<label>Product</label>

        			<select class="form-control form-control-lg" id="exampleFormControlSelect1" name="product">

          				<option selected disabled>Select Product</option>';

    		foreach ($vendorProducts as $product) 
    		{
			$output .= '<option value="'.$product->id.'">'.$product->product_name.'<img src='.$product->image_public_id.'></option>';        			
    		}

    		$output .= '</select>

    			</div>

			<button type="submit" class="btn btn-primary mr-2">Submit</button>';

    		return response()->json(['output'=>$output, 'status'=>1], 200);
    	}

    	public function store()
    	{
    		$this->validate(request(), [

    			'product' => 'required'

    		]);

    		$this->_offer->addAsOffer(request('product'));

    		return back()->with('success', 'Offer is created!');
    	}

    	public function destroy($id)
    	{
    		$this->_offer->destroyOffer($id);

    		return back()->with('success', 'Offer is successfully destroyed!');
    	}
}
