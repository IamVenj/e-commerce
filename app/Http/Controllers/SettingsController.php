<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CompanySettings;

class SettingsController extends Controller
{
	private $_setting;

    public function __construct()
    {
    	$this->middleware(['auth', 'admin']);

    	$this->_setting = new CompanySettings();
    }

    public function index()
    {
        $setting = $this->_setting::first();

    	return view('post-login.pages.settings.index', compact('setting'));
    }

    public function store()
    {
    	/*
    	/--------------------------------------------
    	/ variables sent from form ---> request() -->
    	/--------------------------------------------
    	*/

    	$location = request('location');
    	$slug = request('slug');
    	$company_name = request('company_name');
    	$email = request('email');
    	$phone_number = request('phone_number');
    	$facebook = request('facebook');
    	$twitter = request('twitter');
    	$google_plus = request('google_plus');
    	$instagram = request('instagram');
    	$logo = request('logo');
    	$favicon = request('favicon');
        $products_amount_for_discount = request('products_amount_for_discount');
        $discount = request('discount');
        $delivery_per_km = request('delivery_per_km');
        $map_address = request('map_address');
        $lat = request('lat');
        $lng = request('lng');
        $cut_individual = request('cut_individual');
        $cut_business = request('cut_business');
        $credit_hint = request('credit_hint');
        $online_payment_limit = request('online_payment_limit');
        $symmart_express_price = request('symmart_express_price');
        $symmart_standard_price = request('symmart_standard_price');
        $symmart_premium_price = request('symmart_premium_price');
        $symmart_national_price = request('symmart_national_price');

        if($discount > $cut_individual || $discount > $cut_business)
        {
            return back()->withErrors('The discount % cannot be greater than the amount your going to charge! -- Please Decrease the percentage discount or the individual or business cut!');
        }


    	/*
    	/------------------------
    	/ validating the form
    	/------------------------
    	*/

    	if(!is_null($logo) && !is_null($favicon))
    	{

	    	$this->validate(request(),[

	    		'location' => 'required',
	    		'slug' => 'required',
	    		'email' => 'required',
	    		'phone_number' => 'required',
	    		'company_name' => 'required',
	    		'logo' => 'required|mimes:png,svg|max:7000',
                'cut_business' => 'required',
                'cut_individual' => 'required',
	    		'favicon' => 'required|mimes:png,svg|max:7000'

	    	]);

    	}

    	elseif(!is_null($favicon) && is_null($logo))
    	{

    		$this->validate(request(),[

	    		'location' => 'required',
	    		'slug' => 'required',
	    		'email' => 'required',
	    		'phone_number' => 'required',
	    		'company_name' => 'required',
	    		'favicon' => 'required|mimes:png,svg|max:7000',
                'cut_business' => 'required',
                'cut_individual' => 'required',

	    	]);

    	}

    	elseif(is_null($favicon) && !is_null($logo))
    	{

    		$this->validate(request(),[

	    		'location' => 'required',
	    		'slug' => 'required',
	    		'email' => 'required',
	    		'phone_number' => 'required',
	    		'company_name' => 'required',
	    		'logo' => 'required|mimes:png,svg|max:7000',
                'cut_business' => 'required',
                'cut_individual' => 'required',

	    	]);

    	}

    	else
    	{

    		$this->validate(request(),[

	    		'location' => 'required',
	    		'slug' => 'required',
	    		'email' => 'required',
	    		'company_name' => 'required',
	    		'phone_number' => 'required',
                'cut_business' => 'required',
                'cut_individual' => 'required',

	    	]);

    	}

    	/*
    	/--------------------
    	/ Updating Settings
    	/--------------------
    	*/

    	$this->_setting->update_settings($company_name, $slug, $location, $phone_number, $email, $facebook, $twitter, $google_plus, $instagram, $logo, $favicon, $products_amount_for_discount, $discount, $delivery_per_km, $map_address, $lat, $lng, $cut_individual, $cut_business, $credit_hint, $online_payment_limit, $symmart_express_price, $symmart_standard_price, $symmart_premium_price, $symmart_national_price);

    	return back()->with('success', 'Setting is successfully updated');
    }
}
