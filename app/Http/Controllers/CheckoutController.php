<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CheckoutController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth')->except('index');

		$this->middleware('wizard');
	}

    public function index()
    {
    	return view('pre-login.pages.checkout');
    }
}
