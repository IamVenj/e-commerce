<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DeliveryP;
use App\MainOrder;

class DeliveryPersonnel extends Controller
{
	private $_deliveryP, $_mainOrder;
	public function __construct() {
		$this->_deliveryP = new DeliveryP();
        $this->_mainOrder = new MainOrder();
		$this->middleware(['auth', 'admin']);
	}
    public function index() {
    	$delivery_personnel = $this->_deliveryP::all();
    	return view('post-login.pages.deliverers.index', compact('delivery_personnel'));
    }

    public function store(Request $request) {
    	$this->validate(request(), [
    		'name' => 'required',
    		'phone_number' => 'required'
    	]);

    	$this->_deliveryP->createDeliverer($request->name, $request->phone_number);
    	return back()->with('success', 'Delivery Personnel is successfully created!');
    }

    public function assignPersonnel($id, Request $request) {
        $this->validate(request(), [
            'del_personnel' => 'required'
        ]);

        $this->_mainOrder->assignDeliveryPersonnel($id, $request->del_personnel);
        return back()->with('success', 'Personnel is successfully assigned');
    }

    public function update($id, Request $request) {
    	$this->validate(request(), [
    		'name' => 'required',
    		'phone_number' => 'required'
    	]);

    	$this->_deliveryP->updateDeliverer($id, $request->name, $request->phone_number);
    	return back()->with('success', 'Delivery Personnel is successfully updated!');
    }

    public function destroy($id) {
    	$this->_deliveryP->destroyDeliverer($id);
    	return back()->with('success', 'Delivery Personnel is successfully deleted!');
    }
}
