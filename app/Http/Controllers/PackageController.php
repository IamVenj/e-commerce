<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SpecialPackage;
use App\Product;
use App\User;
use App\ProductImage;
use App\PackageProduct;

class PackageController extends Controller
{
	private $_package, $_product, $_user, $_image, $_packageProduct;
    public function __construct()
    {
    	$this->middleware(['auth', 'admin'])->except('showCustomer');
    	$this->_package = new SpecialPackage();
    	$this->_product = new Product();
        $this->_user = new User();
        $this->_image = new ProductImage();
        $this->_packageProduct = new PackageProduct();
    }

    public function index()
    {
    	$packages = $this->_package::with(['packageProduct', 'images'])->latest()->paginate(9);
    	return view('post-login.pages.special-package.index', compact('packages'));
    }

    public function indexVendor() {
        $productUser = $this->_product::all()->groupBy('user_id');
        $users = [];
        foreach ($productUser as $key => $value) {
            $user = $this->_user::find($key);
            array_push($users, $user);
        }
        return view('post-login.pages.special-package.indexVendor', compact('users'));
    }

    public function create($shopname,$userId)
    {
    	$products = $this->_product::where('user_id', $userId)->where('availability', '>', 0)->get();
    	return view('post-login.pages.special-package.create', compact('products', 'userId'));
    }

    public function store(Request $request)
    {
    	$this->validate(request(), [

    		'product' => 'required|array',
            'image'=>'required|array', 'image.*'=>'required|mimes:jpeg,png|max:6000',
    		'package_name' => 'required',
    		'slug' => 'required',
    		'availability' => 'required',
            'discountPercent' => 'required'

    	]);

    	$images = $request->file('image');

    	$this->_package->createPackage($request->product, $request->package_name, $images, $request->slug, $request->availability, $request->discountPercent, $request->userID);
    	return redirect('/package')->with('success', 'Package is successfully created!');
    }

    public function showCustomer($package_name, $package_id, Request $request)
    {
        $package = $this->_package::with(['user', 'packageProduct', 'images'])->find($package_id);
        $related_packages = $this->_package::where('id', '!=', $package_id)->orderByRaw('package_name', '%'.strtolower($package_name).'%')->inRandomOrder()->take(6)->get();
        // $this->_product->visits($product_id);
        return view('pre-login.pages.package-detail', compact('package', 'related_packages', 'package_name'));
    }

    /*
    |-----------------------------------------------------------
    | IMAGES
    |-----------------------------------------------------------
    */

    public function showPackageImages($packageName, $packageId) {
        $package = $this->_package::with(['images'])->find($packageId);
        $productImages = $package->images;
        return view('post-login.pages.special-package.show-images', compact('packageName', 'productImages', 'packageId'));
    }

    public function destroyPackageImage($imageId) {
        $this->_image->destroySelectedImage($imageId);
        return back()->with('success', 'successfully deleted!');
    }

    public function updatePackageImage($imageId, $packageId, Request $request) {
        $this->validate(request(), [
            'replaced_image' => 'required|mimes:png,jpeg|max:6000'
        ]);
        $this->_package->updateImageOfPackage($request->replaced_image, $imageId, $packageId);
        return back()->with('success', 'Image is successfully updated!');
    }

    public function addPackageImage($packageName, $packageId, Request $request) {
        $this->validate(request(), [
            'image' => 'required|mimes:png,jpeg|max:6000'
        ]);
        $this->_package->addImageForPackage($request->image, $packageId);
        return back()->with('success',' Image is successfully added!');
    }

    /*
    |-------------------------------------------------------------------
    | END IMAGES
    |-------------------------------------------------------------------
    */

    /*
    |-----------------------------------------------------------
    | PRODUCTS
    |-----------------------------------------------------------
    */

    public function indexProduct($package_name, $packageId) {
        $packageProducts = $this->_packageProduct::with(['product'])->where('package_id', $packageId)->paginate(9);
        $pg = $this->_package::with(['packageProduct', 'user'])->find($packageId);
        return view('post-login.pages.special-package.product', compact('packageProducts', 'packageId', 'pg'));
    }

    public function removeProduct($packageId, $packageProductId, Request $request) {
        $this->_packageProduct->removeProductFromPackage($packageProductId);
        $package = $this->_package->getPackage($packageId);
        $price_array = [];
        for ($i=0; $i < $package->packageProduct->count(); $i++) { 
            $productPrice2 = $this->_product::find($package->packageProduct[$i]->product->id)->current_price;
            array_push($price_array, $productPrice2);
        }
        $discountPrice = array_sum($price_array) - array_sum($price_array) * ($package->discount_percent / 100);
        $this->_package->updateDiscountPrice($discountPrice, $packageId);
        return back()->with('success', 'Product is successfully removed!');
    }

    public function addProduct($packageId, Request $request) {
        $this->validate(request(), [
            'product' => 'required|array'
        ]);
        $product = $request->product;
        $price_array = [];
        for ($i=0; $i < count($product); $i++) {
            $this->_packageProduct->createPackageRelation($packageId, $product[$i]);
        }
        $package = $this->_package->getPackage($packageId);
        for ($i=0; $i < $package->packageProduct->count(); $i++) { 
            $productPrice2 = $this->_product::find($package->packageProduct[$i]->product->id)->current_price;
            array_push($price_array, $productPrice2);
        }
        $discountPrice = array_sum($price_array) - (array_sum($price_array) * ($package->discount_percent / 100));
        $this->_package->updateDiscountPrice($discountPrice, $packageId);
        return back()->with('success', 'Product is successfully added!');
    }

    /*
    |-------------------------------------------------------------------
    | END PRODUCTS
    |-------------------------------------------------------------------
    */

    public function update(Request $request, $id)
    {
    	$this->validate(request(), [

    		'package_name' => 'required',
    		'discountPercent' => 'required',
    		'slug' => 'required',
    		'availability' => 'required'

    	]);
    	$this->_package->updatePackage($request->package_name, $request->discountPercent, $request->slug, $request->availability, $id, $request->sum);
    	return back()->with('success', 'Package is successfully updated!');
    }

    public function destroy($id) {
    	$this->_package->destroyPackage($id);
    	return back()->with('success', 'Package is successfully deleted!');
    }
}
