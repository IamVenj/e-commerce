<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanySettings;

class ShippingReturnController extends Controller
{
    private $_settings;
    public function __construct() {
    	$this->middleware(['auth', 'admin'])->except(['indexCustomer']);
    	$this->_settings = new CompanySettings();
    }

    public function index() {
        $shipping_return = $this->_settings::first()->shipping_return;
    	return view('post-login.pages.custom-pages.shipping.return.index', compact('shipping_return'));
    }

    public function indexCustomer() {
        $shipping_return = $this->_settings::first()->shipping_return;
        return view('pre-login.pages.shipping-return', compact('shipping_return'));
    }

    public function update(Request $request) {
    	$this->validate(request(), [
    		'ship_return' => 'required',
            'shipping_and_return_pdf' => 'mimes:pdf'
    	]);

    	$this->_settings->updateReturn($request->ship_return, $request->shipping_and_return_pdf);
    	return back()->with('success', 'Shipping & return is successfully updated!');
    }
}
