<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

use App\User;

use App\Color;

use Cloudder;

class SearchController extends Controller
{

	private $_product, $_color, $_user;


	public function __construct()
	{
		$this->_product = new Product();

		$this->_color = new Color();

		$this->_user = new User();
	}

    public function index()
    {
    	return view('pre-login.pages.search-result');
    }

    public function autoComplete()
    {
    	$query = request('query');

    	$category = request('category');

    	if($category == 0)
    	{
    		$results = $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')->get();
    	}
    	else
    	{
    		$results = $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')->where('category_id', $category)->get();
    	}

    	$data = array();

    	foreach ($results as $result) 
    	{
    		$data[] = $result->product_name;
    	}

    	return json_encode($data);

    }

    public function search()
    {
    	$query = request('query');

    	$category = request('category');


    	if($category == 0)
    	{
    		$products = $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')->orderBy('created_at', 'desc')->get();
    	}
    	else
    	{
    		$products = $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')->where('category_id', $category)->orderBy('created_at', 'desc')->get();
    	}


    	$all_colors = $this->_color::all();

    	$vendors = $this->_user::where('role', 2)->whereNotNull('shop_name')->get();


    	return view('pre-login.pages.search-result', compact('products', 'all_colors', 'vendors', 'query', 'category'));

    }


    public function searchProductsBasedOnCategory(Request $request)
    {


        $minPrice = explode(" Birr", $request->min_price)[0];

        $maxPrice = explode(" Birr", $request->max_price)[0];
        // productsize
        $multiple_size_filter = $request->multiple_size_filter;
        //productColor  
        $multiple_colors_filters = $request->multiple_colors_filter;
        // user
        $vendor_filter = $request->vendor_filter;

        $sort_by = $request->sort_by;

        $category_id = $request->category_id;   

        $query = $request->get('query');     


         /*
        |-------------------------------------------------------------------------------
        | Variables for pagination
        |-------------------------------------------------------------------------------
        */

         $records_per_page = $request->records_per_page;

         $page = '';

         

         if($request->page != '')
         {
            $page = $request->page;
         }
         else
         {
            $page = 1;
         }


        /*
        |--------------------------------------------------------------------------------
        */


    
        if($minPrice != "" && $maxPrice != "" && $multiple_size_filter != null && $multiple_colors_filters != null && $vendor_filter != null)
        {
            
            
            if($sort_by == '0')
            {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')
                        
                        ->forPage($page, $records_per_page)

                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }
                else
                {

                $products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }

            }
            elseif($sort_by == '1')
            {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }


            }

            elseif($sort_by == '2')
            {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                }
                else
                {
                    $products =  $this->_product::with(['productColors', 'productSize', 'user'])
                            
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])
                            
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                }


            }
                      
            
        }
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter != null && $multiple_colors_filters != null && $vendor_filter == null)
        {
            
            if($sort_by == '0')
            {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }


            }
            elseif($sort_by == '1')
            {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }


            }
            elseif($sort_by == '2')
            {

                if($category_id == 0)
                {

                $products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
                }
                else
                {
                    

                $products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
                }
            }
        }
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter != null && $multiple_colors_filters == null && $vendor_filter != null)
        {

            if($sort_by == '0')
            {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }

            }


            elseif($sort_by == '1')
            {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();


                    $unlimited_products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }

                
            }
            elseif($sort_by == '2')
            {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                }

                
            }
        }
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters != null && $vendor_filter != null)
        {
            if($sort_by == '0')
            {


                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }

            }
            elseif($sort_by == '1')
            {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }


            }
            elseif($sort_by == '2')
            {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                }


            }
        }
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters != null && $vendor_filter != null)
        {
            if($sort_by == '0'){

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productColors', 'productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }
                else
                {
                $products =  $this->_product::with(['productColors', 'productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                    
                }


            }

            elseif($sort_by == '1')
            {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productColors', 'productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productColors', 'productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }


            }
            elseif($sort_by == '2')
            {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productColors', 'productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current', 'asc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productColors', 'productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current', 'asc')
                        
                        ->get();
                }

                
            }
        }
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters == null && $vendor_filter != null)
        {

            if($sort_by == '0'){

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }


            }
            elseif ($sort_by == '1') {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }
                

            }
            elseif ($sort_by == '2') {

                if($category_id == 0)
                {
                     $products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                }
                

            }
        }
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters != null && $vendor_filter == null)
        {
            if($sort_by == '0')
            {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }

            }
            elseif ($sort_by == '1') {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }

                
            }
            elseif ($sort_by == '2') {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                }

                
            }
        }
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter != null && $multiple_colors_filters == null && $vendor_filter == null)
        {
            if($sort_by == '0'){

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }


            }
            elseif ($sort_by == '1') {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }


            }

            elseif ($sort_by == '2') {

                if($category_id == 0)
                {
                    $products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                }

                
            }
        }
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters == null && $vendor_filter == null)
        {
            if($sort_by == '0'){

                if($category_id == 0)
                {
                    $products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
                }


            }
            elseif ($sort_by == '1') {

                if($category_id == 0)
                {
                    $products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }
                else
                {
                    
                    $products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                }


            }
            elseif ($sort_by == '2') {

                if($category_id == 0)
                {
                    $products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                }
                else
                {
                    
                    $products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                    $unlimited_products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                }


            }

        }
        else
        {
            if($sort_by == '0'){

                if($category_id == 0)
                {
                    $products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')
                                        
                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
        
                        ->get();

                    $unlimited_products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')
                                        
                        ->orderBy('created_at', 'desc')
        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('category_id', $request->category_id)
                                        
                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
        
                        ->get();

                $unlimited_products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('category_id', $request->category_id)
                                        
                        ->orderBy('created_at', 'desc')
        
                        ->get();
                }


            }
            elseif ($sort_by == '1') {

                if($category_id == 0)
                {
                    $products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')
                                        
                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
        
                        ->get();

                    $unlimited_products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')
                                        
                        ->orderBy('current_price', 'desc')
        
                        ->get();
                }
                else
                {
                    
                $products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('category_id', $request->category_id)
                                        
                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
        
                        ->get();

                $unlimited_products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('category_id', $request->category_id)
                                        
                        ->orderBy('current_price', 'desc')

                        ->get();
                }


            }
            elseif ($sort_by == '2') {

                if($category_id == 0)
                {
                    $products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')
                                        
                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
        
                        ->get();

                    $unlimited_products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')
                                        
                        ->orderBy('current_price', 'asc')
        
                        ->get();
                
                }
                else
                {
                    
                $products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('category_id', $request->category_id)
                                        
                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
        
                        ->get();

                $unlimited_products =  $this->_product::where('product_name', 'LIKE', '%'.strtolower($query).'%')

                        ->where('category_id', $request->category_id)
                                        
                        ->orderBy('current_price', 'asc')

                        ->get();
                
                }

            }

        }



        $pagination_output = '';

        $total_pages = ceil($unlimited_products->count()/$records_per_page);

        $pagination_output .= '<ul class="store-pages">
                
                            <li><span class="text-uppercase">Page:</span></li>';

        for ($i=1; $i <= $total_pages; $i++) { 
            
            $pagination_output .= '<li class="pagination_link" style="cursor: pointer; margin-left:7px;" id="'.$i.'">'.$i.'</li>';

        }

        $pagination_output .= '</ul>';



        
        $output = "";


        if(count($products) == 0)
        {
            $output .= '<div class="section section-grey col-md-8 col-md-offset-2 cart" style="padding-top: 100px; padding-bottom: 100px;">
                    
                    <span class="fa fa-shopping-cart text-center" style="font-size: 50px; padding-left: 50%; padding-right: 50%; "></span>  

                    <h3 class="text-center text-white">We couldn\'t find what you were looking for</h3>

                    <p class="text-center text-white">Keep calm and search again</p>

                </div>';
        }


        foreach ($products as $product) 
        {

            if($product->user()->first()->activation == 0) {

                if($product->availability > 0) {
                
                    if($request->view_status == 1)
                    {
                        $output .= '<div class="col-md-3">
              
                            <div class="product product-single loading" style="display: flex;" id="product-single'.$product->id.'">
              
                                <div class="product-thumb" id="product-thumb'.$product->id.'" style="display: none;">';

                                    if ($product->current_price < $product->old_price)
                                    {

              
                        $output .= '<div class="product-label">
                    
                                        <span class="sale"> '.

                                        round((($product->current_price/$product->old_price)*100)-100)

                                        .'%</span>
              
                                    </div>';


                                    }
                                    
              
                        $output .= '<a data-toggle="modal" href="#" data-target="#quick-preview'.$product->id.'"><button class="main-btn quick-view" style="font-size: 10px; letter-spacing: 0.5px;"><i class="fa fa-search-plus"></i> Quick view</button></a>
              
                                    <img style="object-fit: cover; height: 200px; overflow: hidden;" src="'.$product->productImage()->first()->image_public_id.'" alt="'.$product->productImage()->first()->image_public_id.'">
              
                                </div>
              
                                <div class="product-body" id="product-body'.$product->id.'" style="display: none;">
              
                                    <h4 class="product-price ">'.$product->current_price.' Birr<del class="product-old-price" style="margin-left: 10px;">'.$product->old_price.' Birr</del></h4>
              

                                    <h2 class="product-name"><a href="/'.$product->product_name.'/detail/'.$product->id.'">'.str_limit($product->product_name, 20).'</a></h2>
              
                                    <p >Sold by: <span style="font-weight: bold;">'.$product->user()->first()->shop_name.'</span></p>
              
                                    <div class="product-btns">
              
                                        <button class="main-btn icon-btn" type="button" id="wish" onclick="add_wish('.$product->id.')"><i class="fa fa-heart"></i></button>
              
                                        <a class="main-btn icon-btn" href="/'.$product->product_name.'/detail/'.$product->id.'"><i class="fa fa-eye"></i></a>
                                       
                                    </div>
              
                                </div>
              
                            </div>
              
                        </div>';

                    }
                    elseif($request->view_status == 2)
                    {
               
                        $output .= '<div class="col-md-12">
                    
                            <div class="product product-single loading" style="display: flex;" id="product-single'.$product->id.'">
                    
                                <div class="product-thumb" id="product-thumb'.$product->id.'" style="display: none;">';
                    
                                    if ($product->current_price < $product->old_price)
                                    {

              
                        $output .= '<div class="product-label">
                    
                                        <span class="sale"> -'.

                                        round((($product->current_price/$product->old_price)*100)-100)

                                        .'%</span>
              
                                    </div>';


                                    }
                    
                        $output .= '<a data-toggle="modal" href="#" data-target="#quick-preview"><button class="main-btn quick-view" style="font-size: 10px; letter-spacing: 0.5px;"><i class="fa fa-search-plus"></i> Quick view</button></a>
                    
                                    <img style="object-fit: cover; align-items: flex-start; height: 200px; overflow: hidden;" src="'.$product->productImage()->first()->image_public_id.'" alt="'.$product->productImage()->first()->image_public_id.'">
                    
                                </div>
                    
                                <div class="product-body" id="product-body'.$product->id.'" style="display: none;">
                    
                                    <h4 class="product-price ">'.$product->current_price.' Birr<del class="product-old-price" style="margin-left: 10px;">'.$product->old_price.' Birr</del></h4>
                                    
                                    <h2 class="product-name"><a href="/'.$product->product_name.'/detail/'.$product->id.'">'.str_limit($product->product_name, 20).'</a></h2>
                    
                                    <p >Sold by: <span style="font-weight: bold;">'.$product->user()->first()->shop_name.'</span></p>
                    
                                    <div class="product-btns">
                    
                                        <button class="main-btn icon-btn" type="button" id="wish" onclick="add_wish('.$product->id.')"><i class="fa fa-heart"></i></button>
                    
                                        <a class="main-btn icon-btn" href="/'.$product->product_name.'/detail/'.$product->id.'"><i class="fa fa-eye"></i></a>
                                       
                                    </div>
                    
                                </div>
                    
                            </div>
                    
                        </div>';
               
                    }
                }
            }
        }


        return response()->json(['data'=>$output, 'pagination'=>$pagination_output]);
    }
}
