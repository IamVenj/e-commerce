<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Address;
use Cloudinary;
use App\paymentInfo;
use App\Newsletter;
use App\CompanySettings;
use App\Bank;

use Illuminate\Support\Facades\Input;

class WizardController extends Controller
{
	private $_user, $_delivery, $_address, $_bankStatement, $_settings;

	public function __construct()
	{
		$this->middleware(['auth', 'authenticatedWizard']);

		$this->_user = new User();
		$this->_address = new Address();
		$this->_newsletter = new Newsletter();
		// $this->_delivery = new Delivery();
		$this->_bankStatement = new Bank();
		$this->_settings = new CompanySettings();
	}

    public function index()
    {
    	$credit_hint = $this->_settings::first()->credit_hint;
    	return view('post-login.pages.wizard.wizard', compact('credit_hint'));
    }

    public function store(Request $request)
    {

    	if($this->_user->isConnected() == true)
    	{

	    	$newsletter_register = $request->newsletter_register;
	    	$string_address = null;
	    	$array_address = null;
	    	$name = null;
	    	$delivery = 0;
	    	$shop_name = null;
	    	$about_shop = null;
	    	$current_user_id = auth()->user()->id;
	    	$current_user_role = auth()->user()->role;
	    	$current_user_email = auth()->user()->email;
	    	$business_type = $request->business_type;
	    	$tin = null;
	    	$credit = 0;

	    	if(auth()->user()->role == 2)
	    	{

	    		if($business_type == "individual")
	    		{
	    			$this->validate(request(), [

			    		'fullname' => 'required',
			    		'phone_number' => 'required',
			    		'agreement'=>'required',

			    	]);

			    	$name = $request->fullname;

	    		}
	    		elseif($business_type == "business")
	    		{
			    	$this->validate(request(), [

			    		'shop_name' => 'required',
			    		'tin' => 'required',
			    		'shop_slug' => 'required',
			    		'phone_number' => 'required',
			    		'agreement'=>'required',

			    	]);

			    	$shop_name = $request->shop_name;
			    	$tin = $request->tin;
			    	$about_shop = $request->shop_slug;

	    		}

	    		$this->validate(request(), [

		    		'phone_number' => 'required',
		    		'agreement'=>'required',
		    		'bank_name' => 'required',
		    		'account_number' => 'required'

		    	]);

		    	$credit_request = $request->credit;

		    	if($credit_request == "on") {
		    		$credit = 1;
		    	}

	    	}
	    	elseif(auth()->user()->role == 3)
	    	{
		    	$this->validate(request(), [

		    		'fullname' => 'required',
		    		'phone_number' => 'required',
		    		'agreement'=>'required',

		    	]);

		    	$name = $request->fullname;
	    	}


	    	if(is_null(request('address_')))
	    	{
	    		$this->validate(request(), ['address'=>'required|array', 'address.*'=>'required']);

	    		$array_address = $request->address;
	    		$lat = $request->lat;
	    		$lng = $request->lng;

	    	}

	    	if(is_null(request('address')))
	    	{
				$this->validate(request(), ['address_'=>'required']);

	    		$string_address = $request->address_;
	    		$lat = $request->lat;
	    		$lng = $request->lng;
	    	}


	    	// if(request('m-delivery') == 'on')
	    	// {
	    	// 	$this->validate(request(), ["price" => "required"]);

	    	// 	$price = $request->price;
	    	// 	$delivery = 1;
		    // 	$this->_delivery->addDeliveryStatus($price, $delivery, $current_user_id);
	    	// }

	    	if(request('newsletters') == 'on')
	    	{
	    		$newletter_register = 1;
	    	}

	    	$image_public_id = null;
	    	$image_version = null;

	    	if(!is_null($request->file('image')))
	    	{
		    	$image = $request->file('image');
		    	$image_name = $image->getRealPath();
		    	$_image_ = \Cloudinary\Uploader::upload($image_name, array("folder"=>"profile"));
		    	$image_public_id = $_image_['secure_url'];
		    	$image_version = $_image_['version'];
	    	}

	    	$phone_number = $request->phone_number;
	    	$phone_number2 = $request->phone_number2;
	    	$phone_number3 = $request->phone_number3;


	    	/*
	    	| -------------------------------------------------- |
	    	| list($width, $height) = getimagesize($image_name); |
	    	| -------------------------------------------------- |
	    	*/


	    	$this->_address->addAddress($string_address, $array_address, $lat, $lng, $current_user_id);

	    	$this->_user->updateAccount($image_public_id, $phone_number, $name, $shop_name, $about_shop, $current_user_id, $current_user_role, $delivery, $image_version, $credit, $phone_number2, $phone_number3, $tin, $business_type);


	    	$pdt_token = $request->pdt_token;
	    	$merchant_code = $request->merchant_code;

	    	if($current_user_role == 2) {
	    		$this->_bankStatement->add($request->account_number, $request->bank_name, $current_user_id);
	    	}

	    	if ($newsletter_register == 1) {
	    		$this->_newsletter->addUserForNewsLetter($current_user_email);
	    	}

            $this->_user->deactivate();
	    	return response()->json(['status' => 'success', "role" => $current_user_role]);
    	}
    	else
    	{
    		return response()->json(['status'=>'error', "message" => "Internet Connection is required!"]);
    	}

    }

}
