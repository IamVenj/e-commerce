<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

use App\Color;

use App\Category;

use Str;

use Cloudder;

use Cloudinary;

use Carbon\Carbon;

use App\ProductColor;

use App\ProductImage;

use App\User;

use App\ProductSize;

class SellerProductController extends Controller
{

    private $_product_color, $_product_image, $_product, $_user, $_product_size;

    /*
    |--------------------------------------------------------------------------
    | Seller Product Controller Constructor
    |--------------------------------------------------------------------------
    */

	public function __construct()
	{
		$this->middleware(['auth', 'seller', 'wizard']);

        $this->_product_color = new ProductColor();

        $this->_product_image = new ProductImage();

        $this->_product = new Product();

        $this->_user = new User();

        $this->_product_size = new ProductSize();
	}

    /*
    |--------------------------------------------------------------------------
    | PRODUCT VIEW
    |--------------------------------------------------------------------------
    */

    public function create()
    {
        $all_colors = Color::all();

        $categories = Category::where('parent_id', null)->get();

        // $category_collection = [];

        // foreach ($categories as $category) {

        //     $get_previous_category = Category::where('parent_id', $category->parent_id)->get();

        //     array_push($category_collection, $get_previous_category);

        // }


        $addresses = auth()->user()->addresses()->get();

    	return view('post-login.pages.product.create', compact('all_colors', 'categories', 'addresses'));
    }

    public function index()
    {

        $categories = Category::all();

        $addresses = auth()->user()->addresses()->get();

        $products = $this->_product::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->paginate('12');

    	return view('post-login.pages.product.index', compact('products', 'categories', 'addresses'));
    }

    public function show_product_images($product_name, $product_id)
    {

        $product = $this->_product::find($product_id);

        $productImages = $product->productImage()->get();

        return view('post-login.pages.product.show-images', compact('productImages', 'product_name', 'product_id'));
    }

    public function show_product_colors($product_name, $product_id)
    {
        $product = $this->_product::find($product_id);

        $productColors = $product->productColors()->get();

        $all_colors = Color::all();

        return view('post-login.pages.product.show-colors', compact('product_name', 'product_id', 'productColors', 'all_colors'));

    }


    /*
    |--------------------------------------------------------------------------
    | PRODUCT IN GENERAL
    |--------------------------------------------------------------------------
    */


    public function destroy($id)
    {

        $this->_product->destroyProduct($id);

        return back()->with('success', 'Product is successfully removed!');

    }

    public function update($id)
    {

        $this->validate(request(), [

            'product_name' => 'required',

            'slug' => 'required',

            'brand' => 'required',

            'current_price' => 'required',

            'category' => 'required',

            'product_location' => 'required',

            'availability' => 'required',

        ]);

        $product_name = request('product_name');
        
        $slug = request('slug');
        
        $price = request('price');
        
        $brand = request('brand');
        
        $category = request('category');
        
        $product_location = request('product_location');

        $current_price = request('current_price');

        $old_price = request('old_price');

        $availability = request('availability');

        $size = request('product_size');

        (!is_null($size)) ? $this->_product_size->updateProductSize($id, $size) : null;

        $this->_product->updateProduct($product_name, $slug, $brand, $current_price, $category, $product_location, $old_price, $availability, $id);

        return back()->with('success', 'Product is successfully updated!');

    }

    /*
    |--------------------------------------------------------------------------
    | PRODUCT storing or saving IN GENERAL
    |--------------------------------------------------------------------------
    */

    public function store(Request $request)
    {

        if($this->_user->isConnected() == true)
        {

            $this->validate(request(), [

                'product_name' => 'required',

                'slug' => 'required',

                'category' => 'required',

                'brand' => 'required',

                'current_price' => 'required',

                'colors' => 'required',

                'product_location' =>'required',

                'availability' => 'required',

            ]);

            $this->validate(request(), [

                'image'=>'required|array', 'image.*'=>'required|max:6000'

            ]);


            
            $product_name = $request->product_name;

            $slug = $request->slug;

            $category_id = $request->category;

            $brand = $request->brand;

            $price = $request->price;

            $product_location = $request->product_location;

            $current_price = $request->current_price;

            $old_price = $request->old_price;

            $availability = $request->availability;

            $sku = \Str::random(4).date('ysmd', strtotime(now()));
            


            $images = $request->file('image');

            $image_array = array();

            $count = 0;

            if($request->hasFile('image'))
            {
                
                foreach ($images as $image) {

                    $count = $count + 1;

                    $image_name = $image->getRealPath();

                    $image_result = \Cloudinary\Uploader::upload($image_name, array("folder"=>"product"));
                    
                    $image_public_id = $image_result['secure_url'];

                    $image_version = $image_result['version'];

                    array_push($image_array, $image_public_id);

                }

            }

            $product = $this->_product::create([

                'product_name' => $product_name,

                'slug' => $slug,

                'category_id' => $category_id,

                'brand' => $brand,

                'current_price' => $current_price,

                'old_price' => $old_price,

                'product_location' => $product_location,

                'availability' => $availability,

                'user_id' => auth()->user()->id,

                'sku' => $sku

            ]);

            $size = $request->product_size;

            $this->_product_size->addProductSize($product->id, $size);

            
            $colors = $request->colors;

            foreach($colors as $color)
            {
                $this->_product_color->addColorsForaSingleProduct($color, $product->id);   
            }

            foreach ($image_array as $public_id) {

            // $public_id = 'static_id';

            // $image_version = 'static_version';

                $this->_product_image->addProductImageForASingleProduct($product->id, $public_id, $image_version, null);

            }

            return response()->json(['status' => 1, "message"=>'Product is Successfully created'], 200);

        }
        else
        {
            return response()->json(['status' => 0, "message" => "Internet connection is required!"], 200);
        }

    }


    /*
    |--------------------------------------------------------------------------
    | PRODUCT IMAGE
    |--------------------------------------------------------------------------
    */


    public function destroy_selected_image($productImage_id)
    {

        if($this->_user->isConnected() == true)
        {

            $this->_product_image->destroySelectedImage($productImage_id);

            return back()->with('success', "Product image is successfully deleted!");

        }
        else
        {
            return back()->withErrors('Internet Connection is required!');
        }
    
    }


    public function update_selected_image($productImage_id, Request $request)
    {
        if($this->_user->isConnected() == true)
        {

            $this->validate(request(), ['replaced_image'=>'required|mimes:jpeg,png|max:6000']);

            $product_image_publicId = request('product-image-publicId');

            $image = $request->file('replaced_image');

            $product_id = request('product-id');

            $image_name = $image->getRealPath();

            // Cloudder::destroy($product_image_publicId);

            $result = \Cloudinary\Uploader::upload($image_name, array("folder"=>"product"));

            $this->_product_image->updateProductImageForASingleProduct($product_id, $result['secure_url'], $result['version'], null, $productImage_id);

            return back()->with('success', "Image is successfully updated!");
        }
        else
        {
            return back()->withErrors('Internet Connection is required!');
        }
    }



    public function add_image(Request $request)
    {
        if($this->_user->isConnected() == true)
        {

            $this->validate(request(), ['new_image'=>'required|mimes:jpeg,png|max:6000']);

            $product_id = request('product_id');

            $image = $request->file('new_image');
            
            $image_name = $image->getRealPath();

            $result = \Cloudinary\Uploader::upload($image_name, array('folder'=>'product'));

            $this->_product_image->addProductImageForASingleProduct($product_id, $result['secure_url'], $result['version'], null);

            return back()->with('success', 'Image is successfully added!');
        }
        else
        {
            return back()->withErrors('Internet Connection is required!');
        }
    }


    /*
    |--------------------------------------------------------------------------
    | PRODUCT COLOR
    |--------------------------------------------------------------------------
    */

    public function destroy_selected_color($productColor_id)
    {

        $this->_product_color->removeProductColor($productColor_id);

        return back()->with('success', 'Product Color is successfully removed!');

    }


    public function update_selected_color($productColor_id)
    {
        $this->validate(request(), ['color-update'=>'required']);

        $color_id = request('color-update');

        $this->_product_color->updateProductColor($productColor_id, $color_id);

        return back()->with('success', 'Product color is successfully updated!');

    }


    public function add_selected_color()
    {

        $this->validate(request(), ['colors'=>'required|array', 'colors.*'=>'required']);

        $colors = request('colors');

        $product_id = request('product_id');

        foreach ($colors as $color) {
            
            $this->_product_color->addColorsForaSingleProduct($color, $product_id); 

        }

        return back()->with('success', 'color is successfully added');
    }



}
