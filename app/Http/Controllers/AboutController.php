<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanySettings;

class AboutController extends Controller
{
    private $_settings;
    public function __construct() {
    	$this->middleware(['auth', 'admin'])->except(['indexCustomer']);
    	$this->_settings = new CompanySettings();
    }

    public function index() {
    	$about = $this->_settings->first()->about;
    	return view('post-login.pages.custom-pages.about.index', compact('about'));
    }

    public function indexCustomer() {
        $about = $this->_settings->first()->about;
        return view('pre-login.pages.about', compact('about'));
    }

    public function update(Request $request) {
    	$this->validate(request(), [
    		'about' => 'required'
    	]);

    	$this->_settings->updateAbout($request->about);
    	return back()->with('success', 'About is successfully updated!');
    }
}
