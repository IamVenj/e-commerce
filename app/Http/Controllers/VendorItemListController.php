<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\User;

class VendorItemListController extends Controller
{
    public function __construct() {
    	$this->middleware(['auth', 'admin']);
    	$this->_product = new Product();
    	$this->_user = new User();
    }

    public function index() {
    	$usersFromProducts = $this->_product::latest()->get()->groupBy('user_id');
    	$users = [];
    	foreach ($usersFromProducts as $key => $value) { 
    		$vendors = $this->_user::find($key);
    		array_push($users, $vendors);
    	}
    	return view('post-login.pages.vendor-item.index', compact('users'));
    }

    public function showItems($vendorName, $userId) {
    	$products = $this->_product::where('user_id', $userId)->paginate(8);
    	$user = $this->_user::find($userId);
    	return view('post-login.pages.vendor-item.show', compact('products', 'user'));
    }

    public function deleteItem($productId) {
    	$this->_product->destroyProduct($productId);
    	return back()->with('success', 'You have successfully deleted The Item!');
    }
}
