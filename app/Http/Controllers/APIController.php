<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\SpecialOffers;
use App\HomeCarousel;
use App\Product;
use App\Category;
use App\SpecialPackage;
use App\Address;
use Laravel\Passport\Passport;
use App\WishList;
use App\MainOrder;
use App\Order;
use App\Color;
use App\CompanySettings;
use App\Mail\OrderMail;
use App\Debt;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

class APIController extends Controller
{
    private $_user, $_token, $_offer, $_carousel, $_product, $_category, $_package, $_address, $_wishList, $_mainOrder, $_setting, $_debt, $_order;

    use SendsPasswordResetEmails;
    /*
    |---------------------------------------------------------------------
    | Constructor for ApiController Class
    |---------------------------------------------------------------------
    */

    public function __construct()
    {
        $this->_user = new User();
        $this->_token = null;
        $this->_offer = new SpecialOffers();
        $this->_carousel = new HomeCarousel();
        $this->_product = new Product();
        $this->_category = new Category();
        $this->_package = new SpecialPackage();
        $this->_address = new Address();
        $this->_wishList = new WishList();
        $this->_mainOrder = new MainOrder();
        $this->_order = new Order();
        $this->_color = new Color();
        $this->_setting = new CompanySettings();
        $this->_debt = new Debt();
    }


    /*
    |---------------------------------------------------------------------
    | A user registers & gets his/her access token
    |---------------------------------------------------------------------
    */

    public function register(Request $request)
    {
        $this::validate(request(),[
            'fullname' => 'required',
            'email' => 'required|email|unique:users',
            'phoneNumber' => 'required',
            'password' => 'required|confirmed|min:4',
            'address' => 'required'
        ]);

        $name = $request->fullname;
        $password = bcrypt($request->password);
        $email = $request->email;
        $phoneNumber = $request->phoneNumber;
        $role = 3;
        $photoUrl = $request->photoUrl;
        $address = $request->address;

        $created_user = User::create([
            'name' => $name,
            'email' => $email,
            'phone_number' => $phoneNumber,
            'password' => $password,
            'role' => $role,
            'photo_url' => $photoUrl
        ]);

        for ($i=0; $i < count($address); $i++) {
            $this->_address->addAddress(
                $address[$i]["address"],
                null,
                $address[$i]['latitude'],
                $address[$i]['longitude'],
                $created_user->id
            );
        }

        $credentials = ['email' => $request->email, "password" => $request->password];

        if(auth()->attempt($credentials))
        {
            $this->_token = auth()->user()->createToken('ThisIsTheTokenIwantedToCreate')->accessToken;
            $request->headers->set('authorization', 'Bearer ' . $this->_token);
            return response()->json(["token" => $this->_token, "user"=>User::with(['addresses', 'wish', 'wish.product', 'wish.product.category', 'wish.product.productColors', 'wish.product.productColors.colors', 'wish.product.productImage', 'wish.product.productSize', 'wish.product.productSize.product', 'wish.product.Review', 'wish.product.user', 'wish.product.user.addresses', 'wish.product.user.wish', 'wish.product.order'])->find(\Auth::id())], 200);
        }

        return response()->json(["sym_error"=>"Unauthorized"], 401);
    }

    /*
    |---------------------------------------------------------------------
    | A user logs in & gets his/her access token
    |---------------------------------------------------------------------
    */

    public function login(Request $request)
    {
        $credentials = ['email' => $request->email, "password" => $request->password];

        if(auth()->attempt($credentials))
        {
            if(\Auth::user()->role == 3 && \Auth::user()->activation == 0) {
                $this->_token = auth()->user()->createToken('ThisIsTheTokenIwantedToCreate')->accessToken;
                $request->headers->set('authorization', 'Bearer ' . $this->_token);
                return response()->json(["status"=>1, "token" => $this->_token, "user"=>User::with(['addresses', 'wish', 'wish.product', 'wish.product.category', 'wish.product.productColors', 'wish.product.productColors.colors', 'wish.product.productImage', 'wish.product.productSize', 'wish.product.productSize.product', 'wish.product.Review', 'wish.product.user', 'wish.product.user.addresses', 'wish.product.user.wish', 'wish.product.order'])->find(\Auth::id())], 200);
            } else if (\Auth::user()->activation == 1) {
                $this->guard()->logout();
                \Cookie::queue(\Cookie::forget(Passport::cookie()));
                return response()->json(["status"=>0, "error" => "unauthorized", "message"=>"Sorry! Your account is deactivated!"], 200);
            }  else {
                $this->guard()->logout();
                \Cookie::queue(\Cookie::forget(Passport::cookie()));
                return response()->json(["status"=>0, "error" => "unauthorized", "LOGOUT"=>"loggedout"], 401);
            }
        }
        else
        {
            return response()->json(["error" => "unauthorized"], 401);
        }
    }

    protected function guard() {
        return \Auth::guard('web');
    }

    public function destroyUserLogin(Request $request) {
        if(auth()->check()) {
            $token = \Auth::user()->token();
            \DB::table('oauth_refresh_tokens')->where('access_token_id', $token->id)->update(['revoked' => true]);
            $token->revoke();
            $this->guard()->logout();
            \Cookie::queue(\Cookie::forget(Passport::cookie()));
            return response()->json(["success"=>true], 200);
        }
        return response()->json(["success"=>false], 200);
    }

    /*
    |--------------------------------------------------------------------------
    | Home Request
    |--------------------------------------------------------------------------|
    */

    public function checkAuth() {
        return response()->json(['auth'=>true], 200);
    }

    public function homeRequest() {
        $mainArray = [];

        /*
        |--------------------
        | Latest Products
        |---------------------
        */

        $latestP = $this->_product::with(['category', 'productColors', 'productColors.colors', 'productImage', 'productSize', 'productSize.product', 'Review', 'user', 'user.addresses', 'user.products', 'user.wish', 'order'])
            ->whereHas('user', function($query) {
                $query->where('role', 2);
            })->latest()->take(10)->get();

        /*
        |---------------------
        | Recommended Offers
        |---------------------
        */

        $recommendedOffers = $this->_product::with(['category', 'productColors', 'productColors.colors', 'productImage', 'productSize', 'productSize.product', 'Review', 'user', 'user.addresses', 'user.products', 'user.wish', 'order'])->whereHas('user', function($query) { $query->where('credit', 1)->where('role', 2); })->where('availability', '!=', 0)->latest()->get()->take(30)->groupBy('user_id')->map(function($query) { return $query->take(1); });

        $recommendedOffersArray = [];
        foreach ($recommendedOffers as $key => $value) {
            for ($i=0; $i < count($recommendedOffers[$key]); $i++) {
                array_push($recommendedOffersArray, $recommendedOffers[$key][$i]);
            }
        }

        /*
        |---------------------
        | Packages
        |---------------------
        */

       $packages = $this->_package::with(['images', 'packageProduct', 'packageProduct.product', 'user', 'packageProduct.product.category', 'packageProduct.product.productColors', 'packageProduct.product.productColors.colors', 'packageProduct.product.productImage', 'packageProduct.product.productSize', 'packageProduct.product.productSize.product', 'packageProduct.product.Review', 'packageProduct.product.user', 'packageProduct.product.user.addresses', 'packageProduct.product.user.products', 'packageProduct.product.user.wish', 'packageProduct.product.order'])
            ->whereHas('user', function($query) {
                $query->where('role', 2);
            })->latest()->take(30)->get();

       /*
        |---------------------
        | Best Sold
        |---------------------
        */

       $best_products = $this->_product::with(['category', 'productColors', 'productColors.colors', 'productImage', 'productSize', 'productSize.product', 'Review', 'user', 'user.addresses', 'user.products', 'user.wish', 'order'])
            ->whereHas('user', function($query) {
                $query->where('role', 2);
            })->where('orders', '!=', 0)->where('availability', '>', 0)->orderBy('orders', 'desc')->take(10)->get();

        /*
        |-----------------------
        | Carousel & categories
        |-----------------------
        */

        $carousel = $this->_carousel::latest()->with('category')->get();
        $categories = $this->_category::whereNotNull('parent_id')->get();
        $sideCategories = $this->_category::with(['parent'])->whereNull('parent_id')->get();
        $setting = $this->_setting::first();

        array_push($mainArray, ['latestProduct' => $latestP]);
        array_push($mainArray, ['recommendedOffers' => $recommendedOffersArray]);
        array_push($mainArray, ['packages' => $packages]);
        array_push($mainArray, ['bestSold' => $best_products]);
        array_push($mainArray, ['carousel' => $carousel]);
        array_push($mainArray, ['category' => $categories]);
        array_push($mainArray, ['drawCategory' => $sideCategories]);
        array_push($mainArray, ['setting' => $setting]);

        return response()->json(['home' => $mainArray]);
    }

    public function userAccount() {
        $account = User::with(['wish', 'addresses'])->find(\Auth::id());
        return response()->json(['account'=>$account]);
    }

    public function myWishes() {
        $wish = $this->_wishList::with(['product', 'product.category', 'product.productColors', 'product.productColors.colors', 'product.productImage', 'product.productSize', 'product.productSize.product', 'product.Review', 'product.user', 'product.user.addresses', 'product.user.products', 'product.user.wish', 'product.order'])->where('user_id', \Auth::id())->get();
        return response()->json(['wish'=>$wish]);
    }

    public function mainOrder() {
        $order = $this->_mainOrder::with(['user',
                                            'order',
                                            'dPersonnel',
                                            'user.wish',
                                            'user.addresses',
                                            'order.product',
                                            'order.package',
                                            'order.color',
                                            'order.vendor',
                                            'order.package.packageProduct',
                                            'order.package.images',
                                            'order.package.user',
                                            'order.package.packageProduct.product.productImage',
                                            'order.package.user.addresses',
                                            'order.package.user.wish'])->where('user_id', \Auth::id())->latest()->get();
        return response()->json(['order'=>$order]);
    }

    public function subOrders($orderId) {
        $order = $this->_order::with([])->find($orderId);
        return response()->json(['order'=>$order]);
    }

    public function parentCategory() {
        $categories = $this->_category::with(['parent'])->whereNull('parent_id')->get();
        return response()->json(['categories'=>$categories], 200);
    }

    public function showProducts(Request $request) {
        $categoryId = $request->category_Id;
        $minprice = $request->startprice;
        $maxprice = $request->endprice;
        $size = $request->size;
        $colorz = $request->color;
        $vendor = $request->vendor;
        $sort = $request->sort;

        $products =  $this->_product::with(['category', 'productColors', 'productColors.colors', 'productImage', 'productSize', 'productSize.product', 'Review', 'user', 'user.addresses', 'user.products', 'user.wish', 'order'])->where('category_id', $categoryId)->where('availability', '>', 0);

        if ($minprice != null && $maxprice != null) {
            $products = $products->where('current_price', '>=', $minprice)->where('current_price', '<=', $maxprice);
        }
        if (count($size) > 0) {
            $products = $products->whereHas('productSize', function($query) use($size) {$query->whereIn('size', $size);});
        }
        if (count($colorz) > 0) {
            $products = $products->whereHas('productColors', function($query) use($colorz) {$query->whereIn('color_id', $colorz);});
        }
        if (count($vendor) > 0) {
            $products = $products->whereHas('user', function($query) use($vendor) {$query->whereIn('id', $vendor);});
        }
        if ($sort == "Latest") {
            $products = $products->latest();
        } elseif ($sort == "Price: High - Low") {
            $products = $products->orderBy('current_price', 'desc');
        } elseif ($sort == "Price: Low - High") {
            $products = $products->orderBy('current_price', 'asc');
        }

        $products = $products->paginate(12);

        $color = $this->_color::all();
        $vendors = $this->_user::where('role', 2)->whereNotNull('shop_name')->get();
        return response()->json(['products'=>$products, 'colors'=>$color, 'vendors'=>$vendors], 200);
    }

    public function showSelectedProduct($productId) {
        $products = $this->_product::with(['category', 'productColors', 'productColors.colors', 'productImage', 'productSize', 'productSize.product', 'Review', 'user', 'user.addresses', 'user.products', 'user.wish', 'order'])->find($productId);

        return response()->json(['product'=>$products], 200);
    }

    public function pay(Request $request)
    {
        $paymentMethod = $request->payments;
        $deliveryPrice = $request->delivery_price;
        $totalPrice = $request->total_price_main;
        $totalDiscount = $request->discount;
        $deliveryMethod = $request->deliveryMethod;
        $subTotal = $request->subtotal;
        $order_pin = \Auth::id().\Str::random(3).Date('s');
        $cart = $request->cartList;

        $cutBusiness = $this->_setting::first()->cut_business;
        $cutIndividual = $this->_setting::first()->cut_individual;

        if(is_null($cutBusiness) || is_null($cutIndividual))
            return response()->json(["status"=>0, "message"=>"Some settings need to be setup on behalf of the admin! -- Please setup the business or individual cut!"], 200);

        $countableOrderItem = [];
        $company_discount = 100;

        if(count($cart) >= $this->_setting->first()->products_amount_for_discount) {
            $company_discount = $this->_setting->first()->discount;
        }

        $mainOrder = [
            'email' => \Auth::user()->email,
            'amount' => $totalPrice,
            'delivery' => $deliveryPrice,
            'discount' => $totalDiscount,
            'contents' => $cart,
            'subtotal' => $subTotal,
            'currency' => 'ETB',
            'order_pin' => $order_pin,
            'route' => "api"
        ];

        \Mail::send(new OrderMail($mainOrder, $this->_setting::first()));
        $orderID = $this->_mainOrder->generateOrderCode();

        if($paymentMethod == 'on-delivery')
        {
            $mainOrder = $this->_mainOrder->createMainOrder(null, $orderID, $totalPrice, $totalDiscount, $deliveryPrice, $paymentMethod, \Auth::id(), $deliveryMethod, $order_pin);
        }
        elseif($paymentMethod == 'bank')
        {
            $uniqueBankCode = $this->_mainOrder->generateBankCode();
            $mainOrder = $this->_mainOrder->createMainOrder($uniqueBankCode, $orderID, $totalPrice, $totalDiscount, $deliveryPrice, $paymentMethod, \Auth::id(), $deliveryMethod, $order_pin);
        }

        foreach($cart as $item)
        {
            $productId = null;
            $packageId = null;

            if($item['associatedmodel'] == "App\Product") {
                $vendor = $this->_product::with(['user'])->find($item['productid']);
                $productId = $item['productid'];
                $orderCode = $this->_mainOrder->generateOrderCode();
                $color = $this->_color::where('hex', strtoupper($item['color']))->first()->id;
                $qty = $item['qty'];
                $price = $item['price'];
            } elseif ($item['associatedmodel'] == "App\SpecialPackage") {
                $vendor = $this->_package::with(['user'])->find($item['packageid']);
                $packageId = $item['packageid'];
                $orderCode = $this->_mainOrder->generateOrderCode();
                $color = null;
                $qty = $item['qty'];
                $price = $item['price'];
            }

            $discount_calculate = ($company_discount / 100) * count($cart);
            ($company_discount !== 100) ? $discount = $price * $discount_calculate : $discount = 0;
            ($price!=$discount) ? $paid_amount = $price - $discount : $paid_amount = $price;

            $businessType = $vendor->user->business_type;
            $debt = 0;

            if ($businessType == "business")
                $debt = $paid_amount - ($cutBusiness / 100) * $paid_amount;
            elseif ($businessType == "individual")
                $debt = $paid_amount - ($cutIndividual / 100) * $paid_amount;

            if($item['associatedmodel'] == "App\Product")
                $this->_product->updateAvailableItems($productId, $qty);
            elseif($item['associatedmodel'] == "App\SpecialPackage")
                $this->_package->updateAvailableItems($packageId, $qty);

            $this->_order->createOrder(\Auth::id(), $productId, $packageId, $orderCode, $paymentMethod, $color, $qty, $vendor->user->id, $debt, $paid_amount, $mainOrder->id);
        }

        return response()->json(['status'=>1, 'message' => 'Successfully ordered!']);
    }

    public function changeClientPassword(Request $request) {
        $this->validate(request(), [

            'newPassword' => 'required',
            'oldPassword' => 'required',

        ]);

        $oldPassword = $request->oldPassword;
        $newPassword = $request->newPassword;

        if($this->_user->checkIfCurrentPasswordIsCorrect($oldPassword)) {
            $this->_user->changePassword($newPassword);
            $user = $this->_user::with(['addresses',
                            'wish',
                            'wish.product',
                            'wish.product.category',
                            'wish.product.productColors',
                            'wish.product.productColors.colors',
                            'wish.product.productImage',
                            'wish.product.productSize',
                            'wish.product.productSize.product',
                            'wish.product.Review',
                            'wish.product.user',
                            'wish.product.user.addresses',
                            'wish.product.user.wish',
                            'wish.product.order'])->find(\Auth::id());
            return response()->json(['status'=>1, "message"=>'Password is successfully changed!', "user"=>$user], 200);
        } else {
            return response()->json(["status"=>0, "message"=>'Your current password is incorrect!'], 200);
        }
    }

    public function updateProfile(Request $request) {
        $name = $request->name;
        $phoneNumber = $request->phoneNumber;

        $this->_user->updateMyProfile($name, $phoneNumber);
        $user = $this->_user::with(['addresses',
                            'wish',
                            'wish.product',
                            'wish.product.category',
                            'wish.product.productColors',
                            'wish.product.productColors.colors',
                            'wish.product.productImage',
                            'wish.product.productSize',
                            'wish.product.productSize.product',
                            'wish.product.Review',
                            'wish.product.user',
                            'wish.product.user.addresses',
                            'wish.product.user.wish',
                            'wish.product.order'])->find(\Auth::id());
        return response()->json(["message" => "Your profile is successfully updated!", "user"=>$user], 200);
    }

    public function updateClientAddress(Request $request) {
        $address = $request->address;
        $this->_address->updateAddress($address["address"], $address["latitude"], $address["longitude"], $reqesut->id);
        $user = $this->_user::with(['addresses',
                            'wish',
                            'wish.product',
                            'wish.product.category',
                            'wish.product.productColors',
                            'wish.product.productColors.colors',
                            'wish.product.productImage',
                            'wish.product.productSize',
                            'wish.product.productSize.product',
                            'wish.product.Review',
                            'wish.product.user',
                            'wish.product.user.addresses',
                            'wish.product.user.wish',
                            'wish.product.order'])->find(\Auth::id());
        return response()->json(["message" => "Address is successfully updated!", "user"=>$user], 200);
    }

    public function deactivateAccount() {
        $this->_user->deactivate();
        $token = \Auth::user()->token();
        \DB::table('oauth_refresh_tokens')->where('access_token_id', $token->id)->update(['revoked' => true]);
        $token->revoke();
        $this->guard()->logout();
        \Cookie::queue(\Cookie::forget(Passport::cookie()));
        return response()->json(["message" => "Account is successfully deactivated!"], 200);
    }

    public function removeWish($productId) {
        $wish = $this->_wishList::with(['product', 'product.category', 'product.productColors', 'product.productColors.colors', 'product.productImage', 'product.productSize', 'product.productSize.product', 'product.Review', 'product.user', 'product.user.addresses', 'product.user.products', 'product.user.wish', 'product.order'])->where('product_id', $productId)->first();
        ($wish != null) ? $wish->delete() : null;
        return response()->json(["status"=>1], 200);
    }

    public function addWish(Request $request) {
        $product_id = $request->product_id;
        $product_data = $this->_product::find($product_id);
        $wishCount = $this->_wishList::where('product_id', $product_id)->where('user_id', \Auth::id())->count();
        $wish = [];
    	if($wishCount == 0)
    	{
	    	$wish = $this->_wishList::create([
	    		'product_id' => $product_id,
	    		'user_id' => \Auth::id()
	    	]);
        }
        $list = $this->_wishList::with(['product', 'product.category', 'product.productColors', 'product.productColors.colors', 'product.productImage', 'product.productSize', 'product.productSize.product', 'product.Review', 'product.user', 'product.user.addresses', 'product.user.products', 'product.user.wish', 'product.order'])->find($wish->id);
    	return response()->json(["wish"=>$list], 200);
    }

    public function search(Request $request) {
        $searchquery = $request->searchquery;
        $category = $request->category;

        if ($category == null) {
            $results = $this->_product::with(['category',
                                        'productColors',
                                        'productColors.colors',
                                        'productImage',
                                        'productSize',
                                        'productSize.product',
                                        'Review',
                                        'user',
                                        'user.addresses',
                                        'user.products',
                                        'user.wish',
                                        'order'])->where('product_name', 'LIKE', '%'.strtolower($searchquery).'%')->get();
        } else {
            $results = $this->_product::with(['category',
                                        'productColors',
                                        'productColors.colors',
                                        'productImage',
                                        'productSize',
                                        'productSize.product',
                                        'Review',
                                        'user',
                                        'user.addresses',
                                        'user.products',
                                        'user.wish',
                                        'order'])->where('product_name', 'LIKE', '%'.strtolower($searchquery).'%')->where('category_id', $category)->get();
        }

        return response()->json(['results'=>$results]);
    }

    public function updateProfilePicture(Request $request) {
        $image = $request->image;
        $user = $this->_user::with(['addresses', 'wish', 'wish.product', 'wish.product.category', 'wish.product.productColors', 'wish.product.productColors.colors', 'wish.product.productImage', 'wish.product.productSize', 'wish.product.productSize.product', 'wish.product.Review', 'wish.product.user', 'wish.product.user.addresses', 'wish.product.user.wish', 'wish.product.order'])->find(\Auth::id());
        $user->photo_url = $image;
        $user->save();
        return response()->json(['user'=>$user]);
    }

    public function showSelectedPackage($packageId) {
        $package = $this->_package::with(['packageProduct',
                                            'images',
                                            'user',
                                            'packageProduct.product.productImage',
                                            'user.addresses',
                                            'user.wish'])->find($packageId);

        return response()->json(['package'=>$package]);
    }

    public function destroyMainOrder($orderId) {
        $order = $this->_mainOrder::find($orderId);
        if ($order!=null) {
            $order->delete();
        } else {
            return response()->json(["status"=>"failed"]);
        }
        return response()->json(["status"=>"success"]);
    }

    public function destroyOrder($orderId) {
        $order = $this->_order::find($orderId);
        if ($order->count() > 0) {
            $order->delete();
        }
        return response()->json(["order"=>$order]);
    }

    public function forgotPassword(Request $request) {
        $this->validateEmail($request);

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($request, $response)
                    : $this->sendResetLinkFailedResponse($request, $response);

    }

    public function broker(){
        return Password::broker();
    }

    protected function sendResetLinkResponse(Request $request, $response){
        return response()->json(['status'=>"success", 'message'=>trans($response)]);
    }


    protected function sendResetLinkFailedResponse(Request $request, $response) {
        return response()->json(['status'=>"failed", 'message'=>trans($response)]);
    }

    protected function validateEmail(Request $request) {
        $request->validate(['email' => 'required|email']);
    }
}
