<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    private $_user;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');

        $this->_user = new User();
    }


    public function index()
    {
        return view('pre-login.auth.signup');
    }

    public function store()
    {

        $this->validate(request(), [

            'email' => 'required|email|max:255|unique:users',

            'password' => 'required|min:6|confirmed'

        ]);

        $password = bcrypt(request('password'));

        $email = request('email');

        $seller = request('seller');

        $user = $this->_user->createAccount($email, $password, $seller);

        if(auth()->attempt(request(['email', 'password']))){

            if(auth()->user()->role == 1)
            {
                return response()->json(["data"=>$user, "status"=>"success", "message"=>"You are successfully registered!", "seller"=>$seller, "role_type"=>"admin"], 200);
            }
            elseif(auth()->user()->role == 2)
            {
                return response()->json(["data"=>$user, "status"=>"success", "message"=>"You are successfully registered!", "seller"=>$seller, "role_type"=>"seller"], 200);
            }
            elseif(auth()->user()->role == 3)
            {
                return response()->json(["data"=>$user, "status"=>"success", "message"=>"You are successfully registered!", "seller"=>$seller, "role_type"=>"customer"], 200);
            }

        }


    }
}
