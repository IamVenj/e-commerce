<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('destroy');
    }

    public function index()
    {
        return view('pre-login.auth.login');
    }

    public function destroy(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect('/login');
    }

    protected function guard()
    {
        return \Auth::guard();
    }

    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    protected function credentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    public function store(Request $request)
    {
        if (!$this->attemptLogin($request)) {
            return response()->json(["status"=>"login_failed", "message"=>'Please Check Your credentials and Try again.']);
        }
        else
        {
            if(auth()->user()->role == 1)
            {
                return response()->json(["status"=>"login_success", "role_type"=>"admin"]);
            }
            elseif(auth()->user()->role == 2)
            {
                return response()->json(["status"=>"login_success", "role_type"=>"seller"]);
            }
            elseif(auth()->user()->role == 3)
            {
                return response()->json(["status"=>"login_success", "role_type"=>"customer"]);
            }
        }
    }


}
