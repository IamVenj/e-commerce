<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Cart;
use App\Order;
use App\Product;
use App\Debt;
use App\CompanySettings;
use App\MainOrder;
use App\SpecialPackage;
use App\Mail\OrderMail;

class PaymentController extends Controller
{
    private $_order, $_product, $_debt, $_setting, $_mainOrder, $_package;

    public function __construct()
    {
        $this->middleware('auth');

        $this->_order = new Order();
        $this->_product = new Product();
        $this->_debt = new Debt();
        $this->_setting = new CompanySettings();
        $this->_mainOrder = new MainOrder();
        $this->_package = new SpecialPackage();
    }

    public function pay($id, Request $request)
    {
        $paymentMethod = $request->payments;
        $deliveryPrice = $request->delivery_price;
        $totalPrice = $request->total_price_main;
        $totalDiscount = $request->discount;
        $deliveryMethod = $request->deliveryMethod;
        $order_pin = auth()->user()->id.\Str::random(3).Date('s');

        $cutBusiness = $this->_setting::first()->cut_business;
        $cutIndividual = $this->_setting::first()->cut_individual;

        if(is_null($cutBusiness) || is_null($cutIndividual))
            return back()->withErrors("Some settings need to be setup on behalf of the admin! -- Please setup the business or individual cut!");

        $countableOrderItem = [];
        $deliveryStatus = 0;

        if(!is_null($request->delivery_price_per_km)) {
            $deliveryStatus = 1;
        }

        $company_discount = 100;

        if(Cart::count() >= $this->_setting->first()->products_amount_for_discount) {
            $company_discount = $this->_setting->first()->discount;
        }

        $mainOrder = [
            'email' => auth()->user()->email,
            'amount' => $totalPrice,
            'delivery' => $deliveryPrice,
            'discount' => $totalDiscount,
            'contents' => Cart::content(),
            'subtotal' => Cart::subtotal(),
            'currency' => 'ETB',
            'order_pin' => $order_pin,
            "route" => "web"
        ];

        \Mail::send(new OrderMail($mainOrder, $this->_setting::first()));

        if($paymentMethod == 'on-delivery')
        {
            $orderID = $this->_mainOrder->generateOrderCode();
            
            $mainOrder = $this->_mainOrder->createMainOrder(null, $orderID, $totalPrice, $totalDiscount, $deliveryPrice, $paymentMethod, auth()->user()->id, $deliveryMethod, $order_pin);
            
            foreach(Cart::content() as $item)
            {
                $productId = null;
                $packageId = null;

                if($item->associatedModel == "App\Product") {
                    $productId = $item->model->id;
                    $orderCode = $item->rowId.strtotime("now");
                    $color = $item->options->color;
                    $qty = $item->qty;
                    $price = $item->model->current_price;
                } elseif ($item->associatedModel == "App\SpecialPackage") {
                    $packageId = $item->model->id;
                    $orderCode = $item->rowId.strtotime("now");
                    $color = null;
                    $qty = $item->qty;
                    $price = $item->model->discount_price;
                }

                $discount_calculate = ($company_discount / 100) * Cart::instance('default')->count();
                ($company_discount !== 100) ? $discount = $price * $discount_calculate : $discount = 0;
                ($price!=$discount) ? $paid_amount = $price - $discount : $paid_amount = $price;                

                $businessType = $item->model->user()->first()->business_type;
                $debt = 0;

                if ($businessType == "business")
                    $debt = $paid_amount - ($cutBusiness / 100) * $paid_amount;
                elseif ($businessType == "individual")
                    $debt = $paid_amount - ($cutIndividual / 100) * $paid_amount;

                if($item->associatedModel == "App\Product")
                    $this->_product->updateAvailableItems($productId, $qty);
                elseif($item->associatedModel == "App\SpecialPackage")
                    $this->_package->updateAvailableItems($packageId, $qty);

                $this->_order->createOrder(auth()->user()->id, $productId, $packageId, $orderCode, $paymentMethod, $color, $qty, $item->model->user()->first()->id, $debt, $paid_amount, $mainOrder->id);
            }

            Cart::destroy();            
            return back()->with('success', 'Successfully ordered');
        }
        elseif($paymentMethod == 'bank')
        {

            // return redirect('/under-construction');

            $uniqueBankCode = $this->_mainOrder->generateBankCode();
            $orderID = $this->_mainOrder->generateOrderCode();

            $mainOrder = $this->_mainOrder->createMainOrder($uniqueBankCode, $orderID, $totalPrice, $totalDiscount, $deliveryPrice, $paymentMethod, auth()->user()->id, $deliveryMethod, $order_pin);
            foreach(Cart::content() as $item)
            {
                $productId = null;
                $packageId = null;
                
                if($item->associatedModel == "App\Product") {
                    $productId = $item->model->id;
                    $orderCode = $item->rowId.strtotime("now");
                    $color = $item->options->color;
                    $qty = $item->qty;
                    $price = $item->model->current_price;
                } elseif ($item->associatedModel == "App\SpecialPackage") {
                    $packageId = $item->model->id;
                    $orderCode = $item->rowId.strtotime("now");
                    $color = null;
                    $qty = $item->qty;
                    $price = $item->model->discount_price;
                }
                $discount_calculate = ($company_discount / 100) * Cart::instance('default')->count();
                ($company_discount !== 100) ? $discount = $price * $discount_calculate : $discount = 0;
                ($price!=$discount) ? $paid_amount = $price - $discount : $paid_amount = $price;

                $cutBusiness = $this->_setting::first()->cut_business;
                $cutIndividual = $this->_setting::first()->cut_individual;

                if(is_null($cutBusiness) || is_null($cutIndividual))
                    return back()->withErrors("Sorry! Some settings need to be setup on behalf of the admin! -- Please setup the business or individual cut!");

                $businessType = $item->model->user()->first()->business_type;
                $debt = 0;

                if ($businessType == "business")
                    $debt = $paid_amount - ($cutBusiness / 100) * $paid_amount;
                elseif ($businessType == "individual")
                    $debt = $paid_amount - ($cutIndividual / 100) * $paid_amount;

                if($item->associatedModel == "App\Product")
                    $this->_product->updateAvailableItems($productId, $qty);
                elseif($item->associatedModel == "App\SpecialPackage")
                    $this->_package->updateAvailableItems($packageId, $qty);

                $this->_order->createOrder(auth()->user()->id, $productId, $packageId, $orderCode, $paymentMethod, $color, $qty, $item->model->user()->first()->id, $debt, $paid_amount, $mainOrder->id);
            }

            Cart::destroy();            
            return back()->with('success', 'Successfully ordered!');

        }

    }
}
