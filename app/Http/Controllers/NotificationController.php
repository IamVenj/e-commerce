<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Notification;

class NotificationController extends Controller
{
	private $_notification;

	public function __construct()
	{
		$this->_notification = new Notification();
	}

    public function index()
    {
    	return auth()->user()->unreadNotifications;
    }

    public function read(Request $request)
    {
    	return auth()->user()->unreadNotifications()->find($request->id)->MarkAsRead();
    }
}
