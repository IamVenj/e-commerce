<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use App\HomeCarousel;
use App\Product;
use App\Order;
use App\SpecialOffers;
use App\User;
use App\PackageProduct;
use App\SpecialPackage;

class HomeController extends Controller
{

	private $_carousel, $_product, $_order, $_category, $_user, $_packageProduct, $_specialPackage;

	public function __construct()
	{
		$this->middleware('wizard');

		$this->_carousel = new HomeCarousel();
		$this->_product = new Product();
		$this->_order = new Order();
		$this->_category = new Category();
        $this->_offer = new SpecialOffers();
        $this->_user = new User();
        $this->_packageProduct = new PackageProduct();
        $this->_specialPackage = new SpecialPackage();
	}

    public function index()
    {
    	$carousels = $this->_carousel::latest()->get();
    	$latestP = $this->_product::with(['user'])->latest()->take(30)->get();
    	$bestP = $this->_product::with(['user'])->where('orders', '!=', 0)->orderBy('orders')->first();
    	$categories = $this->_category::whereNotNull('parent_id')->get();
        $offers = $this->_product->offers();
        // $offers = $this->_offer->getLatestOffers();

        $best_products = null;
        if(!is_null($bestP))
    	   $best_products = $this->_product::with(['user'])->where('id', '!=', $bestP->id)->where('orders', '>', 0)->orderBy('orders', 'desc')->get();

        $packages = $this->_specialPackage::with(['images', 'packageProduct', 'user'])->latest()->take(30)->get();

        // foreach ($packages as $package) {
        //     dd($package->packageProduct->first()->product->user()->first()['activation']);
        // }
    	return view('pre-login.pages.home', compact('carousels', 'latestP', 'bestP', 'best_products', 'categories', 'offers', 'packages'));
    }
}
