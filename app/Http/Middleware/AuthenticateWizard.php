<?php

namespace App\Http\Middleware;

use Closure;

class AuthenticateWizard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!is_null(auth()->user()))
        {
            if(auth()->user()->role == 3)
            {
                if(is_null(auth()->user()->name) || auth()->user()->addresses()->count() == 0) {
                    return redirect(route('wizard-view'));
                } elseif (is_null(auth()->user()->phone_number)) {
                    return redirect(route('wizard-view'));
                } elseif (auth()->user()->addresses()->count() == 0) {
                    return redirect(route('wizard-view'));
                } else {
                    return $next($request);
                }
            }
            elseif(auth()->user()->role == 2)
            {
                if(is_null(auth()->user()->shop_name) && auth()->user()->activation == 0) {
                    return redirect(route('wizard-view'));
                } elseif (is_null(auth()->user()->phone_number) && auth()->user()->activation == 0) {
                    return redirect(route('wizard-view'));
                } elseif (auth()->user()->addresses()->count() == 0 && auth()->user()->activation == 0) {
                    return redirect(route('wizard-view'));
                } elseif (auth()->user()->activation == 1) {
                    auth()->logout();
                    return redirect('/login')->withErrors('Your subscription is pending for approval. Please contact admin.');
                } else {
                    return $next($request);
                }
            }

        }


        return $next($request);


    }

}
