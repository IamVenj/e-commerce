<?php

namespace App\Http\Middleware;

use Closure;

class CloudinaryMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $cloudinary_config = \Config::get('cloudinary');

        \Cloudinary::config($cloudinary_config);

        return $next($request);
    }
}
