<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

use App\Product;

use App\Notifications\NotifyMerchantOnProductReview;

class Review extends Model
{
    protected $fillable = ['review', 'rating', 'product_id', 'user_id'];

    protected $hidden = ['product_id', 'user_id'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function postReview($review, $rating, $product_id, $user)
    {
    	$_review = $this::create([

    		'review' => $review,

    		'rating' => $rating,

    		'product_id' => $product_id,

    		'user_id' => $user

    	]);

        $selectedProduct = Product::find($product_id);

        User::find($selectedProduct->user()->first()->id)->notify(new NotifyMerchantOnProductReview($_review));
    }

    public function _updateReview($rating, $review, $id)
    {
    	$_review = $this::find($id);

    	$_review->rating = $rating;

    	$_review->review = $review;

    	$_review->save();
    }

    public function destroyReview($id)
    {
        $this::find($id)->delete();
    }
}
