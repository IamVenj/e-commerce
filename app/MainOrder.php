<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class MainOrder extends Model
{
    protected $fillable = ['unique_bank_code', 'order_code', 'total_price', 'discount_price', 'delivery_price', 'payment_method', 'user_id', 'delivery_method', 'vendor_delivery_confirmation', 'customer_delivery_confirmation', 'payment_status', 'order_pin', 'deliveryp_id'];

    public function generateOrderCode() {
    	return \Str::random(4).date('sdm', strtotime(now())).\Str::random(2);
    }

    public function generateBankCode() {
        return \Str::random(2).date('sd', strtotime(now())).\Str::random(2);
    }

    public function order() {
    	return $this->hasMany('App\Order');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function dPersonnel() {
        return $this->belongsTo('App\DeliveryP', 'deliveryp_id', 'id');
    }

    public function assignDeliveryPersonnel($id, $deliveryp_id) {
        $mainOrder = $this::find($id);
        $mainOrder->deliveryp_id = $deliveryp_id;
        $mainOrder->save();
    }

    public function createMainOrder($uniqueBankCode, $orderCode, $totalPrice, $discountPrice, $deliveryPrice, $paymentMethod, $userID, $deliveryMethod, $order_pin) {
    	$order = $this::create([

    		'unique_bank_code' => $uniqueBankCode,
    		'order_code' => $orderCode,
    		'total_price' => $totalPrice,
    		'discount_price' => $discountPrice,
    		'delivery_price' => $deliveryPrice,
    		'payment_method' => $paymentMethod,
            'user_id' => $userID,
            'delivery_method' => $deliveryMethod,
            'order_pin' => $order_pin

    	]);

    	return $order;
    }

    public function getUserInOrder($role) {
        if($role == 1) {
            return $this::latest()->get()->groupBy('user_id');
        }
        else
        {
            return $this::with(['order'])
                    ->whereHas('order', function($query) {
                        $query->where('vendor_id', auth()->user()->id);
                    })->latest()->get()->groupBy('user_id');
        }
    }

    public function getOrderCountForUserTimeline($user_id, $role)
    {
        if($role == 1) {
            return $this::where('user_id', $user_id)->whereBetween('created_at', [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->count();
        } else {
            return $this::with('order')
                    ->whereHas('order', function($query) {
                        $query->where('vendor_id', auth()->user()->id);
                    })
                    ->where('user_id', $user_id)->whereBetween('created_at', [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->count();
        }
    }

    public function getUserDate($user_id, $role) {
        if($role == 1) {
            return $this::where('user_id', $user_id)
                        ->latest()
                        ->get()
                        ->groupBy(function($item) {
                            return $item->created_at->format('Y-m-d');
                        });
        } else {
            return $this::with('order')
                        ->whereHas('order', function($query) {
                            $query->where('vendor_id', auth()->user()->id);
                        })
                        ->where('user_id', $user_id)
                        ->latest()
                        ->get()
                        ->groupBy(function($item) {
                            return $item->created_at->format('Y-m-d');
                        });
        }
    }

    public function confirmedAdminOrders($user_id, $timestamp, $role)
    {
        if($role == 1) {
            return $this::where('user_id', $user_id)->whereDate('created_at', $timestamp)
                    ->where('vendor_delivery_confirmation', 1)
                    ->get();
        } else {
            return $this::with('order')
                    ->whereHas('order', function($query) {
                        $query->where('vendor_id', auth()->user()->id);
                    })
                    ->where('vendor_delivery_confirmation', 1)
                    ->where('user_id', $user_id)->whereDate('created_at', $timestamp)
                    ->get();
        }
    }

    public function confirmedCustomerOrders($user_id, $timestamp, $role)
    {
        if($role == 1) {
            return $this::where('user_id', $user_id)->whereDate('created_at', $timestamp)
                        ->where('customer_delivery_confirmation', 1)
                        ->get();
        } else {
            return $this::with('order')
                        ->whereHas('order', function($query) {
                            $query->where('vendor_id', auth()->user()->id);
                        })
                        ->where('customer_delivery_confirmation', 1)
                        ->where('user_id', $user_id)->whereDate('created_at', $timestamp)
                        ->get();
        }
    }

    public function checkgroupVendorAdminDeliveryConfirmation($user_id, $timestamp)
    {
        $countOfOrders = count($this->getOrder($user_id, $timestamp, auth()->user()->role));
        $countOfConfirmedOrders = count($this->confirmedAdminOrders($user_id, $timestamp, auth()->user()->role));
        return ($countOfOrders == $countOfConfirmedOrders) ? true : false;
    }

    public function checkgroupCustomerAdminDeliveryConfirmation($user_id, $timestamp)
    {
        $countOfOrders = count($this->getOrder($user_id, $timestamp, auth()->user()->role));
        $countOfConfirmedOrders = count($this->confirmedCustomerOrders($user_id, $timestamp, auth()->user()->role));
        return ($countOfOrders == $countOfConfirmedOrders) ? true : false;
    }

    public function getOrder($user_id, $timestamp, $role)
    {
        if($role == 1) {
            return $this::with(['order', 'dPersonnel', 'user'])->whereHas('order', function($query) {
                        $query->whereNotNull('vendor_id');
                    })->where('user_id', $user_id)->whereDate('created_at', $timestamp)->get();
        } else {
            return $this::with('order')
                    ->whereHas('order', function($query) {
                        $query->where('vendor_id', auth()->user()->id);
                    })
                    ->where('user_id', $user_id)->whereDate('created_at', $timestamp)->get();
        }
    }

     public function groupVendorAdminDeliveryConfirmation($user_id, $timestamp)
    {
        $orders = $this->getOrder($user_id, $timestamp, auth()->user()->role);

        for($i = 0; $i < count($orders); $i++)
        {
            $orders[$i]->vendor_delivery_confirmation = 1;
            $orders[$i]->payment_status = 1;
            $orders[$i]->save();
        }
    }

    public function customerDeliveryConfirmation($id, $status)
    {
        $order = $this::find($id);
        $order->customer_delivery_confirmation = $status;
        $order->save();
    }

    public function destroyOrder($id) {
        return $this::find($id)->delete();
    }

}
