<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\User;

class NotifyUserRegistration extends Notification
{
    use Queueable;

    public $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {

        if($this->user->role == 2)
        {
            return [

                'name' => $this->user->shop_name,

                'role' => $this->user->role

            ];
        }
        elseif ($this->user->role == 3) 
        {
            return [

                'name' => $this->user->name,

                'role' => $this->user->role

            ];
        }

    }

    public function toBroadcast($notifiable)
    {

        if($this->user->role == 2)
        {
            return [

                'name' => $this->user->shop_name,

                'role' => $this->user->role

            ];
        }
        elseif ($this->user->role == 3) 
        {
            return [

                'name' => $this->user->name,

                'role' => $this->user->role

            ];
        }

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
