<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Order;
use App\User;

class NotifyMerchantOnOrder extends Notification
{
    use Queueable;

    public $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order, User $user)
    {
        $this->order = $order;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {
        return [

            'order_id' => $this->order->id,
            'order_code' => $this->order->order_code,
            'from' => $this->user->name,
            'product_id' => $this->order->product_id,
            'package_id' => $this->order->package_id,
            'created_at' => $this->order->created_at->diffForHumans()

        ];
    }

    public function toBroadcast($notifiable)
    {
        return [

            'order_id' => $this->order->id,
            'order_code' => $this->order->order_code,
            'from' => $this->user->name,
            'product_id' => $this->order->product_id,
            'package_id' => $this->order->package_id,
            'created_at' => $this->order->created_at->diffForHumans()

        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
