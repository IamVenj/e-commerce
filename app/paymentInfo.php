<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paymentInfo extends Model
{
    protected $fillable = ['merchant_code', 'user_id', 'pdt_token'];

    protected $hidden = ['user_id', 'pdt_token'];

    public function createMerchantYenePayBankIdentity($user_id, $pdt_token, $merchant_code)
    {
    	$this::create([

    		'user_id' => $user_id,

    		'pdt_token' => $pdt_token,

    		'merchant_code' => $merchant_code

    	]);
    }
}
