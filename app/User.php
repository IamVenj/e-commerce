<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Address;
use App\Mail\VendorEmail;
use App\CompanySettings;

use App\Delivery;
use Laravel\Passport\HasApiTokens;

use App\Notifications\NotifyUserRegistration;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
    seller == 2
    admin == 1
    customer == 3
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'shop_name', 'phone_number', 'phone_number2', 'phone_number3', 'delivery', 'about_shop', 'photo_url','image_version', 'activation', 'credit', 'tin', 'business_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function addresses()
    {
        return $this->hasMany(Address::class, 'user_id', 'id');
    }

    public function wish()
    {
        return $this->hasMany('App\WishList');
    }

    public function bank() {
        return $this->hasOne('App\Bank');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function getUser($id) {
        return $this->find($id);
    }

    public function createAccount($email, $password, $seller)
    {
        if($seller == 'true')
        {

            $vendor = $this::create([

                'email' => $email,
                'password' => $password,
                'role' => 2,

            ]);

            $settings = CompanySettings::first();
            \Mail::send(new VendorEmail($vendor, $settings));

        }

        else
        {

            $this::create([

                'email' => $email,
                'password' => $password,
                'role' => 3

            ]);

        }

    }

    public function updateAccount($image_public_id, $phone_number, $name, $shop_name, $about_shop, $current_user_id, $current_user_role, $delivery, $image_version, $credit, $phone_number2, $phone_number3, $tin, $business_type)
    {
        $current_user = $this::find($current_user_id);

        if($current_user_role == 2)
        {

            if($business_type == "individual") {
                $current_user->photo_url = $image_public_id;
                $current_user->phone_number = $phone_number;
                $current_user->phone_number2 = $phone_number2;
                $current_user->phone_number3 = $phone_number3;
                $current_user->credit = $credit;
                $current_user->shop_name = $name;
                $current_user->business_type = $business_type;
                // $current_user->delivery = $delivery;
                $current_user->image_version = $image_version;

                $current_user->save();
            } elseif($business_type == "business") {
                $current_user->photo_url = $image_public_id;
                $current_user->phone_number = $phone_number;
                $current_user->phone_number2 = $phone_number2;
                $current_user->phone_number3 = $phone_number3;
                $current_user->credit = $credit;
                $current_user->business_type = $business_type;
                $current_user->shop_name = $shop_name;
                $current_user->tin = $tin;
                $current_user->about_shop = $about_shop;
                // $current_user->delivery = $delivery;
                $current_user->image_version = $image_version;

                $current_user->save();
            }


        }
        else
        {

            $current_user->photo_url = $image_public_id;
            $current_user->phone_number = $phone_number;
            $current_user->phone_number2 = $phone_number2;
            $current_user->phone_number3 = $phone_number3;
            $current_user->name = $name;
            $current_user->image_version = $image_version;

            $current_user->save();

        }

        $admin = $this::where('role', 1)->first();
        $this::find($admin->id)->notify(new NotifyUserRegistration($current_user));

    }

    public function updateCustomer($name, $email, $phone_number, $id)
    {

        $user = $this::find($id);

        $user->name = $name;
        $user->email = $email;
        $user->phone_number = $phone_number;

        $user->save();

    }

    public function deletePreviousPhotoURL($id)
    {

        $profile_img = $this::find($id);

        if(!is_null($profile_img->photo_url))
            \Cloudder::destroy($profile_img->photo_url);

    }

    public function updateProfilePicture($image, $id)
    {

        $image_name = $image->getRealPath();
        $image_result = \Cloudinary\Uploader::upload($image_name, array("folder"=>"profile"));
        // $this->deletePreviousPhotoURL($id);

        $user = $this::find($id);

        $user->photo_url = $image_result['secure_url'];
        $user->image_version = $image_result['version'];

        $user->save();

    }

    public function checkIfCurrentPasswordIsCorrect($current_password)
    {
        if(\Hash::check($current_password, \Auth::user()->password))
            return true;
        return false;
    }

    public function changePassword($new_password)
    {
        $user = $this::find(\Auth::id);
        $user->password = bcrypt($new_password);
        $user->save();
    }

    public function updateMyProfile($name, $phoneNumber) {
        $user = $this::find(\Auth::id());
        $user->name = $name;
        $user->phone_number = $phoneNumber;
        $user->save();
    }

    public function updateSeller($currentUserId, $shop_name, $about_shop, $phone_number, $email)
    {
        $user = $this::find($currentUserId);

        $user->shop_name = $shop_name;
        $user->about_shop = $about_shop;
        $user->phone_number = $phone_number;
        $user->email = $email;

        $user->save();
    }

    public function updateAdmin($currentUserId, $name, $email)
    {
        $user = $this::find($currentUserId);

        $user->name = $name;
        $user->email = $email;

        $user->save();
    }

    public function updateDeliveryStatus($status)
    {
        $user = $this::find(auth()->user()->id);
        $user->delivery = $status;
        $user->save();

    }

    public function isConnected()
    {
        $connected = @fsockopen("www.google.com", 80);

        if($connected)
            return true;
        else
            return false;
    }

    public function role_vendor()
    {
        return $this::where('role', 2);
    }

    public function role_customer()
    {
        return $this::where('role', 3);
    }

    public function destroyUser($id) {
        $user = $this::find($id);
        // $this->destroyImage($user);
        $user->delete();
    }

    public function destroyImage($user) {
        (!is_null($user->photo_url)) ? \Cloudinary\Uploader::delete($user->photo_url) : null;
    }

    public function getVendors()
    {
        return $this::where('role', 2)->whereNotNull('shop_name')->get();
    }

    public function addVendor($email, $password, $shopname, $about_shop, $phone_number, $address, $lat, $lng)
    {
        $user = $this::create([

            'shop_name' => $shopname,
            'phone_number' => $phone_number,
            'about_shop' => $about_shop,
            'email' => $email,
            'password' => bcrypt($password),
            'role' => 2

        ]);

        $_address = new Address();
        $_address->addVendorAddress($address, $lat, $lng, $user->id);
    }

    public function activate($id)
    {
        $user = $this::find($id);
        $user->activation = 0;
        $user->save();
    }

    public function deactivate()
    {
        $user = $this::find(\Auth::id());
        $user->activation = 1;
        $user->save();
    }

    public function deactivateAdmin($id)
    {
        $user = $this::find($id);
        $user->activation = 1;
        $user->save();
    }

    public function updateRoleAndStatus($id, $role) {
        $this->updateData($id, $role);
        $this->updateRole($id, $role);
    }

    public function updateRole($id, $role) {
        $user = $this::find($id);
        $user->role = $role;
        $user->save();
    }

    public function updateData($id, $role) {
        $user = $this::with(['addresses'])->find($id);
        /*
        |--------------------------------------------------------------------------------------------------------
        | role 2 is defined for vendors but in this case | IF its role 2 then the updated data will be of role 3
        |--------------------------------------------------------------------------------------------------------
        */
        if($role == 2) {
            // data of role3 is being updated if its role2
            // $this->destroyImage($user);
            $user->name = null;
            $user->phone_number = null;
            $user->phone_number2 = null;
            $user->phone_number3 = null;
            $user->photo_url = null;
            $user->image_version = null;
            $user->save();
        } elseif ($role == 3) {
            // data of role2 will be updated if its role3
            // $this->destroyImage($user);
            $user->shop_name = null;
            $user->phone_number = null;
            $user->phone_number2 = null;
            $user->phone_number3 = null;
            $user->about_shop = null;
            $user->photo_url = null;
            $user->image_version = null;
            $user->credit = 0;
            $user->tin = null;
            $user->business_type = null;
            $user->save();
        }

        $_address = new Address();
        if($user->addresses->count() > 0) {
            foreach ($user->addresses as $address) {
                $_address->destroyAddress($address->id);
            }
        }

    }

}
