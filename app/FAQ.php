<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FAQ extends Model
{
    protected $fillable = ['question', 'answer'];

    public function createFAQ($question, $answer) {
    	$this::create([
    		'question' => $question,
    		'answer' => $answer
    	]);
    }

    public function updateFAQ($question, $answer, $id) {
    	$faq = $this::find($id);
    	$faq->question = $question;
    	$faq->answer = $answer;
    	$faq->save();
    }

    public function destroyFAQ($id) {
    	$this::find($id)->delete();
    }
}
