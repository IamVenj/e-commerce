<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Debt;
use App\User;
use App\Notifications\NotifyMerchantOnOrder;
use App\Notifications\NotifyMerchantOnDebt;
use App\CompanySettings;
use Carbon\Carbon;
use App\Product;
use App\MainOrder;

class Order extends Model
{
    protected $fillable = ['vendor_id', 'product_id', 'package_id', 'order_code', 'quantity', 'color_id', 'paid_amount', 'main_order_id'];

    protected $hidden = ['vendor_id', 'product_id', 'package_id', 'order_code', 'main_order_id'];

    public function product() {
        return $this->belongsTo('App\Product');
    }

    public function mainOrderId() {
        return $this->belongsTo('App\MainOrder', 'main_order_id', 'id');
    }

    public function package() {
        return $this->belongsTo('App\SpecialPackage');
    }

    public function vendor() {
        return $this->belongsTo('App\User', 'vendor_id', 'id');
    }

    public function color() {
        return $this->belongsTo('App\Color');
    }

    public function getUserInOrder($role) {
        if($role == 1) {
            return $this::latest()->get()->groupBy('user_id');
        }
        else
        {
            return $this::with(['product', 'package'])
                    ->whereHas('product', function($query) {
                        $query->where('user_id', auth()->user()->id);
                    })->orWhereHas('package', function($query) {
                        $query->where('user_id', auth()->user()->id);
                    })->latest()->get();
        }
    }

    
    public function getTheOrder($id) {
        if(auth()->user()->role == 1) {
            return $this::with(['product', 'mainOrderId', 'mainOrderId.user', 'mainOrderId.user.addresses'])
                    ->where('main_order_id', $id)->get();
        } else {
            return $this::with(['product', 'mainOrderId', 'mainOrderId.user'])
                    ->where('vendor_id', auth()->user()->id)
                    ->where('main_order_id', $id)->get();
        }
    }
    

    public function convertDate($date) {
        return Carbon::parse($date);
    }

    public function createOrder($userId, $productId, $packageId, $orderCode, $paymentMethod, $color, $quantity, $vendor_id, $debt, $paid_amount, $main_order_id)
    {

    	$order = $this::create([

            'product_id' => $productId,
    		'package_id' => $packageId,
    		'order_code' => $orderCode,
    		'payment_method' => $paymentMethod,
            'color_id' => $color,
            'quantity' => $quantity,
            'paid_amount' => $paid_amount,
            'main_order_id' => $main_order_id,
            'vendor_id' => $vendor_id

    	]);

        $creditUser = User::where('id', $vendor_id)->where('credit', 1)->count();

        if ($creditUser > 0 && !is_null($debt)) {
            $_debt = new Debt();
            $_debt->addDebt($vendor_id, $debt, $order->id);    
        }

        $_order = $this::find($order->id);
        $user = User::find($userId);
        User::find($vendor_id)->notify(new NotifyMerchantOnOrder($_order, $user));
        User::where("role", 1)->first()->notify(new NotifyMerchantOnOrder($_order, $user));

    }

    public function updatePaymentStatus($paymentStatus, $id)
    {
    	$order = $this::find($id);
    	$order->payment_status = $paymentStatus;
    	$order->save();
    }

   
    // public function order_count($userId, $productId, $quantity, $color, $packageId)
    // {
    //     if($productId !== null)
    //         $product_count = $this::where('user_id', $userId)->where('product_id', $productId)->where('quantity', $quantity)->where('color_id', $color)->count();
    //     elseif($packageId !== null)
    //         $product_count = $this::where('user_id', $userId)->where('package_id', $packageId)->where('quantity', $quantity)->where('color_id', $color)->count();


    //     return $product_count;
    // }

    public function destroyOrder($id) {
        $order = $this::with('mainOrderId.order')->find($id);
        $mainOrder = new MainOrder();
        ($order->mainOrderId->order->count() == 1) ? $mainOrder->destroyOrder($order->mainOrderId->id) : $this::find($id)->delete();
    }

    public function getAllMonthsOrDaysOrYrs($type)
    {
        $date_array = [];

        if ($type == 'd')
        {

                $orders_dates = $this::with(['product'])
                                        ->whereHas('product', function($query) {
                                            $query->where('user_id', auth()->user()->id);    
                                        })
                                        ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                                        ->pluck('created_at');

        }
        elseif ($type == 'comparison')
        {

                $orders_dates = $this::with(['product'])
                                        ->whereHas('product', function($query) {
                                            $query->where('user_id', auth()->user()->id);    
                                        })
                                        ->whereBetween('created_at', [Carbon::now()->startOfMonth()->subMonth(), Carbon::now()->endOfMonth()])
                                        ->pluck('created_at');

        }
        elseif ($type == 'this_yr')
        {
                $orders_dates = $this::whereBetween('created_at', [Carbon::now()->startOfYear(), Carbon::now()->endOfYear()])
                                        ->pluck('created_at');
        }

        elseif ($type == 'last_yr')
        {
                $orders_dates = $this::whereBetween('created_at', [Carbon::now()->startOfYear()->subYear(), Carbon::now()->endOfYear()->subYear()])->pluck('created_at');
        }

        elseif ($type == 'before_yr')
        {
                $orders_dates = $this::whereNotBetween('created_at', [Carbon::now()->startOfYear()->subYear(), Carbon::now()->endOfYear()])->pluck('created_at');
        }
        else
        {
            $orders_dates = $this::with(['product'])
                                        ->whereHas('product', function($query) {
                                            $query->where('user_id', auth()->user()->id);    
                                        })
                                        ->oldest()->pluck('created_at');
        }

        $orders_dates = json_decode($orders_dates);
        
        if(!empty($orders_dates))
        {
      
            foreach ($orders_dates as $unformatted_date) {
      
                $date = new \DateTime($unformatted_date);

                if($type == 'm' || $type == 'comparison' || $type == 'this_yr' || $type == 'last_yr' || $type == 'before_yr') 
                {

                    $month_no = $date->format('m');
                    $month_name = $date->format('M');
                    $date_array[$month_no] = $month_name;

                }

                elseif($type == 'd')
                {

                    $day_no = $date->format('d');
                    $day_name = $date->format('D');
                    $date_array[$day_no] = $day_name;

                }

                elseif($type == 'yr')
                {

                    $year = $date->format('Y');
                    $date_array[$year] = $year;

                }

            }
      
        }

        return $date_array;
    }



    public function getProductPrice($date, $type, $role)
    {
        $yr = date('Y');
        $month = date('m');
        $_settings = new CompanySettings();

        if($type == 'm')
            $orders = $this::whereMonth('created_at', $date)->get();

        if($type == 'yr')
            $orders = $this::whereYear('created_at', $date)->get();

        if($type == 'd' )
            $orders = $this::whereDate('created_at', $yr.'-'.$month.'-'.$date)->get();

        $price = [];

        foreach ($orders as $order) {

            $vendorAttemptOrder = $this::with('product')
            ->whereHas('product', function($query) use ($order) {
                $query->where('id', $order->product_id);
            })->latest()->get()->groupBy('product_id');

            $product = null;

            if($role == 'admin') {
                foreach($vendorAttemptOrder as $key => $value) {
                    $product = Product::find($key);
                    $vendor = User::find($product->user_id);
                    if($vendor->business_type == "business")
                        $product = $order->product()->sum('current_price') * ($_settings::first()->cut_business/100);
                    elseif($vendor->business_type == "individual")
                        $product = $order->product()->sum('current_price') * ($_settings::first()->cut_individual/100);
                }
            }

            if($role == 'vendor') {
                $me = User::find(auth()->user()->id);
                if($me->business_type == "business")
                        $product = $order->product()->sum('current_price') * (1 - $_settings::first()->cut_business/100);
                elseif($me->business_type == "individual")
                        $product = $order->product()->sum('current_price') * (1 - $_settings::first()->cut_individual/100);
            }
            
            array_push($price, $product);
        
        }

        return array_sum($price);
    
    }


}
