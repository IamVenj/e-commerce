<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{
    protected $fillable = ['product_id', 'user_id'];

    public function product()
    {
    	return $this->belongsTo('App\Product');
    }


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function add_wish($product_id, $user_id)
    {

    	$wishCount = $this::where('product_id', $product_id)->where('user_id', $user_id)->count();

    	if($wishCount == 0)
    	{

	    	$this::create([

	    		'product_id' => $product_id,

	    		'user_id' => $user_id

	    	]);

    	}


    }

    public function destroyWish($id)
    {
    	$this::find($id)->delete();
    }
}
