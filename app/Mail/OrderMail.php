<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\CompanySettings;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;
    public $mainOrder, $settings;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mainOrder, CompanySettings $settings)
    {
        $this->settings = $settings;
        $this->mainOrder = $mainOrder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->mainOrder['route'] == "web") {
            return $this->subject("Order successfull -- ".$this->settings->company_name)
                    ->from('support@symmart.com')
                    ->to($this->mainOrder['email'])
                    ->view('pre-login.partials.email.orderEmail')->with(["buyer"=>$this->mainOrder, "settings"=>$this->settings]);
        }
        elseif ($this->mainOrder['route'] == "api") {
            return $this->subject("Order successfull -- ".$this->settings->company_name)
                    ->from('support@symmart.com')
                    ->to($this->mainOrder['email'])
                    ->view('pre-login.partials.email.orderEmailApi')->with(["buyer"=>$this->mainOrder, "settings"=>$this->settings]);
        }
    }
}
