<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\CompanySettings;
use App\User;

class VendorEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $vendor, $settings;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $vendor, CompanySettings $settings)
    {
        $this->vendor = $vendor;
        $this->settings = $settings;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Welcome to ".$this->settings->company_name)
                    ->from('support@symmart.com')
                    ->to($this->vendor->email)
                    ->view('pre-login.partials.email.vendor-mail')->with(["vendor"=>$this->vendor, "settings"=>$this->settings]);
    }
}
