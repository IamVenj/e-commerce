<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\CompanySettings;
use App\Contact;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $email, $settings;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $request, CompanySettings $settings)
    {
        $this->email = $request;
        $this->settings = $settings;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->email->subject)
                    ->from($this->email->email)
                    ->to('support@symmart.com')
                    ->view('pre-login.partials.email.contact-mail')->with(["email"=>$this->email, "settings"=>$this->settings]);
    }
}
