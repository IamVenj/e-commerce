<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProductImage;
use App\Product;
use App\PackageProduct;

class SpecialPackage extends Model
{
	protected $fillable = ['package_name', 'discount_price', 'slug', 'availability', 'user_id', 'discount_percent'];

	public function packageProduct() {
		return $this->hasMany('App\PackageProduct', 'package_id', 'id');
	}

	public function images() {
		return $this->hasMany('App\ProductImage', 'package_id', 'id');
	}

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function thisCreate($product, $package_name, $discountPercent, $availability, $slug, $user_id) {
		$price_array = [];

        for ($i=0; $i < count($product); $i++) {
        	$productPrice = Product::find($product[$i])->current_price;
        	array_push($price_array, $productPrice);
        }

    	$productSum = array_sum($price_array);
    	$discountPrice = $productSum - $productSum * ($discountPercent / 100);

		$package = SpecialPackage::create([
			'package_name' => $package_name,
			'discount_price' => $discountPrice,
			'discount_percent' => $discountPercent,
			'availability' => $availability,
			'slug' => $slug,
			'user_id' => $user_id
		]);

		return $package;
	}

	public function createPackage($product, $package_name, $images, $slug, $availability, $discountPercent, $user_id) {
		$package = $this->thisCreate($product, $package_name, $discountPercent, $availability, $slug, $user_id);

		$PP = new PackageProduct();
        for ($i=0; $i < count($product); $i++) {
        	$PP->createPackageRelation($package->id, $product[$i]);
        }

        $this->addImageForPackage($images, $package->id);
	}

	public function addImageForPackage($images, $packageId) {
		$productImage = new ProductImage();
		if(is_array($images)) {
	        foreach ($images as $image) {
	            $image_name = $image->getRealPath();
	            $return = \Cloudinary\Uploader::upload($image_name, array("folder"=>"package"));
	        	$productImage->addProductImageForASingleProduct(null, $return['secure_url'], $return['version'], $packageId);
	        }
		} else {
			$image_name = $images->getRealPath();
            $return = \Cloudinary\Uploader::upload($image_name, array("folder"=>"package"));
        	$productImage->addProductImageForASingleProduct(null, $return['secure_url'], $return['version'], $packageId);
		}
	}

	public function updatePackage($package_name, $discountPercent, $slug, $availability, $id, $sum) {
    	$discountPrice = $sum - $sum * ($discountPercent / 100);
    	$package = $this::find($id);
		$package->package_name = $package_name;
		$package->discount_price = $discountPrice;
		$package->discount_percent = $discountPercent;
		$package->slug = $slug;
		$package->availability = $availability;
		$package->save();
	}

	public function updateDiscountPrice($discountPrice, $id) {
		$package = $this::find($id);
		$package->discount_price = $discountPrice;
		$package->save();
	}

	public function updateImageOfPackage($image, $image_id, $packageId) {
        $image_name = $image->getRealPath();
        $result = \Cloudinary\Uploader::upload($image_name, array("folder"=>"package"));
        
        $productImage = new ProductImage();
        // $productImage->destroyImage($image_id);
		$productImage->updateProductImageForASingleProduct(null, $result['secure_url'], $result['version'], $packageId, $image_id);
	}

	// public function destroyPackage($id) {
	// 	$package = $this->getPackage($id);
	// 	for ($i=0; $i < $package->count(); $i++) { 
	// 		foreach ($package[$i]->images as $image) {
	//         	Cloudder::destroy($image);
	// 		}
	// 		$package[$i]->delete();
	// 	}
	// }

	public function destroyPackage($id) {
		$package = $this::find($id);
		// foreach ($package->images as $image) {
  //       	Cloudder::destroy($image);
		// }
		$package->delete();
	}

	public function getPackage($id) {
		return $this::with(['images', 'packageProduct'])->find($id);
	}

	public function getPackageUsingName($name) {
		return $this::with(['images', 'packageProduct'])->where('package_name', $name)->get();
	}

	// public function getProduct($id) {
	// 	return $this::with(['packageProduct'])->find($id);
	// }

	public function updateAvailableItems($packageId, $qty) {
		$package = $this::find($packageId);
		$package->availability = $package->availability - $qty;
		$package->save();
	}
}
