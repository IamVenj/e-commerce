<?php

return [

	'short_amh' => 'AMH',

    'short_eng' => 'ENG',

    'amh' => 'Amharic',

    'eng' => 'English',

    'welcome' => 'Welcome',

    'filter_price' => 'Filter By Price',

    'filter_color' => 'Filter By Color',

    'filter_size' => 'Filter By Size',

    'filter_vendor' => 'Filter by Vendor',

    'all_categories' => 'All Categories',

    'categories' => 'Categories',

    'search' => 'Enter your keyword',

    'my_account' => 'My Account',

    'login' => 'Login',

    'my_cart' => 'My Cart',

    'view_cart' => 'View Cart',

    'join' => 'Join',

    'my_wishlist' => 'My Wishlist',

    'my_address' => 'My Address',

    'my_orders' => 'My Orders',

    'logout' => 'Logout',

    'cust_service' => 'Customer Service',

    'about_us' => 'About Us',

    'ship_return' => 'Shipping & Return',

    'ship_guide' => 'Shipping Guide',

    'faq' => 'FAQ',

    'stay_connected' => 'Stay Connected',

    'enter_email' => 'Enter Email Address',

    'join_letter' => 'Join Newsletter',

    'special_offers' => 'Special Offers',

    'rights' => 'All rights reserved',

    'copyright' => 'Copyright',

    'developer_note' => 'Designed & Developed with',

    'by' => 'by',

    'developer_name' => 'J-Dev',

    'best_seller' => 'Best Selling Products',

    'quick_view' => 'Quick view',

    'popular_categories' => 'Popular Categories',

    'latest_products' => 'Latest Products',

    'home' => 'Home',

    'cart' => 'Cart',

    'empty_cart' => 'Your cart is empty',

    'empty_added_cart' => 'You have not added any item to your cart',

    'order_review' => 'Order Review',

    'product' => 'Product',

    'price' => 'Price',

    'quantity' => 'Quantity',

    'include_delivery' => 'Include Delivery',

    'total' => 'Total',

    'birr' => 'Birr',

    'SUBTOTAL' => 'SUBTOTAL',

    'DELIVERY' => 'DELIVERY',

    'no_delivery' => 'No Delivery',

    'DISCOUNT' => 'DISCOUNT',

    'payment_methods' => 'Payment Methods',

    'Yenepay' => 'Yenepay',

    'cash_delivery' => 'Cash on Delivery',

    'place_order' => 'Place Order',

    'left' => 'Left',

    'Availability' => 'Availability',

    'Brand' => 'Brand',

    'Size' => 'Size',

    'Color' => 'Color',

    'add_cart' => 'Add to Cart',

    'Description' => 'Description', 

    'Reviews' => 'Reviews',

    'Review' => 'Review',

    'Location' => 'Location',

    'Edit' => 'Edit',

    'your_rating' => 'Your Rating',

    'your_review' => 'Your Review',

    'Update' => 'Update',

    'Delete' => 'Delete',

    'write_review' => 'Write Your Review',

    'logging_required' => 'Logging in is required',

    'emailWillNotBePublished' => 'Your email address will not be published',

    'Submit' => 'Submit',

    'Related' => 'Related',

    'sold_by' => 'Sold by',

    'ItemDeliveredState' => 'Is the item delivered?',

    'changeDeliveryState' => 'Change Delivery status',

    'yes' => 'Yes',

    'no' => 'No',

    'Products' => 'Products',

    'sort_by' => 'Sort By',

    'Latest' => 'Latest',

    'Price_High_Low' => 'Price - High to Low',

    'Price_Low_High' => 'Price - Low to High',

    'Show' => 'Show',

    'search_results' => 'Search results',

    'have_account' => 'Have an account ?',

    'Register' => 'Register',

    'forgot_pass' => 'Forgot Password',

    'reset' => 'Reset-Password',

    'already_user' => 'Already a user ?',

    'seller_acct' => 'Seller Account',

    'password' => 'Password',

    'email' => 'Email',

    'c_password' => 'Confirm Password',

    'fill_profile' => 'Fill in Basic Profile',

    'profile' => 'Profile',

    'address' => 'Address',

    'finish' => 'Finish',

    'profile_image' => 'Profile Image',

    'choose_image' => 'Choose Image',

    'full_name' => 'Full Name',

    'phone_number' => 'Phone Number',

    'revieve_news' => 'I would like to receive newsletters',

    'terms_conditions' => 'I Agree with the terms and conditions ',
    
    'terms_and_conditions' => 'Terms and conditions ',

    'shop_name' => 'Shop Name',

    'about_shop' => 'About Your Shop',

    'add_more' => 'Add More',

    'select_address' => 'Select Address Inputs',

    'mer_code' => 'Merchant Code (Seller Code)',

    'pdt_token' => 'PDT (Payment Data Transfer) token',

    'optional' => 'optional',

    'customer_delivery' => 'We deliver any material to customers',

    'yene_setup' => 'Yenepay Setup',

    'Dashboard' => 'Dashboard',

    'customize_pages' => 'Customize Pages',

    'home_carousel' => 'Home Carousel',

    'special_offers' => 'Special Offers',

    'Category' => 'Category',

    'Vendor_Reviews' => 'Vendor Reviews',

    'Users' => 'Users',

    'Debts' => 'Debts',

    'Newsletter' => 'Newsletter',

    'Orders' => 'Orders',

    'My_Debt' => 'My Debt',

    'Create_Product' => 'Create Product',

    'View_Products' => 'View Products',

    'SIDEBAR_SKINS' => 'SIDEBAR SKINS',

    'Light' => 'Light',

    'Dark' => 'Dark',

    'HEADER_SKINS' => 'HEADER SKINS',

    'Settings' => 'Settings',

    'developer_note2' => 'Hand-crafted & made with',

    'Create_Category' => 'Create Category',

    'category_desc' => 'Create category and sub-category',

    'Category_Name' => 'Category Name',

    'Select_Category' => 'Select Category',

    'Category_Image' => 'Category Image',

    'Create' => 'Create',

    'Main_Categories' => 'Main-Categories',

    'File_upload' => 'File upload',

    'Manage' => 'Manage',

    'Update_Image' => 'Update Image',

    'Welcome_back' => 'Welcome back',

    'analytics_statement' => 'Your analytics dashboard',

    'Analytics' => 'Analytics',

    'Cash_sales' => 'Cash sales',

    'Sales_Comparison' => 'Sales Comparison',

    'Sales_last_month' => 'Sales last month',

    'Gross_sales_of' => 'Gross sales of',

    'Previous' => 'Previous',

    'Next' => 'Next',

    'View_Your_Monthly_income' => 'View Your Monthly income',

    'Monthly_income' => 'Monthly income',

    'Sales_income' => 'Sales income',

    'Purchases' => 'Purchases',

    'Yearly_sales' => 'Yearly sales',

    'View_Your_Yearly_sales' => 'View Your Yearly sales',

    'View_your_Daily_deposits' => 'View your Daily deposits',

    'Daily_deposits' => 'Daily deposits',

    'Total_sales' => 'Total sales',

    'Gross_sales_over_the_years' => 'Gross sales over the years',

    'Vendors' => 'Vendors',

    'Vendor' => 'Vendor',

    'This_is_the_list_of_total_customers_for' => 'This is the list of total customers for ',

    'customers' => 'customers',

    'customer' => 'customer',

    'Debt' => 'Debt',

    'Order' => 'Order',

    'Item_SKU' => 'Item SKU',

    'Debt_Status' => 'Debt Status',

    'Total_Debt' => 'Total Debt',

    'Unpaid' => 'Unpaid',

    'Paid' => 'Paid',

    'check_debt_status' => 'If the debt is fully covered please click on the check button below',

    'Create_Newsletter' => 'Create Newsletter',

    'Title' => 'Title',

    'Slug' => 'Slug',

    'Send_Newsletter' => 'Send Newsletter',

    'order_date' => 'Order Date',

    'Delivery_status' => 'Delivery status',

    'payment_method' => 'Payment Method',

    'Payment_status' => 'Payment status',

    'Sent_Date' => 'Sent Date',

    'Pickup' => 'Pickup',

    'New' => 'New',

    'Delivered' => 'Delivered',

    'View_Orders' => 'View Orders',

    'Customer_Information' => 'Customer Information',

    'Delivery_Information' => 'Delivery Information',

    'Order_Detail' => 'Order Detail',

    'Product_Information' => 'Product Information',

    'Payment_Information' => 'Payment Information',

    'customers_wants_pickup' => 'Customer says i will Pickup the item',

    'customer_wants_delivery' => 'Customer wants the item delivered to his/her address',

    'NotDeliveredYet' => 'Not Delivered Yet',

    'available' => 'available',

    'Pending' => 'Pending',

    'Not_Paid' => 'Not Paid',

    'Total_Price' => 'Total Price',

    'cash_delivery_pickup' => 'Cash on Delivery / picking up the item ',

    'PaymentTransactionID' => 'Payment Transaction ID',

    'Product_Name' => 'Product Name',

    'unique_code' => 'Product Unique Code (SKU)',

    'Product_Description' => 'Product Description',

    'Upload_Images' => 'Upload Images',

    'show_images' => 'Show selected images',

    'select_pColor' => 'Select Product Color',

    'Select_Colors' => 'Select Colors',

    'Product_Brand' => 'Product Brand',

    'old_price' => "Products' Old/Base Price",

    'current_price' => "Products' Current Price",

    'Price_in_birr' => 'Price in birr',

    'how_many_available' => 'How many are available',

    'Number_of_stock' => 'Number of stock',

    'ProductsLocation' => "Product's Location",

    'Select_Location' => 'Select Location',

    'All' => 'All',

    'Add_Products' => 'Add Products',

    'Stores' => 'Stores',

    'Store' => 'Store',

    'Manage_Images' => 'Manage Images',

    'Product_Reviews' => 'Product Reviews',

    'Manage_Colors' => 'Manage Colors',

    'Replace_Color' => 'Replace Color',

    'Replace_Image' => 'Replace Image',

    'Type_of_Users' => 'Type of Users',

    'Rating' => 'Rating',

    'View_Reviews' => 'View Reviews',

    'Company_Name' => 'Company Name',

    'company_slug' => 'Very Short Description about the company',

    'charge_per_percent' => 'How much does the company charge per percent',

    'discount_product' => 'What is the required amount of products purchased for discount',

    'amount_of_products' => 'amount of products',

    'discount_for' => 'discount for',

    'products_per_percent' => 'products per percent',

    'Discount_per_percent' => 'Discount per percent',

    'facebook' => 'facebook',

    'twitter' => 'twitter',

    'Google_plus' => 'Google +',

    'linked_in' => 'Linked In',

    'Logo' => 'Logo',

    'Upload_Logo' => 'Upload Logo',

    'Select_Logo' => 'Select Logo',

    'Current_Logo' => 'Current Logo',

    'Favicon' => 'Favicon',

    'Upload_Favicon' => 'Upload Favicon',

    'Select_Favicon' => 'Select Favicon',

    'Current_Favicon' => 'Current Favicon',

    'select_vendor' => 'Select Vendor',

    'change_pic' => 'Change Profile Picture',

    'change_delivery_option' => 'Change Delivery Option',

    'Change_Password' => 'Change Password',

    'Name' => 'Name',

    'UpdateProfilePicture' => 'Update Profile Picture',

    'Save' => 'Save',

    'UpdateDeliveryOption' => 'Update Delivery Option',

    'Close' => 'Close',

    'Current_Password' => 'Current Password',

    'NewPassword' => 'New Password',

    'Edit_Address' => 'Edit-Address',

    'Add_Address' => 'Add-Address',

    'Add' => 'Add',

    'Remove_Map' => 'Remove Map',

    'Remove' => 'Remove',

    'Edit_Category' => 'Edit Category',

    'UpdateImageCategory' => 'Update Image Category',

    'Upload_Image' => 'Upload Image',

    'Delete_Category' => 'Delete Category',

    'DeleteOffer' => 'Delete Offer',

    'Add_colors' => 'Add Colors',

    'OK' => 'Ok',

    'Selected_Images' => 'Selected Images',

    'AddImage'=> 'Add Image',

    'Edit_Carousel' => 'Edit Carousel',

    'Delete_Carousel' => 'Delete Carousel',

    'Send' => 'Send',

    'Edit_Newsletter' => 'Edit Newsletter',

    'Delete_Newsletter' => 'Delete Newsletter',

    'Save_Changes' => 'Save Changes',

    'no_items' => 'There are no items here',

    'buy' => 'Buy',

    'AddVendors' => 'Add Vendors',

    'RememberMe' => 'Remember Me',

    'deactivate' => 'Deactivate',

    'business_type' => 'Business Type',

    'recommended_offers' => 'Recommended Offers',

    'delivery_price_per_km' => 'Delivery Price per km',

    'delivery_confirmation' => 'Delivery Confirmation',

    'time' => 'Time',

    'update' => 'Update',

    'vendor_item_list' => 'Vendor Item List',

    'Question' => 'Question',

    'Answer' => 'Answer',

    'cut_for_individual' => 'Cut for individual',

    'cut_for_business' => 'Cut for business',

    'View_Items' => 'View Items',

    'View' => 'View',

    'tin' => 'Tin',

    'select_PDF_file' => 'Select PDF File',

    'credit_hint' => 'Credit Support Hint',

    'hint' => 'Hint',

    'Bank' => 'Bank',

    'on_delivery_payment_limit' => 'Cash on Delivery payment Limit',

    'Package' => 'Package',

    'View_Packages' => 'View Packages',

    'create_packages' => 'Create Packages',

    'add_package' => 'Add Package',

    'edit_products' => 'Edit Products',

    'Package_Name' => 'Package Name',

    'Package_Description' => 'Package Description',

    'Select_products' => 'Select products',

    'discountPercent' => 'Discount Percent',

    'Payment_Method' => 'Payment Method',

    'symmart_standard_price' => 'Symmart Standard Price',

    'symmart_express_price' => 'Symmart Express Price',

    'symmart_premium_price' => 'Symmart Premium Price',

    'symmart_national_price' => 'Symmart National Price',

    'My_Credit' => 'My Credit',

    'package_offers' => 'Package Offers'

];