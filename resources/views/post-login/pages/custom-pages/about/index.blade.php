@extends('post-login.index.index')
@section('content')

<div class="col-12 grid-margin stretch-card">
  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">

    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>
    <li class="breadcrumb-item active" aria-current="page"><span>{{__('app.about_us')}}</span></li>

  </ol>
</div>
<div class="col-12 grid-margin stretch-card">
	{{-- <div class="card"> --}}
    	{{-- <div class="card-body"> --}}
			<form action="/update/about" method="post">
			  	@csrf
			  	<div class="form-group">
			  		<textarea class="summernote" name="about">{{ $about }}</textarea>
			  	</div>
			  	<button class="btn btn-success" type="submit">update</button>
			</form>
		{{-- </div> --}}
	{{-- </div> --}}
</div>

@include('_session_.error2')
@include('_session_.success2')

@endsection