@extends('post-login.index.index')
@section('content')

<div class="col-12 grid-margin stretch-card">
  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">

    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>
    <li class="breadcrumb-item active" aria-current="page"><span>{{__('app.ship_guide')}}</span></li>

  </ol>
</div>
<div class="col-12 grid-margin stretch-card">
	{{-- <div class="card"> --}}
    	{{-- <div class="card-body"> --}}
			<form action="/update/shipping/guide" method="post" enctype="multipart/form-data">
			  	@csrf
			  	<div class="form-group">
			  		<textarea class="summernote" name="guide">{{ $guide }}</textarea>
			  	</div>
			  	<div class="form-group">
		            <input type="file" id="image-category-upload" name="shipping_guide_pdf" class="file-upload-default">		        
		            <div class="input-group col-xs-12">
		              <input type="text" class="form-control file-upload-info" id="file-upload-info" disabled="" placeholder="Select PDF File">
		              <span class="input-group-append">			          
		                <button class="file-upload-browse btn btn-primary" type="button">{{__('app.select_PDF_file')}}</button>
		              </span>
		            </div>
		        </div>
		        <div class="form-group">
			        @if(\Storage::exists('public/uploads/document/Symmart-Shipping-Guide.pdf'))
	                    <a class="btn btn-warning btn-block" target="_blank" href="{{ Storage::url('public/uploads/document/Symmart-Shipping-Guide.pdf') }}">Download PDF<span class="mdi mdi-download" style="margin-left: 10px;"></span></a>
	                @endif
            	</div>
			  	<button class="btn btn-success" type="submit">update</button>
			</form>
		{{-- </div> --}}
	{{-- </div> --}}
</div>

@include('_session_.error2')
@include('_session_.success2')

@endsection