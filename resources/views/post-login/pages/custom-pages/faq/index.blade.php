@extends('post-login.index.index')
@section('content')

<div class="col-12 grid-margin stretch-card">
  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">

    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>
    <li class="breadcrumb-item active" aria-current="page"><span>{{__('app.faq')}}</span></li>

  </ol>
</div>

<div class="col-12 grid-margin stretch-card">
  	<div class="card">
    	<div class="card-body">
			<form action="/create/faq" method="post">
			  	@csrf
			  	<div class="form-group">
			  		<label>{{__('app.Question')}}</label>
			  		<textarea class="form-control" rows="5" name="question" placeholder="The Question"></textarea>
			  	</div>
			  	<div class="form-group">
			  		<label>{{__('app.Answer')}}</label>
			  		<textarea class="form-control" rows="5" name="answer" placeholder="The answer to the question"></textarea>
			  	</div>
			  	<button class="btn btn-success"> Create</button>
			</form>
		</div>
	</div>
</div>

<div class="col-12 grid-margin stretch-card">
  	<div class="card">
    	<div class="card-body">
			<div class="table-responsive">
				<table class="order-listing table">
		            <thead>
		              	<tr>
		                	<th>{{__('app.Question')}}</th>
		                  	<th>{{__('app.Answer')}}</th>
		                  	<th></th>
		              	</tr>
		            </thead>
		            <tbody>
		                @foreach($faqz as $faq)
		              	<tr>
		                	<td>{{$faq->question}}</td>
		                	<td>{{$faq->answer}}</td>
		                	<td>
		                 		<div class="ticket-actions mb-3">
				                    <div class="btn-group dropdown">

				                      <button type="button" class="btn btn-secondary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				                          {{__('app.Manage')}}
				                      </button>

            		               		@include('post-login.partials.modal.faqModal')
				                      <div class="dropdown-menu">
				                        <a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#edit-faq'.$faq->id; ?>">
				                          <i class="mdi mdi-pencil mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Edit')}}</a>

				                        <div class="dropdown-divider"></div>
				                        <a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#delete-faq'.$faq->id; ?>">
				                          <i class="mdi mdi-delete-forever mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Delete')}}</a>

				                      </div>
				                    </div>
				                  </div>
		                	</td>
		                </tr>
		                @endforeach
		            </tbody>
		        </table>
			</div>
		</div>
	</div>
</div>
@include('_session_.error2')
@include('_session_.success2')

@endsection