@extends('post-login.index.index')

@section('content')



<div class="col-12 grid-margin stretch-card">

  	<div class="card">

    	<div class="card-body">

          	<h4 class="card-title" style="font-size: 20px;">

          		{{__('app.Newsletter')}}

          	</h4>

          	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">

            	{{__('app.Create_Newsletter')}}

          	</p>

          	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>

          	<form class="forms-sample" action="/newsletter/store" method="post">

          		@csrf

          		<div class="form-group">

                  	<label for="title" style="font-size: 20px;">{{__('app.Title')}}</label>

                  	<input type="text" class="form-control" id="title" name="news_title" placeholder=" {{__('app.Title')}}" required>

                </div>
                

                <div class="form-group">

                  	<label for="slug" style="font-size: 20px;">{{__('app.Description')}}</label>

                  	<textarea class="form-control" rows="10" id="slug" name="slug" required></textarea>

                </div>

        		<button type="submit" class="btn btn-success mr-2 mt-5">{{__('app.Create')}}</button>

          	</form>

        </div>

    </div>

</div>


<div class="col-12 grid-margin stretch-card">

	<div class="card">

	  	<div class="card-body">

		    <h4 class="card-title">

		    	{{__('app.Newsletter')}}

		    </h4>

		    <div class="row">

		      	<div class="col-12">

			        <div class="table-responsive">

			          <table class="order-listing table">

			            <thead>

			              	<tr>

			                  	<th>#</th>

			                  	<th>{{__('app.Title')}}</th>

			                  	<th>{{__('app.Slug')}}</th>

			                  	<th></th>
			                  
			              	</tr>

			            </thead>

			            <tbody>

			              	<?php $count = 0;?>

			              	@foreach($newsletters as $newsletter)

			        		
			              	
			              	<?php $count = $count + 1;?>
			              
			              	<tr>

				                <td>{{$count}}</td>

				                <td>{{$newsletter->title}}</td>

				                <td>{{$newsletter->slug}}</td>
				                
				                <td>
			                	
			                		<div class="btn-group dropdown">

			                			@include('post-login.partials.modal.newsletter-modal')

					                	<button type="button" class="btn btn-secondary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								                            	
					                    {{__('app.Manage')}}

					                	</button>

				                		<div class="dropdown-menu">

					                  <a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#send-newsletter'.$newsletter->id; ?>">

				                    		<i class="mdi mdi-send mr-2" style="color: rgba(0,0,0,0.5);"></i>

				                    		{{__('app.Send_Newsletter')}}

				                    	</a>

					                  	
					                  <div class="dropdown-divider"></div>

					                  	
					                    <a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#edit-newsletter'.$newsletter->id; ?>">

				                    		<i class="mdi mdi-tooltip-edit mr-2" style="color: rgba(0,0,0,0.5);"></i>

				                    		{{__('app.Edit')}}

				                    	</a>

					                  	
					                  <div class="dropdown-divider"></div>

					                  	
					                    <a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#delete-newsletter'.$newsletter->id; ?>">

				                    		<i class="mdi mdi-delete-forever mr-2" style="color: rgba(0,0,0,0.5);"></i>

				                    		{{__('app.Delete')}}

				                    	</a>

				                		</div>
				                	</div>

			                	</td>
			                
			              	</tr>

			              	@endforeach

			            </tbody>

			          </table>

			        </div>

		      	</div>

		    </div>

	  	</div>

	</div>

</div>


@include('_session_.success2')
@include('_session_.error2')

@endsection