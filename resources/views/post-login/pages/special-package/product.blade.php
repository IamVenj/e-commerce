@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">

    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>
    <li class="breadcrumb-item" style="color: #4d83ff;">{{__('app.Package')}}</li>
    <li class="breadcrumb-item active" aria-current="page"><span>{{__('app.View_Packages')}}</span></li>

  </ol>

</div>

<div class="col-12 grid-margin stretch-card">

    <a href="/package" style="text-decoration: none;"><button class="btn btn-success btn-block" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">{{__('app.View_Packages')}}<span class="mdi mdi-eye ml-2"></span></button></a>

</div>


<div class="col-12 grid-margin stretch-card">
  <div class="card">

      <div class="card-body">
        <div class="row">

        @foreach($packageProducts as $packageProduct)

        <div class="col-md-4 mt-3">
            <div class="card" style="border-top-left-radius: 20px; border-bottom-right-radius: 20px;" >

              @if($packageProduct->product->productImage()->count() > 0)

              <img class="card-img-top" id="image-public" style="height: 250px; object-fit: cover; border-top-left-radius: 20px; border-bottom-right-radius: 20px;" src="{{ $packageProduct->product->productImage()->first()->image_public_id }}" alt="{{$packageProduct->product->productImage()->first()->image_public_id}}">

              @else

              <div style="padding-top: 68%; border-top-left-radius: 20px; border-bottom-right-radius: 20px;  background: linear-gradient(120deg, #00e4d0, #429FFD);" alt="Card image cap"></div>

              @endif

              <div class="card-body">

                  <h4 class="card-title mb-3" style="font-weight: bold;">{{$packageProduct->product->product_name}}</h4>
                  <div class="dropdown-divider"></div>

                  <p style="font-size: 14px;">{{str_limit($packageProduct->product->slug, 30)}}</p>
                  <div class="dropdown-divider"></div>

                  <p class="mb-3 mt-3" style="text-transform: uppercase; background: #e8e8e850; padding:10px 0px 10px 10px; border-top-left-radius: 20px; border-bottom-right-radius: 20px;"><span class="mdi mdi-view-dashboard mr-3"></span>{{$packageProduct->product->category()->first()->category_name}}</p>

                  <div class="dropdown-divider"></div>
                  <p class="mt-3"><span class="mdi mdi-tag mr-3"></span>{{$packageProduct->product->brand}}</p>

                  <div class="dropdown-divider"></div>
                  <p class="mt-3"><span class="mdi mdi-cash mr-3"></span>{{$packageProduct->product->current_price}}</p>              

                  <div class="dropdown-divider"></div>
                  <p class="mt-3"><span class="mdi mdi-map-marker-check mr-3"></span>{{$packageProduct->product->product_location}} @if($packageProduct->product->product_location == 'all') {{__('app.Stores')}} @else {{__('app.Store')}} @endif</p>

                  <div class="dropdown-divider"></div>  
                  <p class="mt-3"><span class="mdi mdi-calendar-multiple mr-3"></span>{{$packageProduct->product->availability}}</p>
                
                  <div class="dropdown-divider"></div>
                  <p class="mt-3"><span class="mdi mdi-barcode-scan mr-3"></span>{{$packageProduct->product->sku}}</p>
                  
                  <div class="dropdown-divider"></div>
                  <p class="mt-3"><span class="mdi mdi-eye mr-3"></span>Visits: {{$packageProduct->product->visits}}</p>
                  <div class="dropdown-divider"></div>
                          
                  @include('post-login.partials.modal.package-products-modal')

                  <div class="ticket-actions mb-3">
                      <div class="btn-group dropdown">

                          <button type="button" class="btn btn-secondary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{__('app.Manage')}}

                          </button>


                          <div class="dropdown-menu">

                            <a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#remove-product'.$packageProduct->product->id;?>">
                                <i class="mdi mdi-close mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Remove')}}</a>

                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#show-product'.$packageProduct->product->id;?>">
                                <i class="mdi mdi-eye mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.View_Item')}}</a>
                          </div>

                      </div>

                  </div>

                  <p class="card-text" style="color: rgba(0,0,0,0.3); font-size: 12px;"><i class="mdi mdi-clock"></i> {{$packageProduct->product->created_at->diffForHumans()}}</p> 

              </div>

          </div>

        </div>

        @endforeach
       
        @include('post-login.partials.modal.package-add-modal')

        <div class="col-md-12 mt-4">
          <a class="btn btn-primary btn-block" data-toggle="modal" href="#" data-target="#add-products">
            <span class="mdi mdi-plus"></span></a>
        </div>


      </div>

      {{$packageProducts->links()}}

    </div>

    </div>

</div>

@include('_session_.error2')
@include('_session_.success2')

@endsection