@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">

    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>
    <li class="breadcrumb-item" style="color: #4d83ff;">{{__('app.Special_package')}}</li>
    <li class="breadcrumb-item active" aria-current="page"><span>{{__('app.View_packages')}}</span></li>

  </ol>

</div>

<div class="col-12 grid-margin stretch-card">

    <a href="/create/package" style="text-decoration: none;"><button class="btn btn-success btn-block" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">{{__('app.add_package')}}<span class="mdi mdi-plus ml-2"></span></button></a>

</div>


<div class="col-12 grid-margin stretch-card">

  <div class="card">

      <div class="card-body">

        <div class="row">

        @foreach($packages as $package)

        <div class="col-md-4 mt-3">

            <div class="card" style="border-top-left-radius: 20px; border-bottom-right-radius: 20px;" >

              <img class="card-img-top" id="image-public" style="height: 250px; object-fit: cover; border-top-left-radius: 20px; border-bottom-right-radius: 20px;" src="{{ $package->images->first()['image_public_id'] }}" alt="{{ $package->images->first()['image_public_id'] }}">

              <div class="card-body">

                  <h4 class="card-title mb-3" style="font-weight: bold;">{{$package->package_name}}</h4>

                  <div class="dropdown-divider"></div>

                  <p style="font-size: 14px;">{{str_limit($package->slug, 30)}}</p>
                    <div class="dropdown-divider"></div>

                    <div class="row mt-3">

                      <div class="col-md-12">
                        
                        <p class=""><span class="mdi mdi-cash mr-3"></span>{{$package->discount_price}} ETB</p>
                        
                      </div>

                    </div>

                    <div class="dropdown-divider"></div>

                    <div class="row mt-3">

                        <div class="col-md-12">
                            
                            <p class=""><span class="mdi mdi-calendar-multiple mr-3"></span>{{$package->availability}} package available</p>

                        </div>

                    </div>

                    <div class="dropdown-divider"></div>


                    <div class="ticket-actions mb-3">

                    <div class="btn-group dropdown">

                        <button type="button" class="btn btn-secondary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          {{__('app.Manage')}}

                        </button>

                        @include('post-login.partials.modal.package-modal')

                        <div class="dropdown-menu">

                          <a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#edit-package'.$package->id;?>">
                          <i class="mdi mdi-pen mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Edit')}}</a>

                          <div class="dropdown-divider"></div>

                          <a class="dropdown-item" href="/package/{{$package->package_name}}/images/{{$package->id}}">
                          <i class="mdi mdi-image mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Manage_Images')}}</a>

                          <div class="dropdown-divider"></div>

                          <a class="dropdown-item" href="/package/{{$package->package_name}}/products/{{$package->id}}">
                          <i class="mdi mdi-package mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.edit_products')}}</a>

                          <div class="dropdown-divider"></div>

                          <a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#delete-package'.$package->id; ?>">
                          <i class="mdi mdi-delete-forever mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Delete')}}</a>

                        </div>

                    </div>

                </div>

                    <p class="card-text" style="color: rgba(0,0,0,0.3); font-size: 12px;"><i class="mdi mdi-clock"></i> {{$package->created_at->diffForHumans()}}</p> 

                </div>

            </div>

        </div>

        @endforeach

      </div>

      {{$packages->links()}}

    </div>

    </div>

</div>

@include('_session_.error2')

@include('_session_.success2')

@endsection