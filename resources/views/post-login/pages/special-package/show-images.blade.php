@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">
 
    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>
    <li class="breadcrumb-item" style="color: #4d83ff;">{{__('app.Packages')}}</li>
    <li class="breadcrumb-item"><a href="/package">{{__('app.View_Packages')}}</a></li>
    <li class="breadcrumb-item active" aria-current="page"><span>{{ $packageName }}</span></li>
 
  </ol>

</div>
	
<div class="col-10 grid-margin stretch-card">

	<a href="/package" style="text-decoration: none;"><button class="btn btn-success btn-block" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">{{__('app.View_Packages')}}<span class="mdi mdi-eye ml-2"></span></button></a>

</div>


<div class="col-12 grid-margin stretch-card">

	<div class="card">

	    <div class="card-body">

			<div class="row">
		                        
			    @foreach($productImages as $productImage)
			    
			    <div class='col-md-4'>

					<form action="/package/{{$productImage->id}}/image/destroy" method="POST">

						@csrf

						@method("DELETE")
						
			        	<button class="close" type="submit" style="color: #000; cursor: pointer;"><span class="mdi mdi-close-circle" style="color: #000;"></span></button>
			        	
					</form>

			        <img class="card-image loading" style='max-width: 100%; height: 300px; object-fit: cover; margin-bottom:20px; border-top-left-radius: 20px; border-bottom-right-radius: 20px; position: relative;' src="{{ $productImage->image_public_id }}" />

			        <div id="pending_status_edit"></div>

			        <form action="/package/{{$productImage->id}}/image/update/{{ $packageId }}" method="POST" enctype="multipart/form-data">

			        	@csrf

			        	@method('PATCH')


			        	<div class="row">
			        		
							<div class="col-md-10">
								
						        <div class="form-group">
						                
						            <input type="file" name="replaced_image" class="file-upload-default">
						          
						            <div class="input-group col-xs-12">
						          
						                <input type="text" class="form-control file-upload-info" id="file-upload-info" disabled="" placeholder="Upload Image">
						          
						                <span class="input-group-append">
						          
						                    <button class="file-upload-browse btn btn-success" type="button">{{__('app.Replace_Image')}}</button>
						          
						                </span>

						            </div>
						          
						        </div>

							</div>	        		

							<div class="col-2">
		        
						        <button class="btn btn-warning btn-block save-image-update" type="submit" ><span class="mdi mdi-check"></span></button>

							</div>

			        	</div>

			    	</form>

			    </div>

			    @endforeach

			    @include('post-login.partials.modal.package-image-modal')

			    @if($productImages->count() < 6)

			    <a class="btn btn-primary" data-toggle="modal" href="#" data-target="#add-image"><span class="mdi mdi-plus"></span></a>

			    @endif


			</div>

		</div>

	</div>
	
</div>

@include('_session_.error2')

@include('_session_.success2')

@endsection