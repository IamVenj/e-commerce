@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">
    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>
    <li class="breadcrumb-item" style="color: #4d83ff;">{{__('app.Package')}}</li>
    <li class="breadcrumb-item active" aria-current="page"><span>{{__('app.create_package')}}</span></li>
  </ol>

</div>

<div class="col-12 grid-margin stretch-card">

		<a href="/package" style="text-decoration: none;"><button class="btn btn-success btn-block" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">{{__('app.View_Packages')}}<span class="mdi mdi-eye ml-2"></span></button></a>

</div>


<div class="col-12 grid-margin stretch-card ">
  <div class="card">
     <div class="card-body">
      <h4 class="card-title">{{__('app.Create_package')}}</h4>
      <form class="forms-sample" action="/create/package" method="POST" enctype="multipart/form-data">

        @csrf

        <input type="hidden" name="userID" value="{{ $userId }}">

      	<div class="form-group">

          <label for="product_name">{{__('app.Package_Name')}}</label>
          <input id="product_name" class="form-control required" name="package_name" type="text" placeholder="{{__('app.Package_Name')}}" required="">

        </div>

        <div class="form-group">

          <label for="maxlength-textarea">{{__('app.Package_Description')}}</label>
          <textarea id="maxlength-textarea" name="slug" class="form-control" maxlength="2000" rows="5" placeholder="{{__('app.Package_Description')}}"></textarea>

      	</div>

        <div class="row">
          
          <div class="col-md-6">
            
            <div class="form-group">

              <label>{{__('app.Upload_Images')}}</label>
              <div class="input-group col-xs-12">
                <input type="file" name="image[]" multiple="" class="required" required="">
              </div>

            </div>

          </div>

          <div class="col-md-6">

            <div class="panel-group" id="example-collapse-accordion">
              <div class="panel panel-default" style="overflow:visible;">
                  <div class="panel-heading">
                      <h6 class="panel-title">
                          {{-- <a data-toggle="collapse" data-parent="#example-collapse-accordion" href="#example-collapse-accordion-one"> --}}
                             {{ __('app.Select_products') }}
                          {{-- </a> --}}
                      </h6>
                  </div>
                  <div>
                      <div class="panel-body">
                          <select id="product_multiselect_option" name="product[]" required="" class="required" multiple="multiple">
                            @foreach($products as $product)
                            <option value="{{ $product->id }}">{{ $product->product_name }}</option>
                            @endforeach               
                          </select>
                      </div>
                  </div>
              </div>
          </div>
          </div>

        </div>

        <div class="row">
          
          <div class="col-md-12">            

            <div class="form-group">

              <label for="discountPercent">{{__('app.discountPercent')}}</label>
              <input id="discountPercent" class="form-control" name="discountPercent" type="number" placeholder="{{__('app.discountPercent')}}">

            </div>

          </div>

          <div class="col-md-12">            

            <div class="form-group">

              <label for="availability">{{__('app.how_many_available')}}?</label>
              <input id="availability" class="form-control" name="availability" type="number" placeholder="{{__('app.Number_of_stock')}}">

            </div>

          </div>

        </div>

        <hr>
            
        <button type="submit" class="btn btn-primary" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">{{__('app.Create')}} <span class="mdi mdi-creation"></span></button>
    
      </form>
 
    </div>
 
  </div>

</div>

@include('_session_.error2')
@include('_session_.success2')

@endsection