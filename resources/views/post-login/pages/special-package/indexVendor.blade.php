@extends('post-login.index.index')

@section('content')

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Vendors')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table">

            <thead>

              <tr>

                  <th>{{__('app.Vendors')}}</th>

                  <th></th>
                  
              </tr>

            </thead>

            <tbody>

              @foreach($users as $vendor)

              <tr>

                <td>{{$vendor->shop_name}}</td>
                
                <td>
                  
                  <a href="/{{$vendor->shop_name}}/package/create/{{$vendor->id}}"><button class="btn btn-warning">{{__('app.create_package')}}</button></a>

                </td>
              </tr>

                @endforeach

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

</div>

@endsection