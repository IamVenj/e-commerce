@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">
    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>
    <li class="breadcrumb-item" style="color: #4d83ff;">{{__('app.AddVendors')}}</li>
  </ol>

</div>

<div class="col-12 grid-margin stretch-card">

		<a href="/admin/vendors" style="text-decoration: none;"><button class="btn btn-success btn-block" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">View Vendors<span class="mdi mdi-eye ml-2"></span></button></a>

</div>


<div class="col-12 grid-margin stretch-card ">

  <div class="card">
 
    <div class="card-body">
 
      <h4 class="card-title">{{__('app.AddVendors')}}</h4>

      
      <form class="forms-sample" action="/add-vendors" method="POST">

      	@csrf

        <div class="form-group">

          <label for="email">{{__('app.email')}}</label>

          <input id="email" class="form-control" name="email" type="text" placeholder="{{__('app.email')}}">

        </div>

     	<div class="form-group input-password">

          <label for="password">{{__('app.password')}}</label>

          <input id="password" class="form-control" name="password" type="password" placeholder="{{__('app.password')}}">

          <i class="mdi mdi-eye password-show" onclick="myFunction()"></i>

        </div>

        <div class="form-group input-password">

          <label for="c_password">{{__('app.c_password')}}</label>

          <input id="c_password" class="form-control" name="password_confirmation" type="password" placeholder="{{__('app.c_password')}}">

		   <i class="mdi mdi-eye password-show" onclick="myFunction2()"></i>

        </div>

        <div class="form-group">

          <label for="shopname">{{__('app.shop_name')}}</label>

          <input id="shopname" class="form-control" name="shopname" type="text" placeholder="{{__('app.shop_name')}}">

        </div>

        <div class="form-group">

          <label for="phone_number">{{__('app.phone_number')}}</label>

          <input id="phone_number" class="form-control" name="phone_number" type="text" placeholder="{{__('app.phone_number')}}">

        </div>

        <div class="form-group">

          	<label for="address">{{__('app.address')}}</label>

        	<input id="__address" class="form-control" type="text" name="address" placeholder="{{__('app.search')}}">

            <div id="_map" class="show-map2"></div>
            <input type="hidden" name="lat" id="lat">
            <input type="hidden" name="lng" id="lng">

        </div>

        <div class="form-group">

          <label for="maxlength-textarea">{{__('app.about_shop')}}</label>

          <textarea id="maxlength-textarea" name="about_shop" class="form-control" maxlength="2000" rows="5" placeholder="{{__('app.about_shop')}}"></textarea>

      	</div>


        <hr>

        <div class="row">
          
          <div class="col-md-12">
            
              <button type="submit" class="btn btn-success btn-block">{{__('app.Create')}} <span class="mdi mdi-creation"></span></button>

          </div>

        </div>
    
      </form>
 
    </div>
 
  </div>

</div>

<script src="{{asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">
	
  function myFunction() {
    var x = document.getElementById("password");
    if (x.type == "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }

  function myFunction2() {
    var x = document.getElementById("c_password");
    if (x.type == "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
</script>

@include('_session_.error2')
@include('_session_.success2')

@endsection