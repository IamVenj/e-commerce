@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

    <a href="/add-vendors" style="text-decoration: none;"><button class="btn btn-success btn-block" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">{{__('app.AddVendors')}}<span class="mdi mdi-plus ml-2"></span></button></a>

</div>

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Vendors')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table">

            <thead>

              <tr>

                  <th>#</th>
                  <th>{{__('app.Vendor')}}</th>
                  {{-- <th></th> --}}
                  <th></th>

              </tr>

            </thead>

            <tbody>

              <?php $count = 0;?>

              @foreach($vendors as $vendor)

              <tr>


                <?php $count = $count + 1;?>

                <td>{{$count}}</td>
                <td>{{$vendor->shop_name}}</td>

                <td>
                    @include('post-login.partials.modal.vendors-modal')
                    <a data-toggle="modal" href="#" data-target="#delete-vendor{{$vendor->id}}" style="color: #fff; text-decoration: none;">
                        <button class="btn btn-danger" title="delete Vendor" type="submit" style="border-radius: 50%;">
                        <i class="mdi mdi-account-remove" style="font-size: 20px;"></i>
                        </button>
                    </a>
                </td>
                <td>
                  @if($vendor->activation == 0)
                    <a data-toggle="modal" href="#" data-target="#deactivate-vendor{{$vendor->id}}" style="color: #fff; text-decoration: none;">
                        <button class="btn btn-danger" title="deactivate vendor" type="submit" style="border-radius: 50%;">
                        <i class="mdi mdi-account-off" style="font-size: 20px;"></i>
                        </button>
                    </a>
                  @else
                    <a data-toggle="modal" href="#" data-target="#activate-vendor{{$vendor->id}}" style="color: #fff; text-decoration: none;">
                        <button class="btn btn-success" title="activate vendor" type="submit" style="border-radius: 50%;">
                        <i class="mdi mdi-account-check"></i>
                        </button>
                    </a>
                  @endif

                </td>

              </tr>

              @endforeach

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

</div>

@include('_session_.error2')
@include('_session_.success2')

@endsection
