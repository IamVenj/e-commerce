@extends('post-login.index.index')
@section('content')

<div class="col-12 grid-margin stretch-card">
  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">

    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>
    <li class="breadcrumb-item" style="color: #4d83ff;">{{__('app.Products')}}</li>
    <li class="breadcrumb-item active" aria-current="page"><span>{{ $user->shop_name }}</span></li>

  </ol>
</div>

<div class="col-12 grid-margin stretch-card">
  <div class="card">
      <div class="card-body">
        <div class="row">

          @foreach($products as $product)
          <div class="col-md-4 mt-3">
            <div class="card" style="border-top-left-radius: 20px; border-bottom-right-radius: 20px;" >
              @if($product->productImage()->count() > 0)
                <img class="card-img-top" id="image-public" style="height: 250px; object-fit: cover; border-top-left-radius: 20px; border-bottom-right-radius: 20px;" src="{{ $product->productImage()->first()->image_public_id }}" alt="{{$product->productImage()->first()->image_public_id}}">
              @else
                <div style="padding-top: 68%; border-top-left-radius: 20px; border-bottom-right-radius: 20px;  background: linear-gradient(120deg, #00e4d0, #429FFD);" alt="Card image cap"></div>
              @endif
              <div class="card-body">
                <h4 class="card-title mb-3" style="font-weight: bold;">{{$product->product_name}}</h4>
                  <div class="dropdown-divider"></div>
                  <p style="font-size: 14px;">{{str_limit($product->slug, 30)}}</p>
                  <div class="dropdown-divider"></div>

                  <p class="mb-3 mt-3" style="text-transform: uppercase; background: #e8e8e850; padding:10px 0px 10px 10px; border-top-left-radius: 20px; border-bottom-right-radius: 20px;"><span class="mdi mdi-view-dashboard mr-3"></span>{{$product->category()->first()->category_name}}</p>

                  <div class="dropdown-divider"></div>
                  <p class="mt-3"><span class="mdi mdi-tag mr-3"></span>{{$product->brand}}</p>
  
                  <div class="dropdown-divider"></div>
                  <p class="mt-3"><span class="mdi mdi-cash mr-3"></span>{{$product->current_price}}</p>              
  
                  <div class="dropdown-divider"></div>
                  <p class="mt-3"><span class="mdi mdi-map-marker-check mr-3"></span>{{$product->product_location}} @if($product->product_location == 'all') {{__('app.Stores')}} @else {{__('app.Store')}} @endif</p>

                  <div class="dropdown-divider"></div>  
                  <p class="mt-3"><span class="mdi mdi-calendar-multiple mr-3"></span>{{$product->availability}}</p>
                  
                  <div class="dropdown-divider"></div>
                  <p class="mt-3"><span class="mdi mdi-barcode-scan mr-3"></span>{{$product->sku}}</p>

                  <div class="dropdown-divider"></div>
                  <p class="mt-3"><span class="mdi mdi-eye mr-3"></span>Visits: {{$product->visits}}</p>
                  <div class="dropdown-divider"></div>

                  @include('post-login.partials.modal.item-modal')

                  <a class="btn btn-warning btn-block" data-toggle="modal" href="#" data-target="<?= '#view-product'.$product->id; ?>"><i class="mdi mdi-eye mr-2"></i>{{__('app.View')}}</a>
                  <a class="btn btn-danger btn-block" data-toggle="modal" href="#" data-target="<?= '#delete-product'.$product->id; ?>"><i class="mdi mdi-delete-forever mr-2"></i>{{__('app.Delete')}}</a>

                  <p class="card-text mt-3" style="color: rgba(0,0,0,0.3); font-size: 12px;"><i class="mdi mdi-clock"></i> {{$product->created_at->diffForHumans()}}</p> 

                </div>
              </div>
            </div>
          @endforeach

          </div>
          {{$products->links()}}
        </div>
    </div>
</div>

@include('_session_.error2')
@include('_session_.success2')

@endsection