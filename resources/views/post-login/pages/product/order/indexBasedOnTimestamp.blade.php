@extends('post-login.index.index')

@section('content')

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Orders')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table datatable-1">

            <thead>

              <tr>

                  <th>{{__('app.time')}} </th>
                  <th></th>
                  <th></th>

              </tr>

            </thead>

            <tbody>

              @for($i = 0; $i < count($timestamp); $i++)

              <tr>

                  <td>{{$timestamp[$i]}}</td>

                  <td>
                    <a href="/orders/{{ $user_id }}/users/{{ $timestamp[$i] }}/timestamp">
                      <button class="btn btn-outline-primary" style="border-radius: 50px;"><span class="mdi mdi-eye"></span> 
                      </button>
                    </a>
                  </td>

                  <td>

                    <?php
                    $_order = new App\MainOrder;
                    ?>

                    @if($_order->checkgroupVendorAdminDeliveryConfirmation($user_id, $timestamp[$i]))

                    <label class="badge badge-success">{{__('app.Delivered')}}<span class="mdi mdi-truck-check ml-2" style="padding: 10px; border-radius: 50px; background-color: green;"></span></label> 

                    @else
                    @if(auth()->user()->role == 2)
                    <label class="badge badge-danger">Not delivered yet<span class="mdi mdi-truck-fast ml-2" style="padding: 10px; border-radius: 50px; background-color: red; color: #fff;"></span></label> 
                    @endif

                    @if(auth()->user()->role == 1)

                    @include('post-login.partials.modal.order-modal')

                    <button class="btn btn-outline-primary" style="border-radius: 50px;" data-target="#deliverystatus{{ $timestamp[$i] }}"  data-toggle="modal">
                      <span class="mdi mdi-truck"></span> 
                    </button>

                    @endif

                    @endif

                  </td>

              </tr>

              @endfor

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

</div>

@include('_session_.error2')
@include('_session_.success2')

@endsection