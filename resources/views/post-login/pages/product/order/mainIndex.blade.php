@extends('post-login.index.index')

@section('content')

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Orders')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table datatable-1">

            <thead>

              <tr>

                  <th>{{__('app.Order')}} #</th>

                  <th>{{__('app.order_date')}}</th>

                  <th>{{__('app.customer')}}</th>

                  <th>{{__('app.price')}}</th>

                  <th>{{__('app.Item_SKU')}}</th>

                  <th>{{__('app.quantity')}}</th>

                  @if(auth()->user()->role == 1)

                  <th>{{__('app.Payment_status')}}</th>

                  @endif

                  <th>{{__('app.Sent_Date')}}</th>

                  <th></th>

                  <th></th>
                  
                  <th></th>

              </tr>

            </thead>

            <tbody>

              @for($i = 0; $i < count($orders); $i++)

              <tr>

                  <td>{{$orders[$i]['order_code']}}</td>

                  <td><?= date('d M y' ,strtotime($orders[$i]['created_at']));?></td>

                  <td>
                    @if($orders[$i]->mainOrderId->user->role == 3)
                    {{$orders[$i]->mainOrderId->user->name}}
                    @elseif($orders[$i]->mainOrderId->user->role == 2)
                    {{$orders[$i]->mainOrderId->user->shop_name}}
                    @endif
                  </td>
        
                  <td>
                    @if(\Auth::user()->role == 2)
                    @if(\Auth::user()->business_type == "business")
                    {{$orders[$i]->paid_amount - ($orders[$i]->paid_amount * (App\CompanySettings::first()->cut_business / 100))}} Birr
                    @elseif(\Auth::user()->business_type == "individual")
                    {{$orders[$i]->paid_amount -  ($orders[$i]->paid_amount * (App\CompanySettings::first()->cut_individual / 100))}} Birr
                    @endif
                    @elseif(\Auth::user()->role == 1)
                    {{$orders[$i]->paid_amount}} Birr
                    @endif
                  </td>

                  @if(!is_null($orders[$i]->product))
                  
                  <td>{{$orders[$i]->product()->first()->sku}}</td>
                  
                  @else
                  
                  <td></td>
                  
                  @endif

                  <td>x{{$orders[$i]['quantity']}}</td>

                  @if(auth()->user()->role == 1)

                  <td>

                     @if($orders[$i]->mainOrderId->payment_status == 0 && $orders[$i]->mainOrderId->payment_method == 'bank')
                    <label class="badge badge-warning" style="background-color: #D6A90E;">
                      Pending
                      <span class="mdi mdi-refresh" style="margin-left: 1px;"></span>
                    </label>
                    @elseif($orders[$i]->mainOrderId->payment_status == 0 && $orders[$i]->mainOrderId->payment_method == 'on-delivery')
                    <label class="badge badge-danger" style="background-color: red;">
                      Not Paid
                      <span class="mdi mdi-close" style="margin-left: 1px;"></span>
                    </label>
                    @elseif($orders[$i]->mainOrderId->payment_status == 1)
                    <label class="badge badge-success" style="background-color: green;">
                      Paid
                      <span class="mdi mdi-check" style="margin-left: 1px;"></span>
                    </label>
                    @endif

                  </td>

                  @endif

                  <td>{{$orders[$i]['created_at']->diffForHumans()}}</td>

                  <td> 

                    @if(strtotime(date('Y-m-d')) - strtotime($orders[$i]['created_at']) < 86400)

                    <label class="badge badge-success">{{__('app.New')}} Order</label> 

                    @endif

                  </td>

                   <td> 

                    @if($orders[$i]->mainOrderId->vendor_delivery_confirmation == 1)

                    <label class="badge badge-success">{{__('app.Delivered')}}<span class="mdi mdi-check ml-2" style="padding: 10px; border-radius: 50px; background-color: green;"></span></label> 

                    @else

                    @if(auth()->user()->role == 2)
                    <label class="badge badge-danger">Not delivered yet<span class="mdi mdi-truck-fast ml-2" style="padding: 10px; border-radius: 50px; background-color: red; color: #fff;"></span></label> 
                    @endif

                    @endif

                  </td>

                  @if($orders[$i]->mainOrderId->customer_delivery_confirmation == 0)

                  <td>
                    @if(auth()->user()->role == 1)
                    <a href="/admin/orders/{{$orders[$i]['id']}}/detail"><button class="btn btn-outline-primary" style="border-radius: 50px;"><span class="mdi mdi-eye"></span> </button></a>
                    @else
                    <a href="/vendor/orders/{{$orders[$i]['id']}}/detail"><button class="btn btn-outline-primary" style="border-radius: 50px;"><span class="mdi mdi-eye"></span> </button></a>
                    @endif
                  </td>

                  @else

                  <td></td>

                  @endif

              </tr>

              @endfor

            </tbody>

          </table>

        </div>

      </div>

      @if(auth()->user()->role == 1)

      <div class="col-12">
        <!-- vendors location -->

         <?php $count = 0; ?>

         @for($i = 0; $i < count($settings); $i++)

         <?php $count = $count + 1; ?>

        <div class="show-map2" id="zmapz{{$count}}"></div>

        <input type="hidden" id="vendor_lat{{$count}}" value="{{$settings[$i]->lat}}">

        <input type="hidden" id="vendor_lng{{$count}}" value="{{$settings[$i]->lng}}">

         <div id="km{{$count}}" style="font-weight: bold;"></div>

         @endfor

         <input type="hidden" name="add_count" value="{{count($settings) + 1}}">

         <!-- Customers locations -->

         @if(count($customer) > 0)

         <input type="hidden" id="customer_lat" value="{{$customer->first()->latitude}}">

         <input type="hidden" id="customer_lng" value="{{$customer->first()->longitude}}">

         @endif
      </div>

      @endif

    </div>

  </div>

</div>

@endsection