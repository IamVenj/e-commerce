@extends('post-login.index.index')

@section('content')

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Orders')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table datatable-1">

            <thead>

              <tr>
                  <th>Order code</th>
                  <th>Total Amount</th>
                  @if(\Auth::user()->role == 1)
                  <th>Discount</th>
                  <th>Delivery Price</th>
                  <th>Delivery Method</th>
                  <th>Payment Method</th>
                  <th>Payment</th>
                  <th>Unique bank code</th>
                  <th>Order PIN</th>
                  @endif
                  <th>Order Time</th>
                  @if(auth()->user()->role == 1)
                  <th>Delivery Assigned Personnel</th>
                  @endif
                  <th></th>
              </tr>

            </thead>

            <tbody>

              @for($i = 0; $i < count($orders); $i++)

              <tr>
                  <td>{{$orders[$i]->order_code}}</td>
                  @if(\Auth::user()->role == 2)
                  <td>
                    {{ $orders[$i]->order->count() }}
                    <?php
                    $amount = [];
                    ?>
                    @if($orders[$i]->order->count() > 1)
                      @foreach($orders[$i]->order as $itemOrder)
                        <?php array_push($amount, $itemOrder->paid_amount); ?>
                      @endforeach
                      @if(\Auth::user()->business_type == "business")
                        {{ array_sum($amount) - (App\CompanySettings::first()->cut_business / 100 * array_sum($amount))}} {{ __('app.birr') }}
                      @elseif(\Auth::user()->business_type == "individual")
                        {{ array_sum($amount) - (App\CompanySettings::first()->cut_individual / 100 * array_sum($amount))}} {{ __('app.birr') }}
                      @endif
                    @else
                      @if(\Auth::user()->business_type == "business")
                      {{$orders[$i]->order->first()->paid_amount - (App\CompanySettings::first()->cut_business / 100 * $orders[$i]->order->first()->paid_amount)}} {{ __('app.birr') }}
                      @elseif(\Auth::user()->business_type == "individual")
                      {{$orders[$i]->order->first()->paid_amount - (App\CompanySettings::first()->cut_individual / 100 * $orders[$i]->order->first()->paid_amount)}} {{ __('app.birr') }}
                      @endif
                    @endif
                  </td>
                  @else
                  <td>{{$orders[$i]->total_price}} {{ __('app.birr') }}</td>
                  @endif
                  @if(\Auth::user()->role == 1)
                  <td>{{$orders[$i]->discount_price}} {{ __('app.birr') }}</td>
                  <td>{{$orders[$i]->delivery_price}} {{ __('app.birr') }}</td>
                  <td>{{$orders[$i]->delivery_method}}</td>
                  <td>{{$orders[$i]->payment_method}}</td>
                  <td>
                    @if($orders[$i]->payment_status == 0 && $orders[$i]->payment_method == 'bank')
                  <label class="badge badge-warning" style="background-color: #D6A90E;">
                    Pending
                    <span class="mdi mdi-refresh" style="margin-left: 1px;"></span>
                  </label>
                  @elseif($orders[$i]->payment_status == 0 && $orders[$i]->payment_method == 'on-delivery')
                  <label class="badge badge-danger" style="background-color: red;">
                    Not Paid
                    <span class="mdi mdi-close" style="margin-left: 1px;"></span>
                  </label>
                  @elseif($orders[$i]->payment_status == 1)
                  <label class="badge badge-success" style="background-color: green;">
                    Paid
                    <span class="mdi mdi-check" style="margin-left: 1px;"></span>
                  </label>
                  @endif
                  </td>
                  <td>{{$orders[$i]->unique_bank_code}}</td>
                  <td>{{ $orders[$i]->order_pin }}</td>
                  @endif
                  
                  <td>{{$orders[$i]->created_at->diffForHumans()}}</td>
                  @if(auth()->user()->role == 1)
                  <td>
                      @if(is_null($orders[$i]->dPersonnel))
                      @include('post-login.partials.modal.deliveryPersonnelModal')
                      <a class="btn btn-info" data-toggle="modal" href="#" data-target="#assignPersonnel" style="border-radius: 50px; color: #fff;">Assign Personnel</a>
                      @else
                      {{ $orders[$i]->dPersonnel->name }}
                      @endif
                  </td>
                  @endif
                  <td>
                    @if(auth()->user()->role == 1)
                    @if(!is_null($orders[$i]->dPersonnel))
                    <a href="/orders/{{$orders[$i]['id']}}/print" class="btnPrintForDPersonnel"><button class="btn btn-success">Print </button></a>
                    @endif
                    <a href="/admin/orders/{{$orders[$i]['id']}}"><button class="btn btn-outline-primary" style="border-radius: 50px;"><span class="mdi mdi-eye"></span> </button></a>
                    @else
                    <a href="/vendor/orders/{{$orders[$i]['id']}}"><button class="btn btn-outline-primary" style="border-radius: 50px;"><span class="mdi mdi-eye"></span> </button></a>
                    @endif
                  </td>
              </tr>

              @endfor

            </tbody>

          </table>

        </div>

      </div>

      @if(auth()->user()->role == 1)

      <div class="col-12">
        <!-- vendors location -->

         <?php $count = 0; ?>

         @for($i = 0; $i < count($settings); $i++)

         <?php $count = $count + 1; ?>

        <div class="show-map2" id="zmapz{{$count}}"></div>

        <input type="hidden" id="vendor_lat{{$count}}" value="{{$settings[$i]->lat}}">

        <input type="hidden" id="vendor_lng{{$count}}" value="{{$settings[$i]->lng}}">

         <div id="km{{$count}}" style="font-weight: bold;"></div>

         @endfor

         <input type="hidden" name="add_count" value="{{count($settings) + 1}}">

         @if(count($customer->addresses) > 0)

         <!-- Customers locations -->

         <input type="hidden" id="customer_lat" value="{{$customer->addresses->first()->latitude}}">

         <input type="hidden" id="customer_lng" value="{{$customer->addresses->first()->longitude}}">

         <!-- end of map -->

         @endif
      </div>

      @endif

    </div>

  </div>

</div>

@endsection