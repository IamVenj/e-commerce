@extends('post-login.index.index')

@section('content')

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Orders')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table datatable-1">

            <thead>

              <tr>
                  <th>{{__('app.Order')}} pin</th>
                  <th>Item name</th>
                  <th>Package name</th>
                  <th>{{__('app.Item_SKU')}}</th>
                  <th>{{__('app.quantity')}}</th>
                  <th>Color</th>
                  <th>Paid Amount</th>
                  <th>{{__('app.order_date')}}</th>
              </tr>

            </thead>

            <tbody>

              @foreach($orders as $order)

              <tr>

                  <td>{{$order->mainOrderId->order_pin}}</td>
                  <td>
                  @if(!is_null($order->product))
                    {{$order->product->product_name}}
                  @endif
                  </td>
                  <td>
                  @if(!is_null($order->package))
                    {{$order->package->package_name}}
                  @endif
                  </td>
                  <td>
                  @if(!is_null($order->product))
                    {{$order->product->sku}}
                  @endif
                  </td>
                  <td>{{$order->quantity}}</td>
                  <td><div style="width: 20px; height: 20px; background-color: {{$order->color->hex}}"></div></td>
                  <td>
                    @if(\Auth::user()->business_type == "business")
                    {{$order->paid_amount - ($order->paid_amount * (App\CompanySettings::first()->cut_business / 100))}} Birr
                    @elseif(\Auth::user()->business_type == "individual")
                    {{$order->paid_amount -  ($order->paid_amount * (App\CompanySettings::first()->cut_individual / 100))}} Birr
                    @endif
                  </td>
                  <td><?= date('d M y' ,strtotime($order->created_at));?></td>

              </tr>

              @endforeach

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

</div>

@endsection