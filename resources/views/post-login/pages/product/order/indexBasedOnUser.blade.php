@extends('post-login.index.index')

@section('content')

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Orders')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table datatable-1">

            <thead>

              <tr>

                  <th>{{__('app.customer')}} </th>
                  
                  <th></th>

              </tr>

            </thead>

            <tbody>

              @for($i = 0; $i < count($users); $i++)

              <tr>

                  <td>
                    @if($users[$i]['role'] == 3)
                    Customer: {{ $users[$i]['name'] }} 
                    @elseif($users[$i]['role'] == 2)
                    Vendor: {{ $users[$i]['shop_name'] }} 
                    @else
                    {{ $users[$i]['name'] }}
                    @endif
                    
                    @if($timeline[$i] > 0) 
                    
                    <label class="badge badge-success"> Today's Order
                      <span class="mdi mdi-shopping ml-2" style="padding: 10px; border-radius: 50px; background-color: green;">
                      </span>
                    </label> 
                    
                    @endif

                  </td>

                  <td>
                    <a href="/orders/{{$users[$i]['id']}}/users">
                      <button class="btn btn-outline-primary" style="border-radius: 50px;"><span class="mdi mdi-eye"></span> 
                      </button>
                    </a>
                  </td>

              </tr>

              @endfor

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

</div>

@endsection