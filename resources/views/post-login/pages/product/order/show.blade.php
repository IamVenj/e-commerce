@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">

    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>

    <li class="breadcrumb-item" style="color: #4d83ff;">{{__('app.Orders')}}</li>

    <li class="breadcrumb-item active" aria-current="page"><span>{{$order->order_code}}</span></li>

  </ol>

</div>

<div class="col-12 grid-margin stretch-card">

    @if(auth()->user()->role == 1)
    <a href="/admin/orders" style="text-decoration: none;"><button class="btn btn-success btn-block" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">{{__('app.View_Orders')}}<span class="mdi mdi-eye ml-2"></span></button></a>
    @else
    <a href="/orders" style="text-decoration: none;"><button class="btn btn-success btn-block" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">{{__('app.View_Orders')}}<span class="mdi mdi-eye ml-2"></span></button></a>
    @endif

</div>


<div class="col-md-12 grid-margin stretch-card d-none d-md-flex">

  <div class="card">

    <div class="card-body">

      <h4 class="card-title">{{$order->order_code}}</h4>

      <div class="row">

        <div class="col-4">

          <ul class="nav nav-tabs nav-tabs-vertical" role="tablist">

            

            <li class="nav-item">

              <a class="nav-link active" id="profile-tab-vertical" data-toggle="tab" href="#customer-info" role="tab" aria-controls="profile-2" aria-selected="false">

              {{__('app.Customer_Information')}}

              <i class="mdi mdi-account-outline text-danger ml-2"></i>

              </a>

            </li>

            @if(auth()->user()->role == 1)

            <li class="nav-item">

              <a class="nav-link" id="contact-tab-vertical" data-toggle="tab" href="#delivery-info" role="tab" aria-controls="contact-2" aria-selected="false">

              {{__('app.Delivery_Information')}}

              <i class="mdi mdi-train-car text-danger ml-2"></i>

              </a>

            </li>

            @endif

            <li class="nav-item">

              <a class="nav-link" id="home-tab-vertical" data-toggle="tab" href="#order-info" role="tab" aria-controls="home-2" aria-selected="true">

              {{__('app.Order_Detail')}}

              <i class="mdi mdi-receipt text-danger ml-2"></i>

              </a>

            </li>

            <li class="nav-item">

              <a class="nav-link" id="contact-tab-vertical" data-toggle="tab" href="#product-info" role="tab" aria-controls="contact-2" aria-selected="false">

              {{__('app.Product_Information')}}

              <i class="mdi mdi-reproduction text-danger ml-2"></i>

              </a>

            </li>

            <li class="nav-item">

              <a class="nav-link" id="contact-tab-vertical" data-toggle="tab" href="#payment-info" role="tab" aria-controls="contact-2" aria-selected="false">

              {{__('app.Payment_Information')}}

              <i class="mdi mdi-cash text-danger ml-2"></i>

              </a>

            </li>

          </ul>

        </div>

        <div class="col-8">

          <div class="tab-content tab-content-vertical">

            <div class="tab-pane fade show active" id="customer-info" role="tabpanel" aria-labelledby="profile-tab-vertical">

                <div class="picture-container">
              
                  <div class="picture">
            
                    <img src="@if(!is_null($customer->photo_url)){{ $customer->photo_url }}@else{{URL::asset('images/default-avatar.png')}}@endif" class="picture-src" id="profilePicturePreview" title=""/>
                            
                  </div>
                              
                </div>

                <hr>

                <h2 class="mt-0">{{$customer->name}}</h2>

                @if(auth()->user()->role == 1)

                <p class="mb-0">

                  <i class="mdi mdi-phone text-primary"></i> 

                  {{$customer->phone_number}}

                </p>

                <p class="mb-0">

                  <?php $count = 1; ?>

                  @foreach($customer->addresses()->get() as $address)

                  <i class="mdi mdi-map-marker text-primary"></i> 

                   {{$count}}. {{$address->address}} 

                   <?php $count++; ?>

                   @endforeach

                </p>

                @endif

            </div>

            @if(auth()->user()->role == 1)

            <div class="tab-pane fade" id="delivery-info" role="tabpanel" aria-labelledby="home-tab-vertical">

              <h2>{{__('app.Delivery_status')}}</h2>

               <?php $count = 0; ?>

               @for($i = 0; $i < count($settings); $i++)

               <?php $count = $count + 1; ?>

              <div class="show-map2" id="zmapz{{$count}}"></div>

              <input type="hidden" id="vendor_lat{{$count}}" value="{{$settings[$i]->lat}}">

              <input type="hidden" id="vendor_lng{{$count}}" value="{{$settings[$i]->lng}}">

               <div id="km{{$count}}" style="font-weight: bold;"></div>

               @endfor

               <input type="hidden" name="add_count" value="{{count($settings) + 1}}">

               <!-- Customers locations -->

               @if(count($customer->addresses) > 0)

               <input type="hidden" id="customer_lat" value="{{$customer->addresses->first()->latitude}}">

               <input type="hidden" id="customer_lng" value="{{$customer->addresses->first()->longitude}}">

               @endif

               <!-- end of map -->


               <hr>

            </div>

            @endif

            <div class="tab-pane fade" id="order-info" role="tabpanel" aria-labelledby="home-tab-vertical">

              <h2>{{__('app.Order_Detail')}}</h2>

              <hr>

              <h5>order #: <span style="color: red;"> {{$order->order_code}} </span></h5>

              <h5>{{__('app.order_date')}}: <span style="color: green;"> {{$order->created_at->diffForHumans()}} | {{date('d M y', strtotime($order->created_at))}} </span></h5>

            </div>

            <div class="tab-pane fade" id="product-info" role="tabpanel" aria-labelledby="contact-tab-vertical">

              <h2> {{__('app.Product_Information')}} </h2>

              <hr>

              <div class="row">
                
                <div class="col-md-4">

                  @if(!is_null($order->product))
                  
                  <img class="card-img-top" style="height: 200px; object-fit: cover; border-top-left-radius: 20px; border-bottom-right-radius: 20px;" src="{{ $order->product->productImage->first()->image_public_id }}" alt="{{ $order->product->productImage->first()->image_public_id }}">

                  @else

                  <img class="card-img-top" style="height: 200px; object-fit: cover; border-top-left-radius: 20px; border-bottom-right-radius: 20px;" src="{{ $order->package->images->first()->image_public_id }}" alt="{{ $order->package->images->first()->image_public_id }}">

                  @endif

                </div>

                <div class="col-md-8">

                  @if(!is_null($order->product))

                  <p class="mb-4">{{$order->product->product_name}}</p>

                  <p class="mb-4">{{$order->product->slug}}</p>
                  
                  <p class="mb-4">{{$order->product->category->category_name}}</p>
                  
                  <p class="mb-4">{{$order->product->brand}}</p>

                  <p class="mb-4">{{__('app.Item_SKU')}}: {{$order->product->sku}}</p>
                  
                  <p class="mb-4">

                    @if($order->product->product_location == 'all')

                    {{$order->product->product_location}} Stores 

                    @else 

                    {{$order->product->product_location}} 

                    @endif

                  </p>
                  
                  <p class="mb-4">{{$order->product->availability}} {{__('app.available')}}</p>

                  @endif

                  @if(!is_null($order->package))

                  <p class="mb-4">{{$order->package->package_name}}</p>

                  <p class="mb-4">{{$order->package->slug}}</p>
                  
                  <p class="mb-4">{{$order->package->availability}} {{__('app.available')}}</p>

                  @endif
                
                </div>

              </div>


              
            </div>

            <div class="tab-pane fade" id="payment-info" role="tabpanel" aria-labelledby="contact-tab-vertical">

              <h2> {{__('app.Payment_Information')}} </h2>

              <hr>

              @if($order->mainOrderId->payment_status == 0 && $order->mainOrderId->payment_method == 'bank')

              <label class="badge badge-warning">{{__('app.Pending')}}<span class="mdi mdi-refresh" style="margin-left: 10px;"></span></label>

              @elseif($order->mainOrderId->payment_status == 0 && $order->mainOrderId->payment_method == 'on-delivery')

              <label class="badge badge-danger">{{__('app.Not_Paid')}}<span class="mdi mdi-close" style="margin-left: 10px;"></span></label>

              @elseif($order->mainOrderId->payment_status == 1)

              <label class="badge badge-success">{{__('app.Paid')}}<span class="mdi mdi-check" style="margin-left: 10px;"></span></label>

              @endif


              <p>{{__('app.price')}}: 
              @if(\Auth::user()->role == 2)
              @if(\Auth::user()->business_type == "business")
              {{$order->paid_amount - ($order->paid_amount * (App\CompanySettings::first()->cut_business / 100))}}  {{__('app.birr')}}
              @elseif(\Auth::user()->business_type == "individual")
              {{$order->paid_amount -  ($order->paid_amount * (App\CompanySettings::first()->cut_individual / 100))}}  {{__('app.birr')}}
              @endif
              @elseif(\Auth::user()->role == 1)
              {{$order->paid_amount}} Birr
              @endif
              </p>
              
              <p>{{__('app.Total_Price')}}: 
                @if(\Auth::user()->role == 2)
                @if(\Auth::user()->business_type == "business")
                {{($order->paid_amount - ($order->paid_amount * (App\CompanySettings::first()->cut_business / 100))) * $order->quantity}}  {{__('app.birr')}}
                @elseif(\Auth::user()->business_type == "individual")
                {{($order->paid_amount -  ($order->paid_amount * (App\CompanySettings::first()->cut_individual / 100))) * $order->quantity}}  {{__('app.birr')}}
                @endif
                @elseif(\Auth::user()->role == 1)
                {{$order->paid_amount * $order->quantity}} Birr
                @endif
              </p>

              @if(auth()->user()->role == 1)

              <p>{{__('app.Payment_Method')}}: 

                <span style="color: #F8694A;">

                @if($order->mainOrderId->payment_method == 'on-delivery') 

                  {{__('app.cash_delivery_pickup')}}

                @else

                  {{ $order->mainOrderId->payment_method }}

                @endif

                </span>

              </p>

              @if($order->mainOrderId->payment_method == 'bank')
              
              @if(!is_null($order->mainOrderId->unique_bank_code))
              
              <p>Unique Bank Code: {{$order->mainOrderId->unique_bank_code}}</p>
              
              @endif

              <p>{{__('app.PaymentTransactionID')}}: <span style="color: #F8694A;"> {{$order->mainOrderId->unique_bank_code}}</span></p>
              @endif

              @endif

            </div>


          </div>

        </div>

      </div>

    </div>

  </div>
  
</div>
      
<div class="message"></div>

@endsection