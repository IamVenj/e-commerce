@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <div class="card">

    <div class="card-body">

      <h4 class="card-title">{{__('app.Settings')}}</h4>

      <form class="forms-sample" action="/settings" method="post" enctype="multipart/form-data">

        @csrf

        @method('PATCH')
        
        <hr>

        <h1>Basic Information</h1>

        <hr>

        <div class="form-group">
        
            <label for="name" style="font-size: 15px;">{{__('app.Company_Name')}} <small style="color: red;">*</small></label>
        
            <input type="text" class="form-control" name="company_name" id="name" placeholder="{{__('app.Company_Name')}}" value="{{$setting->company_name}}" style="font-size: 15px; ">
        
        </div>

        <div class="form-group">
        
            <label for="about_company" style="font-size: 15px;">{{__('app.company_slug')}} <small style="color: red;">*</small></label>
        
            <textarea class="form-control" name="slug" id="about_company" placeholder="{{__('app.company_slug')}}" style="font-size: 15px; " rows="7">{{$setting->slug}}</textarea>
        
        </div>

        <div class="form-group">
        
            <label for="location" style="font-size: 15px;">{{__('app.Location')}} <small style="color: red;">*</small></label>
        
            <input type="text" name="location" class="form-control" id="location" placeholder="{{__('app.Location')}}" value="{{$setting->location}}" style="font-size: 15px; ">
        
        </div>

        <input type="hidden" id="address_map" value="{{$setting->address}}">

        @if(is_null($setting->address))
                                  
        <input id="__address" class="form-control mb-20" type="text" name="map_address" size="50" placeholder="{{__('app.search')}}" required="" area-required="true">

        <div id="_map" class="show-map2" style="margin-bottom: 20px;"></div>
        <input type="hidden" name="lat" id="lat">
        <input type="hidden" name="lng" id="lng">
     
        @else
        
        <input id="searchTextField{{$setting->id}}" class="form-control mb-20" type="text" value="{{$setting->address}}" name="map_address" size="50" placeholder="{{__('app.search')}}">

        <div id="map{{$setting->id}}" class="show-map2"></div>
        <input type="hidden" name="lat" id="_lat{{$setting->id}}" value="{{$setting->lat}}">
        <input type="hidden" name="lng" id="_lng{{$setting->id}}" value="{{$setting->lng}}">

        @endif

        <div class="form-group">
        
            <label for="Phone_Number" style="font-size: 15px;">{{__('app.phone_number')}}</label>
        
            <input type="text" name="phone_number" class="form-control" id="Phone_Number" placeholder="{{__('app.phone_number')}}" value="{{$setting->phone_number}}" style="font-size: 15px; ">
        
        </div>

        <div class="form-group">
         
            <label for="email" style="font-size: 15px;">{{__('app.email')}}</label>
        
            <input type="email" name="email" value="{{$setting->email}}" class="form-control" id="email" placeholder="{{__('app.email')}}" style="font-size: 15px; ">
        
        </div>

        <hr>

        <h1>Charge</h1>

        <hr>

        <div class="form-group">
         
            <label for="delivery_price" style="font-size: 15px;">{{__('app.delivery_price_per_km')}}</label>
        
            <input type="number" min="1" name="delivery_per_km" value="{{$setting->delivery_per_km}}" class="form-control" id="delivery_price" placeholder="{{__('app.delivery_price_per_km')}}" style="font-size: 15px; ">
        
        </div>

        <div class="form-group">
         
            <label for="online_payment_limit" style="font-size: 15px;">{{__('app.on_delivery_payment_limit')}}</label>
        
            <input type="number" min="500" name="online_payment_limit" value="{{ $setting->online_payment_limit }}" class="form-control" id="delivery_price" placeholder="{{__('app.on_delivery_payment_limit')}}" style="font-size: 15px; ">
        
        </div>

        <div class="row">
          <div class="col-md-6">
            
            <div class="form-group">
             
                <label for="products_amount_for_discount" style="font-size: 15px;">{{__('app.discount_product')}}?</label>
            
                <input type="number" name="products_amount_for_discount" value="{{$setting->products_amount_for_discount}}" class="form-control" placeholder="{{__('app.amount_of_products')}}" style="font-size: 15px; ">
            
            </div>

          </div>
          <div class="col-md-6">
            
            <div class="form-group">
             
                <label for="discount" style="font-size: 15px;"> {{__('app.discount_for')}} {{$setting->products_amount_for_discount}} {{__('app.products_per_percent')}}?</label>
            
                <input type="number" step="0.01" name="discount" value="{{$setting->discount}}" class="form-control" id="discount" placeholder="{{__('app.Discount_per_percent')}}" style="font-size: 15px; ">
            
            </div>

          </div>
        
        </div>

        <div class="row">
          <div class="col-md-6">
            
          <div class="form-group">
           
              <label for="cut_for_business" style="font-size: 15px;">{{__('app.cut_for_business')}}?</label>
          
              <input type="number" min="1" name="cut_business" value="{{$setting->cut_business}}" class="form-control" id="cut_business" placeholder="{{__('app.cut_for_business')}}" style="font-size: 15px; ">
          
          </div>
          </div>
          <div class="col-md-6">
            
            <div class="form-group">
             
                <label for="cut_for_individual" style="font-size: 15px;">{{__('app.cut_for_individual')}}?</label>
            
                <input type="number" min="1" name="cut_individual" value="{{$setting->cut_individual}}" class="form-control" placeholder="{{__('app.cut_for_individual')}}" style="font-size: 15px; ">
            
            </div>

          </div>
        
        </div>

        <hr>

        <h1>Delivery Price Setup</h1>

        <hr>


        <div class="row">

          <div class="col-md-6">

            <div class="form-group">

                <label for="symmart_standard_price" style="font-size: 15px;">{{__('app.symmart_standard_price')}}</label>

                <input type="number" name="symmart_standard_price" value="{{$setting->symmart_standard_price}}" class="form-control" id="symmart_standard_price" placeholder="{{__('app.symmart_standard_price')}}" style="font-size: 15px; ">

            </div>  

          </div>

          <div class="col-md-6">

            <div class="form-group">

                <label for="symmart_express_price" style="font-size: 15px;">{{__('app.symmart_express_price')}}</label>

                <input type="number" name="symmart_express_price" value="{{$setting->symmart_express_price}}" class="form-control" id="symmart_express_price" placeholder="{{__('app.symmart_express_price')}}" style="font-size: 15px; ">

            </div>

          </div>

          <div class="col-md-6">

            <div class="form-group">

                <label for="symmart_premium_price" style="font-size: 15px;">{{__('app.symmart_premium_price')}}</label>

                <input type="number" name="symmart_premium_price" value="{{$setting->symmart_premium_price}}" class="form-control" id="symmart_premium_price" placeholder="{{__('app.symmart_premium_price')}}" style="font-size: 15px; ">

            </div>

          </div>

          <div class="col-md-6">

            <div class="form-group">

                <label for="symmart_national_price" style="font-size: 15px;">{{__('app.symmart_national_price')}}</label>

                <input type="number" name="symmart_national_price" value="{{$setting->symmart_national_price}}" class="form-control" id="symmart_national_price" placeholder="{{__('app.symmart_national_price')}}" style="font-size: 15px; ">

            </div>

          </div>
          
        </div>

         <hr>

        <h1>{{ __('app.hint') }}</h1>

        <hr>

        <div class="form-group">
         
            <label for="delivery_price" style="font-size: 15px;">{{__('app.credit_hint')}}</label>
        
            <textarea name="credit_hint" rows="5" class="form-control" id="credit_hint" placeholder="{{__('app.credit_hint')}}" style="font-size: 15px; ">{{$setting->credit_hint}}</textarea>
        
        </div>
        
        <hr>

        <h1>Social</h1>

        <hr>


        <div class="row">

          <div class="col-md-3">

            <div class="form-group">

                <label for="facebook" style="font-size: 15px;">{{__('app.facebook')}}</label>

                <input type="facebook" name="facebook" value="{{$setting->facebook}}" class="form-control" id="facebook" placeholder="{{__('app.facebook')}}" style="font-size: 15px; ">

            </div>  

          </div>

          <div class="col-md-3">

            <div class="form-group">

                <label for="twitter" style="font-size: 15px;">{{__('app.twitter')}}</label>

                <input type="twitter" name="twitter" value="{{$setting->twitter}}" class="form-control" id="twitter" placeholder="{{__('app.twitter')}}" style="font-size: 15px; ">

            </div>

          </div>

          <div class="col-md-3">

            <div class="form-group">

                <label for="google_plus" style="font-size: 15px;">{{__('app.Google_plus')}}</label>

                <input type="google_plus" name="google_plus" value="{{$setting->google_plus}}" class="form-control" id="google_plus" placeholder="{{__('app.Google_plus')}}" style="font-size: 15px; ">

            </div>

          </div>

          <div class="col-md-3">

            <div class="form-group">

                <label for="linked_in" style="font-size: 15px;">{{__('app.linked_in')}}</label>

                <input type="linked_in" name="linked_in" value="{{$setting->linked_in}}" class="form-control" id="linked_in" placeholder="{{__('app.linked_in')}}" style="font-size: 15px; ">

            </div>

          </div>
          
        </div>

        <hr>

        <h1>Company Icons</h1>

        <hr>

        <div class="row">

          <div class="col-md-6">


            <div class="form-group">

              <label for="logo" style="font-size: 15px;">{{__('app.Logo')}} <small style="color: red;">*</small></label>

              <input type="file" name="logo" class="file-upload-default">

              <div class="input-group col-xs-12">

                <input type="text" class="form-control file-upload-info" disabled="" placeholder="{{__('app.Upload_Logo')}}">

                <span class="input-group-append">

                  <button class="file-upload-browse btn btn-primary" type="button">{{__('app.Select_Logo')}}</button>

                </span>

              </div>

            </div>



            <label style="font-size: 15px; font-weight: bold">{{__('app.Current_Logo')}} <i class="mdi mdi-arrow-down"></i></label>

            <div class="form-group">

                <img src="{{URL::asset('storage/uploads/logo.png')}}" class="mt-4" style="width: 300px; height: 200px; object-fit: contain">

            </div>

          </div>

          <div class="col-md-6">

            <div class="form-group">

              <label for="favicon" style="font-size: 15px;">{{__('app.Favicon')}} <small style="color: red;">*</small></label>

              <input type="file" name="favicon" class="file-upload-default">

              <div class="input-group col-xs-12">

                <input type="text" class="form-control file-upload-info" disabled="" placeholder="{{__('app.Upload_Favicon')}}">

                <span class="input-group-append">

                  <button class="file-upload-browse btn btn-primary" type="button">{{__('app.Select_Favicon')}}</button>

                </span>

              </div>

            </div>

            <label style="font-size: 15px; font-weight: bold">{{__('app.Current_Favicon')}} <i class="mdi mdi-arrow-down"></i></label>

            <div class="form-group">

                <img src="{{URL::asset('storage/uploads/favicon.png')}}" class="mt-4" style="width: 300px; height: 200px; object-fit: contain;">
   
            </div>
   
          </div>
   
        </div>
        
        <button type="submit" class="btn btn-primary mr-2">{{__('app.Update')}}</button>

      </form>

    </div>

  </div>

</div>

@include('_session_.error2')

@include('_session_.success2')

@endsection