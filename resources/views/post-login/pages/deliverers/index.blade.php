@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">
  	<div class="card">
    	<div class="card-body">

          	<h4 class="card-title" style="font-size: 20px;">
          		Delivery Personnel
          	</h4>

          	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">
            	Create Personnel
          	</p>

          	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>

          	<form class="forms-sample" action="/delivery/personnel/create" method="post">
          		@csrf
          		<div class="form-group">
                  	<label for="name" style="font-size: 20px;">Delivery Personnel Name</label>
                  	<input type="text" class="form-control" id="name" name="name" placeholder="Delivery Personnel Name" required>
                </div>
                
                <div class="form-group">
                  	<label for="phone" style="font-size: 20px;">Delivery Personnel Phone Number</label>
                  	<input type="text" class="form-control" id="phone" name="phone_number" placeholder="Delivery Personnel Phone Number" required>
                </div>

        		<button type="submit" class="btn btn-success mr-2 mt-5">{{__('app.Create')}}</button>
          	</form>
        </div>
    </div>
</div>


<div class="col-12 grid-margin stretch-card">
	<div class="card">
	  	<div class="card-body">
		    <h4 class="card-title">
		    	Delivery Personnel
		    </h4>

		    <div class="row">
		      	<div class="col-12">
			        <div class="table-responsive">
			          <table class="order-listing table">
			            <thead>
			              	<tr>
			                  	<th>#</th>
			                  	<th>{{__('app.Name')}}</th>
			                  	<th>{{__('app.phone_number')}}</th>
			                  	<th></th>
			              	</tr>
			            </thead>
			            <tbody>
			            	@if(!is_null($delivery_personnel))
			            	<?php $count = 0;?>
							@foreach($delivery_personnel as $dp)
							<?php $count++; ?>
								<tr>
									<td>{{ $count }}</td>
									<td>{{ $dp->name }}</td>
									<td>{{ $dp->phone_number }}</td>
									<td>
										<div class="btn-group dropdown">
										@include('post-login.partials.modal.deliverers-modal')
					                	<button type="button" class="btn btn-secondary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					                    {{__('app.Manage')}}
					                	</button>

				                		<div class="dropdown-menu">

						                <div class="dropdown-divider"></div>

					                    <a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#edit-delivery'.$dp->id; ?>">
				                    		<i class="mdi mdi-tooltip-edit mr-2" style="color: rgba(0,0,0,0.5);"></i>
				                    		{{__('app.Edit')}}
				                    	</a>
					                  	
					                  	<div class="dropdown-divider"></div>

					                    <a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#delete-delivery'.$dp->id; ?>">
				                    		<i class="mdi mdi-delete-forever mr-2" style="color: rgba(0,0,0,0.5);"></i>
				                    		{{__('app.Delete')}}
				                    	</a>
				                		</div>
				                	</div>
									</td>
								</tr>
							@endforeach			
							@endif              	
			            </tbody>
			          </table>
			        </div>
		      	</div>
		    </div>
	  	</div>
	</div>
</div>


@include('_session_.success2')
@include('_session_.error2')

@endsection