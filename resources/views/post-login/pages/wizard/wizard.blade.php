@extends('post-login.index.index')

@section('content')

<div class="row">

  <div class="col-12 grid-margin">

    <div class="card" style="border-bottom-left-radius: 500px; border-top-right-radius: 500px;">

      <div class="card-body">

        <h4 class="card-title">{{__('app.fill_profile')}}</h4>

        <form id="accountForm" action="/wizard" method="post" enctype="multipart/form-data">

          <input type="hidden" name="csrf-token" value="{{csrf_token()}}">

          <input type="hidden" name="role" value="{{auth()->user()->role}}">

          <div>

            <h3>{{__('app.profile')}}</h3>

            <section>

              <div class="picture-container">

                <div class="picture">

                  <img src="@if(auth()->user()->role == 3){{URL::asset('images/default-avatar.png')}}@elseif(auth()->user()->role == 2){{URL::asset('images/default-avatar.png')}}@endif" class="picture-src" id="profilePicturePreview" title=""/>

                </div>

              </div>

              <div class="form-group">

                <label>{{__('app.profile_image')}} *</label>

                <input type="file" name="image" class="file-upload-default">

                <div class="input-group col-xs-12">

                  <input type="text" class="form-control file-upload-info" disabled="" placeholder="{{__('app.choose_image')}}">

                  <span class="input-group-append">

                    <button class="file-upload-browse btn btn-primary" type="button">{{__('app.choose_image')}}</button>

                  </span>

                </div>

                <div id="image_error"></div>

              </div>

              @if(auth()->user()->role == 3)

              <div class="form-group">

                <label for="fullname">{{__('app.full_name')}} *</label>

                <input id="fullname" name="fullname" type="text" class="required form-control" placeholder="{{__('app.full_name')}}">

              </div>

              @endif

              @if(auth()->user()->role == 2)

              <hr>

             <div class="form-group">

                <label for="business_type">{{__('app.business_type')}} *</label>

              </div>

                <div class="form-check">
                    <label class="form-check-label">
                        <input class="checkbox" class="business_type" name="business_type" value="individual" checked="" type="radio" required="" >
                        Individual
                        <i class="input-helper"></i>
                    </label>
                </div>

                <div class="form-check">
                    <label class="form-check-label">
                        <input class="checkbox" class="business_type" name="business_type" value="business" type="radio" required="">
                        Business
                        <i class="input-helper"></i>
                    </label>
                </div>

              <hr>

              <div id="business" style="display: none;">
                <div class="form-group">

                  <label for="shop_name">{{__('app.shop_name')}} *</label>
                  <input id="shop_name" name="shop_name" type="text" class="required form-control" placeholder="{{__('app.shop_name')}}">

                </div>

                <div class="form-group">
                  <label for="tin">{{__('app.tin')}}</label>
                  <input id="tin" name="tin" type="text" class="required form-control" placeholder="{{__('app.tin')}}">
                </div>

                <div class="form-group">

                  <label for="maxlength-textarea">{{__('app.about_shop')}} *</label>
                  <textarea id="maxlength-textarea" name="shop_slug" class="required form-control" maxlength="400" rows="5" placeholder="{{__('app.about_shop')}}"></textarea>

                </div>
              </div>

              <div id="individual" style="display: none;">
                <div class="form-group">

                  <label for="fullname">{{__('app.full_name')}} *</label>
                  <input id="fullname" name="fullname" type="text" class="required form-control" placeholder="{{__('app.full_name')}}">

                </div>
              </div>



              @endif

            </section>


            <h3>{{__('app.address')}}</h3>

            <section>

              <div class="form-group">

                <label for="phone">{{__('app.phone_number')}}</label>
                <input id="phone" name="phone_number" type="text" class="required form-control" placeholder="{{__('app.phone_number')}}">

              </div>

              <div class="form-group">

                <label for="phone">{{__('app.phone_number')}} 2 <small style="color: red;">(optional)</small></label>
                <input id="phone" name="phone_number2" type="text" class="form-control" placeholder="{{__('app.phone_number')}} 2">

              </div>

              <div class="form-group">

                <label for="phone">{{__('app.phone_number')}} 3 <small style="color: red;">(optional)</small></label>
                <input id="phone" name="phone_number3" type="text" class="form-control" placeholder="{{__('app.phone_number')}} 3">

              </div>

             <div class="form-group" id="address_main">

                <label for="address1">{{__('app.address')}} 1</label>

                <div class="row">

                  <div class="col-md-11">

                    <input id="address_" name="address_" type="text" class="required form-control address_uno" placeholder="{{__('app.address')}} 1">

                  </div>

                  <div class="col-md-1">

                    <button class="btn btn-warning" type="button" id="search_map1"><i class="mdi mdi-map-search"></i></button>

                  </div>

                </div>


                <div id="map" class="show-map2"></div>

                <input type="hidden" name="lat" id="lat">

                <input type="hidden" name="lng" id="lng">

              </div>


              @if(auth()->user()->role != 3)

              <div id="address2"></div>

              <div class="row">

                <div class="col-3">

                  <button type="button" class="btn btn-warning" id="more_address">{{__('app.add_more')}} <i class="mdi mdi-plus"></i></button>

                </div>

                <div class="col-9">

                  <div class="form-group" id="address_inputs" style="display: none;">
                    <select class="form-control form-control-lg" name="address_input" id="address_input">
                      <option selected disabled>{{__('app.select_address')}}</option>
                      <option value="2">1</option>
                      <option value="3">2</option>
                      <option value="4">3</option>
                     <option value="5">4</option>
                      <option value="6">5</option>
                    </select>
                  </div>
                </div>
              </div>

              @endif

            </section>



            @if(auth()->user()->role == 2)

            <h3>Bank Information</h3>

            <section>

              <div class="form-group">

                <label for="bank_name"> Bank Name<small style="color: red;">*</small> </label>
                <input id="bank_name" name="bank_name" type="text" class="form-control required" placeholder="Bank Name">

              </div>

              <div class="form-group">

                <label for="account_number">Account Number <small style="color: red;">*</small></label>
                <input id="account_number" name="account_number" type="text" class="form-control required" placeholder="Account Number">

              </div>

            </section>

            @endif


            <h3>{{__('app.finish')}}</h3>

            <section>

              @if(auth()->user()->role == 2)

              <div class="form-check">

                <label class="form-check-label">

                  <input class="checkbox" name="credit" type="checkbox" id="credit-support">

                  Credit Support
                  <i class="input-helper"></i>

                </label>

              </div>

              @if(!is_null($credit_hint))
              <small class="mb-3">Hint: <span style="color: #9aa58f;">{{ $credit_hint }}</span></small>
              @endif

              @endif

              <div class="form-check">

                <label class="form-check-label">

                  <input class="checkbox" value="1" name="newsletter_register" type="checkbox">

                  {{__('app.revieve_news')}}
                  <i class="input-helper"></i>

                </label>

              </div>

              <div class="form-check">

                  <label class="form-check-label">

                    <input class="checkbox required" name="agreement" type="checkbox">

                    I agree with the Terms and Conditions
                    <i class="input-helper"></i>

                  </label>

              </div>


              <div id="agreement_error"></div>

              <button type="submit" id="submit-wizard-btn" class="btn btn-success">{{__('app.Submit')}}<span class="mdi mdi-check ml-2"></span></button>
              <a href="@if(auth()->user()->role == 3) /Terms-and-conditions @elseif(auth()->user()->role == 2) /Terms-and-conditions/vendor @else # @endif" target="_blank" class="btn btn-warning">Read our Terms and Conditions<span class="mdi mdi-view-list ml-2"></span></a>

            </section>

            <div id="pending_status"></div>

          </div>

        </form>

      </div>

    </div>

  </div>

</div>

<div class="message"></div>

@endsection
