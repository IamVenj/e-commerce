</!DOCTYPE html>
<html>
<head>
	<title>Order Preview</title>
	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<style type="text/css">
		table{
			font-family: 'Montserrat', sans-serif;
		}
	</style>
</head>
<body>
	<table class="table table-striped">
		<thead>
			<tr>
		    	<th scope="col">Vendor Name</th>
	          	<th scope="col">Item Name</th>
	          	<th scope="col">Item Id</th>
	          	<th scope="col">Price</th>
	          	<th scope="col">Qty</th>
	          	<th scope="col">Vendors location</th>
	          	<th scope="col">Vendors Phone Number</th>
	          	<th scope="col">Vendors Phone Number2</th>
	          	<th scope="col">Vendors Phone Number3</th>
			</tr>
		</thead>
		<tbody>
	    	@foreach($orderInfo->order as $order)
        	<tr>
          		<td scope="row">{{ $order->vendor->shop_name }}</td>
          		<td>{{ $order->product->product_name }}</td>
          		<td>{{ $order->product->sku }}</td>
          		<td>{{ $order->product->current_price }} ETB</td>
          		<td>{{ $order->quantity }}</td>
          		<td>
          		@if($order->product->product_location == "all")
          			@foreach($order->vendor->addresses as $add)
          				{{ $add->address }} |
          			@endforeach
          		@else
          			{{ $order->product->product_location }}
          		@endif
          		</td>
          		<td>{{ $order->vendor->phone_number }}</td>
          		@if(!is_null($order->vendor->phone_number2))
          		<td>{{ $order->vendor->phone_number2 }}</td>
          		@else
          		<td>- empty -</td>
          		@endif
          		@if(!is_null($order->vendor->phone_number2))
          		<td>{{ $order->vendor->phone_number3 }}</td>
          		@else
          		<td>- empty -</td>
          		@endif
        	</tr>
        	@endforeach
        	
		</tbody>
	</table>

	<table class="table table-striped">
		<thead>
			<tr>
		    	<th scope="col">Client Name</th>
	          	<th scope="col">Clients location</th>
	          	<th scope="col">Clients Phone Number</th>
	          	<th scope="col">Clients Phone Number2</th>
	          	<th scope="col">Clients Phone Number3</th>
			</tr>
		</thead>
		<tbody>
        	<tr>
          		<td scope="row">{{ $orderInfo->user->name }}</td>
          		<td>
          			@foreach($orderInfo->user->addresses as $add)
          				{{ $add->address }}
          			@endforeach
          		</td>
          		<td>{{ $orderInfo->user->phone_number }}</td>
          		@if(!is_null($orderInfo->user->phone_number2))
          		<td>{{ $orderInfo->user->phone_number2 }}</td>
          		@else
          		<td> - empty - </td>
          		@endif
          		@if(!is_null($orderInfo->user->phone_number3))
          		<td>{{ $orderInfo->user->phone_number3 }}</td>
          		@else
          		<td> - empty - </td>
          		@endif
        	</tr>
        	<tr style="font-size: 20px; font-weight: bold;">
        		<td>Discount:</td>
        		<td colspan="8">{{ $orderInfo->discount_price }} ETB</td>
        	</tr>
          <tr style="font-size: 20px; font-weight: bold;">
            <td>Delivery:</td>
            <td colspan="8">{{ $orderInfo->delivery_price }} ETB</td>
          </tr>
          <tr style="font-size: 20px; font-weight: bold;">
            <td>Total:</td>
            <td colspan="8">{{ $orderInfo->total_price }} ETB</td>
          </tr>
		</tbody>
	</table>

</body>
</html>