<div class="modal modal-edu-general fade" role="dialog" id="send-newsletter{{$newsletter->id}}">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="mdi mdi-send"></i>{{__('app.Send_Newsletter')}} </h4>

                <hr>

            </div>

            <form method="POST" action="/newsletter/{{$newsletter->id}}/send">

                @csrf
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Send')}}</a>

                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>

                </div>

            </form>

        </div>

    </div>

</div>

<div class="modal modal-edu-general fade" role="dialog" id="edit-newsletter{{$newsletter->id}}">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="mdi mdi-tooltip-edit"></i>{{__('app.Edit_Newsletter')}} </h4>

                <hr>

            </div>

            <form method="POST" action="/newsletter/{{$newsletter->id}}/edit">

                @csrf

                @method('PATCH')

                <div class="modal-body">

                    <div class="form-group">

                        <label for="title" style="font-size: 20px;">{{__('app.Title')}}</label>

                        <input type="text" class="form-control" id="title" name="news_title" placeholder="{{__('app.Title')}}" value="{{$newsletter->title}}" required>

                    </div>

                    

                    <div class="form-group">

                        <label for="slug" style="font-size: 20px;">{{__('app.Description')}}</label>

                        <textarea class="form-control" rows="10" id="slug" name="slug" required>{{$newsletter->slug}}</textarea>

                    </div>

                </div>
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Save')}}</a>

                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>

                </div>

            </form>

        </div>

    </div>

</div>

<div class="modal modal-edu-general fade" role="dialog" id="delete-newsletter{{$newsletter->id}}">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="mdi mdi-trash"></i>{{__('app.Delete_Newsletter')}} </h4>

                <hr>

            </div>

            <form method="POST" action="/newsletter/{{$newsletter->id}}/destroy">

                @csrf

                @method('DELETE')
                
                <div class="modal-footer">

                    <button class="btn btn-danger btn-block" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Delete')}}</a>

                </div>

            </form>

        </div>

    </div>

</div>