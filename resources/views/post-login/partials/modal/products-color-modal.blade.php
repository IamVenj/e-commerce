<div class="modal modal-edu-general fade" role="dialog" id=<?= 'select-color'; ?>>

    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i>{{__('app.Select_Colors')}}</h4>

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

            </div>

            <form action="/add-selected-colors" method="post">
                
                @csrf

                <input type="hidden" name="product_id" value="{{$product_id}}">

                <div class="modal-body">

                    <div class="row">
              
                        @foreach($all_colors as $colors)
                       
                          <div class="col-md-2">
                            
                            <div class="form-check">
                            
                              <label class="form-check-label">
                            
                                {{$colors->colour_name}}
                            
                                <div style="padding: 5%; background: {{$colors->hex}};"></div>
                            
                                <input class="checkbox" id="multiple_colors" type="checkbox" name="colors[]" value="{{$colors->id}}">
                            
                              <i class="input-helper"></i></label>
                            
                            </div>

                          </div>
                       
                        @endforeach

                    </div>

                </div>

                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Add_colors')}}</button>

                </div>

            </form>


        </div>

    </div>

</div>