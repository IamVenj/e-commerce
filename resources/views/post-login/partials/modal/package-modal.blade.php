<div class="modal modal-edu-general fade" role="dialog" id="edit-package{{ $package->id }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="fa fa-edit"></i>{{__('app.Edit_Package')}} </h4><div id="pending_status_edit"></div>
                <hr>
            </div>

            <form method="POST" action="/package/edit/{{ $package->id }}">
                @csrf
                @method('PATCH')
                <div class="modal-body">
                  <?php
                  $packages = App\SpecialPackage::with(['packageProduct'])->find($package->id);
                  $priceSum = [];
                  for ($i=0; $i < $packages->packageProduct->count(); $i++) { 
                      array_push($priceSum, $packages->packageProduct[$i]->product->current_price);
                  }

                  ?>
                    <input type="hidden" name="sum" value="@if($priceSum != []){{ array_sum($priceSum) }} @else 0 @endif">
                    <div class="form-group">

                      <label for="product_name">{{__('app.Package_Name')}}</label>
                      <input id="product_name" class="form-control required" name="package_name" type="text" placeholder="{{__('app.Package_Name')}}" required="" value="{{ $package->package_name }}">

                    </div>

                    <div class="form-group">

                      <label for="maxlength-textarea">{{__('app.Package_Description')}}</label>
                      <textarea id="maxlength-textarea" name="slug" class="form-control" maxlength="2000" rows="5" placeholder="{{__('app.Package_Description')}}">{{ $package->slug }}</textarea>

                    </div>

                    <div class="row">
                      
                      <div class="col-md-12">            

                        <div class="form-group">

                          <label for="discountPercent">{{__('app.discountPercent')}}</label>
                          <input id="discountPercent" min="1" step="1" class="form-control" name="discountPercent" type="number" placeholder="{{__('app.discountPercent')}}" value="{{ $package->discount_percent}}">

                        </div>

                      </div>

                      <div class="col-md-12">            

                        <div class="form-group">

                          <label for="availability">{{__('app.how_many_available')}}?</label>
                          <input id="availability" class="form-control" name="availability" type="number" placeholder="{{__('app.Number_of_stock')}}" value="{{ $package->availability }}">

                        </div>

                      </div>

                    </div>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Submit')}}</a>
                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal modal-edu-general fade" role="dialog" id="delete-package{{ $package->id }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="fa fa-trash"></i>{{__('app.Delete_Package')}} </h4><div id="pending_status_edit"></div>
                <hr>
            </div>

            <form method="POST" action="/package/destroy/{{ $package->id }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <p class="lead text-center">Are You sure?</p>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Submit')}}</a>
                </div>
            </form>
        </div>
    </div>
</div>
