<div class="modal modal-edu-general fade" role="dialog" id=<?= "delete-product".$product->id;?>>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="fa fa-trash"></i>{{__('app.Delete')}} - {{$product->product_name}} </h4>
                <hr>
            </div>

            <form method="POST" action="/items/{{ $product->id }}/destroy ">
                @csrf
                @method('delete')
                <div class="modal-footer">
                    <button class="btn btn-danger" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Delete')}}</a>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal modal-edu-general fade" role="dialog" id=<?= "view-product".$product->id;?>>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="card-title mb-3" style="font-weight: bold;">{{$product->product_name}}</h4>
                  <div class="dropdown-divider"></div>
                  <p style="font-size: 14px;">{{$product->slug}}</p>
                  <div class="dropdown-divider"></div>

                  <p class="mb-3 mt-3" style="text-transform: uppercase; background: #e8e8e850; padding:10px 0px 10px 10px; border-top-left-radius: 20px; border-bottom-right-radius: 20px;"><span class="mdi mdi-view-dashboard mr-3"></span>{{$product->category()->first()->category_name}}</p>

                  <div class="dropdown-divider"></div>

                  <div class="row mt-3">
                    <div class="col-md-6">
                        <p class=""><span class="mdi mdi-tag mr-3"></span>{{$product->brand}}</p>
                    </div>
                    <div class="col-md-6">
                      <p class=""><span class="mdi mdi-cash mr-3"></span>{{$product->current_price}}</p>                      
                    </div>
                  </div>

                  <div class="dropdown-divider"></div>

                  
                  <p class="mt-3"><span class="mdi mdi-map-marker-check mr-3"></span>{{$product->product_location}} @if($product->product_location == 'all') {{__('app.Stores')}} @else {{__('app.Store')}} @endif</p>

                  <div class="dropdown-divider"></div>
                  
                  <p class="mt-3"><span class="mdi mdi-calendar-multiple mr-3"></span>{{$product->availability}}</p>
                  <div class="dropdown-divider"></div>

                  <p class="mt-3"><span class="mdi mdi-barcode-scan mr-3"></span>{{$product->sku}}</p>
            </div>
            <div class="modal-footer">    
                <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>
            </div>
        </div>
    </div>
</div>