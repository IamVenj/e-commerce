<div class="modal modal-edu-general fade" role="dialog" id="status-paid-change{{$debt->id}}">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="fa fa-edit"></i>Change Debt Status</h4><div id="pending_status_edit"></div>
                <hr>
            </div>

            <form action="/debt/item/{{$debt->id}}/status/0" method="post">
                @csrf
                @method('PATCH')
                <div class="modal-body">
                    <p class="lead text-center">Are you sure?</p>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-danger" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Yes, I want to change!</a>
                </div>

            </form>
        </div>
    </div>
</div>
<div class="modal modal-edu-general fade" role="dialog" id="status-unpaid-change{{$debt->id}}">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="fa fa-edit"></i>Change Debt Status</h4><div id="pending_status_edit"></div>
                <hr>
            </div>

            <form action="/debt/item/{{$debt->id}}/status/1" method="post">
                @csrf
                @method('PATCH')
                <div class="modal-body">
                    <p class="lead text-center">Are you sure?</p>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Yes, I want to change!</a>
                </div>

            </form>
        </div>
    </div>
</div>