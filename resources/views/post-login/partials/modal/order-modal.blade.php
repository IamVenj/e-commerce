<div class="modal modal-edu-general fade" role="dialog" id="deliverystatus{{ $timestamp[$i] }}">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="fa fa-edit"></i>{{__('app.delivery_confirmation')}} </h4>
                <div class="modal-close-area modal-close-df">
                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>
                </div>
                <hr>
            </div>
            <div class="modal-body">
                <p>You will be updating the status of delivery as delivered</p>
                <p class="lead">Are you sure?</p>
            </div>

            <form method="post" action="/orders/{{ $user_id }}/users/{{ $timestamp[$i] }}/timestamp">
                @csrf
                @method('PATCH')
                <div class="modal-footer">
                    <button class="btn btn-danger" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.update')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>