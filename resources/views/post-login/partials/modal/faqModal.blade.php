<div class="modal modal-edu-general fade" role="dialog" id="edit-faq{{$faq->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="mdi mdi-tooltip-edit" style="margin-right: 10px;"></i>{{__('app.Faq')}} </h4>
                <hr>
            </div>

            <form method="POST" action="/faq/{{$faq->id}}/edit">
                @csrf
                @method('PATCH')
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{__('app.Question')}}</label>
                        <textarea class="form-control" rows="5" name="question" placeholder="The Question">{{ $faq->question }} </textarea>
                    </div>
                    <div class="form-group">
                        <label>{{__('app.Answer')}}</label>
                        <textarea class="form-control" rows="5" name="answer" placeholder="The answer to the question">{{ $faq->answer }} </textarea>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Save')}}</a>
                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal modal-edu-general fade" role="dialog" id="delete-faq{{$faq->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="mdi mdi-delete-forever" style="margin-right: 10px;"></i>{{__('app.Faq')}} </h4>
                <hr>
            </div>

            <form method="POST" action="/faq/{{$faq->id}}/destroy">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    <p class="lead text-center">Are you sure?</p>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Delete')}}</a>
                </div>
            </form>
        </div>
    </div>
</div>