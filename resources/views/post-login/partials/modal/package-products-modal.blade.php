<div class="modal modal-edu-general fade" role="dialog" id=<?= "remove-product".$packageProduct->product->id;?>>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="fa fa-trash"></i>{{__('app.Remove')}} - {{$packageProduct->product->product_name}} </h4>
                <hr>
            </div>

            <form method="POST" action="/package/{{ $packageId }}/product/{{ $packageProduct->id }}/remove">
                @csrf
                @method('delete')
                <div class="modal-body">
                  <p class="lead text-center">Are you sure?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Delete')}}</a>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal modal-edu-general fade" role="dialog" id=<?= "show-product".$packageProduct->product->id;?>>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title">{{$packageProduct->product->product_name}} </h4>
                <hr>
            </div>
            <div class="modal-body">
              <h4 class="card-title mb-3" style="font-weight: bold;">{{$packageProduct->product->product_name}}</h4>
              <div class="dropdown-divider"></div>

              <p style="font-size: 14px;">{{ $packageProduct->product->slug }}</p>
              <div class="dropdown-divider"></div>

              <p class="mb-3 mt-3" style="text-transform: uppercase; background: #e8e8e850; padding:10px 0px 10px 10px; border-top-left-radius: 20px; border-bottom-right-radius: 20px;"><span class="mdi mdi-view-dashboard mr-3"></span>{{$packageProduct->product->category()->first()->category_name}}</p>

              <div class="dropdown-divider"></div>
              <p class="mt-3"><span class="mdi mdi-tag mr-3"></span>{{$packageProduct->product->brand}}</p>

              <div class="dropdown-divider"></div>
              <p class="mt-3"><span class="mdi mdi-cash mr-3"></span>{{$packageProduct->product->current_price}}</p>              

              <div class="dropdown-divider"></div>
              <p class="mt-3"><span class="mdi mdi-map-marker-check mr-3"></span>{{$packageProduct->product->product_location}} @if($packageProduct->product->product_location == 'all') {{__('app.Stores')}} @else {{__('app.Store')}} @endif</p>

              <div class="dropdown-divider"></div>  
              <p class="mt-3"><span class="mdi mdi-calendar-multiple mr-3"></span>{{$packageProduct->product->availability}}</p>
            
              <div class="dropdown-divider"></div>
              <p class="mt-3"><span class="mdi mdi-barcode-scan mr-3"></span>{{$packageProduct->product->sku}}</p>
              
              <div class="dropdown-divider"></div>
              <p class="mt-3"><span class="mdi mdi-eye mr-3"></span>Visits: {{$packageProduct->product->visits}}</p>
            </div>
        </div>
    </div>
</div>

