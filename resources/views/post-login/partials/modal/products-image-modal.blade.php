<div class="modal modal-edu-general fade" role="dialog" id="add-image">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i>{{__('app.AddImage')}} </h4><div id="pending_status_edit"></div>

                <hr>

            </div>

            <form method="POST" action="/add-product-images" id="edit_product_images_form" enctype="multipart/form-data">

                @csrf

                <input type="hidden" name="product_id" value={{$product_id}}>

                <input type="hidden" name="product_name" value={{$product_name}}>

                <div class="modal-body">

                    <div class="form-group">
                                
                        <input type="file" name="new_image" class="file-upload-default">
                      
                        <div class="input-group col-xs-12">
                      
                            <input type="text" class="form-control file-upload-info" id="file-upload-info" disabled="" placeholder="Upload Image">
                      
                            <span class="input-group-append">
                      
                                <button class="file-upload-browse btn btn-primary" type="button">{{__('app.AddImage')}}</button>
                      
                            </span>

                        </div>
                      
                    </div>

                </div>
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Submit')}}</a>

                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>

                </div>

            </form>

        </div>

    </div>

</div>
