<div class="modal modal-edu-general fade" role="dialog" id="delete-vendor{{$vendor->id}}">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="fa fa-edit"></i>Delete Vendor - {{ $vendor->shop_name }}</h4><div id="pending_status_edit"></div>
                <hr>
            </div>

            <form action="/destroy/{{$vendor->id}}/vendor" method="post">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    <p class="lead text-center">Are you sure?</p>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-danger" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Yes, I want to Delete!</a>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal modal-edu-general fade" role="dialog" id="deactivate-vendor{{$vendor->id}}">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="fa fa-edit"></i>Deactivate Vendor - {{ $vendor->shop_name }}</h4><div id="pending_status_edit"></div>
                <hr>
            </div>

            <form action="/deactivate/{{$vendor->id}}/vendor" method="post">
                @csrf
                @method('PATCH')
                <div class="modal-body">
                    <p class="lead text-center">Are you sure?</p>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-danger" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Yes, I want to Deactivate!</a>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal modal-edu-general fade" role="dialog" id="activate-vendor{{$vendor->id}}">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="fa fa-edit"></i>Activate Vendor - {{ $vendor->shop_name }}</h4><div id="pending_status_edit"></div>
                <hr>
            </div>

            <form action="/activate/{{$vendor->id}}/vendor" method="post">
                @csrf
                @method('PATCH')
                <div class="modal-body">
                    <p class="lead text-center">Are you sure?</p>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Yes, I want to Activate!</a>
                </div>

            </form>
        </div>
    </div>
</div>