<div class="modal modal-edu-general fade" role="dialog" id="assignPersonnel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title">Assign Personnel</h4>
                <hr>
            </div>

            <form method="POST" action="/delivery/personnel/{{$orders[$i]->id}}/assign">
                @csrf
                @method('PATCH')
                <div class="modal-body">
                    <div class="form-group">
                        <label for="phone" style="font-size: 20px;">Delivery Personnel </label>
                        <select name="del_personnel">
                            <option>Select Personnel</option>
                            @foreach($delivery_personnel as $dp)
                            <option value="{{ $dp->id }}">{{ $dp->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Save')}}</a>
                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>
                </div>
            </form>
        </div>
    </div>
</div>