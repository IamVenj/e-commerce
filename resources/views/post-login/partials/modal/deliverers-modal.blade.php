<div class="modal modal-edu-general fade" role="dialog" id="edit-delivery{{$dp->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="mdi mdi-tooltip-edit" style="margin-right: 10px;"></i>Edit {{ $dp->name }} </h4>
                <hr>
            </div>

            <form method="POST" action="/delivery/personnel/{{$dp->id}}/edit">
                @csrf
                @method('PATCH')
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name" style="font-size: 20px;">Delivery Personnel Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Delivery Personnel Name" required value="{{ $dp->name }}">
                    </div>
                    
                    <div class="form-group">
                        <label for="phone" style="font-size: 20px;">Delivery Personnel Phone Number</label>
                        <input type="text" class="form-control" id="phone" name="phone_number" placeholder="Delivery Personnel Phone Number" required value="{{ $dp->phone_number }}">
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Save')}}</a>
                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal modal-edu-general fade" role="dialog" id="delete-delivery{{$dp->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="mdi mdi-delete-forever" style="margin-right: 10px;"></i>Delete {{ $dp->name }} </h4>
                <hr>
            </div>

            <form method="POST" action="/delivery/personnel/{{$dp->id}}/destroy">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    <p class="lead text-center">Are you sure?</p>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Delete')}}</a>
                </div>
            </form>
        </div>
    </div>
</div>