<div class="modal modal-edu-general fade" role="dialog" id=<?= "add-products"?>>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-3">
                <h4 class="modal-title"><i class="fa fa-trash"></i>{{__('app.Add')}}</h4>
                <hr>
            </div>

            <form method="POST" action="/package/product/{{ $packageId }}/add">
                @csrf
                <div class="modal-body">
                    <div class="panel-group" id="example-collapse-accordion">
                        <div class="panel panel-default" style="overflow:visible;">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                     {{ __('app.Select_products') }}
                                </h6>
                            </div>
                            <div>
                                <div class="panel-body">
                                    <select id="product_multiselect_option" name="product[]" required="" class="required" multiple="multiple">
                                    <?php
                                    $products = App\Product::where('user_id', $pg->user->id)->where('availability', '>', 0)->get();
                                    ?>
                                    @foreach($products as $product)
                                        <option value="{{ $product->id }}">{{ $product->product_name }}</option>
                                    @endforeach               
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Add')}}</a>
                </div>
            </form>
        </div>
    </div>
</div>