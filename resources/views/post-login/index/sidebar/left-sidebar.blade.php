<nav class="sidebar sidebar-offcanvas" id="sidebar">

  <ul class="nav">
    <li class="nav-item">
      <a class="nav-link" href="/dashboard">
        <i class="mdi mdi-home menu-icon"></i>
        <span class="menu-title">{{__('app.Dashboard')}}</span>
      </a>
    </li>

    @if(auth()->user()->role == 1)
    
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#customize-pages" aria-expanded="false" aria-controls="customize-pages">
        <i class="mdi mdi-layers menu-icon"></i>
        <span class="menu-title">{{__('app.customize_pages')}}</span>
        <i class="menu-arrow"></i>
      </a>

      <div class="collapse" id="customize-pages">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="/custom-carousel">{{__('app.home_carousel')}}</a></li>
          {{-- <li class="nav-item"> <a class="nav-link" href="/special-offers">{{__('app.special_offers')}}</a></li> --}}
          <li class="nav-item"> <a class="nav-link" href="/create/faq">{{__('app.faq')}}</a></li>
          <li class="nav-item"> <a class="nav-link" href="/update/shipping/guide">{{__('app.ship_guide')}}</a></li>
          <li class="nav-item"> <a class="nav-link" href="/update/shipping/return">{{__('app.ship_return')}}</a></li>
          <li class="nav-item"> <a class="nav-link" href="/update/about">{{__('app.about_us')}}</a></li>
          <li class="nav-item"> <a class="nav-link" href="/update/Terms-and-conditions">{{__('app.terms_and_conditions')}}</a></li>
        </ul>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="/category">
        <i class="mdi mdi-format-list-bulleted-type menu-icon"></i>
        <span class="menu-title">{{__('app.Category')}}</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="/admin/vendors">
        <i class="mdi mdi-account-multiple menu-icon"></i>
        <span class="menu-title">{{__('app.Vendors')}}</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#admin-product" aria-expanded="false" aria-controls="admin-product">
        <i class="mdi mdi-view-headline menu-icon"></i>
        <span class="menu-title">{{__('app.product')}}</span>
        <i class="menu-arrow"></i>
      </a>

      <div class="collapse" id="admin-product">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="/vendor/items">{{__('app.vendor_item_list')}}</a></li>
          <li class="nav-item"> <a class="nav-link" href="/vendor-reviews">{{__('app.Vendor_Reviews')}}</a></li>
        </ul>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#package" aria-expanded="false" aria-controls="form-elements">
        <i class="mdi mdi-package menu-icon"></i>
        <span class="menu-title">{{__('app.Package')}}</span>
        <i class="menu-arrow"></i>
      </a>

      <div class="collapse" id="package">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"><a class="nav-link" href="/package">{{__('app.View_Packages')}}</a></li>
          <li class="nav-item"><a class="nav-link" href="/create/package">{{__('app.create_packages')}}</a></li>
        </ul>
      </div>
    </li>   
    
    <li class="nav-item">
      <a class="nav-link" href="/delivery/personnel">
        <i class="mdi mdi-truck menu-icon"></i>
        <span class="menu-title">Delivery personnel</span>
      </a>
    </li>    

    <li class="nav-item">
      <a class="nav-link" href="/admin/orders">
        <i class="mdi mdi-format-list-numbered menu-icon"></i>
        <span class="menu-title">{{__('app.Orders')}}</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="/users">
        <i class="mdi mdi-account menu-icon"></i>
        <span class="menu-title">{{__('app.Users')}}</span>
      </a>
    </li>

    

    <li class="nav-item">
      <a class="nav-link" href="/debts">
        <i class="mdi mdi-cash menu-icon"></i>
        <span class="menu-title">{{__('app.Debts')}}</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="/newsletter">
        <i class="mdi mdi-send menu-icon"></i>
        <span class="menu-title">{{__('app.Newsletter')}}</span>
      </a>
    </li>

    @endif

    @if(auth()->user()->role == 2)

    <li class="nav-item">
      <a class="nav-link" href="/orders/{{ \Auth::user()->shop_name }}/vendor">
        <i class="mdi mdi-speaker menu-icon"></i>
        <span class="menu-title">{{__('app.Orders')}}</span>
      </a>
    </li>

    @if(auth()->user()->credit == 1)

    <li class="nav-item">
      <a class="nav-link" href="/my-debt">
        <i class="mdi mdi-cash menu-icon"></i>
        <span class="menu-title">{{__('app.My_Credit')}}</span>
      </a>
    </li>

    @endif

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#products" aria-expanded="false" aria-controls="form-elements">
        <i class="mdi mdi-view-headline menu-icon"></i>
        <span class="menu-title">{{__('app.Products')}}</span>
        <i class="menu-arrow"></i>
      </a>

      <div class="collapse" id="products">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"><a class="nav-link" href="/create-product">{{__('app.Create_Product')}}</a></li>
          <li class="nav-item"><a class="nav-link" href="/view-products">{{__('app.View_Products')}}</a></li>
        </ul>
      </div>
    </li> 

     

    @endif

  </ul>
</nav>