@extends('pre-login.index.index')

@section('content')

<div id="breadcrumb">
	<div class="container">
		<ul class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">{{__('app.terms_and_conditions')}}</li>
		</ul>
	</div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="shiping-methods">
                    <div class="section-title">
                        <h4 class="title">{{__('app.terms_and_conditions')}}</h4>
                    </div>
                    <div class="caption" style="font-size: 20px; text-align: center;">
                        @if(!is_null($terms_and_condition))
                        <p><?=  $terms_and_condition; ?></p>
                        @else
                        <h4 class="alert-danger" style="min-width: 100%; padding:10px;"><span class="fa fa-frown-o" style="margin-right: 5px;"></span>Sorry! There is no content here!</h4>
                        @endif
                        @if(\Storage::exists('public/uploads/document/Symmart-Terms-and-Conditions.pdf'))
                            <a class="btn btn-success" target="_blank" style="margin-top: 40px;" href="{{ Storage::url('public/uploads/document/Symmart-Terms-and-Conditions.pdf') }}">Download PDF<span class="fa fa-download" style="margin-left: 10px;"></span></a>
                        @endif
                    </div>
                </div>                  
            </div>
        </div>
    </div>
</div>

@endsection