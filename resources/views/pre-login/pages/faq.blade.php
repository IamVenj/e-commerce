@extends('pre-login.index.index')

@section('content')

<div id="breadcrumb">
	<div class="container">
		<ul class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">FAQ</li>
		</ul>
	</div>
</div>
@if(is_null($faqz))
<div class="col-md-12">        
    <h4 class="alert-danger" style="min-width: 100%; padding:10px;"><span class="fa fa-frown-o" style="margin-right: 5px;"></span>There are no FAQ listed!</h4>
</div>
@endif
<section class="cd-faq">
    <ul class="cd-faq-group">
        @foreach($faqz as $faq)
        <li>
            <a class="cd-faq-trigger" href="#0"><span class="fa fa-chevron-down" style="margin-right: 10px;"></span> {{$faq->question}} </a>
            <div class="cd-faq-content">
                <p>{{ $faq->answer }}</p>
            </div>
        </li>
        @endforeach
    </ul>
</section>

@endsection