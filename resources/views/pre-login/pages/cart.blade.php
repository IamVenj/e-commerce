@extends('pre-login.index.index')

@section('content')

<!-- BREADCRUMB -->

<div id="breadcrumb">

	<div class="container">

		<ul class="breadcrumb">

			<li><a href="#">{{__('app.home')}}</a></li>

			<li class="active">{{__('app.cart')}}</li>

		</ul>

	</div>

</div>

<!-- /BREADCRUMB -->

<!-- section -->

<div class="section">

	<div class="container">

		<div class="row">

			@if(Cart::instance('default')->count() == 0)

			<div class="section section-grey col-md-8 col-md-offset-2 cart" style="margin-bottom: 50px; margin-top: 50px;">
					
					<span class="fa fa-shopping-cart text-center" style="font-size: 50px; padding-left: 50%; padding-right: 50%; "></span>	

					<h1 class="text-center text-white">{{__('app.empty_cart')}}</h1>

					<p class="text-center text-white">{{__('app.empty_added_cart')}}</p>

			</div>

			@else

			<div class="section section-grey col-md-12" style="margin-top: 20px;padding-left: 25px; padding-right: 25px; border-radius: 5px; border-top-right-radius: 100px; border-bottom-left-radius: 100px;">

				<div class="order-summary clearfix">

					<div class="section-title">

						<h3 class="title">{{__('app.order_review')}}</h3>

					</div>


					<form action="/payment/{{auth()->user()->id}}/pay" method="post">

						@csrf

						<table class="shopping-cart-table table">

							<thead>

								<tr>

									<th>{{__('app.product')}}</th>

									<th></th>

									<th class="text-center">{{__('app.price')}}</th>

									<th class="text-center">{{__('app.quantity')}}</th>

									<th class="text-center">{{__('app.total')}}</th>

									<th class="text-right"></th>

								</tr>

							</thead>

							<tbody>

								<input type="hidden" name="global-token" value="{{csrf_token()}}">

								<?php $i = 1; ?>

								@foreach(Cart::content() as $item)

								<input type="hidden" name="itemId_pay" value="{{ $item->rowId }}">

								@if($item->associatedModel == "App\SpecialPackage")

								<input type="hidden" name="itemName_pay" value="{{$item->model->package_name}}">
								<input type="hidden" name="price_pay" class="total-price-of-one-item{{$i}}" value="{{$item->model->discount_price * $item->qty}}">
								<input type="hidden" id="current_price{{$i}}" value="{{$item->model->discount_price}}">

								@elseif($item->associatedModel == "App\Product")


								<input type="hidden" name="itemName_pay" value="{{$item->model->product_name}}">
								<input type="hidden" name="price_pay" class="total-price-of-one-item{{$i}}" value="{{$item->model->current_price * $item->qty}}">
								<input type="hidden" id="current_price{{$i}}" value="{{$item->model->current_price}}">

								@endif

								<tr>

									@if($item->associatedModel == "App\Product")

									<td class="thumb">

										<img src="{{ $item->model->productImage()->first()->image_public_id }}" alt="{{$item->model->productImage()->first()->image_public_id}}">

									</td>

									<td class="details">

										<a href="#">{{$item->model->product_name}}</a>

										<ul>

											@if(!is_null($item->options->size))

											<li><span>Size: {{$item->options->size}}</span></li>

											@endif

											<li><span>Color: <?php $color = App\Color::find($item->options->color); ?><div style="background-color: {{$color->hex}}; height: 10px; width: 10px;"></div> </span></li>

										</ul>

									</td>

									<td class="price text-center">

										<strong>{{$item->model->current_price}} {{__('app.birr')}}</strong>

										@if(!is_null($item->model->old_price))

										<br>

										<del class="font-weak">

											<small>{{$item->model->old_price}} {{__('app.birr')}}</small>

										</del>

										@endif

									</td>

									<td class="qty text-center">

										<input class="input" id="qty{{$i}}" min="1" name="quantity_pay[]" onchange="update_cart(<?= $i; ?>)" type="number" value="{{$item->qty}}">

									</td>

									<td class="total text-center">

										<strong class="primary-color total-price-of-one-item{{$i}}">{{$item->qty * $item->model->current_price}}</strong>

									</td>

									<td class="text-right">

										<button class="main-btn icon-btn" type="button" onclick="removeFromCart(<?= $i; ?>)"><i class="fa fa-close"></i></button>

									</td>

									@elseif($item->associatedModel == "App\SpecialPackage")

									<td class="thumb">

										<img src="{{ $item->model->images->first()->image_public_id }}" alt="{{$item->model->images->first()->image_public_id}}">

									</td>

									<td class="details">

										<a href="#">{{$item->model->package_name}}</a>

									</td>

									<td class="price text-center">

										<strong>{{$item->model->discount_price}} {{__('app.birr')}}</strong>

									</td>

									<td class="qty text-center">

										<input class="input" id="qty{{$i}}" min="1" name="quantity_pay[]" onchange="update_cart(<?= $i; ?>)" type="number" value="{{$item->qty}}">

									</td>

									<td class="total text-center">

										<strong class="primary-color total-price-of-one-item{{$i}}">{{$item->qty * $item->model->discount_price}}</strong>

									</td>

									<td class="text-right">

										<button class="main-btn icon-btn" type="button" onclick="removeFromCart(<?= $i; ?>)"><i class="fa fa-close"></i></button>

									</td>

									@endif

								</tr>

								<input type="hidden" id="rowId{{ $i }}" value="{{ $item->rowId }}">

								<?php $i++; ?>

								@endforeach
								
							</tbody>

							<tfoot>

								<tr>

									<th class="empty" colspan="3"></th>
									<th>{{__('app.SUBTOTAL')}}</th>
									<th colspan="3" class="sub-total" id="sub-total-cart-main"></th>

								</tr>


								<tr>

									<th class="empty" colspan="3"></th>
									<th>{{__('app.DELIVERY')}}</th>
									<td colspan="3" id="delivery_price"></td>
									<input type="hidden" name="delivery_price">



								</tr>

								<tr>

									<th class="empty" colspan="3"></th>
									<th>{{__('app.DISCOUNT')}}</th>
									<th colspan="3" class="sub-total" id="discount"></th>
									<input type="hidden" name="discount" id="discountPrice">

								</tr>


								<tr>

									<th class="empty" colspan="3"></th>
									<th style="text-transform: uppercase;">{{__('app.total')}}</th>
									<th colspan="3" class="total"  id="total_price_main"></th>
									<input type="hidden" name="total_price_main">

								</tr>


							</tfoot>

						</table>

						<div class="row">
							<div class="col-md-6">
								<div class="payments-methods">
				
									<div class="section-title" style="margin-bottom: 30px;">
										<h4 class="title">{{__('app.DeliveryMethod')}}</h4>
									</div>
					
									<div class="input-checkbox" style="margin-bottom: 5px;">
										<input type="radio" name="deliveryMethod" id="delivery-1" value="Symmart Standard" checked="" onchange="get_cart()">
										<label for="delivery-1" style="font-size: 14px; font-weight: 500;">Symmart Standard Delivery<small class="business-days"><span class="fa fa-clock-o"></span> 3-5 Business days (Addis Ababa)</small></label>
									</div>
					
									<div class="input-checkbox" style="margin-bottom: 5px;">
										<input type="radio" name="deliveryMethod" id="delivery-2" value="Symmart Express" onchange="get_cart()">
										<label for="delivery-2" style="font-size: 14px; font-weight: 500;">Symmart Express (Not Applicable yet)<small class="business-days"><span class="fa fa-clock-o"></span> 2 Business Days (Addis Ababa)</small></label>
									</div>	

									<div class="input-checkbox" style="margin-bottom: 5px;">
										<input type="radio" name="deliveryMethod" id="delivery-3" value="Symmart Premium" onchange="get_cart()">
										<label for="delivery-3" style="font-size: 14px; font-weight: 500;">Symmart Premium<small class="business-days"><span class="fa fa-clock-o"></span> 1 Business Day (Addis Ababa)</small></label>
									</div>
					
									<div class="input-checkbox">
										<input type="radio" name="deliveryMethod" id="delivery-4" value="Symmart National" onchange="get_cart()">
										<label for="delivery-4" style="font-size: 14px; font-weight: 500;">Symmart National<small class="business-days"><span class="fa fa-clock-o"></span> 5-6 Business Days (All Regions)</small></label>
									</div>				
						
								</div>
							</div>
							<div class="col-md-6">
								<div class="payments-methods">
				
									<div class="section-title" style="margin-bottom: 30px;">
										<h4 class="title">{{__('app.payment_methods')}}</h4>
									</div>

									<h6 id="warning-payment-method"></h6>
					
									<div class="input-checkbox" id="bank-payment" style="margin-bottom: 5px; display: none;">
										<input type="radio" name="payments" id="payments-1" value="bank">
										<label for="payments-1" style="font-size: 14px; font-weight: 500;">{{__('app.Bank')}}</label>
									</div>
					
									<div class="input-checkbox" id="cash-payment" style="display: none;">
										<input type="radio" name="payments" value="on-delivery" id="payments-2">
										<label for="payments-2" style="font-size: 14px; font-weight: 500;">{{__('app.cash_delivery')}}</label>
									</div>				
						
								</div>
							</div>
						</div>

						<div class="pull-right">
							<button class="primary-btn" type="submit" id="place_order_btn">{{__('app.place_order')}}</button>
						</div>

					</form>

				</div>

			</div>

			@endif

		</div>

	</div>

</div>

@endsection