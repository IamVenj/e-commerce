@extends('pre-login.index.index')

@section('content')

<!-- BREADCRUMB -->

<div id="breadcrumb">

	<div class="container">

		<ul class="breadcrumb">

			<li><a href="/">Home</a></li>

			<li class="active">My Account</li>

		</ul>

	</div>

</div>

@include('post-login.partials.modal.account-modal')
<!-- /BREADCRUMB -->

<div class="demo" id="section-load"></div>

<!-- section -->

<div class="section" id="section-wish" style="display: none;">

	<div class="container">

		<div class="row" style="margin-top: 20px;">
		        	
        	<div class="col-md-4">
        		
        		<a data-toggle="modal" href="#" data-target="#profile-pic-update" style="color: #fff; text-decoration: none;"><button type="button" class="btn btn-success btn-block" style="border-top-left-radius: 30px; border-bottom-right-radius: 30px;">Change Profile Picture</button></a>

        	</div>

        	<div class="col-md-4">
        		
        		<a href="#" data-toggle="modal" href="#" data-target="#upassword-change" style="color: #fff; text-decoration: none;"><button type="button" class="btn btn-success btn-block" style="border-top-left-radius: 30px; border-bottom-right-radius: 30px;">Change Password</button></a>

        	</div>

        	<div class="col-md-4">
        		
        		<a href="#" data-toggle="modal" href="#" data-target="#status-change" style="color: #fff; text-decoration: none;"><button type="button" class="btn btn-danger btn-block" style="border-top-left-radius: 30px; border-bottom-right-radius: 30px;">Change Status To Vendor</button></a>

        	</div>	

        </div>

        <form action="/account" method="post">

        	@csrf

        	@method('PATCH')

	    	<div class="section">

				<div class="row">

					<div class="col-md-12">
						
						<div class="form-group">
	         
				            <label for="name" style="font-size: 15px;">Name</label>
				        
				            <input type="text" name="name" value="{{$user->name}}" class="form-control" id="name" placeholder="Name" style="font-size: 15px; ">
				        
				        </div>

					</div>

					<div class="col-md-12">
						
						<div class="form-group">
	         
				            <label for="email" style="font-size: 15px;">Email</label>
				        
				            <input type="email" name="email" value="{{$user->email}}" class="form-control" id="email" placeholder="email" style="font-size: 15px; ">
				        
				        </div>

					</div>

					<div class="col-md-12">
						
						<div class="form-group">
	         
				            <label for="phone_number" style="font-size: 15px;">Phone Number</label>
				        
				            <input type="text" name="phone_number" value="{{$user->phone_number}}" class="form-control" id="phone_number" placeholder="Phone Number" style="font-size: 15px; ">
				        
				        </div>

					</div>

				</div>

	        </div>
				
			<button class="main-btn primary-btn">Update</button>

		</form>

	</div>

</div>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">

	$(window).on('load', function() {

		document.getElementById("section-load").style.display = 'none';
		
		document.getElementById("section-wish").style.display = 'block';

	});

	$(document).ready(function() {

  		var image_select = $(".image");

  		image_select.change(function(){
      
		    if (this.files && this.files[0]) {

		      var reader = new FileReader();

		      reader.onload = function (e) {

		          $('.picture-src').attr('src', e.target.result);

		      }

		      reader.readAsDataURL(this.files[0]);

		    }
		  
		});

	});

</script>

@endsection