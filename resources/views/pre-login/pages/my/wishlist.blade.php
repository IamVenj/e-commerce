@extends('pre-login.index.index')

@section('content')

<!-- BREADCRUMB -->

<div id="breadcrumb">

	<div class="container">

		<ul class="breadcrumb">

			<li><a href="/">Home</a></li>

			<li class="active">WishList</li>

		</ul>

	</div>

</div>

<!-- /BREADCRUMB -->

<div class="demo" id="section-load"></div>

<!-- section -->

<div class="section" id="section-wish" style="display: none;">

	<div class="container">

		<div class="row">

			@if($user_wishes->count() == 0)

			<div class="section section-grey col-md-8 col-md-offset-2 cart" style="margin-bottom: 50px; margin-top: 50px;">
					
					<span class="fa fa-shopping-cart text-center" style="font-size: 50px; padding-left: 50%; padding-right: 50%; "></span>	

					<h1 class="text-center text-white">Your WishList is empty</h1>

					<p class="text-center text-white">You have not added any wishes to your Wish list</p>

			</div>

			@else

			<div class="section section-grey col-md-12" style="margin-top: 20px;padding-left: 25px; padding-right: 25px; border-radius: 5px; border-top-right-radius: 100px; border-bottom-left-radius: 100px;">

				<div class="order-summary clearfix">

					<div class="section-title">

						<h3 class="title">Wishes</h3>

					</div>

					<div class="table-responsive">

			          <table class="shopping-cart-table table dataTable1">

			            <thead>

			              <tr>

			                  <th>Product</th>

			                  <th>Product Name</th>

			                  <th>Brand</th>

			                  <th>Price</th>

			                  <th>Vendor</th>

			                  <th>Location</th>

			                  <th></th>

			              </tr>

			            </thead>

			            <tbody>

			            	@foreach($user_wishes as $wish)

			              <tr>

			                  <td class="thumb"><img src="{{ $wish->product->productImage()->first()->image_public_id }}" alt="{{$wish->product->productImage()->first()->image_public_id}}"></td>

			                  <td>{{$wish->product->product_name}}</td>

			                  <td>{{$wish->product->brand}}</td>

			                  <td>{{$wish->product->current_price}}</td>

			                  <td>{{$wish->product->user()->first()->shop_name}}</td>

			                  <td>{{$wish->product->product_location}}</td>


			                  <td>

			                  	<form action="/remove-wish/{{$wish->id}}" method="post">

			                  		@csrf

			                  		@method("DELETE")
			                  		
			                    	<button class="main-btn icon-btn" type="submit"><i class="fa fa-close"></i></button>

			                  	</form>

			                  </td>

			              </tr>

			              @endforeach

			            </tbody>

			          </table>

			        </div>

				</div>

			</div>

			@endif

		</div>

	</div>

</div>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">
	
	$(document).ready(function() {

		$(".dataTable1").DataTable();
	
	});

	$(window).on('load', function() {

		document.getElementById("section-load").style.display = 'none';
		
		document.getElementById("section-wish").style.display = 'block';

	});

</script>

@endsection