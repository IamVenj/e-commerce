@extends('pre-login.index.index')

@section('content')

<!-- BREADCRUMB -->

<div id="breadcrumb">

	<div class="container">

		<ul class="breadcrumb">

			<li><a href="/">Home</a></li>

			<li class="active">Orders</li>

		</ul>

	</div>

</div>

<!-- /BREADCRUMB -->

<div class="demo" id="section-load"></div>

<!-- section -->

<div class="section" id="section-order" style="display: none;">

	<div class="container">

		<div class="row">

			@if(is_null($orders))

			<div class="section section-grey col-md-8 col-md-offset-2 cart" style="margin-bottom: 50px; margin-top: 50px;">
					
				<h1 class="text-center text-white">Empty</h1>

				<p class="text-center text-white">You have not ordered any item</p>

			</div>

			@else			

			<div class="section section-grey col-md-12">
				
				<div class="table-responsive">

		          <table class="order-listing table dataTable1">

		            <thead>

		              <tr>

		                  <th>Order code</th>
		                  <th>Total Amount</th>
		                  <th>Discount</th>
		                  <th>Delivery Price</th>
		                  <th>Delivery Method</th>
		                  <th>Payment Method</th>
		                  <th>Payment</th>
		                  <th>Unique bank code</th>
		                  <th>Order Time</th>
		                  <th></th>
		                  <th></th>
		                  
		              </tr>

		            </thead>

		            <tbody>

        				@foreach($orders as $order)

		              	<tr>
		              		@include('pre-login.partials.modal.order-modal')

		                  	<td>{{$order->order_code}}</td>
		                  	<td>{{$order->total_price}} {{ __('app.birr') }}</td>
		                  	<td>{{$order->discount_price}} {{ __('app.birr') }}</td>
		                  	<td>{{$order->delivery_price}} {{ __('app.birr') }}</td>
		                  	<td>{{$order->delivery_method}}</td>
		                  	<td>{{$order->payment_method}}</td>
		                  	<td>
		                    	@if($order->payment_status == 0 && $order->payment_method == 'bank')
				              	<label class="badge badge-warning" style="background-color: #D6A90E;">
				              		Pending
				              		<span class="mdi mdi-refresh" style="margin-left: 1px;"></span>
				              	</label>
				              	@elseif($order->payment_status == 0 && $order->payment_method == 'on-delivery')
				              	<label class="badge badge-danger" style="background-color: red;">
				              		Not Paid
				              		<span class="mdi mdi-close" style="margin-left: 1px;"></span>
				              	</label>
				              	@elseif($order->payment_status == 1)
				              	<label class="badge badge-success" style="background-color: green;">
				              		Paid
				              		<span class="mdi mdi-check" style="margin-left: 1px;"></span>
				              	</label>
				              	@endif
		                  	</td>

		                  	<td>{{$order->unique_bank_code}}</td>
		                  	<td>{{$order->created_at->diffForHumans()}}</td>
		                  	<td><a href="/my/{{ $order->id }}/orders" class="btn btn-warning" title="View Order Detail"><span class="fa fa-eye"></span></a></td>
		                  	@if($order->vendor_delivery_confirmation == 1)
		                  	<td><p class="text-success" style="font-weight: bold; font-size: 15px;">Delivered <span class="fa fa-check" style="padding: 10px; border-radius: 50px; background-color: green; color: #fff;"></span></p></td>
		                  	@else
		                  	<td><a data-toggle="modal" href="#" data-target="#cancel-order<?= $order->id; ?>"><button class="btn btn-danger">Cancel Order</button></a></td>
		                  	@endif
		              	</tr>

		              	@endforeach

		            </tbody>

		          </table>

		        </div>

			</div>

			@endif

		</div>

	</div>

</div>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">
	
	$(document).ready(function() {

		$(".dataTable1").DataTable();
	
	});

	$(window).on('load', function() {

		document.getElementById("section-load").style.display = 'none';
		
		document.getElementById("section-order").style.display = 'block';

	});

</script>

@endsection