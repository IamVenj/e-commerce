@extends('pre-login.index.index')

@section('content')

<!-- BREADCRUMB -->

<div id="breadcrumb">

	<div class="container">

		<ul class="breadcrumb">

			<li><a href="/">Home</a></li>

			<li class="active">Orders</li>

		</ul>

	</div>

</div>

<!-- /BREADCRUMB -->

<div class="demo" id="section-load"></div>

<!-- section -->

<div class="section" id="section-order" style="display: none;">

	<div class="container">

		<div class="row">

			@if(is_null($orders))

			<div class="section section-grey col-md-8 col-md-offset-2 cart" style="margin-bottom: 50px; margin-top: 50px;">
					
				<h1 class="text-center text-white">Empty</h1>

				<p class="text-center text-white">You have not ordered any item</p>

			</div>

			@else

			

			<div class="section section-grey col-md-12">
				
				<div class="table-responsive">

		          <table class="order-listing table dataTable3">

		            <thead>

		              <tr>

		                  <th>Order #</th>

		                  <th>Item Name</th>

		                  <th>Item SKU</th>
		                  
		                  <th>Item Price</th>

		                  <th>Quantity</th>

		                  <th>Total Price</th>

		                  <th>Vendor</th>

		                  <th>Vendor's Phone number</th>

		                  <th>Payment status</th>

		                  <th></th>
		                  
		              </tr>

		            </thead>

		            <tbody>

        				@for($i=0; $i < $orders->count(); $i++)

		              	<tr>

		              		@include('pre-login.partials.modal.delivery-modal')

		                  	<td>{{$orders[$i]->order_code}}</td>

		                  	@if(!is_null($orders[$i]->product_id))

		                  	<td>{{ $orders[$i]->product->product_name }}</td>
		                  	
		                  	<td>{{$orders[$i]->product->sku}}</td>
		                  	
		                  	<td>{{$orders[$i]->product->current_price}} {{ __('app.birr') }}</td>	

		                  	@endif

		                  	@if(!is_null($orders[$i]->package_id))

		                  	<td>{{$orders[$i]->package->package_name}}</td>
		                  	
		                  	<td></td>
		                  	
		                  	<td>{{$orders[$i]->package->discount_price}} {{ __('app.birr') }}</td>	

		                  	@endif	               

		                  	<td>x{{$orders[$i]->quantity}}</td>

		                  	<td>{{$orders[$i]->quantity * $orders[$i]->paid_amount}} {{ __('app.birr') }}</td>

		                  	@if(!is_null($orders[$i]->product_id))

		                  	<td>{{ $orders[$i]->product->user->shop_name }}</td>

		                  	<td>{{ $orders[$i]->product->user->phone_number }}</td>

		                  	@endif

		                  	@if(!is_null($orders[$i]->package_id))

		                  	<td>{{ $orders[$i]->package->user->shop_name }}</td>

		                  	<td>{{ $orders[$i]->package->user->phone_number }}</td>

		                  	@endif

		                  	<td>
		                    	@if($orders[$i]->mainOrderId->payment_status == 0 && $orders[$i]->mainOrderId->payment_method == 'bank')
				              	<label class="badge badge-warning" style="background-color: #D6A90E;">
				              		Pending
				              		<span class="mdi mdi-refresh" style="margin-left: 10px;"></span>
				              	</label>
				              	@elseif($orders[$i]->mainOrderId->payment_status == 0 && $orders[$i]->mainOrderId->payment_method == 'on-delivery')
				              	<label class="badge badge-danger" style="background-color: red;">
				              		Not Paid
				              		<span class="mdi mdi-close" style="margin-left: 10px;"></span>
				              	</label>
				              	@elseif($orders[$i]->mainOrderId->payment_status == 1)
				              	<label class="badge badge-success" style="background-color: green;">
				              		Paid
				              		<span class="mdi mdi-check" style="margin-left: 10px;"></span>
				              	</label>
				              	@endif
		                  	</td>

		                  	@if($orders[$i]->mainOrderId->vendor_delivery_confirmation == 1 && $orders[$i]->mainOrderId->customer_delivery_confirmation == 1)

		                  	<td><p class="text-success" style="font-weight: bold; font-size: 15px;">Delivered <span class="fa fa-check" style="padding: 10px; border-radius: 50px; background-color: green; color: #fff;"></span></p></td>

		                  	@elseif($orders[$i]->mainOrderId->vendor_delivery_confirmation == 0 && $orders[$i]->mainOrderId->customer_delivery_confirmation == 0)

		                  	<td>
		                  		
		                  		<a data-toggle="modal" href="#" data-target="#cancel-order<?= $orders[$i]->id; ?>"><button class="btn btn-danger">Cancel Item Order</button></a>

		                  	</td>

		                  	@endif

		              	</tr>

		              	@endfor

		            </tbody>

		          </table>

		        </div>


			</div>

			@endif

		</div>

	</div>

</div>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">
	
	$(document).ready(function() {

		$(".dataTable3").DataTable();
	
	});

	$(window).on('load', function() {

		document.getElementById("section-load").style.display = 'none';
		
		document.getElementById("section-order").style.display = 'block';

	});

</script>

@endsection