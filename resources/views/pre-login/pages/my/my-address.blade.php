@extends('pre-login.index.index')

@section('content')

<!-- BREADCRUMB -->

<div id="breadcrumb">

	<div class="container">

		<ul class="breadcrumb">

			<li><a href="/">Home</a></li>

			<li class="active">My Account</li>

		</ul>

	</div>

</div>

<div class="section">

	<div class="container">

		<div class="col-12 grid-margin stretch-card">

			<div class="card">

			    	<div class="card-body">

				    	<h4 class="card-title">My Address</h4>

			  		<?php $count = 0;?>

			  		<input type="hidden" name="location_count" value="{{$locations->count() + 1}}">

					<div class="row">
						
					@for($i = 0; $i < $locations->count(); $i++)

					@include('post-login.partials.modal.address-modal')
						
						<div class="col-md-4">
							
							<?php $count = $count + 1;?>

							<a data-toggle="modal" href="#" data-target="#edit-address{{$locations[$i]->id}}" style="color: #fff; text-decoration: none;">

								<button class="btn btn-warning mb-2" onclick="show_map(<?= $locations[$i]->latitude; ?>, <?= $locations[$i]->longitude;?>, <?= $locations[$i]->id; ?>)"><i class="mdi mdi-pen"></i></button>

							</a>

							<div class="show-map2" id="mapz{{$count}}"></div>

							<input type="hidden" id="lat{{$count}}" value="{{$locations[$i]->latitude}}">

							<input type="hidden" id="lng{{$count}}" value="{{$locations[$i]->longitude}}">


						</div>

					@endfor

					</div>
				        
		  		</div>

			</div>

		</div>
		
	</div>

	

</div>


@include('_session_.error2')

@include('_session_.success2')

@endsection