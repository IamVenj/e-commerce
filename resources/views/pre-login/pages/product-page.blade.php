@extends('pre-login.index.index')

@section('content')

<!-- BREADCRUMB -->

<div id="breadcrumb">

	<div class="container">

		<ul class="breadcrumb">

			<li><a href="/">{{__('app.home')}}</a></li>

			<li>{{__('app.products')}}</li>

			<li><a href="/{{$product->category()->first()->category_name}}/products/{{$product->category()->first()->id}}">{{$product->category()->first()->category_name}}</a></li>

			<li class="active">{{$product->product_name}}</li>

		</ul>

	</div>

</div>

<!-- /BREADCRUMB -->

<!-- section -->
	
<div class="demo" id="section-load"></div>

<div class="section" id="section-1" style="background: #fff; display: none;">

	<div class="container">

		<div class="row">

			<div class="product product-details clearfix">

				<div class="col-md-5">

					<div id="product-main-view" style="box-shadow: 10px 5px 15px #00000004; border-top-left-radius: 100px; border-bottom-right-radius: 100px; max-height: auto !important">

						@foreach($product->productImage()->get() as $proImages)

						<div class="product-view" style="border-top-left-radius: 100px; border-bottom-right-radius: 100px;">

							<img style="height: auto !important;" src='{{ $proImages->image_public_id }}' alt="{{$proImages->image_public_id}}">

						</div>

						@endforeach

					</div>

					<div id="product-view">

						@foreach($product->productImage()->get() as $proImages)

						<div class="product-view">

							<img src='{{ $proImages->image_public_id }}' alt="{{$proImages->image_public_id}}">

						</div>

						@endforeach

					</div>

				</div>

				<div class="col-md-7">

					<form action="/cart" method="post" class="cart-form">

						<div class="product-body">

							@if($product->current_price < $product->old_price)

							<div class="product-label">

								<span class="sale"><?= round((($product->current_price/$product->old_price)*100)-100) ;?>%</span>

							</div>

							@endif
							
							<h2 class="product-name" style="font-size: 25px;">{{$product->product_name}}</h2>

							<h3 class="product-price">{{$product->current_price}} {{__('app.birr')}}

								@if(!is_null($product->old_price))

								<del class="product-old-price" style="margin-left: 10px;">{{$product->old_price}} {{__('app.birr')}}</del>

								@endif

							</h3>

							<p><strong>{{__('app.Availability')}}:</strong> {{$product->availability}} {{__('app.left')}}</p>

							<p><strong>{{__('app.Brand')}}:</strong> {{$product->brand}}</p>

							<p>{{$product->slug}}</p>

							<div class="product-options">

								@if($product->productSize()->count() > 0)

								<ul class="size-option">

									<li><span class="text-uppercase">{{__('app.Size')}}:</span></li>

									<div class="row">

										@foreach($product->productSize()->get() as $productSIze)

										<div class="col-md-1">

											<div class="form-check">

					                            <label class="form-check-label">

					                              <input type="radio" class="form-check-input" name="size" id="size" value="{{$productSIze->id}}">

					                              {{$productSIze->size}}

					                            <i class="input-helper"></i></label>

					                        </div>

				                        </div>

			                         	@endforeach

		                         	</div>

								</ul>

								@endif

								@if($product->productColors()->count() > 0)

								<ul class="color-option">

									<li><span class="text-uppercase">{{__('app.Color')}}:</span></li>

									<div class="row">

										@foreach($product->productColors()->get() as $productColor)

										<div class="col-md-2">

											<div class="form-check">

					                            <label class="form-check-label">

					                            	@foreach($productColor->Colors()->get() as $colors)

		                            				<li style="width: 50px; border-bottom-right-radius: 20px; border-top-left-radius: 20px; height: 50px; background: {{$colors->hex}};"></li>

					                              	<input type="radio" class="form-check-input" name="color" id="color" value="{{$colors->id}}">

					                              	@endforeach

					                            <i class="input-helper"></i></label>

					                        </div>

				                        </div>

			                         	 @endforeach

		                         	</div>

								</ul>

								@endif

							</div>


							<div class="product-btns">

								<div class="qty-input">

									<span class="text-uppercase">{{__('app.quantity')}}: </span>

									<input class="input" id="qty" type="number" min="1" value="1">

								</div>

									<input type="hidden" name="product_name" value="{{$product->product_name}}">

									<input type="hidden" name="product_price" value="{{$product->current_price}}">

									<input type="hidden" name="product_availability" value="{{$product->availability}}">

									<input type="hidden" name="cart-csrf-token" value="{{csrf_token()}}">

									<input type="hidden" name="cart-product-id" value="{{$product_id}}">
									
									@guest

									<a href="/login"><button type="button" class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> {{__('app.add_cart')}}</button></a>

									@else

									<button type="submit" class="primary-btn add-to-cart" id="add_cart"><i class="fa fa-shopping-cart"></i> {{__('app.add_cart')}}</button>		

									<div class="lds-roller" id="loader-roller{{$product->id}}" style="display: none;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>					
									
									@endguest

								<div class="pull-right">

									@auth

									<button type="button" class="main-btn icon-btn" onclick="add_wish(<?= $product_id?>)"><i class="fa fa-heart heart-comp<?= $product_id;?>"></i></button>

									@else

									<a href="/login"><button type="button" class="main-btn icon-btn" ><i class="fa fa-heart"></i></button></a>

									@endauth

								</div>
				
							</div>

						</div>

					</div>

				</form>

				<div class="col-md-12">

					<div class="product-tab">

						<ul class="tab-nav">

							<li class="active"><a data-toggle="tab" href="#tab1">{{__('app.Description')}}</a></li>

							<li><a data-toggle="tab" href="#tab2">{{__('app.Reviews')}} ({{$total_review_count}})</a></li>

							<li><a data-toggle="tab" href="#tab3">{{__('app.Location')}}</a></li>

						</ul>

						<div class="tab-content">

							<div id="tab1" class="tab-pane fade in active">

								<p style="font-size: 15px;">{{$product->slug}}</p>

							</div>

							<div id="tab2" class="tab-pane fade in">

								<div class="row">

									<div class="col-md-6">

										<div class="product-reviews">

											@auth

											@if(!is_null($current_user_review))

											<div class="single-review">

												<div class="review-heading">

													<div><a href="#"><i class="fa fa-user-o"></i>@if(!is_null($current_user_review->user()->first()->name)) {{$current_user_review->user()->first()->name}} @else {{$current_user_review->user()->first()->shop_name}} @endif</a></div>

													<div><a href="#"><i class="fa fa-clock-o"></i> <?= date('d M Y', strtotime($current_user_review->created_at));?></a></div>

													<div class="review-rating pull-right">

														@if($current_user_review->rating == 1)

														<i class="fa fa-star"></i>

														<i class="fa fa-star-o empty"></i>

														<i class="fa fa-star-o empty"></i>

														<i class="fa fa-star-o empty"></i>

														<i class="fa fa-star-o empty"></i>

														@elseif($current_user_review->rating == 2)

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star-o empty"></i>

														<i class="fa fa-star-o empty"></i>

														<i class="fa fa-star-o empty"></i>

														@elseif($current_user_review->rating == 3)

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star-o empty"></i>

														<i class="fa fa-star-o empty"></i>
														
														@elseif($current_user_review->rating == 4)

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star-o empty"></i>
														
														@elseif($current_user_review->rating == 5)

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>
														
														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														@endif

													</div>

												</div>

												<div class="review-body">

													<p>{{$current_user_review->review}}</p>

													@include('pre-login.partials.modal.product-detail-modal')

													 <a data-toggle="modal" href="#" data-target="#edit-review<?= $current_user_review->id;?>"><button class="btn btn-warning"><span class="fa fa-edit"></span></button></a>

													 <a data-toggle="modal" href="#" data-target="#delete-review<?= $current_user_review->id;?>"><button class="btn btn-danger"><span class="fa fa-trash"></span></button></a>

												</div>

											</div>

											@endif

											@endauth

											@foreach($reviews as $review)
											
											<div class="single-review">

												<div class="review-heading">

													<div><a href="#"><i class="fa fa-user-o"></i>@if(!is_null($review->user()->first()->name)) {{$review->user()->first()->name}} @else {{$review->user()->first()->shop_name}} @endif</a></div>

													<div><a href="#"><i class="fa fa-clock-o"></i> <?= date('d M Y', strtotime($review->created_at));?></a></div>

													<div class="review-rating pull-right">

														@if($review->rating == 1)

														<i class="fa fa-star"></i>

														<i class="fa fa-star-o empty"></i>

														<i class="fa fa-star-o empty"></i>

														<i class="fa fa-star-o empty"></i>

														<i class="fa fa-star-o empty"></i>

														@elseif($review->rating == 2)

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star-o empty"></i>

														<i class="fa fa-star-o empty"></i>

														<i class="fa fa-star-o empty"></i>

														@elseif($review->rating == 3)

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star-o empty"></i>

														<i class="fa fa-star-o empty"></i>
														
														@elseif($review->rating == 4)

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star-o empty"></i>
														
														@elseif($review->rating == 5)

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>
														
														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														@endif

													</div>

												</div>

												<div class="review-body">

													<p>{{$review->review}}</p>

												</div>


											</div>

											@endforeach

											<ul class="reviews-pages">

												<li>{{$reviews->links()}}</li>

											</ul>

										</div>

									</div>

									<div class="col-md-6">

										<h4 class="text-uppercase">{{__('app.write_review')}} @guest <small style="color: red;"> {{__('app.logging_required')}} *</small> @endguest </h4>

										<p>{{__('app.emailWillNotBePublished')}}.</p>

										<form class="review-form" action="/review" method="post">

											@csrf

											<input type="hidden" name="product_id" value="{{$product_id}}">

											<div class="form-group">

												<textarea class="input" placeholder="{{__('app.your_review')}}" name="review"></textarea>

											</div>

											<div class="form-group">

												<div class="input-rating">

													<strong class="text-uppercase">{{__('app.your_rating')}}: </strong>

													<div class="stars">

														<input type="radio" id="star10" name="rating" value="5" name="rating" /><label for="star10"></label>

														<input type="radio" id="star9" name="rating" value="4" name="rating" /><label for="star9"></label>

														<input type="radio" id="star8" name="rating" value="3" name="rating" /><label for="star8"></label>

														<input type="radio" id="star7" name="rating" value="2" name="rating" /><label for="star7"></label>

														<input type="radio" id="star6" name="rating" value="1" name="rating" /><label for="star6"></label>

													</div>

												</div>

											</div>

											@guest

											<a href="/login"><button type="button" class="primary-btn">{{__('app.Submit')}}</button></a>

											@else

											<button class="primary-btn">{{__('app.Submit')}}</button>

											@endguest

										</form>

									</div>

								</div>



							</div>

							<div id="tab3" class="tab-pane fade in">

								<?php $count = 0;?>


								<div class="row">
									
								@for($i = 0; $i < $locations->count(); $i++)
									
									<div class="col-md-4">
										
										<?php $count = $count + 1;?>

										<div class="show-map" id="map{{$count}}"></div>

										<input type="hidden" id="lat{{$count}}" value="{{$locations[$i]->latitude}}">

										<input type="hidden" id="lng{{$count}}" value="{{$locations[$i]->longitude}}">
									
									</div>

								@endfor

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>
			<!-- /Product Details -->
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /section -->

<!-- section -->
<div class="section" id="section-2" style="display: none;">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<!-- section title -->

			<div class="col-md-12">

				<div class="section-title">

					<h2 class="title">{{__('app.Related')}}</h2>

				</div>

			</div>
			<!-- section title -->

			@foreach($related_products as $product)

			<!-- Product Single -->
			<div class="col-md-2">

				<div class="product product-single">

					<div class="product-thumb">

						<div class="product-label">

							@if($product->current_price < $product->old_price)

							<div class="product-label">

								<span class="sale"><?= round((($product->current_price/$product->old_price)*100)-100) ;?>%</span>

							</div>

							@endif

						</div>

						<a data-toggle="modal" href="#" data-target="#quick-preview<?= $product->id; ?>"><button class="main-btn quick-view" style="font-size: 10px; letter-spacing: 0.5px;"><i class="fa fa-search-plus"></i> {{__('app.quick_view')}}</button></a>

						<img style="object-fit: cover; height: 200px; overflow: hidden;" src="{{ $product->productImage()->first()->image_public_id }}" alt="{{$product->productImage()->first()->image_public_id}}">

					</div>

					<div class="product-body">

						<h3 class="product-price" style="font-size: 14px;">{{$product->current_price}} {{__('app.birr')}} <del class="product-old-price">{{$product->old_price}} {{__('app.birr')}}</del></h3>
						
						<h2 class="product-name"><a href="/{{$product->product_name}}/detail/{{$product->id}}" class="js-name-detail">{{$product->product_name}}</a></h2>

						<p ><a href="#">{{__('app.sold_by')}}: {{$product->user()->first()->shop_name}}</a></p>

						<div class="product-btns">

							<button class="main-btn icon-btn js-addwish-detail" onclick="add_wish(<?= $product->id?>)"><i class="fa fa-heart heart-comp<?= $product->id?>"></i></button>

						</div>

					</div>

				</div>

			</div>

			@endforeach
			
		</div>

	</div>

</div>

<div class="message"></div>

<!-- /section -->

@include('pre-login.partials.modal.quickViewModal')

<script type="text/javascript" src="{{URL::asset('js/jquery.min.js')}}"></script>

<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyAu_y6IajOPnK1Fsrb6dxCL3n9bRUIPC7s"></script>

<script type="text/javascript">


	$(window).on('load', function() {

		document.getElementById('section-1').style.display = 'block';

		document.getElementById('section-2').style.display = 'block';

		document.getElementById('section-load').style.display = 'none';

	});

	for (var i = 1; i < <?= $locations->count() + 1; ?>; i++) {
		
		var lat = $("#lat"+i).val();
		var lng = $("#lng"+i).val();

		var map = new google.maps.Map(document.getElementById("map"+i), {

			center: {
				lat: parseFloat(lat), 
				lng: parseFloat(lng)
			},
			zoom: 18

		});

		var marker = new google.maps.Marker({

			position: {
				lat: parseFloat(lat),
				lng: parseFloat(lng)
			},
			map: map

		});

	}


		
	
</script>


@endsection