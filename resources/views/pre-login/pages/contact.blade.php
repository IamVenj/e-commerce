@extends('pre-login.index.index')

@section('content')

<div id="breadcrumb">
	<div class="container">
		<ul class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">Contact</li>
		</ul>
	</div>
</div>

<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="shiping-methods">
					<div class="section-title">
						<h4 class="title">Contact</h4>
					</div>
					<div class="caption" style="font-size: 20px;">
						<p><span><i class="fa fa-map-marker" style="font-size: 18px; font-weight: bold; margin-right: 10px;"></i></span> {{$setting->location}}</p>
					</div>
					<div class="caption" style="font-size: 20px;">
						<p><span><i class="fa fa-phone" style="font-size: 18px; font-weight: bold; margin-right: 10px;"></i></span> <a href="tel://1234567920">{{$setting->phone_number}}</a></p>
					</div>
					<div class="caption" style="font-size: 20px;">
						<p><span><i class="fa fa-envelope" style="font-size: 18px; font-weight: bold; margin-right: 10px;"></i></span> <a href="mailto:info@yoursite.com">{{$setting->email}}</a></p>
					</div>
					
				</div>					
			</div>
			<div class="col-md-12">
				<div class="billing-details">
					<div class="section-title">
						<h3 class="title">Leave Your message</h3>
					</div>
					<form id="contact-form" action="/contact" class="clearfix">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input class="input" type="text" name="fullname" placeholder="Full Name" required="" area-required="true">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input class="input" type="email" name="contact_email" placeholder="Email" required="" area-required="true">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<input class="input" type="text" name="subject" placeholder="Subject" required="" area-required="true">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<textarea name="message" id="message-contact" class="form-control" rows="5" placeholder="Write your message..." required="" area-required="true"></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<button class="btn btn-success" type="submit" id="send_contact">Send <i class="fa fa-send" style="margin-left: 10px;"></i></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="message"></div>

<script type="text/javascript" src="{{URL::asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>

<script type="text/javascript">
	//Contact Form Validation
    if($('#contact-form').length){
        $('#contact-form').validate({
            rules: {
                fullname: {
                    required: true
                },
                subject: {
                    required: true
                },
                contact_email: {
                    required: true,
                    email: true
                },
                message: {
                    required: true
                }
            },
            submitHandler: function(form, e){
                e.preventDefault();
                var token = $("meta[name=csrf-token]").attr("content");
                var form = $("#contact-form");
                var message = $("#message-contact").val();
                var email = $("input[name=contact_email]").val();
                var subject = $("input[name=subject]").val();
                var fullname = $("input[name=fullname]").val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': token
                    }
                });

                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: {fullname: fullname, email: email, subject:subject, message: message},
                    beforeSend: function(){
                        $("#send_contact").attr('disabled', "disabled");
                    },
                    success: function(response){
                        $("#send_contact").removeAttr('disabled', "disabled");

                        if(response.status == 'success')
                        {
                            var message = $('.message').fadeIn('fast');

                           var successMessage = '<div class="form-group mt-4">';
                            successMessage += '<div class="alert golden2">';
                            successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
                            successMessage += '<p class="mr-5 ml-5" style="color: #fff;">'+ response.message + '</p>';
                            successMessage +=  '</div></div>';

                            $(message).html(successMessage).delay(4500).fadeOut('slow');

                            $("#message-contact").val("");
                            $("input[name=subject]").val("");
                            $("input[name=contact_email]").val("");
                            $("input[name=fullname]").val("");
                        }
                        else if(response.status == "error")
                        {
                            var message = $('.message').fadeIn('fast');

                            var errorMessage = '<div class="form-group mt-4">';
                            errorMessage += '<div class="alert golden">';
                            errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';
                            errorMessage += '<p class="mr-5 ml-5" style="color: #fff;">'+ response.message + '</p>';
                            errorMessage +=  '</div></div>';
                            
                            $(message).html(errorMessage).delay(4500).fadeOut('slow');
                        }
                    },
                    
                    error: function(error){
                        $("#send_contact").removeAttr('disabled', "disabled");

                        var message = $('.message').fadeIn('fast');
                        if(error.responseJSON['errors'] != null)
                        {
                            
                            var errorMessage = '<div class="form-group mt-4">';
                            errorMessage += '<div class="alert golden">';
                            errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

                            $.each(error.responseJSON['errors'], function(key, errors){
                                errorMessage += '<p class="mr-5 ml-5" style="color: #fff;">'+ errors + '</p>';
                            });
                            
                            errorMessage +=  '</div></div>';
                            $(message).html(errorMessage).delay(4500).fadeOut('slow');                   
                        }
                        else
                        {
                            $(message).html('').fadeIn('slow');
                        }

                    }
                });
            }
        });
    }
</script>

@endsection