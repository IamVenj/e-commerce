@extends('pre-login.index.index')

@section('content')

<!-- BREADCRUMB -->

<div id="breadcrumb">

	<div class="container">

		<ul class="breadcrumb">

			<li><a href="/">{{__('app.home')}}</a></li>
			<li>{{__('app.Package')}}</li>
			<li class="active">{{$package_name}}</li>

		</ul>

	</div>

</div>

<!-- /BREADCRUMB -->

<!-- section -->
	
<div class="section">

	<div class="container">

		<div class="row">

			<div class="product product-details clearfix">

				<div class="col-md-5">

					<div id="product-main-view" style="box-shadow: 10px 5px 15px #00000004; border-top-left-radius: 100px; border-bottom-right-radius: 100px; max-height: auto !important">

						@foreach($package->images as $proImages)

						<div class="product-view" style="border-top-left-radius: 100px; border-bottom-right-radius: 100px;">

							<img style="height: 350px !important; object-fit: contain;" src='{{ $proImages->image_public_id }}' alt="{{$proImages->image_public_id}}">

						</div>

						@endforeach

					</div>

					<div id="product-view">

						@foreach($package->images as $proImages)

						<div class="product-view">

							<img src='{{ $proImages->image_public_id }}' alt="{{$proImages->image_public_id}}">

						</div>

						@endforeach

					</div>

				</div>

				<div class="col-md-7">

					<form action="/cart/package" method="post" class="cart_form_package">

						<div class="product-body">

							<div class="product-label">

								<span class="sale">{{ $package->discount_percent }}%</span>

							</div>
							
							<h2 class="product-name" style="font-size: 25px;">{{$package->package_name}}</h2>

							<p class="product-price"> {{ $package->discount_price }} {{__('app.birr')}}<p>

							<p><strong>{{__('app.Availability')}}:</strong> {{$package->availability}} {{__('app.left')}}</p>

							<p>{{$package->slug}}</p>

							<h3>Items</h3>

							<hr>
							<ul>
								@for($i = 0; $i < $package->packageProduct->count(); $i++)
								<li style="font-size: 16px;"> -> <a href="/{{ $package->packageProduct[$i]->product->product_name }}/detail/{{ $package->packageProduct[$i]->product->id }}">{{ $package->packageProduct[$i]->product->product_name }}</a></li>
								@endfor
							</ul>
							<hr>

							<div class="product-btns">

								<div class="qty-input">

									<span class="text-uppercase">{{__('app.quantity')}}: </span>

									<input class="input" id="qty" type="number" min="1" value="1">

								</div>

									<input type="hidden" name="package_name" value="{{$package->package_name}}">

									<input type="hidden" name="package_price" value="{{$package->discount_price}}">

									<input type="hidden" name="package_availability" value="{{$package->availability}}">

									<input type="hidden" name="cart-csrf-token" value="{{csrf_token()}}">

									<input type="hidden" name="cart-package-id" value="{{$package->id}}">
									
									@guest

									<a href="/login"><button type="button" class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> {{__('app.add_cart')}}</button></a>

									@else

									<button type="submit" class="primary-btn add-to-cart" id="add_cart"><i class="fa fa-shopping-cart"></i> {{__('app.add_cart')}}</button>		

									<div class="lds-roller" id="loader-roller{{$package->id}}" style="display: none;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>					
									
									@endguest
				
							</div>

						</div>

					</div>

				</form>

				<div class="col-md-12">

					<div class="product-tab">

						<ul class="tab-nav">

							<li class="active"><a data-toggle="tab" href="#tab1">{{__('app.Description')}}</a></li>

						</ul>

						<div class="tab-content">

							<div id="tab1" class="tab-pane fade in active">

								<p style="font-size: 15px;">{{$package->slug}}</p>

							</div>

						</div>

					</div>

				</div>

			</div>
			<!-- /Product Details -->
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /section -->

<!-- section -->
<div class="section" id="section-2" style="display: none;">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<!-- section title -->

			<div class="col-md-12">

				<div class="section-title">

					<h2 class="title">{{__('app.Related')}}</h2>

				</div>

			</div>
			<!-- section title -->

			@foreach($related_packages as $package)

			<!-- Product Single -->
			<div class="col-md-2">

				<div class="product product-single">

					<div class="product-thumb">

						<div class="product-label">

							<div class="product-label">

								<span class="sale">{{ $package->discount_price }}%</span>

							</div>

						</div>

						<img style="object-fit: cover; height: 200px; overflow: hidden;" src="{{ $package->productImage->first()->image_public_id }}" alt="{{$package->productImage->first()->image_public_id}}">

					</div>

					<div class="product-body">

						<h3 class="product-price" style="font-size: 14px;">{{$product->current_price}} {{__('app.birr')}}</h3>
						
						<h2 class="product-name"><a href="package/{{$package->package_name}}/detail/{{$package->id}}" class="js-name-detail">{{$package->package_name}}</a></h2>

						<p ><a href="#">{{__('app.sold_by')}}: {{$package->user->shop_name}}</a></p>

					</div>

				</div>

			</div>

			@endforeach
			
		</div>

	</div>

</div>

<div class="message"></div>


@endsection