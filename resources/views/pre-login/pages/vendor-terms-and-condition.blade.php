@extends('pre-login.index.index')

@section('content')

<div id="breadcrumb">
	<div class="container">
		<ul class="breadcrumb">
			<li style="margin-bottom: 10px;"><a href="#">Home</a></li>
			<li class="active">{{__('app.terms_and_conditions')}}</li>
		</ul>
	</div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="shiping-methods" style="margin: 20px;">
                    <div class="section-title">
                        <h4 class="title">{{__('app.terms_and_conditions')}}</h4>
                    </div>
                    <div class="caption" style="font-size: 20px;">
                            <h1>1. Definitions</h1>
                            <p>For the purpose of this Agreement, the following words and phrases shall have the meaning assigned to them under this Article. </p>
                    </div>
                    <ol style="font-size: 16px;">
                        <li style="margin-bottom: 10px;">1.1.“Company” shall mean Symmart.com. </li>
                        <li style="margin-bottom: 10px;">1.2.“Customer” shall mean any individual, group of individuals, firm, company or any other entity placing an order for the Products of the Vendor through the Online Store.  </li>
                        <li style="margin-bottom: 10px;">1.3.“Price” shall mean the cost at which the Products are to be delivered to the Customer inclusive of Shipping charges, if any. </li>
                        <li style="margin-bottom: 10px;">1.4.“Effective Date” shall mean the date on which this Agreement is executed. </li>
                        <li style="margin-bottom: 10px;">1.5.“Form” shall mean Form for Ecommerce Service Agreement to be filled in and executed by the Vendor at the time of execution of this Agreement annexed hereto as Annexure “A”.</li>
                        <li style="margin-bottom: 10px;">1.6.“Vendor” shall mean the entity incorporated or otherwise more specifically described hereinabove, which sells its products through the Online Store and more particularly described in the attached “Form”.</li>
                        <li style="margin-bottom: 10px;">1.7."Online Store” shall mean a virtual electronic store created on Symmart.com website for sale of the Vendor’s Products either through website of the Company or any other gadget or instrument displaying the particulars of the Vendor’s Products available for sale , or any other means by which the Customer places an order for the Product of the Vendor.
                        <li style="margin-bottom: 10px;">
                        1.8.“Order” shall mean an order for purchase of products wherein customer has agreed to purchase the product upon the terms and conditions and at the Price indicated on the online store of the Vendor.
                        </li>
                        <li style="margin-bottom: 10px;">
                        1.9.     “Products” shall mean merchandise items of the Vendor put up for sale on the Online Store by the Vendor. 
                        </li>
                        <li style="margin-bottom: 10px;">
                        1.10.   “Price” means the sale price of a product inclusive of delivery charges and applicable taxes.
                        </li>
                        <li style="margin-bottom: 10px;">
                        1.11.   “Symmart.com” means an online platform owned and operated by the Company that facilitates the shopping transaction between the Vendor and the Customer, in this case Cynex Technology Investment
                        </li>
                        <li style="margin-bottom: 10px;">
                        1.12.   “Shipping Charges” shall mean the logistics/courier/postal charges including all taxes incurred for delivering the product(s) to the Customer. 
                        </li>
                        <li style="margin-bottom: 10px;">
                        1.13.    “Shipment Cost” shall mean the cost and taxes recovered by the Company from the Vendor per order for handling the logistics.
                        </li>
                        <li style="margin-bottom: 10px;">
                        1.14.    “Sign-up Fees” shall mean the non-refundable fees payable by the Vendor at the time of execution of this Agreement towards the initial creation of online store. 
                        </li>
                        <li style="margin-bottom: 10px;">
                        1.15.   “Service charge” shall mean the margin per transaction charged by the Company to the Vendor at the rates agreed to between the parties, upon the sale of product on online store. 
                        </li>

                    </ol>
                    <div class="caption" style="font-size: 20px;">
                            <h1>2.  Arrangement</h1>
                    </div>
                    <ol style="font-size: 16px;">
                        <li style="margin-bottom: 10px;">
                            2.1.    The Company shall offer to the Vendor its services for facilitating online sale of the Vendor’s product which shall include hosting and technology, customer support, logistics services (if availed by the Vendor), payment services and all the other related services to ensure customer satisfaction on behalf of the Vendor. For this arrangement, the Vendor shall pay service charges as specified under these presents, to the Company for the sale being effected through the online store created on the Symmart.com.
                        </li>


                        <li style="margin-bottom: 10px;">
                        2.2.    Based on mutual discussions, it is agreed by and between the parties hereto that the Vendor shall put up for sale its Products on the said symmart.com, subject to the terms and conditions hereinafter contained. The vendor further agrees and acknowledges that the shopping transaction shall be governed by the “Terms of Use” of Symmart.com (incorporated in this agreement by way of reference and forms part of this Agreement) along with this Agreement. 
                        </li>
                    </ol>
                    <div class="caption" style="font-size: 20px;">
                            <h1>3.  Consideration and Payment Terms</h1>
                    </div>
                    <ol style="font-size: 16px;">
                        <li style="margin-bottom: 10px;"> 
                        3.1.    The vendor shall make the payment for Sign-up fees as specified in Form ETB 500 (Five hundred Birr) for the creation of online Store at the time of execution of this Agreement. Payment of sign up fees shall be made 100% in advance unless specified. The service tax if any shall be charged by the Company, at the applicable rates.
                        </li>
                        <li style="margin-bottom: 10px;"> 
                        3.2.     The said sign-up fee is a non- refundable fee for the creation of online store.   The said Online store shall be displayed on the Symmart.com during the subsistence of this Agreement
                        </li>
                        <li style="margin-bottom: 10px;"> 
                        3.3.     The Company shall collect the Payment on behalf of the Vendor in respect of the Orders received through Online Store. The Company shall pay the Vendor an amount recovered as Price minus the sum of shipping charges, service charges and any other cost in respect of approved order(s) through the Online Store. The shipment cost will be levied at in accordance with the customers’ preferred shipping package as specified in the shipping policy of symmart.com. The said shipment cost will be independent of the Quantity shipped for a transaction by a particular customer. However, in the event, the Vendor handles the Shipment of the Products; the Company shall pay to the Vendor an amount recovered as Price minus the service charges.  Any amount to be paid to the Vendor by the Company shall be paid net of reversals. 
                        </li>
                        <li style="margin-bottom: 10px;"> 
                        3.4.    In the event any order is reversed due to “Damaged product”, “Quality Issue”, “Not delivered” or “Wrong Item delivered”, Vendor agrees that the Company shall levy the Service charges, plus a penalty of the service charge of the product (upto a maximum limit of ETB 500) and the said charges will be deducted from the amount due and payable to the vendor.
                        </li>
                        <li style="margin-bottom: 10px;"> 
                        3.5.    Payment reimbursement of the Sale Proceeds to Vendor shall be done by Company in the following manner: 
                            <ol style="margin-left: 25px;">
                                <li style="margin-bottom: 10px;">
                                3.6.1. Vendor shall prepare a consolidated advice list of all orders delivered to customer, 3 times in a month for every 10 days. 
                                </li>
                                <li style="margin-bottom: 10px;">
                                3.6.2.      The Company shall within 7 working days of receipt of advice process the amount due to 
                                Vendor and dispatch the Cheques or bank deposits ________________________” / on line transfers. 
                                </li>
                                <li style="margin-bottom: 10px;">
                                3.6.3.      The company shall deduct charges as specified in 3.4 above and agreed with the Vendor from the total amount collected as Price for the orders received by the Vendor through online store. 
                                </li>
                            </ol>
                        </li>
                        <li style="margin-bottom: 10px;">3.6.    Vendor agrees to bear all the applicable taxes duties, or other similar payments (including VAT) arising out of the sales transaction of the product through the online store.</li>
                    </ol>
                    <div class="caption" style="font-size: 20px;">
                            <h1>4.   Obligations of the Vendor </h1>
                            <p>The Vendor shall: </p>
                    </div>
                    <ol style="font-size: 16px;">
                        <li style="margin-bottom: 10px;">
                            4.1 The vendor only sell products that are included within the clearly specified categories in symmart.com, any legal consequences arising from the vendor failing to do so shall be the vendor’s full responsibility.
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.2 Through the interface provided by the company, on the creation of online Store, the vendor shall upload the product description, images, disclaimer, delivery time lines, price and such other details for the products to be displayed and offered for sale through the said online store.
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.3  Vendor shall ensure not to upload any description/image/text/graphic that is unlawful, illegal, objectionable, obscene, vulgar, opposed to public policy, prohibited or is in violation of intellectual property rights including but not limited to Trademark and Copyright of any third party.
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.4  Vendor shall ensure to upload the product description and image only for the product which is offered for sale through the Online Store and for which the said Online Store is created.
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.5 Vendor shall provide full, correct, accurate and true description of the product so as to enable the customers to make an informed decision. Any form of discontent that occurs due to the vendor’s failure to provide correct, accurate and full information about the product shall be the responsibility of the vendor.  
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.6  Vendor shall be solely responsible for the quality, quantity, merchantability, guarantee, warranties in respect of the products offered for sale through their online store. 
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.7  Vendors are expected to at all times have access to the Internet and their email account to check the status of approved orders, 
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.8  On receipt of the approved order, Vendor shall be able to dispatch / deliver the products within a period not exceeding 24 hours to the company who in turn will dispatch the product to the buyer.
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.9  In the event the products are not accepted by the Customer due to any wrong / damaged products dispatched, then the same shall be replaced by the Vendor at no extra cost to the aggrieved customer. Since the Company is a facilitator, the Vendor hereby authorizes the Company to entertain all claims of return of the Product in the mutual interest of the Vendor as well as the Customer.
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.10    The Vendor shall not send any of its promotional or any other information with the products ordered by the customer and also shall ensure that no material or literature is sent which may be detrimental to the business/commercial interests of the company, 
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.11     The Vendor shall dispatch the products of same description, quality and quantity and price as are described and displayed on the online store and for which the Customer has placed the order.
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.12     The vendor shall produce invoice for all the products ordered on its online store in the name of the company. The company will in turn produce a receipt for the customer/customers who bought the specific products.
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.13     The Vendor shall not offer any products for sale on the online Store, which are prohibited for sale, dangerous, against the public policy, banned, unlawful, illegal or prohibited under the Ethiopian Laws. 
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.14     The Vendor shall ensure that they own all the legal rights in the products that are offered for sale on the Online Store.
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.15     The Vendor shall pass on the legal title, rights and ownership in the products sold to the Customer. 
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.16     Vendor shall be solely responsible for any dispute that may be raised by the customer relating to the goods, merchandise and services provided by the Vendor.
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.17      The Vendor shall at all time during the pendency of this agreement endeavor to protect and promote the interests of the Company and ensure that third parties rights including intellectual property rights are not infringed. 
                        </li>
                        <li style="margin-bottom: 10px;">
                            4.18     The Vendor shall at all times be responsible for compliance of all applicable laws and regulations including but not limited to Intellectual Property Rights, sales tax, Value added tax, Excise and Import duties, Code of Advertising Ethics, etc. 
                        </li>
                    </ol>
                    <div class="caption" style="font-size: 20px; font-weight: 700;">
                            <p>By checking ‘I agree’ while signing up as a vendor on symmart.com, the vendor confirms that it has read, fully understood and agreed with the terms and conditions contained within this vendor agreement.</p>
                    </div>
                </div>                  
            </div>
        </div>
    </div>
</div>

@endsection