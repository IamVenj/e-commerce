@extends('pre-login.index.index')

@section('content')

<!-- BREADCRUMB -->

<div id="breadcrumb">

	<div class="container">

		<ul class="breadcrumb">

			<li><a href="#">{{__('app.home')}}</a></li>

			<li class="active">{{__('app.search_results')}}</li>

		</ul>

	</div>

</div>


<!-- /BREADCRUMB -->

<div class="demo-1" id="section-load"></div>

<!-- section -->
<div class="section" id="section-1" style="display: none;">

	<div class="container">

		<div class="row">

			@include('pre-login.partials.filter')
			
			<input type="hidden" name="csrf-token-" value="{{csrf_token()}}">

			<div id="main" class="col-md-9">
				
				<div class="store-filter clearfix">

					<div class="pull-left">

						<div class="row-filter">

							<input type="hidden" id="view-status" value="1">

							<button id="grid" class="btn btn-warning"><i class="fa fa-th-large"></i></button>

							<button id="linear" class="btn btn-warning"><i class="fa fa-bars"></i></button>

						</div>

						<div class="sort-filter">

							<span class="text-uppercase">{{__('app.sort_by')}}:</span>

							<select class="input" name="sort_by" id="sort_by">

								<option value="0">{{__('app.Latest')}}</option>

								<option value="1">{{__('app.Price_High_Low')}}</option>

								<option value="2">{{__('app.Price_Low_High')}}</option>

							</select>

						</div>
					</div>
					
					<div class="pull-right">
					
						<div class="page-filter">
					
							<span class="text-uppercase">{{__('app.Show')}}:</span>
					
							<select class="input" name="records_per_page" id="records_per_page">
					
								<option value="12">12</option>
				
								<option value="24">24</option>
				
								<option value="36">36</option>
				
							</select>
				
						</div>
				
						<span id="pagination"></span>
				
					</div>
				
				</div>


				<!-- STORE -->
				<div id="store">

					<div class="row">

						@if($category != 0)

						<input type="hidden" name="category_id" value="{{$category}}">

						@endif

						<input type="hidden" id="query" value="{{$query}}">

						@foreach($products as $product)

						<input type="hidden" name="product_id[]" value="{{$product->id}}">

						@include('pre-login.partials.modal.quickViewModal')

						@endforeach

						<div id="get-product"></div>
						
					</div>

				</div>
				<!-- /STORE -->

				
			</div>
			<!-- /MAIN -->
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /section -->
<div class="message"></div>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">
	
	$(window).on('load', function() {

		document.getElementById('section-1').style.display = 'block';

		document.getElementById('section-load').style.display = 'none';

	});

</script>

<script type="text/javascript">

	$(document).ready(function() {

		var view_status = $("#view-status").val();

		$("#grid").click(function() {

			view_status = 1;

			get_products();

		});

		$("#linear").click(function() {

			view_status = 2;

			get_products();

		});

		
		$("#sort_by").change(function() {

			get_products();

		}).change();



		$("input[name='colors[]']").bind('change', function(){
       
	        get_products();
	   
	    });	

	    $("input[name='size[]']").bind('change', function(){
       
	        get_products();
	   
	    });	

	    $("input[name='vendor_filter[]']").bind('change', function(){
       
	        get_products();
	   
	    });	

	    $('#records_per_page').change(function() {

			get_products();

		}).change();


	    var price_range = [document.getElementById("min-price"), document.getElementById("max-price")];

	      document.getElementById('price-slider').noUiSlider.on('change', function(values) {

	        get_products();

	      });


		function get_products(page)
		{
			var product_id = [];

			$("input[name='product_id[]']").each(function(){
       
		        product_id.push($(this).val());
		   
		    });

			var token = $("input[name=csrf-token-]");

			var category_name = $("input[name=category_name]").val();

			var category_id = $("input[name=category_id]").val();

			var sort_by = $("#sort_by").val();

			var query = $("#query").val();

			var records_per_page = $("#records_per_page").val();

			$.ajaxSetup({

				headers: {

					'X-CSRF-TOKEN': token.val()

				}


			});

			var min_price = $("input[name=min-price]").val();

			var max_price = $("input[name=max-price]").val();

			var multiple_colors_filter = [];

			$("input[name='colors[]']:checked").each(function(){
       
		        multiple_colors_filter.push($(this).val());
		   
		    });

		    var multiple_size_filter = [];

		    $("input[name='size[]']:checked").each(function(){
       
		        multiple_size_filter.push($(this).val());
		   
		    });



		    var vendor_filter = [];

			$("input[name='vendor_filter[]']:checked").each(function(){

				vendor_filter.push($(this).val());

			});

			console.log(query);


			$.ajax({

				url: '/search-result',

				type: "POST",

				data: {query:query, category_name: category_name, category_id:category_id, min_price: min_price, max_price: max_price, multiple_size_filter: multiple_size_filter, multiple_colors_filter: multiple_colors_filter, vendor_filter: vendor_filter, sort_by:sort_by, view_status:view_status, records_per_page: records_per_page, page:page},

				success: function(response) 
				{
					console.log(response.sort_by);

					$("#get-product").html(response.data);

					$("#pagination").html(response.pagination);  
					
					$("input[name='product_id[]']").each(function(){	

						var product_id = $(this).val(); 

						setTimeout(function() {

							document.getElementById("product-single"+product_id).style.display = "block";

							document.getElementById("product-thumb"+product_id).style.display = "block";

							document.getElementById("product-body"+product_id).style.display = "block";

							document.getElementById("product-single"+product_id).classList.remove("loading");
						
						}, 2000);


					});


					// },4000);
				},
				error: function (error)
				{
					console.log(error);
				}

			});
		
		}

		$(document).on('click', '.pagination_link', function() {

			var page = $(this).attr('id');

			get_products(page);

		});

		

	});



</script>

@endsection