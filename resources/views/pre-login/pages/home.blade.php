@extends('pre-login.index.index')

@section('content')

<!-- <div class="demo-2" id="section-load-main"></div> -->

<div id="section-main" >

	<div id="home">

		<div class="home-wrap">

			<div id="home-slick">

				@foreach($carousels as $carousel)

				<a href="/{{$carousel->category()->first()->category_name}}/products/{{$carousel->category()->first()->id}}">

					<div class="banner banner-1">

						<img src="{{ $carousel->image_public_id }}" alt="">

					</div>

				</a>

				@endforeach

			</div>

		</div>

	</div>

	@foreach($latestP as $product)

	@include('pre-login.partials.modal.quickViewModal')

	@endforeach

	<div class="section">

		<div class="container">

			<div class="row">

				<div class="col-md-12">

					<div class="section-title">

						<h2 class="title">{{__('app.latest_products')}}</h2>

						<div class="pull-right">

							<div class="product-slick-dots-1 custom-dots"></div>

						</div>

					</div>

				</div>


				<div class="col-md-12 col-sm-12 col-xs-12">

					<div class="row">

						@if(count($latestP) > 0)

						<div id="product-slick-1" class="product-slick">

							@foreach($latestP as $product)

							@if($product->user->activation == 0)

								@if($product->availability > 0)

								<div class="product product-single">

									<div class="product-thumb">

										<div class="product-label">

											@if ($product->current_price < $product->old_price)

											<span class="sale"><?= round((($product->current_price/$product->old_price)*100)-100);?> %</span>

											@endif

										</div>

										<a data-toggle="modal" href="#" data-target="#quick-preview{{$product->id}}">

											<button class="main-btn quick-view" style="font-size: 10px; letter-spacing: 0px;">

												<i class="fa fa-search-plus"></i> {{__('app.quick_view')}}

											</button>

										</a>

										<img style="height: 150px; object-fit: cover;" src="{{ $product->productImage()->first()->image_public_id }}" alt="{{$product->productImage()->first()->image_public_id}}">

									</div>

									<div class="product-body" onclick="product_click('/{{$product->product_name}}/detail/{{$product->id}}')">

										<h4 style="font-size: 16px;" class="product-price">

											{{$product->current_price}} Birr

											@if (!is_null($product->old_price))

											<del class="product-old-price">{{$product->old_price}} Birr</del>

											@endif

										</h4>

										<h2 class="product-name">

											<a href="/{{$product->product_name}}/detail/{{$product->id}}">{{str_limit($product->product_name, 20)}}</a></h2>

										<p >Sold by:

											<span style="font-weight: bold;">

												{{$product->user()->first()->shop_name}}

											</span>

										</p>

										<div class="product-btns">

											<button class="main-btn icon-btn" id="wish" onclick="add_wish(<?= $product->id;?>)">

												<i class="fa fa-heart"></i>

											</button>

											<a class="main-btn icon-btn" href="/{{$product->product_name}}/detail/{{$product->id}}">

												<i class="fa fa-eye"></i></a>

										</div>

									</div>

								</div>

								@endif

							@endif

							@endforeach

						</div>

						@else

						<div class="col-md-12">

							<h4 class="alert-danger" style="min-width: 100%; padding:10px;"><span class="fa fa-frown-o" style="margin-right: 5px;"></span>{{__('app.no_items')}}</h4>

						</div>

						@endif

					</div>

				</div>

			</div>

		</div>

	</div>

	<div class="section" style="background: #fff;">

		<div class="container">

			<div class="row">

				<div class="col-md-12">

					<div class="section-title">

						<h2 class="title">{{__('app.popular_categories')}}</h2>

						<div class="pull-right">

							<div class="product-slick-dots-4 custom-dots"></div>

						</div>

					</div>

				</div>

				<div class="col-md-12 col-sm-12 col-xs-12">

					<div class="row">

						@if(count($categories) > 0)

						<div id="product-slick-4" class="product-slick">

							@foreach($categories as $_c)

							<a class="banner banner-1" href="/{{$_c->category_name}}/products/{{$_c->id}}">

								<div class="overlay-effect"></div>

								<img src="{{ $_c->image_url }}" alt="{{$_c->image_url}}" style="box-shadow: 0px 8px 15px #00000040; border-radius: 5px; height: 250px; object-fit: cover;">

								<div class="banner-caption text-center">

									<h3 class="white-color">{{$_c->category_name}}</h3>

								</div>

							</a>

							@endforeach

						</div>

						@else

						<div class="col-md-12">

							<h4 class="alert-danger" style="min-width: 100%; padding:10px;"><span class="fa fa-frown-o" style="margin-right: 5px;"></span>{{__('app.no_items')}}</h4>

						</div>

						@endif

					</div>

				</div>

			</div>

		</div>

	</div>


	@if(!is_null($bestP))

	<div class="section">

		<div class="container">

			<div class="row">

				<div class="col-md-12">

					<div class="section-title">

						<h2 class="title">{{__('app.best_seller')}}</h2>

						<div class="pull-right">

							<div class="product-slick-dots-2 custom-dots"></div>

						</div>

					</div>

				</div>

				@if($bestP->user->activation == 0)

					@if($product->availability > 0)

					<div class="col-md-2 col-sm-6 col-xs-6">

						<div class="product product-single product-hot">

							<div class="product-thumb">

								<div class="product-label">

									@if ($bestP->current_price < $bestP->old_price)

									<span class="sale"><?= round((($bestP->current_price/$bestP->old_price)*100)-100);?> %</span>

									@endif

								</div>

								<a data-toggle="modal" href="#" data-target="#quick-preview{{$bestP->id}}">

									<button class="main-btn quick-view" style="font-size: 10px; letter-spacing: 0px;">

										<i class="fa fa-search-plus"></i> {{__('app.quick_view')}}

									</button>

								</a>

								<img style="height: 150px; object-fit: cover;" src="{{ $bestP->productImage()->first()->image_public_id }}" alt="{{$bestP->productImage()->first()->image_public_id}}">

							</div>

							<div class="product-body" onclick="product_click('/{{$bestP->product_name}}/detail/{{$bestP->id}}')">

								<h4 style="font-size: 16px;" class="product-price">

									{{$bestP->current_price}} Birr

									@if(!is_null($bestP->old_price))

									<del class="product-old-price">{{$bestP->old_price}} Birr</del>

									@endif

								</h4>

								<h2 class="product-name">

									<a href="/{{$bestP->product_name}}/detail/{{$bestP->id}}">{{str_limit($product->product_name, 20)}}</a></h2>

								<p >Sold by:

									<span style="font-weight: bold;">

										{{$product->user()->first()->shop_name}}

									</span>

								</p>

								<div class="product-btns">

									<button class="main-btn icon-btn" id="wish" onclick="add_wish(<?= $bestP->id;?>)">

										<i class="fa fa-heart"></i>

									</button>

									<a class="main-btn icon-btn" href="/{{$bestP->product_name}}/detail/{{$bestP->id}}">

										<i class="fa fa-eye"></i></a>

								</div>

							</div>

						</div>

					</div>

					@endif

				@endif

				<div class="col-md-10 col-sm-6 col-xs-6">

					<div class="row">

						<div id="product-slick-2" class="product-slick">

							@foreach($best_products as $product)

							@if($product->user->activation == 0)

								@if($product->availability > 0)

								<div class="product product-single">

									<div class="product-thumb">

										<div class="product-label">

											@if ($product->current_price < $product->old_price)

											<span class="sale"><?= round((($product->current_price/$product->old_price)*100)-100);?> %</span>

											@endif

										</div>

										<a data-toggle="modal" href="#" data-target="#quick-preview{{$product->id}}">

											<button class="main-btn quick-view" style="font-size: 10px; letter-spacing: 0px;">

												<i class="fa fa-search-plus"></i> {{__('app.quick_view')}}

											</button>

										</a>

										<img style="height: 150px; object-fit: cover;" src="{{ $product->productImage()->first()->image_public_id }}" alt="{{$product->productImage()->first()->image_public_id}}">

									</div>

									<div class="product-body" onclick="product_click('/{{$product->product_name}}/detail/{{$product->id}}')">

										<h4 style="font-size: 16px;" class="product-price">

											{{$product->current_price}} Birr

											@if (!is_null($product->old_price))

											<del class="product-old-price">{{$product->old_price}} Birr</del>

											@endif

										</h4>

										<h2 class="product-name">

											<a href="/{{$product->product_name}}/detail/{{$product->id}}">{{str_limit($product->product_name, 20)}}</a></h2>

										<p >Sold by:

											<span style="font-weight: bold;">

												{{$product->user()->first()->shop_name}}

											</span>

										</p>

										<div class="product-btns">

											<button class="main-btn icon-btn" id="wish" onclick="add_wish(<?= $product->id;?>)">

												<i class="fa fa-heart"></i>

											</button>

											<a class="main-btn icon-btn" href="/{{$product->product_name}}/detail/{{$product->id}}">

												<i class="fa fa-eye"></i></a>

										</div>

									</div>

								</div>

								@endif

							@endif

							@endforeach

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

	@endif


	<div class="section section-grey">

		<div class="container">

			<div class="row">

				<div class="col-md-12">

					<div class="section-title">

						<h2 class="title">{{__('app.recommended_offers')}}</h2>

						<div class="pull-right">

							<div class="product-slick-dots-3 custom-dots"></div>

						</div>

					</div>

				</div>


				<div class="col-12 col-sm-12 col-xs-12">

					<div class="row">

						@if(count($offers) > 0)

						<div id="product-slick-3" class="product-slick">



							@foreach($offers as $offer)

								@for($i = 0; $i < $offer->count(); $i++)

								@if($offer[$i]->user->activation == 0)

								@if($offer[$i]->availability > 0)

									<div class="product product-single">

										<div class="product-thumb">

											<div class="product-label">

												@if ($offer[$i]->current_price < $offer[$i]->old_price)

												<span class="sale"><?= round((($offer[$i]->current_price/$offer[$i]->old_price)*100)-100);?> %</span>

												@endif

											</div>

											<a data-toggle="modal" href="#" data-target="#quick-preview{{$offer[$i]->id}}">

												<button class="main-btn quick-view" style="font-size: 10px; letter-spacing: 0px;">

													<i class="fa fa-search-plus"></i> {{__('app.quick_view')}}

												</button>

											</a>

											<img style="height: 150px; object-fit: cover;" src="{{ $offer[$i]->productImage()->first()->image_public_id }}" alt="{{$offer[$i]->productImage()->first()->image_public_id}}">

										</div>

										<div class="product-body" onclick="product_click('/{{$offer[$i]->product_name}}/detail/{{$offer[$i]->id}}')">

											<h4 style="font-size: 16px;" class="product-price">

												{{$offer[$i]->current_price}} Birr

												@if (!is_null($offer[$i]->old_price))

												<del class="product-old-price">{{$offer[$i]->old_price}} Birr</del>

												@endif

											</h4>

											<h2 class="product-name">

												<a href="/{{$offer[$i]->product_name}}/detail/{{$offer[$i]->id}}">{{str_limit($offer[$i]->product_name, 20)}}</a></h2>

											<p >Sold by:

												<span style="font-weight: bold;">

													{{$offer[$i]->user()->first()->shop_name}}

												</span>

											</p>

											<div class="product-btns">

												<button class="main-btn icon-btn" id="wish" onclick="add_wish(<?= $offer[$i]->id;?>)">

													<i class="fa fa-heart"></i>

												</button>

												<a class="main-btn icon-btn" href="/{{$offer[$i]->product_name}}/detail/{{$offer[$i]->id}}">

													<i class="fa fa-eye"></i></a>

											</div>

										</div>

									</div>

								@endif

								@endif

								@endfor

							@endforeach

						</div>

						@else

						<div class="col-md-12">

							<h4 class="alert-danger" style="min-width: 100%; padding:10px;"><span class="fa fa-frown-o" style="margin-right: 5px;"></span>{{__('app.no_items')}}</h4>

						</div>

						@endif

					</div>

				</div>

			</div>

		</div>

	</div>

	<div class="section section-grey">

		<div class="container">

			<div class="row">

				<div class="col-md-12">

					<div class="section-title">

						<h2 class="title">{{__('app.package_offers')}}</h2>

						<div class="pull-right">

							<div class="product-slick-dots-5 custom-dots"></div>

						</div>

					</div>

				</div>

				<div class="col-12 col-sm-12 col-xs-12">

					<div class="row">

						@if(count($packages) > 0)

						<div id="product-slick-5" class="product-slick">

							@foreach($packages as $package)

								@if($package->packageProduct->first()->product->user()->first()['activation'] == 0)

									<div class="product product-single">

										<div class="product-thumb">

											<img style="height: 150px; object-fit: cover;" src="{{ $package->images->first()['image_public_id'] }}" alt="{{$package->images->first()['image_public_id']}}">

										</div>

										<div class="product-body" onclick="product_click('/package/{{$package->package_name}}/detail/{{$package->id}}')">

											<h4 style="font-size: 16px;" class="product-price">
												{{$package->discount_price}} Birr
											</h4>

											<h2 class="product-name">
												<a href="#">{{str_limit($package->package_name, 20)}}</a></h2>

											<p >Sold by:
												<span style="font-weight: bold;">
													{{$package->user->shop_name}}
												</span>
											</p>
											<div class="product-btns">

												<a class="btn btn-success" style="border-top-right-radius: 50px; padding-right: 20px;" href="/package/{{$package->package_name}}/detail/{{$package->id}}">See More</a>

											</div>

										</div>

									</div>

								@endif

							@endforeach



						</div>

						@else

						<div class="col-md-12">

							<h4 class="alert-danger" style="min-width: 100%; padding:10px;"><span class="fa fa-frown-o" style="margin-right: 5px;"></span>{{__('app.no_items')}}</h4>

						</div>

						@endif

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<div class="message"></div>

@endsection
