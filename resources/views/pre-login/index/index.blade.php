<?php
$companysettings = App\CompanySettings::first();
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="csrf-token" content="{{csrf_token()}}">

	<title>{{ $companysettings->company_name }}</title>


	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="{{URL::asset('css/bootstrap.min.css')}}" />

	<!-- Slick -->
	<link type="text/css" rel="stylesheet" href="{{URL::asset('css/slick.css')}}" />

	<link type="text/css" rel="stylesheet" href="{{URL::asset('css/slick-theme.css')}}" />

	<link rel="stylesheet" href="{{URL::asset('vendors/mdi/css/materialdesignicons.min.css')}}">

	<!-- nouislider -->
	<link type="text/css" rel="stylesheet" href="{{URL::asset('css/nouislider.min.css')}}" />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="{{URL::asset('css/font-awesome.min.css')}}">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="{{URL::asset('css/style.css')}}" />

  	@if(Request::is('wishes') || Request::is('my-orders'))

	<link rel="stylesheet" href="{{URL::asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">

	@endif

	<link rel="shortcut icon" href="{{URL::asset('storage/uploads/favicon.png')}}" />



</head>

<body>

  <script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyBPCRMMp4wCB1RYgrj0lavgRLSn-v9kz8g&libraries=places"></script>

	<div class="jumping-dots-loader" id="loader-main" style="position: absolute; top: 40%; left: 48%;">

	    <span></span>

	    <span></span>

	    <span></span>

  	</div>



  	<div id="loading-content" class="loading-content">

		@include('pre-login.index.header')

		@yield('content')

		<div id="loading-content2" class="loading-content">

			@include('pre-login.index.footer')

		</div>

  	</div>


	<!-- jQuery Plugins -->



    <script src="{{URL::asset('js/jquery.min.js')}}"></script>

	<script src="{{URL::asset('js/_customJs_/products-click.js')}}"></script>

	@if(Request::is('my-address'))

	<script src="{{URL::asset('js/_customJs_/map_address.js')}}"></script>

	@endif

	<script type="text/javascript">

		$(window).on('load', function() {

  			document.getElementById('loader-main').style.display = 'none';

  			document.getElementById('loading-content').style.display = 'block';

  			document.getElementById('loading-content2').style.display = 'block';

  		});

	</script>

	<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>


	<script src="{{URL::asset('js/slick.min.js')}}"></script>

	<script src="{{URL::asset('js/nouislider.min.js')}}"></script>


	<script src="{{URL::asset('js/jquery.zoom.min.js')}}"></script>

	<script src="{{URL::asset('js/main.js')}}"></script>

	<script src="{{URL::asset('js/sweetalert.min.js')}}"></script>

  	<!-- <script src="{{URL::asset('vendors/jquery-validation/jquery.validate.min.js')}}"></script> -->

  	@if(Request::is('wishes') || Request::is('my-orders'))

  	<script src="{{URL::asset('vendors/datatables.net/jquery.dataTables.js')}}"></script>

  	<script src="{{URL::asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>

  	@endif



	<!-- _customJs _-->

	<!-- <script src="{{URL::asset('js/admin_js/form-validation.js')}}"></script> -->


	@if(auth()->user())

	<script src="{{URL::asset('js/_customJs_/cart.js')}}"></script>

	<script src="{{URL::asset('js/_customJs_/wishlist.js')}}"></script>

	@endif

	@if(Request::is('cart'))

	<script src="{{URL::asset('js/_customJs_/order-show.js')}}"></script>

	@endif

	<script src="{{URL::asset('js/_customJs_/lang.js')}}"></script>

	<script src="{{URL::asset('js/bootstrap3-typeahead.min.js')}}"></script>

	<script src="{{URL::asset('js/_customJs_/search-products.js')}}"></script>

	@if(Request::is('faq'))
	<script src="{{URL::asset('js/_main.js')}}"></script>
	@endif
</body>

</html>
