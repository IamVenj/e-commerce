<?php

$main_categories = App\Category::where('parent_id', null)->get();

?>

<!-- NAVIGATION -->
<div id="navigation">

	<div class="container">

		<div id="responsive-nav">

			<div class="category-nav show-on-click">
				
				<span class="category-header" <?php if(app()->getLocale() == 'am'): ?> style="font-size: 16px;" <?php endif; ?>>{{__('app.categories')}} <i class="fa fa-list"></i></span>

				<ul class="category-list">

					@foreach($main_categories as $main_category)

					<?php

					$categories = App\Category::where('parent_id', $main_category->id)->get();

					?>

					<li class="dropdown side-dropdown">

						<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">{{$main_category->category_name}} <i class="fa fa-angle-right"></i></a>

						<div class="custom-menu">

							<div class="row">

								@foreach($categories as $category)

								<div class="col-md-4">

									<ul class="list-links">

										<li>

											<h3 class="list-links-title"><a href="/{{$category->category_name}}/products/{{$category->id}}">{{$category->category_name}}</a></h3></li>

										<?php

										$simple_categories = App\Category::where('parent_id', $category->id)->get();

										?>

										@foreach($simple_categories as $simple_category)
										
										<li><a href="/{{$simple_category->category_name}}/products/{{$simple_category->id}}">{{$simple_category->category_name}}</a></li>
										
										@endforeach

									</ul>

									<hr class="hidden-md hidden-lg">

								</div>

								@endforeach
								
							</div>

							<div class="row hidden-sm hidden-xs">

								<div class="col-md-12">

									<hr>

									<a class="banner banner-1">

										<div class="overlay-effect h-100 w-100"></div>

										<img style="height: 150px !important" src="<?= Cloudder::show($main_category->image_public_id, array("version"=>$main_category->image_version, "quality" => "auto", "height" => 400, "width"=>400));?>" alt="{{$main_category->image_public_id}}">

										<div class="banner-caption text-center">

											<h2 class="white-color">NEW COLLECTION</h2>

											<h3 class="white-color font-weak">HOT DEAL</h3>

										</div>

									</a>

								</div>

							</div>

						</div>

					</li>

					@endforeach
					
				</ul>

			</div>




			<div class="menu-nav">

				<span class="menu-header">Menu <i class="fa fa-bars"></i></span>

				<ul class="menu-list">

					@foreach($main_categories as $main_category)

					<li class="dropdown mega-dropdown full-width"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true" style="cursor: pointer; font-size: 10px; padding-right:0px; font-weight: 700;">{{$main_category->category_name}}<i class="fa fa-caret-down" style="margin-left: 1px;"></i></a>

						<div class="custom-menu">

							<div class="row">

								<?php

								$categories = App\Category::where('parent_id', $main_category->id)->get();

								?>

								@foreach($categories as $category)

								<div class="col-md-2">

									<div class="hidden-sm hidden-xs">

										<a class="banner banner-1" href="/{{$category->category_name}}/products/{{$category->id}}">

											<div class="overlay-effect"></div>

											<img style="height: 100px !important" src="<?= Cloudder::show($category->image_public_id, array("version"=>$category->image_version, "quality" => "auto", "height" => 300, "width"=>300));?>" alt="{{$category->image_public_id}}">

											<div class="banner-caption text-center">

												<h6 class="white-color text-uppercase">{{$category->category_name}}</h6>

											</div>

										</a>

										<hr>

									</div>

									<ul class="list-links">

										<?php

										$simple_categories = App\Category::where('parent_id', $category->id)->get();

										?>

										@foreach($simple_categories as $simple_category)
									
										<li><a href="/{{$simple_category->category_name}}/products/{{$simple_category->id}}">{{$simple_category->category_name}}</a></li>
									
										@endforeach

									</ul>

								</div>

								@endforeach

							</div>

						</div>

					</li>					

					@endforeach

				</ul>

			</div>

		</div>

	</div>

</div>