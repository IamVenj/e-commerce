<!-- HEADER -->

<header>

	<div id="top-header">

		<div class="container">

			<div class="pull-left">

				<span>{{__('app.welcome')}}!</span>

			</div>

			<div class="pull-right">

				<ul class="header-top-links">

					<li>
						<a href="/register" class="btn btn-success" style="font-weight: bold; padding-top: 2px; padding-bottom: 2px;">
							<i class="fa fa-account" style="margin-right: 5px;"></i>Become a vendor
						</a>	
					</li>

					<li>
						<a href="/contact" style="font-weight: bold;">
							<i class="fa fa-phone" style="margin-right: 5px;"></i>Contact Us
						</a>	
					</li>

					<li class="dropdown default-dropdown">

						<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">

							@if(app()->getLocale() == 'en')

							{{__('app.short_eng')}} <i class="fa fa-caret-down"></i>

							@elseif(app()->getLocale() == 'am')

							{{__('app.amh')}} <i class="fa fa-caret-down"></i>

							@endif

						</a>

						<ul class="custom-menu">

							<li onclick="change_language('en')">

								<a style="cursor: pointer;">
										
									{{__('app.eng')}} ({{__('app.short_eng')}})

								</a>

							</li>

							<li onclick="change_language('am')">

								<a style="cursor: pointer;">

									{{__('app.amh')}} ({{__('app.short_amh')}})

								</a>

							</li>

						</ul>

					</li>

					

				</ul>

				

			</div>

		</div>

	</div>


	<div id="header">

		<div class="container">

			<div class="pull-left">


				<div class="header-logo">

					<a class="logo" href="/">

						<img src="{{URL::asset('storage/uploads/logo.png')}}" alt="">

					</a>

				</div>


				<div class="header-search">

					<form action="/search" method="get">
										
						<input class="input search-input" name="query" autocomplete="off" id="search-products" type="text" placeholder="{{__('app.search')}}">
					
						<select class="input search-categories" name="category" id="search-category">
					
							<option value="0">{{__('app.all_categories')}}</option>

							<?php

							$categories = App\Category::all();

							?>

							@foreach($categories as $category)

										<option value="{{$category->id}}">{{$category->category_name}}</option>

							@endforeach

						</select>

						<button class="search-btn" type="submit"><i class="fa fa-search"></i></button>

					</form>

				</div>

			</div>
			

			<div class="pull-right">

				<ul class="header-btns">

					<li class="header-account dropdown default-dropdown">

						<div class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="true">

							@if(is_null(auth()->user())) 
							
							<div class="header-btns-icon">

								<i class="fa fa-user-o"></i> 

							</div>

							@else 

							<div class="picture-container">
          
				                <div class="picture">
				            
				                  	<img src="@auth @if(!is_null(auth()->user()->photo_url))<?= Cloudder::show(auth()->user()->photo_url, ['width'=>200, 'height'=>200, 'crop'=>'fill']);?>@else {{ URL::asset('images/default-avatar.png') }} @endif @endauth" class="picture-src" id="profilePicturePreview" title=""/>
				                          
				                </div>
				                            
			              	</div> 

			            	@endif

							<strong class="text-uppercase" <?php if(app()->getLocale() == 'am'): ?> style="font-size: 16px; padding-left: 0px; padding-right: 50px;" <?php endif; ?>>{{__('app.my_account')}} <i class="fa fa-caret-down"></i></strong>

						</div>

						@guest

						<a href="/login" class="text-uppercase">{{__('app.login')}}</a> / <a href="/register" class="text-uppercase">{{__('app.join')}}</a>

						@else

						<p>{{auth()->user()->name}}</p>

						@endguest

						<ul class="custom-menu">

							<li><a href="/account"><i class="fa fa-user-o"></i> {{__('app.my_account')}}</a></li>

							@auth

							<li><a href="/wishes"><i class="fa fa-heart-o"></i> {{__('app.my_wishlist')}}</a></li>

							<li><a href="/my-address"><i class="fa fa-map-marker" style="margin-right: 20px;"></i> {{__('app.my_address')}}</a></li>

							<li><a href="/my-orders"><i class="fa fa-list"></i> {{__('app.my_orders')}}</a></li>

							@else

							<li><a href="/login"><i class="fa fa-heart-o"></i> {{__('app.my_wishlist')}}</a></li>

							@endauth

							@auth

							<li style="margin-top: 20px;">

								<form action="/logout" method="post">
									@csrf
									<button class="btn btn-default"><i class="fa fa-lock" style="font-size: 18px;"></i> {{__('app.logout')}}</button>
								</form>

							</li>

							@endauth

						</ul>

					</li>


					<li class="header-cart dropdown new-dropdown">

						<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">

							<div class="header-btns-icon">

								<i class="fa fa-shopping-cart"></i>

								<span class="qty" id="cart_count">{{Cart::instance('default')->count()}}</span>

							</div>

							<strong class="text-uppercase" <?php if(app()->getLocale() == 'am'): ?> style="font-size: 16px; padding-left: 0px; padding-right: 50px;" <?php endif; ?>>{{__('app.my_cart')}} <i class="fa fa-caret-down"></i></strong>

							<p id="total-cart"></p>

						</a>

						<div class="custom-menu">

							<div id="shopping-cart">

								<div class="shopping-cart-list">

									<div class="cart-item"></div>

								</div>

								<div class="shopping-cart-btns">

									<a href="/cart" style="font-size: 12px;"><button class="btn main-btn btn-block" style="<?php if(app()->getLocale() == 'am'): ?>font-size: 16px; <?php endif; ?> border-top-right-radius: 60px; border-bottom-left-radius: 60px;">{{__('app.view_cart')}}</button></a>

								</div>

							</div>

						</div>

					</li>


					<li class="nav-toggle">

						<button class="nav-toggle-btn main-btn icon-btn"><i class="fa fa-bars"></i></button>

					</li>

				</ul>

			</div>

		</div>

	</div>

</header>


@include('pre-login.index.nav')