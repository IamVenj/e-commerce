<?php

$setting = App\CompanySettings::first();

?>
<footer id="footer" class="section section-grey">

	<div class="container">

		<div class="row">

			<div class="col-md-3 col-sm-6 col-xs-6">
		
				<div class="footer">

					<div class="footer-logo">

						<a class="logo" href="/">

				            <img src="{{URL::asset('storage/uploads/logo.png')}}" alt="">

				        </a>

					</div>

					<p>{{$setting->slug}}</p>

					<ul class="footer-social">

						@if(!is_null($setting->facebook))
	
						<li><a href="{{$setting->facebook}}"><i class="fa fa-facebook"></i></a></li>

						@endif

						@if(!is_null($setting->twitter))
	
						<li><a href="{{$setting->twitter}}"><i class="fa fa-twitter"></i></a></li>
	
						@endif

						@if(!is_null($setting->instagram))

						<li><a href="{{$setting->instagram}}"><i class="fa fa-instagram"></i></a></li>
	
						@endif

						@if(!is_null($setting->google_plus))

						<li><a href="{{$setting->google_plus}}"><i class="fa fa-google-plus"></i></a></li>
	
						@endif
	
					</ul>

				</div>

			</div>



			<div class="col-md-3 col-sm-6 col-xs-6">

				<div class="footer">

					<h3 class="footer-header">{{__('app.my_account')}}</h3>

					<ul class="list-links">

						@auth

						<li><a href="/account">{{__('app.my_account')}}</a></li>

						<li><a href="/wishes">{{__('app.my_wishlist')}}</a></li>

						@else

						<li><a href="/login">{{__('app.my_account')}}</a></li>

						<li><a href="/login">{{__('app.my_wishlist')}}</a></li>

						@endauth

						@guest

						<li><a href="/login">{{__('app.login')}}</a></li>

						@endguest

					</ul>

				</div>

			</div>


			<div class="clearfix visible-sm visible-xs"></div>


			<div class="col-md-3 col-sm-6 col-xs-6">

				<div class="footer">

					<h3 class="footer-header">{{__('app.cust_service')}}</h3>

					<ul class="list-links">

						<li><a href="/about">{{__('app.about_us')}}</a></li>

						<li><a href="/shipping/return">{{__('app.ship_return')}}</a></li>

						<li><a href="/shipping/guide">{{__('app.ship_guide')}}</a></li>

						<li><a href="/faq">{{__('app.faq')}}</a></li>

						<li><a href="/Terms-and-conditions">{{__('app.terms_and_conditions')}}</a></li>

					</ul>

				</div>

			</div>



			<div class="col-md-3 col-sm-6 col-xs-6">

				<div class="footer">

					<h3 class="footer-header">{{__('app.stay_connected')}}</h3>

					<form action="/join-newsletter" method="post">

						@csrf

						<div class="form-group">

							<input class="input" name="email" type="email" placeholder="{{__('app.enter_email')}}">

						</div>

						<button class="primary-btn" type="submit">{{__('app.join_letter')}}</button>

					</form>

				</div>

			</div>

		</div>

		<hr>

		<div class="row">

			<div class="col-md-8 col-md-offset-2 text-center">

				<div class="footer-copyright">

					{{__('app.copyright')}} &copy;<script>document.write(new Date().getFullYear());</script> {{__('app.rights')}} | {{(__('app.developer_note'))}} <i class="fa fa-heart-o" aria-hidden="true"></i> {{__('app.by')}} <a href="" target="_blank">{{__('app.developer_name')}}</a>

				</div>

			</div>

		</div>

	</div>

</footer>

@include('_session_.error2')

@include('_session_.success2')