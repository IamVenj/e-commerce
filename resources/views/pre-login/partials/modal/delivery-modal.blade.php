<div class="modal customWidth fade" role="dialog" id="cancel-order<?= $orders[$i]->id;?>">

    <div class="modal-dialog modal-sm">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3" style="background: #F8694A; color: #fff;">

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

                <h4 class="modal-title" style="color:#fff;">Are you sure?</h4>
            
            </div>

            <form action="/cancel-my-order/{{$orders[$i]->id}}" method="post">
                    
                @csrf

                @method('DELETE')

                <div class="modal-footer">                       

                    <button class="btn btn-danger btn-block" type="submit">Cancel Order</button>

                </div>

            </form>

        </div>

    </div>

</div>