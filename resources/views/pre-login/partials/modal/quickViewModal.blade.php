<div class="modal customWidth fade" role="dialog" id="quick-preview<?= $product->id; ?>">

    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3" style="background: #F8694A; color: #fff;">

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

            	<h4 class="modal-title" style="color:#fff;">{{$product->product_name}}</h4>
            
            </div>

            <div class="modal-body">
         
            	<div class="row">
	     
	                <div class="product product-details clearfix">

	                	<div class="col-md-4">
	
							<div class="product-view" style="box-shadow: 10px 5px 15px #00000004; border-top-left-radius: 40px; border-bottom-right-radius: 40px;">

								<img style="border-top-left-radius: 40px; box-shadow: 10px 5px 15px #00000004; border-bottom-right-radius: 40px; object-fit: cover; align-items: flex-start; height: auto !important; overflow: hidden;" src="{{ $product->productImage()->first()->image_public_id }}" alt="{{$product->productImage()->first()->image_public_id}}">
							</div>
	
						</div>

						<form action="/cart" method="post" class="cart-form">

							<div class="col-md-8">

								<div class="product-body">

									<div class="product-label">

										@if($product->current_price < $product->old_price)

										<div class="product-label">

											<span class="sale"><?= round((($product->current_price/$product->old_price)*100)-100) ;?>%</span>

										</div>

										@endif

									</div>

									<h2 class="product-name" style="font-size: 25px;">{{$product->product_name}}</h2>

									<h3 class="product-price">{{$product->current_price}} Birr

										@if(!is_null($product->old_price))

										<del class="product-old-price" style="margin-left: 10px;">

											{{$product->old_price}} Birr

										</del>

										@endif

									</h3>

									<p><strong>{{__('app.Availability')}}:</strong> {{$product->availability}} left</p>

									<p><strong>{{__('app.Brand')}}:</strong> {{$product->brand}}</p>

									<p>{{$product->slug}}</p>

									<div class="product-options">

										@if($product->productSize()->count() > 0)

										<ul class="size-option">

											<li><span class="text-uppercase">{{__('app.Size')}}:</span></li>

											<div class="row">

												@foreach($product->productSize()->get() as $productSIze)

												<div class="col-md-1">

													<div class="form-check">

							                            <label class="form-check-label">

							                              <input type="radio" class="form-check-input" name="size{{$product->id}}" id="size" value="{{$productSIze->id}}">

							                              {{$productSIze->size}}

							                            <i class="input-helper"></i></label>

							                        </div>

						                        </div>

					                         	 @endforeach

				                         	</div>

										</ul>

										@endif

										@if($product->productColors()->count() > 0)

										<ul class="color-option">

											<li><span class="text-uppercase">{{__('app.Color')}}:</span></li>

											<div class="row">

												@foreach($product->productColors()->get() as $productColor)

												<div class="col-md-2">

													<div class="form-check">

							                            <label class="form-check-label">

							                            	@foreach($productColor->Colors()->get() as $colors)

				                            				<li style="width: 50px; border-bottom-right-radius: 20px; border-top-left-radius: 20px; height: 50px; background: {{$colors->hex}};"></li>

							                              <input type="radio" class="form-check-input" name="color{{$product->id}}" id="color" value="{{$colors->id}}">

							                              @endforeach

							                            <i class="input-helper"></i></label>

							                        </div>

						                        </div>

					                         	 @endforeach

				                         	</div>

										</ul>

										@endif

									</div>


									<div class="product-btns">

										<div class="qty-input">

											<span class="text-uppercase">{{__('app.quantity')}}: </span>

											<input class="input" id="qty{{$product->id}}" type="number" min="1" value="1">

											

										</div>

										<input type="hidden" name="product_name{{$product->id}}" value="{{$product->product_name}}">

										<input type="hidden" name="product_price{{$product->id}}" value="{{$product->current_price}}">

										<input type="hidden" name="product_availability{{$product->id}}" value="{{$product->availability}}">

										<input type="hidden" name="cart-csrf-token" value="{{csrf_token()}}">

										<input type="hidden" name="cart-product-id{{$product->id}}" value="{{$product->id}}">
										
										@guest

										<a href="/login"><button type="button" class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> {{__('app.add_cart')}}</button></a>

										@else

										<button type="button" onclick="cart_submit(<?= $product->id;?>)" class="primary-btn add-to-cart" id="add_cart"><i class="fa fa-shopping-cart"></i> {{__('app.add_cart')}}</button>	

										<div class="lds-roller" id="loader-roller{{$product->id}}" style="display: none;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>	

										@endguest

										<div class="pull-right">

											<a class="main-btn icon-btn" href="/{{$product->product_name}}/detail/{{$product->id}}"><i class="fa fa-eye"></i></a>

											<button class="main-btn icon-btn" type="button" onclick="add_wish(<?= $product->id; ?>)"><i class="fa fa-heart"></i></button>

											@guest

											<a href="/login"><button type="button" class="btn main-btn"><i class="fa fa-money"></i> {{__('app.buy')}}</button></a>

											@else

											<button type="button" onclick="buy(<?= $product->id;?>)" class="btn main-btn" id="buy_product"><i class="fa fa-money"></i> {{__('app.buy')}}</button>	

											<div class="lds-roller" id="loader-roller2{{$product->id}}" style="display: none;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>	

											@endguest

										</div>
						
									</div>
						
								</div>
						
							</div>

						</form>

					</div>
            		
	        	</div>

	        </div>

	    </div>

	</div>

</div>