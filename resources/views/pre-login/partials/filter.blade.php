<div id="aside" class="col-md-3">

	<!-- /aside widget -->

	<!-- aside widget -->

	<div class="aside" style="padding: 20px; background: #fff; box-shadow: 0px 5px 14px rgba(0,0,0,0.1);">

		<h3 class="aside-title">{{__('app.filter_price')}}</h3>

		<div id="price-slider"></div>

		<input type="hidden" name="max-price" id="max-price">

		<input type="hidden" name="min-price" id="min-price">


	</div>

	<!-- aside widget -->

	<!-- aside widget -->

	<div class="aside" style="padding: 20px; background: #fff; box-shadow: 0px 5px 14px rgba(0,0,0,0.1);">

		<h3 class="aside-title">{{__('app.filter_color')}}</h3>

		<ul class="color-option">

			<div class="row" id="style-111" style="overflow-x: hidden; height: 250px;">

			@foreach($all_colors as $colors)
                             
              <div class="col-md-3">
                
                <div class="form-check">
                
                  <label class="form-check-label">
                                        
                    <li style="padding: 55%; background: {{$colors->hex}};"></li>
                
                    <input class="checkbox" id="multiple_colors_filter<?= $colors->id;?>" type="checkbox" name="colors[]" value="{{$colors->id}}">
                
                  <i class="input-helper"></i></label>
                
                </div>

              </div>

              <input type="hidden" name="colors_id" value="{{$colors->id}}">           

			@endforeach

            </div>

		</ul>

	</div>

	<!-- /aside widget -->

	<!-- aside widget -->
	<div class="aside" style="padding: 20px; background: #fff; box-shadow: 0px 5px 14px rgba(0,0,0,0.1);">

		<h3 class="aside-title">{{__('app.filter_size')}}</h3>

		<ul class="size-option">

			<li>
				
				<div class="form-check">
                        
                  <label class="form-check-label">
                  
                  	XS
                  
                  	<input class="checkbox" id="multiple_size_filter" type="checkbox" name="size[]" value="XS">
                
                  <i class="input-helper"></i></label>
                
                </div>

            </li>
 
            <li>
				
				<div class="form-check">
                        
                  <label class="form-check-label">
                  
                  	S
                  
                  	<input class="checkbox" id="multiple_size_filter" type="checkbox" name="size[]" value="S">
                
                  <i class="input-helper"></i></label>
                
                </div>

            </li>
 
            <li>
				
				<div class="form-check">
                        
                  <label class="form-check-label">
                  
                  	M
                  
                  	<input class="checkbox" id="multiple_size_filter" type="checkbox" name="size[]" value="M">
                
                  <i class="input-helper"></i></label>
                
                </div>

            </li>
 
            <li>
				
				<div class="form-check">
                        
                  <label class="form-check-label">
                  
                  	L
                  
                  	<input class="checkbox" id="multiple_size_filter" type="checkbox" name="size[]" value="L">
                
                  <i class="input-helper"></i></label>
                
                </div>

            </li>
 
            <li>
				
				<div class="form-check">
                        
                  <label class="form-check-label">
                  
                  	XL
                  
                  	<input class="checkbox" id="multiple_size_filter" type="checkbox" name="size[]" value="XL">
                
                  <i class="input-helper"></i></label>
                
                </div>

            </li>
 
            <li>
				
				<div class="form-check">
                        
                  <label class="form-check-label">
                  
                  	XXL
                  
                  	<input class="checkbox" id="multiple_size_filter" type="checkbox" name="size[]" value="XXL">
                
                  <i class="input-helper"></i></label>
                
                </div>

            </li>

		</ul>

	</div>

	<!-- /aside widget -->


	<!-- aside widget -->

	<div class="aside" style="padding: 20px; background: #fff; box-shadow: 0px 5px 14px rgba(0,0,0,0.1);">

		<h3 class="aside-title">{{__('app.filter_vendor')}}</h3>

		<ul class="list-links" id="style-111" style="overflow-x: hidden; height: 250px;">

			@foreach($vendors as $vendor)

			<li>

				<div class="form-check">
                        
                  <label class="form-check-label">

					{{$vendor->shop_name}}

					<input class="checkbox" id="vendor_filter" type="checkbox" name="vendor_filter[]" value="{{$vendor->id}}">
                
                  <i class="input-helper"></i></label>
                
                </div>

			</li>

			@endforeach

		</ul>

	</div>

	<!-- /aside widget -->

</div>


