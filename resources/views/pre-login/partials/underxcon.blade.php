@extends('pre-login.index.index')

@section('content')

<!-- BREADCRUMB -->

<div id="breadcrumb">

	<div class="container">

		<ul class="breadcrumb">

			<li><a href="#">{{__('app.home')}}</a></li>

			<li class="active">{{__('app.sorry')}}</li>

		</ul>

	</div>

</div>

<!-- /BREADCRUMB -->

<!-- section -->

<div class="section">

	<div class="container">

		<div class="row">

			<div class="section section-grey col-md-8 col-md-offset-2 cart" style="margin-bottom: 10px; margin-top: 10px;">
					
				<img src="{{asset('images/construction.jpg')}}" style="margin-right: auto; margin-left: auto; display: block;">

				<h1 class="text-center text-white">The Page is under-Construction</h1>

			</div>

		</div>

	</div>

</div>

@endsection