<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
		<o:AllowPNG/>
		<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="date=no" />
	<meta name="format-detection" content="address=no" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
   	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Raleway:400,400i,700,700i" rel="stylesheet" />
    <!--<![endif]-->
	<title>Email Template</title>
	<!--[if gte mso 9]>
	<style type="text/css" media="all">
		sup { font-size: 100% !important; }
	</style>
	<![endif]-->
	

	<style type="text/css" media="screen">
		/* Linked Styles */
		body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#5D4D44; -webkit-text-size-adjust:none }
		a { color:#000001; text-decoration:none }
		p { padding:0 !important; margin:0 !important } 
		img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
		.mcnPreviewText { display: none !important; }
		.text-footer2 a { color: #ffffff; } 
		
		/* Mobile styles */
		@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
			.mobile-shell { width: 100% !important; min-width: 100% !important; }
			
			.m-center { text-align: center !important; }
			.m-left { text-align: left !important; margin-right: auto !important; }
			
			.center { margin: 0 auto !important; }
			.content2 { padding: 8px 15px 12px !important; }
			.t-left { float: left !important; margin-right: 30px !important; }
			.t-left-2  { float: left !important; }
			
			.td { width: 100% !important; min-width: 100% !important; }

			.content { padding: 30px 15px !important; }
			.section { padding: 30px 15px 0px !important; }

			.m-br-15 { height: 15px !important; }
			.mpb5 { padding-bottom: 5px !important; }
			.mpb15 { padding-bottom: 15px !important; }
			.mpb20 { padding-bottom: 20px !important; }
			.mpb30 { padding-bottom: 30px !important; }
			.m-padder { padding: 0px 15px !important; }
			.m-padder2 { padding-left: 15px !important; padding-right: 15px !important; }
			.p70 { padding: 30px 0px !important; }
			.pt70 { padding-top: 30px !important; }
			.p0-15 { padding: 0px 15px !important; }
			.p30-15 { padding: 30px 15px !important; }			
			.p30-15-0 { padding: 30px 15px 0px 15px !important; }			
			.p0-15-30 { padding: 0px 15px 30px 15px !important; }			


			.text-footer { text-align: center !important; }

			.m-td,
			.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

			.m-block { display: block !important; }

			.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

			.column,
			.column-dir,
			.column-top,
			.column-empty,
			.column-top-30,
			.column-top-60,
			.column-empty2,
			.column-bottom { float: left !important; width: 100% !important; display: block !important; }

			.column-empty { padding-bottom: 15px !important; }
			.column-empty2 { padding-bottom: 30px !important; }

			.content-spacing { width: 15px !important; }
		}
	</style>
</head>
<body class="body"style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#5D4D44; -webkit-text-size-adjust:none;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#5D4D44">
		<tr>
			<td align="center" valign="top">
				<!-- Main -->
				<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
					<tr>
						<td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
							<!-- Header -->
							
							<!-- END Header -->
								
							<!-- Section 1 -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
								
								<tr>
									<td class="p30-15-0" style="padding: 50px 30px 0px;" bgcolor="#ffffff">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="h2-center"style="color:#000000; font-family:'Montserrat', Times, 'Montserrat', serif; font-size:32px; line-height:36px; text-align:center; padding-bottom:20px;">Your Order</td>
											</tr>											
										</table>
									</td>
								</tr>
							</table>
							<!-- END Section 1 -->

							<!-- Section 2 -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FAAF40">
											<tr>
												<td class="fluid-img"style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://habeshalebs.com/public/storage/uploads/support/free_blue_white.jpg" width="650" height="162" border="0" alt="" /></td>
											</tr>
											<tr>
												<td class="p0-15" style="padding: 0px 30px;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														@foreach($buyer['contents'] as $item)
														<tr>
															<td class="pb40"style="padding-bottom:20px;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="event-separator"style="padding-bottom:20px; border-bottom:1px solid #ffffff;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="h5-black black"style="font-family:'Montserrat', Arial,sans-serif; font-size:16px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#fff;"><multiline>{{$item->model->product_name.' x'.$item->qty}}</multiline></td>
																							</tr>
																						</table>
																					</th>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top" width="156"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td align="right">
																									<table class="m-left" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="day"style="color:#fff; font-family:'Raleway', Arial,sans-serif; font-size:20px; line-height:14px; text-align:left;"><multiline>{{$item->model->current_price * $item->qty.' '.$buyer['currency']}}</multiline></td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</th>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														@endforeach

														
														
														<tr>
															<td class="pb40"style="padding-bottom:20px;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="event-separator"style="padding-bottom:20px; border-bottom:1px solid #ffffff;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="h5-black black"style="font-family:'Montserrat', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; color:#fff;"><multiline>Subtotal</multiline></td>
																							</tr>
																						</table>
																					</th>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top" width="156"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td align="right">
																									<table class="m-left" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="day"style="color:#fff; font-family:'Raleway', Arial,sans-serif; font-size:20px; line-height:14px; text-align:left;"><multiline>{{$buyer['subtotal'].' '.$buyer['currency']}}</multiline></td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</th>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>

														<tr>
															<td class="pb40"style="padding-bottom:20px;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="event-separator"style="padding-bottom:20px; border-bottom:1px solid #ffffff;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="h5-black black"style="font-family:'Montserrat', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; color:#fff;"><multiline>Discount</multiline></td>
																							</tr>
																						</table>
																					</th>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top" width="156"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td align="right">
																									<table class="m-left" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="day"style="color:#fff; font-family:'Raleway', Arial,sans-serif; font-size:20px; line-height:14px; text-align:left;"><multiline>{{$buyer['discount'].' '.$buyer['currency']}}</multiline></td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</th>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														
														<tr>
															<td class="pb40"style="padding-bottom:20px;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="event-separator"style="padding-bottom:20px; border-bottom:1px solid #ffffff;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="h5-black black"style="font-family:'Montserrat', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; color:#fff;"><multiline>Delivery</multiline></td>
																							</tr>
																						</table>
																					</th>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top" width="156"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td align="right">
																									<table class="m-left" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="day"style="color:#fff; font-family:'Raleway', Arial,sans-serif; font-size:20px; line-height:14px; text-align:left;"><multiline>{{$buyer['delivery'].' '.$buyer['currency']}}</multiline></td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</th>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class="pb40"style="padding-bottom:20px;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="event-separator"style="padding-bottom:20px; border-bottom:1px solid #ffffff;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="h5-black black"style="font-family:'Montserrat', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#fff;"><multiline>Total</multiline></td>
																							</tr>
																						</table>
																					</th>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top" width="156"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td align="right">
																									<table class="m-left" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="day"style="color:#fff; font-family:'Raleway', Arial,sans-serif; font-size:20px; line-height:14px; text-align:left; font-weight:bold;"><multiline>{{$buyer['amount'].' '.$buyer['currency']}}</multiline></td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</th>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="fluid-img"style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://habeshalebs.com/public/storage/uploads/support/free_white_blue.jpg" width="650" height="160" border="0" alt="" /></td>
								</tr>
							</table>
							<!-- END Section 2 -->
							

							<!-- Section 5 -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
								<tr>
									<td class="p30-15-0" style="padding: 70px 30px 0px 30px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="h2-center"style="color:#000000; font-family:'Montserrat', Times, 'Montserrat', serif; font-size:32px; line-height:36px; text-align:center; padding-bottom:20px;"><multiline>Order PIN: {{$buyer['order_pin']}}</multiline></td>
											</tr>
											
											<tr>
												<td class="text-center pb20"style="color:#5d5c5c; font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:22px; text-align:center; padding-bottom:20px;"><multiline>This is your order confirmation number don't pass it to any person</multiline></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
							<!-- END Section 5 -->

							<!-- Footer -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="p30-15-0" bgcolor="#ffffff" style="border-radius: 0px 0px 20px 20px; padding: 70px 30px 0px 30px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
										 	<tr>
												<td class="m-padder2 pb30" align="center"style="padding-bottom:30px;">
													
												</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
							
						</td>
					</tr>
				</table>
				<!-- END Main -->

			</td>
		</tr>
	</table>
</body>
</html>
