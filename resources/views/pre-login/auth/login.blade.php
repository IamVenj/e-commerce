@extends('pre-login.index.index')

@section('content')


<div id="breadcrumb">

	<div class="container">

		<ul class="breadcrumb">

			<li><a href="/">{{__('app.home')}}</a></li>

			<li class="active">{{__('app.login')}}</li>

		</ul>

	</div>

</div>



<div class="section">

	<div class="container">

		<div class="row">

			<form id="loginForm" class="clearfix" >
				
				<div class="col-md-6 cart"  style="background: #ffffff40; padding-top: 50px; padding-bottom: 50px; padding-left: 40px; padding-right: 40px; box-shadow: 0px 10px 20px #00000010;">

					<div class="billing-details">

						<p>{{__('app.have_account')}} <a href="/register">{{__('app.Register')}}</a></p>

						<div class="section-title">

							<h3 class="title">{{__('app.login')}}</h3>

						</div>

						<input type="hidden" name="csrf-token" value="{{csrf_token()}}"></div>

						<div class="form-group">

							<input class="input" type="email" name="email" placeholder="{{__('app.email')}}" style="border-top-right-radius: 100px;">

						</div>

						<div class="form-group input-password">

							<input class="input" type="password" name="password" id="password" placeholder="{{__('app.password')}}" style="border-bottom-right-radius: 100px;">

							<i class="fa fa-eye password-show" onclick="myFunction()"></i>

						</div>

						<div class="form-check">
			              <label class="form-check-label">
			                <input class="checkbox" type="checkbox" name="remember" style="font-size: 20px;">
			                {{__('app.RememberMe')}}
			              <i class="input-helper"></i></label>
			            </div>

						<div class="row">
							
							<div class="col-md-6">
								
								<button type="submit" class="primary-btn btn-login" style="border-bottom-left-radius: 100px; padding-left: 30px;">{{__('app.login')}}</button>

							</div>

							<div class="col-md-6" style="margin-top: 10px;">
								
								<p style="text-align: right;"><a href="/forgot-password" style="font-size: 14px; font-weight: bold;">{{__('app.forgot_pass')}}?</a></p>

								<hr>

							</div>

						</div>



					</div>

				</div>		
				
			</form>



		</div>

	</div>

</div>

<div class="message"></div>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">

	function myFunction() {
	  var x = document.getElementById("password");
	  if (x.type == "password") {
	    x.type = "text";
	  } else {
	    x.type = "password";
	  }
	}

	var token = $("input[name=csrf-token]");
	
	$('.btn-login').click(function(e){

		$.ajaxSetup({

			headers: {
				'X-CSRF-TOKEN': token.val()
			}

		});


		e.preventDefault();

		var email = $("input[name=email]").val();

		var password = $("input[name=password]").val();
		var remember = $("input[name=remember]").val();

		$.ajax({

			type: 'POST',

			url: window.location.href,

			data: {email: email, password: password, remember: remember},

			success: function(response){

				

				if(response.status == 'login_failed')
				{

					var message = $('.message').fadeIn('fast');

					var errorMessage = '<div class="form-group mt-4">';

		    			errorMessage += '<div class="alert golden">';

		    			errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';

		    			
		    			errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

		    			+ response.message + '</p>';

		      			errorMessage +=  '</div></div>';

		      			$(message).html(errorMessage).delay(2500).fadeOut('slow');
				}
				else
				{
					if(response.role_type == "admin" || response.role_type == "seller")
					{
						location.replace('/dashboard');
					}
					else if(response.role_type == "customer")
					{
						location.replace('/');
					}

				}

			},
			error: function(error){

				console.log(error.responseJson);

			}

		});

	});

</script>

@endsection