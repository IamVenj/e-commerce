@extends('pre-login.index.index')

@section('content')


<div id="breadcrumb">

	<div class="container">

		<ul class="breadcrumb">

			<li><a href="/">{{__('app.home')}}</a></li>

			<li class="active">{{__('app.reset')}}</li>

		</ul>

	</div>

</div>



<div class="section">

	<div class="container">

		<div class="row">
				
			<div class="col-md-6 cart"  style="background: #ffffff40; padding-top: 50px; padding-bottom: 50px; padding-left: 40px; padding-right: 40px; box-shadow: 0px 10px 20px #00000010;">

				<div class="billing-details">

					<div class="section-title">

						<h3 class="title">{{__('app.reset')}}</h3>

					</div>


					<form action="/reset-password" method="post">
						
						@csrf

						<input type="hidden" name="token" value="{{ $token }}">

						<div class="form-group">

							<input class="input" type="email" name="email" placeholder="Email" style="border-top-right-radius: 100px;">

						</div>

						<div class="form-group input-password">

							<input class="input" type="password" name="password" id="password" placeholder="password" style="border-top-right-radius: 100px;">

							<i class="fa fa-eye password-show" onclick="myFunction()"></i>

						</div>

						<div class="form-group">

							<input class="input" type="password" name="password_confirmation" placeholder="Confirm Password" style="border-top-right-radius: 100px;">

						</div>


						<div class="row">
							
							<div class="col-md-6">
								
								<button type="submit" class="primary-btn btn-login" style="border-bottom-left-radius: 100px; padding-left: 30px;">{{__('app.submit')}}</button>

							</div>

						</div>

					</form>
					

				</div>

			</div>		
				
		</div>

	</div>

</div>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">
	function myFunction() {
	  var x = document.getElementById("password");
	  if (x.type == "password") {
	    x.type = "text";
	  } else {
	    x.type = "password";
	  }
	}
</script>

@endsection