@extends('pre-login.index.index')

@section('content')




<div id="breadcrumb">

	<div class="container">

		<ul class="breadcrumb">
			<li><a href="#">{{__('app.home')}}</a></li>

			<li class="active">{{__('app.Register')}}</li>

		</ul>

	</div>

</div>


<div class="section">

	<div class="container">

		<div class="row">

			<form id="checkout-form" class="clearfix">
				
				<div class="col-md-6 cart"  style="background: #ffffff40; padding-top: 50px; padding-bottom: 50px; padding-left: 40px; padding-right: 40px; box-shadow: 0px 10px 20px #00000010;">
					
					<div class="billing-details">


						<p>{{__('app.already_user')}} <a href="/login">{{__('app.login')}}</a></p>
					
						<div class="section-title">
					
							<h3 class="title">{{__('app.Register')}}</h3>
					
						</div>
					
						<input type="hidden" name="csrf-token" value="{{csrf_token()}}"></div>

						<div class="form-group">
					
							<input class="input" type="email" name="email" placeholder="{{__('app.email')}}" style="border-top-right-radius: 100px;">
					
						</div>
					
						<div class="form-group input-password">
					
							<input class="input" type="password" name="password" id="password" placeholder="{{__('app.password')}}" style="border-bottom-right-radius: 100px;">

							<i class="fa fa-eye password-show" onclick="myFunction()"></i>

					
						</div>
					
						<div class="form-group input-password">
					
							<input class="input" type="password" name="password_confirmation" placeholder="{{__('app.c_password')}}" id="c_password" style="border-bottom-right-radius: 100px; border-top-right-radius: 100px;">

							<i class="fa fa-eye password-show" onclick="myFunction2()"></i>

					
						</div>
					
						<hr>
					
						<!-- <input type="checkbox" name="seller" id="seller_id">
					
						<label for="seller_id">Seller Account</label> -->

						<div class="form-check">
			              <label class="form-check-label">
			                <input class="checkbox" type="checkbox" name="seller" style="font-size: 20px;">
			                {{__('app.seller_acct')}}
			              <i class="input-helper"></i></label>
			            </div>
					
						<hr>
					
						<button type="submit" class="primary-btn btn-register" id="btn-register" style="border-bottom-left-radius: 100px; padding-left: 30px;">{{__('app.Register')}}</button>

						<div class="lds-roller" id="loader-roller" style="display: none;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>	

					</div>
				
				</div>
				
			</form>

		</div>

	</div>

</div>

<div class="message"></div>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">

	function myFunction() {
	  var x = document.getElementById("password");
	  if (x.type == "password") {
	    x.type = "text";
	  } else {
	    x.type = "password";
	  }
	}

	function myFunction2() {
	  var x = document.getElementById("c_password");
	  if (x.type == "password") {
	    x.type = "text";
	  } else {
	    x.type = "password";
	  }
	}
	
	var token = $("input[name=csrf-token]");

	$('.btn-register').click(function(e){

		$.ajaxSetup({

			headers: {
				'X-CSRF-TOKEN': token.val()
			}

		});



		e.preventDefault();

		var email = $("input[name=email]").val();

		var password = $("input[name=password]").val();

		var password_confirmation = $("input[name=password_confirmation]").val();

		var seller = $("input[name=seller]").is(':checked');

		$.ajax({

			type: 'POST',

			url: window.location.href,

			data: {email: email, password: password, password_confirmation:password_confirmation, seller: seller},

			beforeSend: function() {
				$("#btn-register").attr("disabled", "disabled");
				document.getElementById('loader-roller').style.display = "inline-block";
			},

			success: function(data){

				document.getElementById('loader-roller').style.display = "none";

				$("#btn-register").removeAttr("disabled", "disabled");

				var message = $('.message').fadeIn('fast');

				$("input[name=email]").val("");

	      		$("input[name=password]").val("");

	      		$("input[name=password_confirmation]").val("");

	      		if(seller == true)
	      		{

	      			$("input[name=seller]").removeAttr('checked');

	      		}


				var successMessage = '<div class="form-group mt-4">';

		    			successMessage += '<div class="alert golden2">';

		    			successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
		    			
		    			successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

		    			+ data.message + '</p>';

		      			successMessage +=  '</div></div>';

	      				$(message).html(successMessage).delay(2500).fadeOut('slow');      			

	      		
	      		if(data.role_type == "admin" || data.role_type == "seller")
				{
					location.replace('/dashboard');
				}
				else if(data.role_type == "customer")
				{
					location.replace('/');
				}

	      		// seller

			},

			error: function(error){

				document.getElementById('loader-roller').style.display = "none";

				$("#btn-register").removeAttr("disabled", "disabled");

				console.log(error.responseJSON);

				var message = $('.message').fadeIn('fast');
				
				if(error.responseJSON['errors'] != null)
				{
								
					var errorMessage = '<div class="form-group mt-4">';

		    			errorMessage += '<div class="alert golden">';

		    			errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';

		    			$.each(error.responseJSON['errors'], function(key, errors)
						{
		    			
		    			errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

		    			+ errors + '</p>';

		    			});

		      			errorMessage +=  '</div></div>';


      					$(message).html(errorMessage).delay(2500).fadeOut('slow');
		      			

		      			
				}

				else
				{

					$(message).html('').fadeIn('slow');

				}


			}

		});

	});

</script>

@endsection