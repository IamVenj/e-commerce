@extends('pre-login.index.index')

@section('content')


<div id="breadcrumb">

	<div class="container">

		<ul class="breadcrumb">

			<li><a href="/">{{__('app.home')}}</a></li>

			<li class="active">{{__('app.forgot_pass')}}</li>

		</ul>

	</div>

</div>



<div class="section">

	<div class="container">

		<div class="row">
				
			<div class="col-md-6 cart"  style="background: #ffffff40; padding-top: 80px; padding-bottom: 90px; padding-left: 40px; padding-right: 40px; box-shadow: 0px 10px 20px #00000010;">

				<div class="billing-details">

					<div class="section-title">

						<h3 class="title">{{__('app.forgot_pass')}}?</h3>

					</div>


					<form action="/forgot-password" method="post">
						
						@csrf

						<div class="form-group">

							<input class="input" type="email" name="email" placeholder="{{__('app.email')}}" style="border-top-right-radius: 100px;">

						</div>


						<div class="row">
							
							<div class="col-md-6">
								
								<button type="submit" class="primary-btn btn-login" style="border-bottom-left-radius: 100px; padding-left: 30px;">{{__('app.Submit')}}</button>

							</div>

						</div>

					</form>



				</div>

			</div>		
				
		</div>

	</div>

</div>


@endsection